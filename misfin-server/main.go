package main

import (
	"bufio"
	"bytes"
	"crypto/x509"
	"errors"
	"fmt"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/clseibold/misfin-server/gemmail"
	"gitlab.com/clseibold/misfin-server/misfin_client"
)

var Version string = "0.5.10c" // Appended with "c" for Misfin(C) version

func main() {
	ServerCommand.Execute()
}

var ServerCommand = &cobra.Command{
	DisableFlagsInUseLine: true,
	Use:                   "misfin-server",
	Short:                 "Misfin Mailserver, using Misfin(C).",
	Long:                  "Misfin Mailserver. Run the 'init' command to setup your server and create the server certificate.\nThen run the 'make mailbox [config_file]' command to make a user mailbox.\nFinally, run the 'serve [config_file]' command to start the server.\n\nYou can also run the 'make mailinglist [config_file]' command to make a mailinglist mailbox.",
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

// ----- Commands -----

func init() {
	var InitCommand = &cobra.Command{
		Use:   "init [config_file]",
		Short: "Sets up the server. This should only be ran once. Pass in the config file to write a config to that filepath. Default config filepath used is '" + DefaultConfiguration.ConfigFilename + "'.",
		Run:   InitCmd,
	}
	InitCommand.Flags().StringVar(&FlagsConfiguration.ServerCertFilename, "server-cert", "", "Passes in a server certificate filepath. If pre-existing cert, command will ask you whether to overwrite or use the existing cert. If not, the new certificate will be written to the passed in filepath.")

	var ServeCommand = &cobra.Command{
		Use:   "serve [config_file]",
		Short: "Starts the mailserver using details in the config filepath or flags, if provided. Default config filepath used is '" + DefaultConfiguration.ConfigFilename + "'.",
		Run:   ServeCmd,
	}
	ServeCommand.Flags().StringVar(&FlagsConfiguration.Port, "port", DefaultConfiguration.Port, "Sets the port of the misfin mailserver. Defaults to "+DefaultConfiguration.Port+".")
	ServeCommand.Flags().StringVar(&FlagsConfiguration.BindAddress, "bind", DefaultConfiguration.BindAddress, "Sets the bind address of the misfin mailserver. This is the network interface that will be listened to for connections. Defaults to '"+DefaultConfiguration.BindAddress+"', which listens on all network interfaces.")
	ServeCommand.Flags().StringVar(&FlagsConfiguration.ServerCertFilename, "server-cert", DefaultConfiguration.ServerCertFilename, "Sets the server certificate of the misfin mailserver. Defaults to '"+DefaultConfiguration.ServerCertFilename+"'.")
	ServeCommand.Flags().StringVar(&FlagsConfiguration.MailboxDirectory, "mailbox-dir", DefaultConfiguration.MailboxDirectory, "Sets the mailbox directory of the misfin mailserver, where mailboxes are stored. Defaults to '"+DefaultConfiguration.MailboxDirectory+"'.")

	var ConfigCommand = &cobra.Command{
		Use:   "config",
		Short: "Modifies configuration files.",
		Run: func(cmd *cobra.Command, args []string) {
			cmd.Help()
		},
	}
	var ConfigServerCommand = &cobra.Command{
		Use:   "server field value [config_file]",
		Short: "Modifies the server's configuration file. Pass in the config file to write the config to that filepath. Default config filepath used is '" + DefaultConfiguration.ConfigFilename + "'.",
		Run:   ConfigServerCmd,
	}
	var ConfigMailboxCommand = &cobra.Command{
		Use:   "mailbox mailbox_name field value [config_file]",
		Short: "Modifies a mailbox's configuration, like its type, or for mailinglists, its description, send permission, and subs function",
		Run:   ConfigMailboxCmd,
	}
	var ConfigMailinglistSubCommand = &cobra.Command{
		Use:   "mailinglist-sub mailbox_name sub_address field value [config_file]",
		Short: "Modifies a mailinglist user's configuration, like it's type",
		Run:   ConfigMailinglistSubCmd,
	}
	var ConfigMailinglistFederateCommand = &cobra.Command{
		Use:   "mailinglist-federate mailbox_name sub_to_address [config_file]",
		Short: "Sends a subscribe message to the mailinglist at sub_to_address using the specified mailbox's cert. This allows for federation between mailinglists.",
		Run:   ConfigMailinglistFederateCmd,
	}
	ConfigCommand.AddCommand(ConfigServerCommand, ConfigMailboxCommand, ConfigMailinglistSubCommand, ConfigMailinglistFederateCommand)

	// Make command and subcommands
	var MakeCommand = &cobra.Command{
		Use:   "make",
		Short: "Create a mailbox or mailinglist certificate.",
		Run: func(cmd *cobra.Command, args []string) {
			cmd.Help()
		},
	}
	var MakeMailboxCommand = &cobra.Command{
		Use:   "mailbox [config_file]",
		Short: "Create a mailbox.",
		Run:   MakeMailboxCmd,
	}
	MakeMailboxCommand.Flags().StringVar(&FlagsConfiguration.ServerCertFilename, "server-cert", DefaultConfiguration.ServerCertFilename, "Sets the server certificate of the misfin mailserver. Defaults to '"+DefaultConfiguration.ServerCertFilename+"'.")
	MakeMailboxCommand.Flags().StringVar(&FlagsConfiguration.MailboxDirectory, "mailbox-dir", DefaultConfiguration.MailboxDirectory, "Sets the mailbox directory of the misfin mailserver, where mailboxes are stored. Defaults to '"+DefaultConfiguration.MailboxDirectory+"'.")
	var MakeMailinglistCommand = &cobra.Command{
		Use:   "mailinglist [config_file]",
		Short: "Create a mailbox.",
		Run:   MakeMailinglistCmd,
	}
	MakeMailinglistCommand.Flags().StringVar(&FlagsConfiguration.ServerCertFilename, "server-cert", DefaultConfiguration.ServerCertFilename, "Sets the server certificate of the misfin mailserver. Defaults to '"+DefaultConfiguration.ServerCertFilename+"'.")
	MakeMailinglistCommand.Flags().StringVar(&FlagsConfiguration.MailboxDirectory, "mailbox-dir", DefaultConfiguration.MailboxDirectory, "Sets the mailbox directory of the misfin mailserver, where mailboxes are stored. Defaults to '"+DefaultConfiguration.MailboxDirectory+"'.")
	var MakeNewsfinGroupCommand = &cobra.Command{
		Use:   "newsfin-group [config_file]",
		Short: "Create a Newsfin group.",
		Run:   MakeNewsfinCmd,
	}
	MakeNewsfinGroupCommand.Flags().StringVar(&FlagsConfiguration.ServerCertFilename, "server-cert", DefaultConfiguration.ServerCertFilename, "Sets the server certificate of the misfin mailserver. Defaults to '"+DefaultConfiguration.ServerCertFilename+"'.")
	MakeNewsfinGroupCommand.Flags().StringVar(&FlagsConfiguration.MailboxDirectory, "mailbox-dir", DefaultConfiguration.MailboxDirectory, "Sets the mailbox directory of the misfin mailserver, where mailboxes are stored. Defaults to '"+DefaultConfiguration.MailboxDirectory+"'.")
	var MakeNewsletterCommand = &cobra.Command{
		Use:   "newsletter [config_file]",
		Short: "Create a newsletter.",
		Run:   MakeMailinglistCmd,
	}
	MakeNewsletterCommand.Flags().StringVar(&FlagsConfiguration.ServerCertFilename, "server-cert", DefaultConfiguration.ServerCertFilename, "Sets the server certificate of the misfin mailserver. Defaults to '"+DefaultConfiguration.ServerCertFilename+"'.")
	MakeNewsletterCommand.Flags().StringVar(&FlagsConfiguration.MailboxDirectory, "mailbox-dir", DefaultConfiguration.MailboxDirectory, "Sets the mailbox directory of the misfin mailserver, where mailboxes are stored. Defaults to '"+DefaultConfiguration.MailboxDirectory+"'.")
	MakeCommand.AddCommand(MakeMailboxCommand, MakeMailinglistCommand, MakeNewsfinGroupCommand, MakeNewsletterCommand)

	var ImportCommand = &cobra.Command{
		Use:   "import",
		Short: "Imports certs and gemboxes to server.",
		Run: func(cmd *cobra.Command, args []string) {
			cmd.Help()
		},
	}
	var ImportMailboxCommand = &cobra.Command{
		Use:   "mailbox mailbox_cert_file [config_file]",
		Short: "Imports a pre-existing mailbox certificate as a regular mailbox, and prompts to copy a gembox if one is provided.",
		Run:   ImportMailboxCmd,
	}
	var ImportMailinglistCommand = &cobra.Command{
		Use:   "mailinglist mailbox_cert_file [config_file]",
		Short: "Imports a pre-existing mailbox certificate as a mailinglist mailbox, and prompts to copy a gembox if one is provided.",
		Run:   ImportMailinglistCmd,
	}
	var ImportNewsletterCommand = &cobra.Command{
		Use:   "newsletter mailbox_cert_file [config_file]",
		Short: "Imports a pre-existing mailbox certificate as a newsletter mailbox, and prompts to copy a gembox if one is provided.",
		Run:   ImportNewsletterCmd,
	}
	var ImportGemboxCommand = &cobra.Command{
		Use:   "gembox mailbox_name [config_file]",
		Short: "Imports a gembox file into the provided mailbox's gembox.",
		Run:   ImportGemboxCmd,
	}
	ImportCommand.AddCommand(ImportMailboxCommand, ImportMailinglistCommand, ImportNewsletterCommand, ImportGemboxCommand)

	// TODO: Add command to add already-existing mailboxes
	// TODO: Delete command to delete mailboxes

	var VersionCommand = &cobra.Command{
		Use:   "version",
		Short: "Prints the current version.",
		Run: func(cmd *cobra.Command, args []string) {
			cmd.Printf("misfin-server version %s, Misfin(C) Version.\n", Version)
		},
	}

	ServerCommand.AddCommand(InitCommand, ServeCommand, ConfigCommand, MakeCommand, ImportCommand, VersionCommand)
}

// TODO: Allow setting flags to skip the interactive prompts
func InitCmd(cmd *cobra.Command, args []string) {
	var config Config
	config.PrevVersion = Version
	config.ServerCertFilename = ""
	config.RateLimitDuration = DefaultConfiguration.RateLimitDuration
	config.RateLimitDuration_Member = DefaultConfiguration.RateLimitDuration_Member
	config.MaxConcurrentConnections = DefaultConfiguration.MaxConcurrentConnections
	if len(args) > 0 {
		config.ConfigFilename = args[0]
	} else {
		config.ConfigFilename = DefaultConfiguration.ConfigFilename
	}

	if FlagsConfiguration.ServerCertFilename != "" {
		config.ServerCertFilename = FlagsConfiguration.ServerCertFilename
	}

	reader := bufio.NewReader(os.Stdin)

	// Check if config file already exists
	_, readErr := os.ReadFile(config.ConfigFilename)
	if !errors.Is(readErr, os.ErrNotExist) && !errors.Is(readErr, fs.ErrNotExist) {
		cmd.Printf("Server configuration already exists at the location '%s'. Use the `config` command to modify it.\n", config.ConfigFilename)
		for {
			cmd.Printf("Do you want to overwrite the file instead? Y/N? ")
			input, readErr := reader.ReadString('\n')
			if readErr == io.EOF {
				cmd.Printf("Aborting without saving configuration...\n")
				os.Exit(1)
			} else if readErr != nil {
				cmd.Printf("Error: could not read input.\n")
				continue
			}
			input = strings.ToUpper(strings.TrimSpace(input))
			if input == "Y" || input == "YES" {
				break
			} else if input == "N" || input == "NO" {
				cmd.Printf("Aborting without saving configuration...\n")
				return
			} else {
				cmd.Printf("Please enter one of the following options: Y or N.\n")
				continue
			}
		}

	}

	// Read port
	for {
		cmd.Printf("Enter port for misfin mailserver [%s]: ", DefaultConfiguration.Port)
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}

		input = strings.TrimSpace(input)
		if input == "" {
			input = DefaultConfiguration.Port
		}

		_, atoiErr := strconv.Atoi(input)
		if atoiErr != nil {
			cmd.Printf("Passed in an invalid number.\n")
			continue
		}
		config.Port = input

		break
	}

	// Read bind address
	for {
		cmd.Printf("Enter the bind IP Address for the network interface you want to receive connections on ['%s' - receive on all interfaces]: ", DefaultConfiguration.BindAddress)
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.TrimSpace(input)
		if input == "" {
			input = DefaultConfiguration.BindAddress
		}
		// TODO: Verify that what was typed in is a valid IP address
		config.BindAddress = input
		break
	}

	var cert_hostname string
	var cert_mailbox_name string
	var cert_mailbox_blurb string
	var keyType KeyType
	var certInfo CertInfo

	// Prompt for certificate filepath if one was not passed in
	writeFile := false
	if config.ServerCertFilename == "" { // TODO: Allow this branch to use the pre-existing config file.
		// Prompt for new cert details
		cert_hostname, cert_mailbox_name, cert_mailbox_blurb, keyType = promptServerCertInfo(cmd, reader)

		// Read Server Cert filepath
		for {
			cmd.Printf("Enter the filepath to write your server certificate file to ['./%s.pem']: ", cert_mailbox_name)
			input, readErr := reader.ReadString('\n')
			if readErr == io.EOF {
				cmd.Printf("Aborting without saving configuration...\n")
				os.Exit(1)
			} else if readErr != nil {
				cmd.Printf("Error: could not read input.\n")
				continue
			}
			input = strings.TrimSpace(input)
			if input == "" {
				input = cert_mailbox_name + ".pem"
			}
			config.ServerCertFilename = input
			break
		}

		// TODO: Check if filepath already exists, and ask to overwrite it or not.
		_, readErr := os.ReadFile(config.ServerCertFilename)
		if errors.Is(readErr, fs.ErrNotExist) || errors.Is(readErr, os.ErrNotExist) {
			// Cert doesn't already exist, create it.
			writeFile = true
		} else {
			// Ask whether to overwrite. If not, then exit.
			// Ask whether to overwrite file or not
			var overwrite = false
			for {
				cmd.Printf("Do you want to overwrite the pre-existing certificate file? Y/N? 'N' will abort the command. [N] ")
				input, readErr := reader.ReadString('\n')
				if readErr == io.EOF {
					cmd.Printf("Aborting without saving configuration...\n")
					os.Exit(1)
				} else if readErr != nil {
					cmd.Printf("Error: could not read input.\n")
					continue
				}
				input = strings.ToUpper(strings.TrimSpace(input))
				if input == "Y" || input == "YES" {
					overwrite = true
				} else if input == "N" || input == "NO" || input == "" {
					overwrite = false
				} else {
					cmd.Printf("Please type one of the following options: Y or N.\n")
					continue
				}
				break
			}

			if overwrite {
				writeFile = true
			} else {
				cmd.Printf("Re-run the command when you've renamed the file, or pass the pre-existing cert in the --server-cert flag to use it to setup your server.\nAborting without saving configuration...\n")
				os.Exit(1)
			}
		}
	} else {
		// Check if passed in certificate filepath already exists. If so, then prompt to overwrite or use existing certificate, otherwise, prompt for cert details and write new file to filepath
		preexistingConfigFile, readErr := os.ReadFile(config.ServerCertFilename)
		if errors.Is(readErr, fs.ErrNotExist) || errors.Is(readErr, os.ErrNotExist) {
			// Cert doesn't already exist, create it.
			cert_hostname, cert_mailbox_name, cert_mailbox_blurb, keyType = promptServerCertInfo(cmd, reader)
			writeFile = true
		} else {
			// Cert already exists. Print out details and ask if you want to use it or overwrite it.
			cert, _, _ := getCertsFromPemData(preexistingConfigFile)
			preexisting_certInfo := getCertInfo(cert)
			cmd.Printf("A preexisting certificate ('%s') was found with the following details:\n", config.ServerCertFilename)
			cmd.Printf("Hostname: %s\n", preexisting_certInfo.domains[0])
			cmd.Printf("Mailbox Name: %s\n", preexisting_certInfo.mailbox)
			cmd.Printf("Full Name/Blurb: %s\n", preexisting_certInfo.blurb)

			if !preexisting_certInfo.IsCA {
				cmd.Printf("This certificate is not a CA. You will not be able to add more mailboxes to the server if you use this cert.\n")
			}

			// Ask whether to overwrite file or not
			var overwrite = false
			for {
				cmd.Printf("Do you want to generate a new cert and overwrite the previous one? Y/N? 'N' will use the existing certificate without overwriting a new one. Press Ctrl+C to abort. [N] ")
				input, readErr := reader.ReadString('\n')
				if readErr == io.EOF {
					cmd.Printf("Aborting without saving configuration...\n")
					os.Exit(1)
				} else if readErr != nil {
					cmd.Printf("Error: could not read input.\n")
					continue
				}
				input = strings.ToUpper(strings.TrimSpace(input))
				if input == "Y" || input == "YES" {
					overwrite = true
				} else if input == "N" || input == "NO" || input == "" {
					overwrite = false
				} else {
					cmd.Printf("Please type one of the following options: Y or N.\n")
					continue
				}
				break
			}

			if overwrite {
				// If overwriting, prompt for details
				cert_hostname, cert_mailbox_name, cert_mailbox_blurb, keyType = promptServerCertInfo(cmd, reader)
				writeFile = true
			} else {
				// If not, get the details from cert and don't set writeFile variable to write a cert file (since it already exists)
				cert_hostname = preexisting_certInfo.domains[0]
				cert_mailbox_name = preexisting_certInfo.mailbox
				cert_mailbox_blurb = preexisting_certInfo.blurb
				certInfo = preexisting_certInfo
			}
		}
	}

	// Write the cert file (if one already exists and not overwriting it with a new cert)
	if writeFile {
		pemBuffer := bytes.NewBuffer([]byte{})
		var certificate *x509.Certificate
		certificate, err := generateCertAndPrivateKey(true, nil, nil, keyType, cert_mailbox_blurb, cert_mailbox_name, cert_hostname, pemBuffer)
		if err != nil {
			cmd.PrintErrf("Failed to generate Cert/Private Key pair: %s\n", err)
			os.Exit(1)
		}

		// TODO: Check if file already exists, and ask to overwrite if it does.
		writeErr := os.WriteFile(config.ServerCertFilename, pemBuffer.Bytes(), 0660)
		if writeErr != nil {
			cmd.PrintErrf("Could not write server certificate file as '%s': %s\n", config.ServerCertFilename, writeErr)
			os.Exit(1)
		}

		certInfo = getCertInfo(certificate)
	}
	cmd.Printf("Saved server certificate as '%s'.\n", config.ServerCertFilename)

	// Read mailboxes directory
	for {
		cmd.Printf("Enter the path to the mailboxes directory where mailboxes are stored ['%s']: ", DefaultConfiguration.MailboxDirectory)
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.TrimSpace(input)
		if input == "" {
			input = DefaultConfiguration.MailboxDirectory
		}
		config.MailboxDirectory = input
		break
	}

	// TODO: Read gemini-permission option

	// Create the mailbox directory
	mkErr := os.MkdirAll(config.MailboxDirectory, 0700)
	if mkErr != nil {
		cmd.Printf("Error: Failed to create mailbox directory at path '%s'.\n", config.MailboxDirectory)
		os.Exit(1)
	}

	// Save the new configuration file
	saveConfig(config)
	cmd.Printf("Config file written as '%s'.\n", config.ConfigFilename)

	// Add mailbox to mailbox.list file
	mailboxes := createEmptyMailboxesMap(config)
	mailbox := CreateMailBox(MailboxListType_Root, certInfo, "", false, false, config)
	mailboxes.AddMailBox(&mailbox)

	// Create GMAP mailbox
	{
		serverCertificate, serverPrivateKey, _ := getCertsFromPem(config.ServerCertFilename)
		serverCertificateInfo := getCertInfo(serverCertificate)
		if !serverCertificateInfo.IsCA {
			cmd.Printf("Your server certificate is not a CA. You cannot add any more mailboxes. Please generate a new server certificate with the `init` command.\n")
			os.Exit(1)
		}

		pemBuffer := bytes.NewBuffer([]byte{})
		var gmap_certificate *x509.Certificate
		gmap_certificate, err := generateCertAndPrivateKey(false, serverCertificate, serverPrivateKey, keyType, "GMAP", "gmap", cert_hostname, pemBuffer)
		if err != nil {
			cmd.PrintErrf("Failed to generate Cert/Private Key pair: %s\n", err)
			os.Exit(1)
		}

		// TODO: Check if file already exists, and ask to overwrite if it does.
		gmap_cert_filename := filepath.Join(config.MailboxDirectory, "gmap.pem")
		writeErr := os.WriteFile(gmap_cert_filename, pemBuffer.Bytes(), 0660)
		if writeErr != nil {
			cmd.PrintErrf("Could not write server certificate file as '%s': %s\n", gmap_cert_filename, writeErr)
			os.Exit(1)
		}

		gmap_certInfo := getCertInfo(gmap_certificate)
		gmap_mailbox := CreateMailBox(MailboxListType_Admin, gmap_certInfo, "", false, false, config)
		mailboxes.AddMailBox(&gmap_mailbox)
	}

	mailboxes.saveMailboxes()

	/*appendErr := appendToMailboxList(MailboxListItem{MailboxListType_Root, cert_mailbox_name, cert_mailbox_blurb, certInfo.fingerprint, cert_hostname, "", "", "", time.Time{}}, config, true)
	if appendErr != nil {
		// TODO: The mailbox list is now out of sync with the actual number of mailboxes. What do I do about this?
		cmd.PrintErrf("Could not append new mailbox to mailbox list.\n")
		os.Exit(1)
	}*/

	cmd.Printf("Mailserver init setup is complete. Run 'misfin-server make mailbox' to make a user mailbox for a multi-mailbox setup, or run the server with 'misfin-server serve'.\n")
}

func promptServerCertInfo(cmd *cobra.Command, reader *bufio.Reader) (string, string, string, KeyType) {
	var cert_hostname, cert_mailbox_name, cert_mailbox_blurb string
	var keyType KeyType

	// Read cert domain/hostname
	for {
		cmd.Printf("Enter Server Certificate's Hostname (this is usually your public DOMAIN name): ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.TrimSpace(input)
		if input == "" {
			cmd.Printf("Error: domain/hostname cannot be blank.\n")
			continue
		}
		cert_hostname = input
		break
	}

	// Read Server Cert mailbox name
	for {
		cmd.Printf("Enter Server Certificate's mailbox name. On multi-mailbox configurations, it is recommended that this be 'admin'. On single-mailbox configurations, set this to your preferred username to be used in the mailaddress ['admin']: ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.TrimSpace(input)
		if input == "" {
			input = "admin"
		}

		// Check that there's no spaces
		index := strings.IndexAny(input, " \t")
		if index != -1 {
			cmd.Printf("Error: You cannot have whitespace in a mailbox name.\n")
			continue
		}

		cert_mailbox_name = input
		break
	}

	// Read Server Cert mailbox blurb/full-name
	for {
		cmd.Printf("Enter Server Certificate's mailbox full name (aka. blurb). On multi-mailbox configurations, this should be your service/brand name. On single-mailbox configurations, this should be your real Full Name: ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.TrimSpace(input)
		if input == "" {
			cmd.Printf("Error: Mailbox's full name cannot be blank.\n")
			continue
		}
		cert_mailbox_blurb = input
		break
	}

	// Read the desired key type: ECDSA, ED25519, or RSA
	for {
		cmd.Printf("Enter the key type that you want to generate (ECDSA, RSA, or ED25519): ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.ToUpper(strings.TrimSpace(input))
		if input == "ECDSA" {
			keyType = KeyType_ECDSA
		} else if input == "ED25519" {
			keyType = KeyType_ED25519
		} else if input == "RSA" {
			keyType = KeyType_RSA
		} else {
			cmd.Printf("Error: Invalid key type. Enter one of the following: ECDSA, RSA, or ED25519.\n")
			continue
		}
		break
	}

	return cert_hostname, cert_mailbox_name, cert_mailbox_blurb, keyType
}

func ServeCmd(cmd *cobra.Command, args []string) {
	var config Config
	var err error
	if len(args) > 0 {
		config, err = getConfigValuesForCommand(args[0])
		if err != nil {
			cmd.Printf("Couldn't read configuration file.\n")
			os.Exit(1)
		}
	} else {
		config, _ = getConfigValuesForCommand(DefaultConfiguration.ConfigFilename)
	}

	RunServer(config)
}

// TODO: Add interactive mode?
func ConfigServerCmd(cmd *cobra.Command, args []string) {
	if len(args) < 1 {
		cmd.Printf("Please specify the field to configure. Options include: port, bind-address, mailbox-dir, server-cert, or gemini-permission.\n\nconfig server: ")
		cmd.Help()
		return
	} else if len(args) < 2 {
		cmd.Printf("Please specify a value for this field.")
		if args[0] == "gemini-permission" {
			cmd.Printf(" Values include: disabled, admin-only, member-only, or public.")
		}
		cmd.Printf("\n")
		return
	}

	var config Config
	var err error
	config_filepath_num := len(args)
	if len(args) >= 3 {
		config_filepath_num = len(args) - 1
		config, err = getConfigValuesForCommand(args[config_filepath_num])
		if err != nil {
			cmd.Printf("Couldn't read configuration file.\n")
			os.Exit(1)
		}
	} else {
		config, err = getConfigValuesForCommand(DefaultConfiguration.ConfigFilename)
		if err != nil {
			cmd.Printf("Couldn't read configuration file '%s'.\n", DefaultConfiguration.ConfigFilename)
			return
		}
	}

	field := args[0]
	value := strings.Join(args[1:config_filepath_num], " ")

	if field == "port" {
		config.Port = value
	} else if field == "bind-address" {
		config.BindAddress = value
	} else if field == "mailbox-dir" {
		config.MailboxDirectory = value
	} else if field == "server-cert" {
		config.ServerCertFilename = value
	} else if field == "gemini-permission" {
		if value == "disabled" {
			config.GeminiServerPermission = GeminiPermission_Disabled
		} else if value == "admin-only" {
			config.GeminiServerPermission = GeminiPermission_AdminOnly
		} else if value == "member-only" {
			config.GeminiServerPermission = GeminiPermission_MemberOnly
		} else if value == "public" {
			config.GeminiServerPermission = GeminiPermission_Public
		} else {
			cmd.Printf("Not a valid value. Specify one of the following: disabled, admin-only, member-only, or public.\n")
			return
		}
	} else if field == "gemini-user-registration-permission" {
		if value == "disabled" {
			config.GeminiUserRegistrationPermission = GeminiRegistrationPermission_Disabled
		} else if value == "admin-only" {
			config.GeminiUserRegistrationPermission = GeminiRegistrationPermission_AdminOnly
		} else if value == "public" {
			config.GeminiUserRegistrationPermission = GeminiRegistrationPermission_Public
		} else {
			cmd.Printf("Not a valid value. Specify one of the following: disabled, admin-only, or public.\n")
			return
		}
	}

	saveConfig(config)
}

func ConfigMailboxCmd(cmd *cobra.Command, args []string) {
	if len(args) < 1 {
		cmd.Printf("Please specify the mailbox to configure.\n")
		return
	} else if len(args) < 2 {
		cmd.Printf("Please specify the field to configure. Options include: type, send, subs-function, archive-view-permission, or description.\n\nconfig mailbox: ")
		cmd.Help()
		return
	} else if len(args) < 3 {
		cmd.Printf("Please specify a value for this field.")
		if args[1] == "type" {
			cmd.Printf(" Values include: admin, user, mailinglist, and root.")
		} else if args[1] == "send" {
			cmd.Printf(" Values include: writers, subs, and open.")
		} else if args[1] == "subs-function" {
			cmd.Printf(" Values include: accept-all, accept-server-members, closed, and prompt.")
		} else if args[1] == "archive-view-permission" {
			cmd.Printf(" Values include: open, subs, admin-only, and disabled.")
		}
		cmd.Printf("\n")
		return
	}

	var config Config
	var err error
	config_filepath_num := len(args)
	if len(args) >= 4 {
		config_filepath_num = len(args) - 1
		config, err = getConfigValuesForCommand(args[config_filepath_num])
		if err != nil {
			cmd.Printf("Couldn't read configuration file.\n")
			os.Exit(1)
		}
	} else {
		config, err = getConfigValuesForCommand(DefaultConfiguration.ConfigFilename)
		if err != nil {
			cmd.Printf("Couldn't read configuration file '%s'.\n", DefaultConfiguration.ConfigFilename)
			return
		}
	}

	mailbox_name := args[0]
	field := args[1]
	value := strings.Join(args[2:config_filepath_num], " ")

	mailboxes, err := loadMailboxes(config)
	if err != nil {
		cmd.Printf("Error loading mailboxes: %s\n", err)
		return
	}

	// Check that mailbox exists
	if !mailboxes.Exists(mailbox_name) {
		cmd.Printf("Error: mailbox named '%s' doesn't exist.\n", mailbox_name)
		return
	}

	mbox := mailboxes.Get(mailbox_name)

	reader := bufio.NewReader(os.Stdin)
	if field == "type" {
		// Reset Values
		mbox.Mailinglist = false
		mbox.NewsfinGroup = false
		if mbox.CA {
			cmd.Printf("Note: You are changing the root mailbox's type. This will change the cert used to sign all other mailboxes as well as the cert that serves all misfin connections. Press Enter to continue or Ctrl+C to abort.\n")
			_, readErr := reader.ReadString('\n')
			if readErr == io.EOF {
				cmd.Printf("Aborting without saving configuration...")
				os.Exit(1)
			}
			mbox.CA = false
		}

		if value == "mailinglist" {
			mbox.Mailinglist = true
			mbox.T = MailboxListType_Mailinglist
		} else if value == "newsfin-group" {
			mbox.T = MailboxListType_NewsfinGroup
			mbox.NewsfinGroup = true
		} else if value == "admin" {
			mbox.T = MailboxListType_Admin
		} else if value == "root" {
			cmd.Printf("Note: You are setting this mailbox to be the root mailbox. This should only be set to the mailbox of the server certificate that acts as the CA to all other mailboxes. Make sure to change the root mailbox's type to something else if swapping. Press Enter to continue or Ctrl+C to abort.\n")
			_, readErr := reader.ReadString('\n')
			if readErr == io.EOF {
				cmd.Printf("Aborting without saving configuration...")
				os.Exit(1)
			}
			mbox.T = MailboxListType_Root
			mbox.CA = true // Assume this just for saving the configuration
		} else if value == "user" {
			mbox.T = MailboxListType_User
		} else {
			cmd.Printf("Value '%s' unknown.\n", value)
			os.Exit(1)
		}
		//mbox.T = MailboxListType
	} else if field == "send-thread" {
		if !mbox.Mailinglist && !mbox.NewsfinGroup {
			cmd.Printf("This is not a mailinglist or newsletter mailbox. This field cannot be set.\n")
			os.Exit(1)
		}

		if value == "writers" {
			mbox.List.SendThreadPermission = MailingListSend_Writers
		} else if value == "subs" {
			mbox.List.SendThreadPermission = MailingListSend_Subs
		} else if value == "open" {
			mbox.List.SendThreadPermission = MailingListSend_Open
		} else {
			cmd.Printf("Value '%s' unknown.\n", value)
			os.Exit(1)
		}
	} else if field == "send-reply" {
		if !mbox.Mailinglist && !mbox.NewsfinGroup {
			cmd.Printf("This is not a mailinglist or newsletter mailbox. This field cannot be set.\n")
			os.Exit(1)
		}

		if value == "writers" {
			mbox.List.SendReplyPermission = MailingListSend_Writers
		} else if value == "subs" {
			mbox.List.SendReplyPermission = MailingListSend_Subs
		} else if value == "open" {
			mbox.List.SendReplyPermission = MailingListSend_Open
		} else {
			cmd.Printf("Value '%s' unknown.\n", value)
			os.Exit(1)
		}
	} else if field == "subs-function" {
		if !mbox.Mailinglist && !mbox.NewsfinGroup {
			cmd.Printf("This is not a mailinglist or newsletter mailbox. This field cannot be set.\n")
			os.Exit(1)
		}

		if value == "accept-all" {
			mbox.List.SubsFunction = MailingListSubs_AcceptAll
		} else if value == "accept-server-members" {
			mbox.List.SubsFunction = MailingListSubs_AcceptServerMembers
		} else if value == "closed" {
			mbox.List.SubsFunction = MailingListSubs_Closed
		} else if value == "prompt" {
			mbox.List.SubsFunction = MailingListSubs_Prompt
		} else {
			cmd.Printf("Value '%s' unknown. Value can be one of the following: accept-all, accept-server-members, closed, or prompt.\n", value)
			os.Exit(1)
		}
	} else if field == "archive-view-permission" {
		if !mbox.Mailinglist && !mbox.NewsfinGroup {
			cmd.Printf("This is not a mailinglist or newsletter mailbox. This field cannot be set.\n")
			os.Exit(1)
		}

		if value == "open" {
			mbox.List.ArchiveViewPermission = MailingListArchiveViewPermission_Open
		} else if value == "subs" {
			mbox.List.ArchiveViewPermission = MailingListArchiveViewPermission_Subs
		} else if value == "admin-only" {
			mbox.List.ArchiveViewPermission = MailingListArchiveViewPermission_AdminOnly
		} else if value == "disabled" {
			mbox.List.ArchiveViewPermission = MailingListArchiveViewPermission_Disabled
		}
	} else if field == "description" {
		mbox.List.Description = value
	} else {
		cmd.Printf("Field '%s' unknown.\n", field)
		os.Exit(1)
	}

	err = mailboxes.saveMailboxes()
	if err != nil {
		cmd.Printf("Error saving mailbox configuration: %s\n", err)
	}
}

// config mailinglist_sub mailbox_name sub_addr field value [config_file]
func ConfigMailinglistSubCmd(cmd *cobra.Command, args []string) {
	if len(args) < 1 {
		cmd.Printf("Please specify the mailbox to configure.\n")
		return
	} else if len(args) < 2 {
		cmd.Printf("Please specify the sub_address.\n")
		return
	} else if len(args) < 3 {
		cmd.Printf("Please specify the field to configure. Options include: type.\n\nconfig mailinglist-sub: ")
		cmd.Help()
		return
	} else if len(args) < 4 {
		cmd.Printf("Please specify a value for this field.")
		if args[2] == "type" {
			cmd.Printf(" Values include: admin, user, writer, federated, or incoming-forwarder.")
		}
		cmd.Printf("\n")
		return
	}

	var config Config
	var err error
	config_filepath_num := len(args)
	if len(args) >= 5 {
		config_filepath_num = len(args) - 1
		config, err = getConfigValuesForCommand(args[config_filepath_num])
		if err != nil {
			cmd.Printf("Couldn't read configuration file.\n")
			os.Exit(1)
		}
	} else {
		config, err = getConfigValuesForCommand(DefaultConfiguration.ConfigFilename)
		if err != nil {
			cmd.Printf("Couldn't read configuration file '%s'.\n", DefaultConfiguration.ConfigFilename)
			return
		}
	}

	mailbox_name := args[0]
	sub_addr := args[1]
	field := args[2]
	value := strings.Join(args[3:config_filepath_num], " ")

	mailboxes, err := loadMailboxes(config)
	if err != nil {
		cmd.Printf("Error loading mailboxes: %s\n", err)
		return
	}

	// Check that mailbox exists
	if !mailboxes.Exists(mailbox_name) {
		cmd.Printf("Error: mailbox named '%s' doesn't exist.\n", mailbox_name)
		return
	}

	mbox := mailboxes.Get(mailbox_name)

	// Make sure it's a mailinglist
	if !mbox.Mailinglist && !mbox.NewsfinGroup {
		cmd.Printf("This mailbox is not a mailinglist.\n")
		return
	}

	// Check that the sub exists
	_, exists := mbox.List.Subscribers[sub_addr]
	if !exists {
		cmd.Printf("Error: Address not a subscriber of this mailinglist.\n")
		return
	}

	//reader := bufio.NewReader(os.Stdin)
	newSubInfo := mbox.List.Subscribers[sub_addr]
	if field == "type" {
		if value == "admin" {
			newSubInfo.T = MailboxListType_Mailinglist_Admin
		} else if value == "user" {
			newSubInfo.T = MailboxListType_Mailinglist_User
		} else if value == "writer" {
			newSubInfo.T = MailboxListType_Mailinglist_Writer
		} else if value == "federated" {
			newSubInfo.T = MailboxListType_Mailinglist_Federated
		} else if value == "incoming-forwarder" {
			newSubInfo.T = MailboxListType_Mailinglist_IncomingForwarder
		} else {
			cmd.Printf("Value '%s' unknown.\n", value)
			os.Exit(1)
		}
	}

	mbox.List.Subscribers[sub_addr] = newSubInfo
	mbox.SaveGemBox()
}

// mailinglist-federate mailbox_name sub_to_address [config_file]
func ConfigMailinglistFederateCmd(cmd *cobra.Command, args []string) {
	if len(args) < 1 {
		cmd.Printf("Please specify the mailbox to configure.\n")
		return
	} else if len(args) < 2 {
		cmd.Printf("Please specify the sub_to_address.\n")
		return
	}

	var config Config
	var err error
	if len(args) >= 3 {
		config_filepath_num := len(args) - 1
		config, err = getConfigValuesForCommand(args[config_filepath_num])
		if err != nil {
			cmd.Printf("Couldn't read configuration file.\n")
			os.Exit(1)
		}
	} else {
		config, err = getConfigValuesForCommand(DefaultConfiguration.ConfigFilename)
		if err != nil {
			cmd.Printf("Couldn't read configuration file '%s'.\n", DefaultConfiguration.ConfigFilename)
			return
		}
	}

	mailbox_name := args[0]
	sub_to_addr := args[1]

	mailboxes, err := loadMailboxes(config)
	if err != nil {
		cmd.Printf("Error loading mailboxes: %s\n", err)
		return
	}

	// Check that mailbox exists
	if !mailboxes.Exists(mailbox_name) {
		cmd.Printf("Error: mailbox named '%s' doesn't exist.\n", mailbox_name)
		return
	}

	mbox := mailboxes.Get(mailbox_name)

	// Make sure it's a mailinglist
	if !mbox.Mailinglist && !mbox.NewsfinGroup {
		cmd.Printf("This mailbox is not a mailinglist.\n")
		return
	}

	gmail := gemmail.CreateGemMailFromBody("")
	gmail.Subject = "Subscribe No-Reply"

	ml_cert, _ := os.ReadFile(mbox.List.CertificateFilepath)
	redirectAttempt := 0 // Keep track of how many times redirected. Don't redirect after 5th attempt
	misfinB := false
	misfinClient := misfin_client.DefaultClient
	for {
		msg := ""
		if misfinB {
			msg = gmail.String_MisfinB()
		} else {
			msg = gmail.String()
		}
		resp, err := misfinClient.SendWithCert("misfin://"+sub_to_addr, ml_cert, ml_cert, msg)
		if err != nil || resp == nil {
			fmt.Printf("Failed to send gemmail to sub %s on mailinglist %s. Failed with err %v\n", sub_to_addr, mbox.CertInfo.mailAddress, err)
		} else if redirectAttempt < 5 && (resp.Status == misfin_client.StatusRedirect || resp.Status == misfin_client.StatusRedirectPermanent) {
			// Handle redirects (stopping at 5th redirect attempt)
			sub_to_addr = strings.TrimPrefix(strings.TrimPrefix(strings.TrimSpace(resp.Meta), "misfin://"), "misfin:")
			redirectAttempt += 1
			// TODO: If permanent redirect, change sub's new address and fingerprint and save it?
			continue
		} else if resp.Status != 20 {
			if (resp.Status == 59 || resp.Status == 40 || resp.Status == 60) && !misfinB {
				misfinClient = misfin_client.DefaultClient_MisfinB
				misfinB = true
				redirectAttempt = 0
				continue
			} else {
				fmt.Printf("Failed to send gemmail to sub %s on mailinglist %s. Failed with misfin error %v\n", sub_to_addr, mbox.CertInfo.mailAddress, resp.Status)
			}
		}

		break
	}

	fmt.Printf("Mailbox %s successfully subscribed to %s.\n", mbox.CertInfo.mailbox, sub_to_addr)
}

// TODO: Allow setting flags to skip the interactive prompts
func MakeMailboxCmd(cmd *cobra.Command, args []string) {
	var config Config
	var err error
	if len(args) > 0 {
		config, err = getConfigValuesForCommand(args[0])
		if err != nil {
			cmd.Printf("Couldn't read configuration file.\n")
			os.Exit(1)
		}
	} else {
		config, err = getConfigValuesForCommand(DefaultConfiguration.ConfigFilename)
		if err != nil {
			cmd.Printf("Couldn't read configuration file '%s'.\n", DefaultConfiguration.ConfigFilename)
			os.Exit(1)
		}
	}

	serverCertificate, serverPrivateKey, _ := getCertsFromPem(config.ServerCertFilename)
	serverCertificateInfo := getCertInfo(serverCertificate)
	if !serverCertificateInfo.IsCA {
		cmd.Printf("Your server certificate is not a CA. You cannot add any more mailboxes. Please generate a new server certificate with the `init` command.\n")
		os.Exit(1)
	}

	var cert_hostname string = serverCertificateInfo.domains[0]
	var cert_mailbox_name string
	var cert_mailbox_blurb string

	reader := bufio.NewReader(os.Stdin)
	// Read mailbox name
	for {
		cmd.Printf("Enter mailbox name. Set this to your preferred username to be used in the mailaddress: ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.TrimSpace(input)
		if input == "" {
			cmd.Printf("Error: mailbox name cannot be empty.\n")
			continue
		}

		// Check that there's no spaces
		index := strings.IndexAny(input, " \t")
		if index != -1 {
			cmd.Printf("Error: you cannot have whitespace in a mailbox name.\n")
			continue
		}

		cert_mailbox_name = input
		break
	}

	// Read mailbox blurb/full-name
	for {
		cmd.Printf("Enter Mailbox's full name (aka. blurb). This should usually be your real Full Name: ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.TrimSpace(input)
		if input == "" {
			cmd.Printf("Error: Cannot be blank.\n")
			continue
		}
		cert_mailbox_blurb = input
		break
	}

	// Read mailbox blurb/full-name
	var admin = false
	for {
		cmd.Printf("Should mailbox be an admin (able to change server configuration)? Y/N? ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.ToUpper(strings.TrimSpace(input))
		if input == "Y" || input == "YES" {
			admin = true
		} else if input == "N" || input == "NO" {
			admin = false
		} else {
			cmd.Printf("Error: Please enter Y/N.\n")
			continue
		}
		break
	}

	// Read the desired key type: ECDSA, ED25519, or RSA
	var keyType KeyType
	for {
		cmd.Printf("Enter the key type that you want to generate (ECDSA, RSA, or ED25519): ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.ToUpper(strings.TrimSpace(input))
		if input == "ECDSA" {
			keyType = KeyType_ECDSA
		} else if input == "ED25519" {
			keyType = KeyType_ED25519
		} else if input == "RSA" {
			keyType = KeyType_RSA
		} else {
			cmd.Printf("Error: Invalid key type. Enter one of the following: ECDSA, RSA, or ED25519.\n")
			continue
		}
		break
	}

	// Load mailboxes and check if mailbox with desired name already exists
	mailboxes, loadErr := loadMailboxes(config)
	if loadErr != nil {
		cmd.Printf("Couldn't load mailbox.list file: %s\n", loadErr)
		return
	}
	if mailboxes.Exists(cert_mailbox_name) {
		cmd.Printf("Mailbox with the name '%s' already exists. Aborting without saving configuration...\n", cert_mailbox_name)
		return
	}

	pemBuffer := bytes.NewBuffer([]byte{})
	var certificate *x509.Certificate
	certificate, err = generateCertAndPrivateKey(false, serverCertificate, serverPrivateKey, keyType, cert_mailbox_blurb, cert_mailbox_name, cert_hostname, pemBuffer)
	if err != nil {
		cmd.PrintErrf("Failed to generate Cert/Private Key pair: %s\n", err)
		os.Exit(1)
	}

	// Save .pem file in server mailboxes directory
	certFilepath := filepath.Join(config.MailboxDirectory, cert_mailbox_name+".pem")
	cmd.Printf("Certificate file '%s' will be written to current working directory. It should be kept private by the user and does not need to be accessible to the server. The server will only store the fingerprint, mailbox name, and mailbox full name (aka. blurb). Press Enter to continue.\n", certFilepath)
	reader.ReadString('\n')

	// Check if file already exists. // TODO: Ask to overwrite if it does.
	_, readErr := os.ReadFile(certFilepath)
	if !errors.Is(readErr, fs.ErrNotExist) || !errors.Is(readErr, os.ErrNotExist) {
		cmd.Printf("Error: Mailbox already exists.\n")
		os.Exit(1)
	}

	writeErr := os.WriteFile(certFilepath, pemBuffer.Bytes(), 0660)
	if writeErr != nil {
		cmd.PrintErrf("Could not write mailbox certificate file as '%s': %s\n", certFilepath, writeErr)
		os.Exit(1)
	}
	cmd.Printf("Saved mailbox certificate as '%s'.\n", certFilepath)

	// Get Cert Info to get the fingerprint
	certInfo := getCertInfo(certificate)

	// Add mailbox to mailbox.list file
	mailbox := CreateMailBox(MailboxListType_User, certInfo, "", false, false, config) // TODO: Ask if should be admin mailbox
	if admin {
		mailbox.T = MailboxListType_Admin
	}
	mailboxes.AddMailBox(&mailbox)
	mailboxes.saveMailboxes()
}

// TODO: Allow setting flags to skip the interactive prompts
func MakeMailinglistCmd(cmd *cobra.Command, args []string) {
	var config Config
	var err error
	if len(args) > 0 {
		config, err = getConfigValuesForCommand(args[0])
		if err != nil {
			cmd.Printf("Couldn't read configuration file.\n")
			os.Exit(1)
		}
	} else {
		config, err = getConfigValuesForCommand(DefaultConfiguration.ConfigFilename)
		if err != nil {
			cmd.Printf("Couldn't read configuration file '%s'.\n", DefaultConfiguration.ConfigFilename)
			os.Exit(1)
		}
	}

	serverCertificate, serverPrivateKey, _ := getCertsFromPem(config.ServerCertFilename)
	serverCertificateInfo := getCertInfo(serverCertificate)
	if !serverCertificateInfo.IsCA {
		cmd.Printf("Your server certificate is not a CA. You cannot add any more mailboxes. Please generate a new server certificate with the `init` command.\n")
		os.Exit(1)
	}

	var cert_hostname string = serverCertificateInfo.domains[0]
	var cert_mailbox_name string
	var cert_mailbox_blurb string

	reader := bufio.NewReader(os.Stdin)
	// Read mailbox name
	for {
		cmd.Printf("Enter mailinglist/newsletter mailbox name. Set this to the preferred username to be used in the mailaddress: ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.TrimSpace(input)
		if input == "" {
			cmd.Printf("Error: mailbox name cannot be blank.\n")
			continue
		}

		// Check that there's no spaces
		index := strings.IndexAny(input, " \t")
		if index != -1 {
			cmd.Printf("Error: you cannot have whitespace in a mailbox name.\n")
			continue
		}

		cert_mailbox_name = input
		break
	}

	// Read mailbox blurb/full-name
	for {
		cmd.Printf("Enter mailinglist/newsletter mailbox's full name (aka. blurb). This should be the name of the mailing list: ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.TrimSpace(input)
		if input == "" {
			cmd.Printf("Error: Cannot be blank.\n")
			continue
		}
		cert_mailbox_blurb = input
		break
	}

	mailinglistSubsFunction := "accept-all"
	for {
		cmd.Printf("Choose a subs function: 'accept-all' automatically accepts all subscription requests, 'closed' automatically denies all requests, and 'prompt' will prompt admins to manually accept each request. [accept-all] ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.ToLower(strings.TrimSpace(input))
		if input == "" {
			input = "accept-all"
		} else if input != "accept-all" && input != "closed" && input != "prompt" {
			cmd.Printf("Error: enter of on the following: accept-all, closed, prompt.\n")
			continue
		}

		mailinglistSubsFunction = input

		break
	}

	mailinglistSendPermission := "subs"
	for {
		cmd.Printf("Type a send permission: 'subs' allows only subs to send to the mailinglist, 'open' allows anyone to send, and 'writers' allows only admins and writers to send (useful for newsletters). [subs] ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.ToLower(strings.TrimSpace(input))
		if input == "" {
			input = "subs"
		} else if input != "subs" && input != "open" && input != "writers" {
			cmd.Printf("Error: enter of on the following: subs, writers, open.\n")
			continue
		}

		mailinglistSendPermission = input

		break
	}

	mailinglistDescription := ""
	for {
		cmd.Printf("Enter a short description of the newsletter: [''] ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.TrimSpace(input)
		mailinglistDescription = input

		break
	}

	// Read the desired key type: ECDSA, ED25519, or RSA
	var keyType KeyType
	for {
		cmd.Printf("Enter the key type that you want to generate (ECDSA, RSA, or ED25519): ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.ToUpper(strings.TrimSpace(input))
		if input == "ECDSA" {
			keyType = KeyType_ECDSA
		} else if input == "ED25519" {
			keyType = KeyType_ED25519
		} else if input == "RSA" {
			keyType = KeyType_RSA
		} else {
			cmd.Printf("Error: Invalid key type. Enter one of the following: ECDSA, RSA, or ED25519.\n")
			continue
		}
		break
	}

	pemBuffer := bytes.NewBuffer([]byte{})
	var certificate *x509.Certificate
	certificate, err = generateCertAndPrivateKey(false, serverCertificate, serverPrivateKey, keyType, cert_mailbox_blurb, cert_mailbox_name, cert_hostname, pemBuffer)
	if err != nil {
		cmd.PrintErrf("Failed to generate Cert/Private Key pair: %s\n", err)
		os.Exit(1)
	}

	// The mailing list does get stored in the mailbox directory because the server needs to use it to forward incoming mail to recipients.
	certFilepath := filepath.Join(config.MailboxDirectory, cert_mailbox_name+".pem")
	cmd.Printf("Certificate file '%s' for mailinglist/newsletter mailbox will be written to the mailbox directory. The server must have access to it to forward incoming mail to mailinglist recipients. Press Enter to continue.\n", certFilepath)

	// TODO: Check if file already exists, and ask to overwrite if it does.
	writeErr := os.WriteFile(certFilepath, pemBuffer.Bytes(), 0660)
	if writeErr != nil {
		cmd.PrintErrf("Could not write mailbox certificate file as '%s': %s\n", certFilepath, writeErr)
		os.Exit(1)
	}
	cmd.Printf("Saved mailinglist/newsletter certificate as '%s'.\n", certFilepath)

	// Get Cert Info to get the fingerprint
	certInfo := getCertInfo(certificate)

	// Create mailinglist file and subscribe the server admin as mailinglist admin
	gemboxFilename := filepath.Join(config.MailboxDirectory, certInfo.mailbox+".gembox")
	newMailbox := CreateMailBox(MailboxListType_Mailinglist, certInfo, gemboxFilename, true, false, config)
	newMailbox.List.Subscribe(MailboxListItem{MailboxListType_Mailinglist_Admin, serverCertificateInfo.mailbox, serverCertificateInfo.blurb, serverCertificateInfo.fingerprint, serverCertificateInfo.domains[0], MailingListReceiveReplies_All, nil, "", "", "", "", "", false, time.Now().UTC()})
	newMailbox.SaveGemBox()

	// Add mailinglist settings
	if mailinglistSendPermission == "subs" {
		newMailbox.List.SendThreadPermission = MailingListSend_Subs
	} else if mailinglistSendPermission == "open" {
		newMailbox.List.SendThreadPermission = MailingListSend_Open
	} else if mailinglistSendPermission == "writers" {
		newMailbox.List.SendThreadPermission = MailingListSend_Writers
	}

	if mailinglistSendPermission == "subs" {
		newMailbox.List.SendReplyPermission = MailingListSend_Subs
	} else if mailinglistSendPermission == "open" {
		newMailbox.List.SendReplyPermission = MailingListSend_Open
	} else if mailinglistSendPermission == "writers" {
		newMailbox.List.SendReplyPermission = MailingListSend_Writers
	}

	if mailinglistSubsFunction == "accept-all" {
		newMailbox.List.SubsFunction = MailingListSubs_AcceptAll
	} else if mailinglistSubsFunction == "closed" {
		newMailbox.List.SubsFunction = MailingListSubs_Closed
	} else if mailinglistSubsFunction == "prompt" {
		newMailbox.List.SubsFunction = MailingListSubs_Prompt
	}

	newMailbox.List.Description = mailinglistDescription

	// Add mailbox to mailbox.list file
	mailboxes, loadErr := loadMailboxes(config)
	if loadErr != nil {
		cmd.Printf("Couldn't load mailbox.list file: %s\n", loadErr)
		return
	}
	mailboxes.AddMailBox(&newMailbox)
	mailboxes.saveMailboxes()
}

// TODO: Allow setting flags to skip the interactive prompts
func MakeNewsfinCmd(cmd *cobra.Command, args []string) {
	var config Config
	var err error
	if len(args) > 0 {
		config, err = getConfigValuesForCommand(args[0])
		if err != nil {
			cmd.Printf("Couldn't read configuration file.\n")
			os.Exit(1)
		}
	} else {
		config, err = getConfigValuesForCommand(DefaultConfiguration.ConfigFilename)
		if err != nil {
			cmd.Printf("Couldn't read configuration file '%s'.\n", DefaultConfiguration.ConfigFilename)
			os.Exit(1)
		}
	}

	serverCertificate, serverPrivateKey, _ := getCertsFromPem(config.ServerCertFilename)
	serverCertificateInfo := getCertInfo(serverCertificate)
	if !serverCertificateInfo.IsCA {
		cmd.Printf("Your server certificate is not a CA. You cannot add any more mailboxes. Please generate a new server certificate with the `init` command.\n")
		os.Exit(1)
	}

	var cert_hostname string = serverCertificateInfo.domains[0]
	var cert_mailbox_name string
	var cert_mailbox_blurb string

	reader := bufio.NewReader(os.Stdin)
	// Read mailbox name
	for {
		cmd.Printf("Enter Newsfin group's mailbox name: ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.TrimSpace(input)
		if input == "" {
			cmd.Printf("Error: mailbox name cannot be blank.\n")
			continue
		}

		// Check that there's no spaces
		index := strings.IndexAny(input, " \t")
		if index != -1 {
			cmd.Printf("Error: you cannot have whitespace in a mailbox name.\n")
			continue
		}

		cert_mailbox_name = input
		break
	}

	// Read mailbox blurb/full-name
	for {
		cmd.Printf("Enter Newsfin group mailbox's full name (aka. blurb). This should be the topic of the Newsfin group: ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.TrimSpace(input)
		if input == "" {
			cmd.Printf("Error: Cannot be blank.\n")
			continue
		}
		cert_mailbox_blurb = input
		break
	}

	mailinglistSubsFunction := "accept-all"
	for {
		cmd.Printf("Choose a subs function: 'accept-all' automatically accepts all subscription requests, 'closed' automatically denies all requests, and 'prompt' will prompt admins to manually accept each request. [accept-all] ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.ToLower(strings.TrimSpace(input))
		if input == "" {
			input = "accept-all"
		} else if input != "accept-all" && input != "closed" && input != "prompt" {
			cmd.Printf("Error: enter of on the following: accept-all, closed, prompt.\n")
			continue
		}

		mailinglistSubsFunction = input

		break
	}

	mailinglistSendPermission := "subs"
	for {
		cmd.Printf("Type a send permission: 'subs' allows only subs to send to the Newsfin group, 'open' allows anyone to send, and 'writers' allows only admins and writers to send (useful for newsletters). [subs] ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.ToLower(strings.TrimSpace(input))
		if input == "" {
			input = "subs"
		} else if input != "subs" && input != "open" && input != "writers" {
			cmd.Printf("Error: enter of on the following: subs, writers, open.\n")
			continue
		}

		mailinglistSendPermission = input

		break
	}

	mailinglistDescription := ""
	for {
		cmd.Printf("Enter a short description of the Newsfin group: [''] ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.TrimSpace(input)
		mailinglistDescription = input

		break
	}

	// Read the desired key type: ECDSA, ED25519, or RSA
	var keyType KeyType
	for {
		cmd.Printf("Enter the key type that you want to generate (ECDSA, RSA, or ED25519): ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.ToUpper(strings.TrimSpace(input))
		if input == "ECDSA" {
			keyType = KeyType_ECDSA
		} else if input == "ED25519" {
			keyType = KeyType_ED25519
		} else if input == "RSA" {
			keyType = KeyType_RSA
		} else {
			cmd.Printf("Error: Invalid key type. Enter one of the following: ECDSA, RSA, or ED25519.\n")
			continue
		}
		break
	}

	pemBuffer := bytes.NewBuffer([]byte{})
	var certificate *x509.Certificate
	certificate, err = generateCertAndPrivateKey(false, serverCertificate, serverPrivateKey, keyType, cert_mailbox_blurb, cert_mailbox_name, cert_hostname, pemBuffer)
	if err != nil {
		cmd.PrintErrf("Failed to generate Cert/Private Key pair: %s\n", err)
		os.Exit(1)
	}

	// The mailing list does get stored in the mailbox directory because the server needs to use it to forward incoming mail to recipients.
	certFilepath := filepath.Join(config.MailboxDirectory, cert_mailbox_name+".pem")
	cmd.Printf("Certificate file '%s' for mailinglist/newsletter mailbox will be written to the mailbox directory. The server must have access to it to forward incoming mail to mailinglist recipients. Press Enter to continue.\n", certFilepath)

	// TODO: Check if file already exists, and ask to overwrite if it does.
	writeErr := os.WriteFile(certFilepath, pemBuffer.Bytes(), 0660)
	if writeErr != nil {
		cmd.PrintErrf("Could not write mailbox certificate file as '%s': %s\n", certFilepath, writeErr)
		os.Exit(1)
	}
	cmd.Printf("Saved mailinglist/newsletter certificate as '%s'.\n", certFilepath)

	// Get Cert Info to get the fingerprint
	certInfo := getCertInfo(certificate)

	// Create mailinglist file and subscribe the server admin as mailinglist admin
	gemboxFilename := filepath.Join(config.MailboxDirectory, certInfo.mailbox+".gembox")
	newMailbox := CreateMailBox(MailboxListType_NewsfinGroup, certInfo, gemboxFilename, false, true, config)
	newMailbox.List.Subscribe(MailboxListItem{MailboxListType_Mailinglist_Admin, serverCertificateInfo.mailbox, serverCertificateInfo.blurb, serverCertificateInfo.fingerprint, serverCertificateInfo.domains[0], MailingListReceiveReplies_All, nil, "", "", "", "", "", false, time.Now().UTC()})
	newMailbox.SaveGemBox()

	// Add mailinglist settings
	if mailinglistSendPermission == "subs" {
		newMailbox.List.SendThreadPermission = MailingListSend_Subs
	} else if mailinglistSendPermission == "open" {
		newMailbox.List.SendThreadPermission = MailingListSend_Open
	} else if mailinglistSendPermission == "writers" {
		newMailbox.List.SendThreadPermission = MailingListSend_Writers
	}

	if mailinglistSendPermission == "subs" {
		newMailbox.List.SendReplyPermission = MailingListSend_Subs
	} else if mailinglistSendPermission == "open" {
		newMailbox.List.SendReplyPermission = MailingListSend_Open
	} else if mailinglistSendPermission == "writers" {
		newMailbox.List.SendReplyPermission = MailingListSend_Writers
	}

	if mailinglistSubsFunction == "accept-all" {
		newMailbox.List.SubsFunction = MailingListSubs_AcceptAll
	} else if mailinglistSubsFunction == "closed" {
		newMailbox.List.SubsFunction = MailingListSubs_Closed
	} else if mailinglistSubsFunction == "prompt" {
		newMailbox.List.SubsFunction = MailingListSubs_Prompt
	}

	newMailbox.List.Description = mailinglistDescription

	// Add mailbox to mailbox.list file
	mailboxes, loadErr := loadMailboxes(config)
	if loadErr != nil {
		cmd.Printf("Couldn't load mailbox.list file: %s\n", loadErr)
		return
	}
	mailboxes.AddMailBox(&newMailbox)
	mailboxes.saveMailboxes()
}

// TODO: Allow setting flags to skip the interactive prompts
func MakeNewsletterCmd(cmd *cobra.Command, args []string) {
	var config Config
	var err error
	if len(args) > 0 {
		config, err = getConfigValuesForCommand(args[0])
		if err != nil {
			cmd.Printf("Couldn't read configuration file.\n")
			os.Exit(1)
		}
	} else {
		config, err = getConfigValuesForCommand(DefaultConfiguration.ConfigFilename)
		if err != nil {
			cmd.Printf("Couldn't read configuration file '%s'.\n", DefaultConfiguration.ConfigFilename)
			os.Exit(1)
		}
	}

	serverCertificate, serverPrivateKey, _ := getCertsFromPem(config.ServerCertFilename)
	serverCertificateInfo := getCertInfo(serverCertificate)
	if !serverCertificateInfo.IsCA {
		cmd.Printf("Your server certificate is not a CA. You cannot add any more mailboxes. Please generate a new server certificate with the `init` command.\n")
		os.Exit(1)
	}

	var cert_hostname string = serverCertificateInfo.domains[0]
	var cert_mailbox_name string
	var cert_mailbox_blurb string

	reader := bufio.NewReader(os.Stdin)
	// Read mailbox name
	for {
		cmd.Printf("Enter newsletter mailbox name. Set this to the preferred username to be used in the mailaddress: ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.TrimSpace(input)
		if input == "" {
			cmd.Printf("Error: mailbox name cannot be blank.\n")
			continue
		}

		// Check that there's no spaces
		index := strings.IndexAny(input, " \t")
		if index != -1 {
			cmd.Printf("Error: you cannot have whitespace in a mailbox name.\n")
			continue
		}

		cert_mailbox_name = input
		break
	}

	// Read mailbox blurb/full-name
	for {
		cmd.Printf("Enter newsletter mailbox's full name (aka. blurb). This should be the name of the newsletter: ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.TrimSpace(input)
		if input == "" {
			cmd.Printf("Error: Cannot be blank.\n")
			continue
		}
		cert_mailbox_blurb = input
		break
	}

	mailinglistSubsFunction := "accept-all"
	for {
		cmd.Printf("Subs function: 'accept-all' automatically accepts all subscription requests, 'closed' automatically denies all requests, and 'prompt' will prompt admins to manually accept each request. [accept-all] ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.ToLower(strings.TrimSpace(input))
		if input == "" {
			input = "accept-all"
		} else if input != "accept-all" && input != "closed" && input != "prompt" {
			cmd.Printf("Error: enter of on the following: accept-all, closed, prompt.\n")
			continue
		}

		mailinglistSubsFunction = input

		break
	}

	mailinglistDescription := ""
	for {
		cmd.Printf("Enter a short description of the newsletter: [''] ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.TrimSpace(input)
		mailinglistDescription = input

		break
	}

	// Read the desired key type: ECDSA, ED25519, or RSA
	var keyType KeyType
	for {
		cmd.Printf("Enter the key type that you want to generate (ECDSA, RSA, or ED25519): ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.ToUpper(strings.TrimSpace(input))
		if input == "ECDSA" {
			keyType = KeyType_ECDSA
		} else if input == "ED25519" {
			keyType = KeyType_ED25519
		} else if input == "RSA" {
			keyType = KeyType_RSA
		} else {
			cmd.Printf("Error: Invalid key type. Enter one of the following: ECDSA, RSA, or ED25519.\n")
			continue
		}
		break
	}

	// Newsletters only allow writers to send to the mailinglist
	mailinglistSendPermission := "writers"

	pemBuffer := bytes.NewBuffer([]byte{})
	var certificate *x509.Certificate
	certificate, err = generateCertAndPrivateKey(false, serverCertificate, serverPrivateKey, keyType, cert_mailbox_blurb, cert_mailbox_name, cert_hostname, pemBuffer)
	if err != nil {
		cmd.PrintErrf("Failed to generate Cert/Private Key pair: %s\n", err)
		os.Exit(1)
	}

	// The mailing list does get stored in the mailbox directory because the server needs to use it to forward incoming mail to recipients.
	certFilepath := filepath.Join(config.MailboxDirectory, cert_mailbox_name+".pem")
	cmd.Printf("Certificate file '%s' for newsletter mailbox will be written to the mailbox directory. The server must have access to it to forward incoming mail to newsletter recipients. Press Enter to continue.\n", certFilepath)

	// TODO: Check if file already exists, and ask to overwrite if it does.
	writeErr := os.WriteFile(certFilepath, pemBuffer.Bytes(), 0660)
	if writeErr != nil {
		cmd.PrintErrf("Could not write mailbox certificate file as '%s': %s\n", certFilepath, writeErr)
		os.Exit(1)
	}
	cmd.Printf("Saved newsletter certificate as '%s'.\n", certFilepath)

	// Get Cert Info to get the fingerprint
	certInfo := getCertInfo(certificate)

	// Create mailinglist file and subscribe the server admin as mailinglist admin
	gemboxFilename := filepath.Join(config.MailboxDirectory, certInfo.mailbox+".gembox")
	newMailbox := CreateMailBox(MailboxListType_Mailinglist, certInfo, gemboxFilename, true, false, config)
	newMailbox.List.Subscribe(MailboxListItem{MailboxListType_Mailinglist_Admin, serverCertificateInfo.mailbox, serverCertificateInfo.blurb, serverCertificateInfo.fingerprint, serverCertificateInfo.domains[0], MailingListReceiveReplies_All, nil, "", "", "", "", "", false, time.Now().UTC()})
	newMailbox.SaveGemBox()

	// Add mailinglist settings
	if mailinglistSendPermission == "subs" {
		newMailbox.List.SendThreadPermission = MailingListSend_Subs
	} else if mailinglistSendPermission == "open" {
		newMailbox.List.SendThreadPermission = MailingListSend_Open
	} else if mailinglistSendPermission == "writers" {
		newMailbox.List.SendThreadPermission = MailingListSend_Writers
	}

	if mailinglistSendPermission == "subs" {
		newMailbox.List.SendReplyPermission = MailingListSend_Subs
	} else if mailinglistSendPermission == "open" {
		newMailbox.List.SendReplyPermission = MailingListSend_Open
	} else if mailinglistSendPermission == "writers" {
		newMailbox.List.SendReplyPermission = MailingListSend_Writers
	}

	if mailinglistSubsFunction == "accept-all" {
		newMailbox.List.SubsFunction = MailingListSubs_AcceptAll
	} else if mailinglistSubsFunction == "closed" {
		newMailbox.List.SubsFunction = MailingListSubs_Closed
	} else if mailinglistSubsFunction == "prompt" {
		newMailbox.List.SubsFunction = MailingListSubs_Prompt
	}

	newMailbox.List.Description = mailinglistDescription

	// Add mailbox to mailbox.list file
	mailboxes, loadErr := loadMailboxes(config)
	if loadErr != nil {
		cmd.Printf("Couldn't load mailbox.list file: %s\n", loadErr)
		return
	}
	mailboxes.AddMailBox(&newMailbox)
	mailboxes.saveMailboxes()

	// Append mailbox to mailbox.list file
	/*appendErr := appendToMailboxList(MailboxListItem{MailboxListType_Mailinglist, cert_mailbox_name, cert_mailbox_blurb, certInfo.fingerprint, cert_hostname, mailinglistDescription, mailinglistSendPermission, mailinglistSubsFunction, time.Time{}}, config, false)
	if appendErr != nil {
		// TODO: The mailbox list is now out of sync with the actual number of mailboxes. What do I do about this?
		cmd.PrintErrf("Could not append new mailbox to mailbox list.\n")
		os.Exit(1)
	}*/
}

func ImportMailboxCmd(cmd *cobra.Command, args []string) {
	var config Config
	var err error
	var importCert string
	if len(args) >= 2 {
		// cert_file config_file
		importCert = args[0]
		config, err = getConfigValuesForCommand(args[1])
		if err != nil {
			cmd.Printf("Couldn't read configuration file.\n")
			return
		}
	} else if len(args) == 1 {
		// cert_file
		importCert = args[0]
		config, err = getConfigValuesForCommand(DefaultConfiguration.ConfigFilename)
		if err != nil {
			cmd.Printf("Couldn't read configuration file '%s'.\n", DefaultConfiguration.ConfigFilename)
			os.Exit(1)
		}
	} else {
		//config, _ = getConfigValuesForCommand(DefaultConfiguration.ConfigFilename)
		cmd.Printf("Error: Please specify cert to import in first argument to command. Optionally, specify a config_file as the second argument.\n")
		os.Exit(1)
	}

	serverCertificate, _, _ := getCertsFromPem(config.ServerCertFilename)
	serverCertificateInfo := getCertInfo(serverCertificate)
	if !serverCertificateInfo.IsCA {
		cmd.Printf("Your server certificate is not a CA. You cannot add any more mailboxes. Please generate a new server certificate with the `init` command.\n")
		return
	}

	// Check that imported certificate was signed by Server Certificate
	importCertificate, _, _ := getCertsFromPem(importCert)
	sigErr := importCertificate.CheckSignatureFrom(serverCertificate)
	if sigErr != nil {
		cmd.Printf("Error: import certificate is not signed by the server certificate; %s\n", sigErr)
		return
	}
	preexisting_certInfo := getCertInfo(importCertificate)

	// Check that hostname matches
	if preexisting_certInfo.domains[0] != serverCertificateInfo.domains[0] {
		cmd.Printf("Hostname of mailbox doesn't match hostname of server certificate. Please create a new mailbox certificate so that they match, or regenerate the server certificate.\n")
		return
	}

	// Print out details of cert.
	cmd.Printf("Here are the details of your mailbox:\n")
	cmd.Printf("Hostname: %s\n", preexisting_certInfo.domains[0])
	cmd.Printf("Mailbox Name: %s\n", preexisting_certInfo.mailbox)
	cmd.Printf("Full Name/Blurb: %s\n", preexisting_certInfo.blurb)
	cmd.Printf("Fingerprint: %s\n", preexisting_certInfo.fingerprint)

	reader := bufio.NewReader(os.Stdin)

	// Read mailbox blurb/full-name
	var admin = false
	for {
		cmd.Printf("Should mailbox be an admin (able to change server configuration)? Y/N? ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.ToUpper(strings.TrimSpace(input))
		if input == "Y" || input == "YES" {
			admin = true
		} else if input == "N" || input == "NO" {
			admin = false
		} else {
			cmd.Printf("Error: Please enter Y/N.\n")
			continue
		}
		break
	}

	// Load mailboxes and check if mailbox with name already exists
	mailboxes, loadErr := loadMailboxes(config)
	if loadErr != nil {
		cmd.Printf("Couldn't load mailbox.list file: %s\n", loadErr)
		return
	}
	if mailboxes.Exists(preexisting_certInfo.mailbox) {
		cmd.Printf("Mailbox with the name '%s' already exists. Aborting without saving configuration...\n", preexisting_certInfo.mailbox)
		return
	}

	// Prompt whether to copy gembox
	var copyGembox = false
	for {
		cmd.Printf("Do you want to copy a gembox to the mailboxes directory? Y/N? ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n") // TODO: Remove previously saved files
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.ToUpper(strings.TrimSpace(input))
		if input == "Y" || input == "YES" {
			copyGembox = true
		} else if input == "N" || input == "NO" {
			copyGembox = false
		} else {
			cmd.Printf("Please type one of the following options: Y or N.\n")
			continue
		}

		break
	}

	// Prompt for gembox file to copy
	if copyGembox {
		for {
			cmd.Printf("Enter filepath of gembox to copy: ")
			input, readErr := reader.ReadString('\n')
			if readErr == io.EOF {
				cmd.Printf("Aborting without saving configuration...\n") // TODO: Remove previously saved files
				os.Exit(1)
			} else if readErr != nil {
				cmd.Printf("Error: could not read input.\n")
				continue
			}
			input = strings.TrimSpace(input)
			if input == "" {
				cmd.Printf("Please enter a filepath.\n")
				continue
			} else if input == "cancel" {
				// Cancel trying to copy gembox file
				break
			} else {
				// Try to open the file
				bytes, readErr := os.ReadFile(input)
				if errors.Is(readErr, os.ErrNotExist) || errors.Is(readErr, fs.ErrNotExist) {
					cmd.Printf("File does not exist. Type in a new file or 'cancel' to Cancel.\n")
					continue
				}

				// If able to open, then save the file.
				newGemboxLocation := filepath.Join(config.MailboxDirectory, preexisting_certInfo.mailbox+".gembox")
				writeErr := os.WriteFile(newGemboxLocation, bytes, 0660)
				if writeErr != nil {
					cmd.PrintErrf("Could not write gembox file as '%s': %s\n", newGemboxLocation, writeErr)
					continue
				}
			}

			break
		}
	}

	// Add mailbox to mailbox.list file
	mailbox := CreateMailBox(MailboxListType_User, preexisting_certInfo, "", false, false, config) // TODO: Ask if should be admin mailbox
	if admin {
		mailbox.T = MailboxListType_Admin
	}
	mailboxes.AddMailBox(&mailbox)
	mailboxes.saveMailboxes()

	// Append mailbox to mailbox.list file
	/*appendErr := appendToMailboxList(MailboxListItem{MailboxListType_User, preexisting_certInfo.mailbox, preexisting_certInfo.blurb, preexisting_certInfo.fingerprint, preexisting_certInfo.domains[0], "", "", "", time.Time{}}, config, false)
	if appendErr != nil {
		// TODO: The mailbox list is now out of sync with the actual number of mailboxes. What do I do about this?
		cmd.PrintErrf("Could not append new mailbox to mailbox list.\n")
		os.Exit(1)
	}*/
}

func ImportMailinglistCmd(cmd *cobra.Command, args []string) {
	var config Config
	var err error
	var importCert string
	if len(args) >= 2 {
		// cert_file config_file
		importCert = args[0]
		config, err = getConfigValuesForCommand(args[1])
		if err != nil {
			cmd.Printf("Couldn't read configuration file.\n")
			return
		}
	} else if len(args) == 1 {
		// cert_file
		importCert = args[0]
		config, err = getConfigValuesForCommand(DefaultConfiguration.ConfigFilename)
		if err != nil {
			cmd.Printf("Couldn't read configuration file '%s'.\n", DefaultConfiguration.ConfigFilename)
			os.Exit(1)
		}
	} else {
		cmd.Printf("Error: Please specify cert to import in first argument to command. Optionally, specify a config_file as the second argument.\n")
		os.Exit(1)
	}

	serverCertificate, _, _ := getCertsFromPem(config.ServerCertFilename)
	serverCertificateInfo := getCertInfo(serverCertificate)
	if !serverCertificateInfo.IsCA {
		cmd.Printf("Your server certificate is not a CA. You cannot add any more mailboxes. Please generate a new server certificate with the `init` command.\n")
		return
	}

	// Check that imported certificate was signed by Server Certificate
	importCertData, readErr := os.ReadFile(importCert)
	if readErr != nil {
		cmd.Printf("Error reading cert: %s\n", readErr)
		return
	}
	importCertificate, _, _ := getCertsFromPemData(importCertData)
	sigErr := importCertificate.CheckSignatureFrom(serverCertificate)
	if sigErr != nil {
		cmd.Printf("Error: import certificate is not signed by the server certificate; %s\n", sigErr)
		return
	}
	preexisting_certInfo := getCertInfo(importCertificate)

	// Check that hostname matches
	if preexisting_certInfo.domains[0] != serverCertificateInfo.domains[0] {
		cmd.Printf("Hostname of mailbox doesn't match hostname of server certificate. Please create a new mailbox certificate so that they match, or regenerate the server certificate.\n")
		return
	}

	// Print out details of cert.
	cmd.Printf("Here are the details of your mailbox:\n")
	cmd.Printf("Hostname: %s\n", preexisting_certInfo.domains[0])
	cmd.Printf("Mailbox Name: %s\n", preexisting_certInfo.mailbox)
	cmd.Printf("Full Name/Blurb: %s\n", preexisting_certInfo.blurb)

	// TODO: Check if mailbox already exists

	reader := bufio.NewReader(os.Stdin)
	mailinglistSubsFunction := "accept-all"
	for {
		cmd.Printf("Choose a subs function: 'accept-all' automatically accepts all subscription requests, 'closed' automatically denies all requests, and 'prompt' will prompt admins to manually accept each request. [accept-all] ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.ToLower(strings.TrimSpace(input))
		if input == "" {
			input = "accept-all"
		} else if input != "accept-all" && input != "closed" && input != "prompt" {
			cmd.Printf("Error: enter of on the following: accept-all, closed, prompt.\n")
			continue
		}

		mailinglistSubsFunction = input

		break
	}

	mailinglistSendPermission := "subs"
	for {
		cmd.Printf("Type a send permission: 'subs' allows only subs to send to the mailinglist, 'open' allows anyone to send, and 'writers' allows only admins and writers to send (useful for newsletters). [subs] ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.ToLower(strings.TrimSpace(input))
		if input == "" {
			input = "subs"
		} else if input != "subs" && input != "open" && input != "writers" {
			cmd.Printf("Error: enter of on the following: subs, writers, open.\n")
			continue
		}

		mailinglistSendPermission = input

		break
	}

	mailinglistDescription := ""
	for {
		cmd.Printf("Enter a short description of the newsletter: [''] ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.TrimSpace(input)
		mailinglistDescription = input

		break
	}

	// Prompt whether to copy gembox
	var copyGembox = false
	for {
		cmd.Printf("Do you want to copy a gembox to the mailboxes directory? Y/N? ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n") // TODO: Remove previously saved files
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.ToUpper(strings.TrimSpace(input))
		if input == "Y" || input == "YES" {
			copyGembox = true
		} else if input == "N" || input == "NO" {
			copyGembox = false
		} else {
			cmd.Printf("Please type one of the following options: Y or N.\n")
			continue
		}

		break
	}

	// Prompt for gembox file to copy
	if copyGembox {
		for {
			cmd.Printf("Enter filepath of gembox to copy: ")
			input, readErr := reader.ReadString('\n')
			if readErr == io.EOF {
				cmd.Printf("Aborting without saving configuration...\n")
				os.Exit(1)
			} else if readErr != nil {
				cmd.Printf("Error: could not read input.\n")
				continue
			}
			input = strings.TrimSpace(input)
			if input == "" {
				cmd.Printf("Please enter a filepath.\n")
				continue
			} else if input == "cancel" {
				// Cancel trying to copy gembox file
				break
			} else {
				// Try to open the file
				bytes, readErr := os.ReadFile(input)
				if errors.Is(readErr, os.ErrNotExist) || errors.Is(readErr, fs.ErrNotExist) {
					cmd.Printf("File does not exist. Type in a new file or 'cancel' to Cancel.\n")
					continue
				}

				// If able to open, then save the file.
				newGemboxLocation := filepath.Join(config.MailboxDirectory, preexisting_certInfo.mailbox+".gembox")
				writeErr := os.WriteFile(newGemboxLocation, bytes, 0660)
				if writeErr != nil {
					cmd.PrintErrf("Could not write gembox file as '%s': %s\n", newGemboxLocation, writeErr)
					continue
				}
			}

			break
		}
	}

	// Copy certificate data to mailboxes location
	newCertLocation := filepath.Join(config.MailboxDirectory, preexisting_certInfo.mailbox+".pem")
	cmd.Printf("Copying certificate to mailboxes directory, at '%s'\n", newCertLocation)

	// TODO: Check if file in new location already exists, and ask to overwrite if it does.
	writeErr := os.WriteFile(newCertLocation, importCertData, 0660)
	if writeErr != nil {
		cmd.PrintErrf("Could not write mailbox certificate file as '%s': %s\n", newCertLocation, writeErr)
		os.Exit(1)
	}

	// Create mailinglist file and subscribe the server admin as mailinglist admin
	gemboxFilename := filepath.Join(config.MailboxDirectory, preexisting_certInfo.mailbox+".gembox")
	newMailbox := CreateMailBox(MailboxListType_Mailinglist, preexisting_certInfo, gemboxFilename, true, false, config)
	newMailbox.List.Subscribe(MailboxListItem{MailboxListType_Mailinglist_Admin, serverCertificateInfo.mailbox, serverCertificateInfo.blurb, serverCertificateInfo.fingerprint, serverCertificateInfo.domains[0], MailingListReceiveReplies_All, nil, "", "", "", "", "", false, time.Now().UTC()})
	newMailbox.SaveGemBox()

	// Add mailinglist settings
	if mailinglistSendPermission == "subs" {
		newMailbox.List.SendThreadPermission = MailingListSend_Subs
	} else if mailinglistSendPermission == "open" {
		newMailbox.List.SendThreadPermission = MailingListSend_Open
	} else if mailinglistSendPermission == "writers" {
		newMailbox.List.SendThreadPermission = MailingListSend_Writers
	}

	if mailinglistSendPermission == "subs" {
		newMailbox.List.SendReplyPermission = MailingListSend_Subs
	} else if mailinglistSendPermission == "open" {
		newMailbox.List.SendReplyPermission = MailingListSend_Open
	} else if mailinglistSendPermission == "writers" {
		newMailbox.List.SendReplyPermission = MailingListSend_Writers
	}

	if mailinglistSubsFunction == "accept-all" {
		newMailbox.List.SubsFunction = MailingListSubs_AcceptAll
	} else if mailinglistSubsFunction == "closed" {
		newMailbox.List.SubsFunction = MailingListSubs_Closed
	} else if mailinglistSubsFunction == "prompt" {
		newMailbox.List.SubsFunction = MailingListSubs_Prompt
	}

	newMailbox.List.Description = mailinglistDescription

	// Add mailbox to mailbox.list file
	mailboxes, loadErr := loadMailboxes(config)
	if loadErr != nil {
		cmd.Printf("Couldn't load mailbox.list file: %s\n", loadErr)
		return
	}
	mailboxes.AddMailBox(&newMailbox)
	mailboxes.saveMailboxes()

	// Append mailbox to mailbox.list file
	/*appendErr := appendToMailboxList(MailboxListItem{MailboxListType_Mailinglist, preexisting_certInfo.mailbox, preexisting_certInfo.blurb, preexisting_certInfo.fingerprint, preexisting_certInfo.domains[0], mailinglistDescription, mailinglistSendPermission, mailinglistSubsFunction, time.Time{}}, config, false)
	if appendErr != nil {
		// TODO: The mailbox list is now out of sync with the actual number of mailboxes. What do I do about this?
		cmd.PrintErrf("Could not append new mailbox to mailbox list.\n")
		os.Exit(1)
	}*/
}

func ImportNewsletterCmd(cmd *cobra.Command, args []string) {
	var config Config
	var err error
	var importCert string
	if len(args) >= 2 {
		// cert_file config_file
		importCert = args[0]
		config, err = getConfigValuesForCommand(args[1])
		if err != nil {
			cmd.Printf("Couldn't read configuration file.\n")
			return
		}
	} else if len(args) == 1 {
		// cert_file
		importCert = args[0]
		config, err = getConfigValuesForCommand(DefaultConfiguration.ConfigFilename)
		if err != nil {
			cmd.Printf("Couldn't read configuration file '%s'.\n", DefaultConfiguration.ConfigFilename)
			os.Exit(1)
		}
	} else {
		//config, _ = getConfigValuesForCommand(DefaultConfiguration.ConfigFilename)
		cmd.Printf("Error: Please specify cert to import in first argument to command. Optionally, specify a config_file as the second argument.\n")
		os.Exit(1)
	}

	serverCertificate, _, _ := getCertsFromPem(config.ServerCertFilename)
	serverCertificateInfo := getCertInfo(serverCertificate)
	if !serverCertificateInfo.IsCA {
		cmd.Printf("Your server certificate is not a CA. You cannot add any more mailboxes. Please generate a new server certificate with the `init` command.\n")
		return
	}

	// Check that imported certificate was signed by Server Certificate
	importCertData, readErr := os.ReadFile(importCert)
	if readErr != nil {
		cmd.Printf("Error reading cert: %s\n", readErr)
		return
	}
	importCertificate, _, _ := getCertsFromPemData(importCertData)
	sigErr := importCertificate.CheckSignatureFrom(serverCertificate)
	if sigErr != nil {
		cmd.Printf("Error: import certificate is not signed by the server certificate; %s\n", sigErr)
		return
	}
	preexisting_certInfo := getCertInfo(importCertificate)

	// Check that hostname matches
	if preexisting_certInfo.domains[0] != serverCertificateInfo.domains[0] {
		cmd.Printf("Hostname of mailbox doesn't match hostname of server certificate. Please create a new mailbox certificate so that they match, or regenerate the server certificate.\n")
		return
	}

	// Print out details of cert.
	cmd.Printf("Here are the details of your mailbox:\n")
	cmd.Printf("Hostname: %s\n", preexisting_certInfo.domains[0])
	cmd.Printf("Mailbox Name: %s\n", preexisting_certInfo.mailbox)
	cmd.Printf("Full Name/Blurb: %s\n", preexisting_certInfo.blurb)

	reader := bufio.NewReader(os.Stdin)
	mailinglistSubsFunction := "accept-all"
	for {
		cmd.Printf("Subs function: 'accept-all' automatically accepts all subscription requests, 'closed' automatically denies all requests, and 'prompt' will prompt admins to manually accept each request. [accept-all] ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.ToLower(strings.TrimSpace(input))
		if input == "" {
			input = "accept-all"
		} else if input != "accept-all" && input != "closed" && input != "prompt" {
			cmd.Printf("Error: enter of on the following: accept-all, closed, prompt.\n")
			continue
		}

		mailinglistSubsFunction = input

		break
	}

	mailinglistDescription := ""
	for {
		cmd.Printf("Enter a short description of the newsletter: [''] ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.TrimSpace(input)
		mailinglistDescription = input

		break
	}

	// Newsletters only allow writers to send to the mailinglist
	mailinglistSendPermission := "writers"

	// Prompt whether to copy gembox
	var copyGembox = false
	for {
		cmd.Printf("Do you want to copy a gembox to the mailboxes directory? Y/N? ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.ToUpper(strings.TrimSpace(input))
		if input == "Y" || input == "YES" {
			copyGembox = true
		} else if input == "N" || input == "NO" {
			copyGembox = false
		} else {
			cmd.Printf("Please type one of the following options: Y or N.\n")
			continue
		}

		break
	}

	// Prompt for gembox file to copy
	if copyGembox {
		for {
			cmd.Printf("Enter filepath of gembox to copy: ")
			input, readErr := reader.ReadString('\n')
			if readErr == io.EOF {
				cmd.Printf("Aborting without saving configuration...\n")
				os.Exit(1)
			} else if readErr != nil {
				cmd.Printf("Error: could not read input.\n")
				continue
			}
			input = strings.TrimSpace(input)
			if input == "" {
				cmd.Printf("Please enter a filepath.\n")
				continue
			} else if input == "cancel" {
				// Cancel trying to copy gembox file
				break
			} else {
				// Try to open the file
				bytes, readErr := os.ReadFile(input)
				if errors.Is(readErr, os.ErrNotExist) || errors.Is(readErr, fs.ErrNotExist) {
					cmd.Printf("File does not exist. Type in a new file or 'cancel' to Cancel.\n")
					continue
				}

				// If able to open, then save the file.
				newGemboxLocation := filepath.Join(config.MailboxDirectory, preexisting_certInfo.mailbox+".gembox")
				writeErr := os.WriteFile(newGemboxLocation, bytes, 0660)
				if writeErr != nil {
					cmd.PrintErrf("Could not write gembox file as '%s': %s\n", newGemboxLocation, writeErr)
					continue
				}
			}

			break
		}
	}

	// Copy certificate data to mailboxes location
	newCertLocation := filepath.Join(config.MailboxDirectory, preexisting_certInfo.mailbox+".pem")
	cmd.Printf("Copying certificate to mailboxes directory, at '%s'\n", newCertLocation)

	// TODO: Check if file in new location already exists, and ask to overwrite if it does.
	writeErr := os.WriteFile(newCertLocation, importCertData, 0660)
	if writeErr != nil {
		cmd.PrintErrf("Could not write mailbox certificate file as '%s': %s\n", newCertLocation, writeErr)
		os.Exit(1)
	}

	// Create mailinglist file and subscribe the server admin as mailinglist admin
	gemboxFilename := filepath.Join(config.MailboxDirectory, preexisting_certInfo.mailbox+".gembox")
	newMailbox := CreateMailBox(MailboxListType_Mailinglist, preexisting_certInfo, gemboxFilename, true, false, config)
	newMailbox.List.Subscribe(MailboxListItem{MailboxListType_Mailinglist_Admin, serverCertificateInfo.mailbox, serverCertificateInfo.blurb, serverCertificateInfo.fingerprint, serverCertificateInfo.domains[0], MailingListReceiveReplies_All, nil, "", "", "", "", "", false, time.Now().UTC()})
	newMailbox.SaveGemBox()

	// Add mailinglist settings
	if mailinglistSendPermission == "subs" {
		newMailbox.List.SendThreadPermission = MailingListSend_Subs
	} else if mailinglistSendPermission == "open" {
		newMailbox.List.SendThreadPermission = MailingListSend_Open
	} else if mailinglistSendPermission == "writers" {
		newMailbox.List.SendThreadPermission = MailingListSend_Writers
	}

	if mailinglistSendPermission == "subs" {
		newMailbox.List.SendReplyPermission = MailingListSend_Subs
	} else if mailinglistSendPermission == "open" {
		newMailbox.List.SendReplyPermission = MailingListSend_Open
	} else if mailinglistSendPermission == "writers" {
		newMailbox.List.SendReplyPermission = MailingListSend_Writers
	}

	if mailinglistSubsFunction == "accept-all" {
		newMailbox.List.SubsFunction = MailingListSubs_AcceptAll
	} else if mailinglistSubsFunction == "closed" {
		newMailbox.List.SubsFunction = MailingListSubs_Closed
	} else if mailinglistSubsFunction == "prompt" {
		newMailbox.List.SubsFunction = MailingListSubs_Prompt
	}

	newMailbox.List.Description = mailinglistDescription

	// Add mailbox to mailbox.list file
	mailboxes, loadErr := loadMailboxes(config)
	if loadErr != nil {
		cmd.Printf("Couldn't load mailbox.list file: %s\n", loadErr)
		return
	}
	mailboxes.AddMailBox(&newMailbox)
	mailboxes.saveMailboxes()

	// Append mailbox to mailbox.list file
	/*appendErr := appendToMailboxList(MailboxListItem{MailboxListType_Mailinglist, preexisting_certInfo.mailbox, preexisting_certInfo.blurb, preexisting_certInfo.fingerprint, preexisting_certInfo.domains[0], mailinglistDescription, mailinglistSendPermission, mailinglistSubsFunction, time.Time{}}, config, false)
	if appendErr != nil {
		// TODO: The mailbox list is now out of sync with the actual number of mailboxes. What do I do about this?
		cmd.PrintErrf("Could not append new mailbox to mailbox list.\n")
		os.Exit(1)
	}*/
}

func ImportGemboxCmd(cmd *cobra.Command, args []string) {
	var config Config
	var err error
	var importCert string
	if len(args) >= 2 {
		// cert_file config_file
		importCert = args[0]
		config, err = getConfigValuesForCommand(args[1])
		if err != nil {
			cmd.Printf("Couldn't read configuration file.\n")
			return
		}
	} else if len(args) == 1 {
		// cert_file
		importCert = args[0]
		config, err = getConfigValuesForCommand(DefaultConfiguration.ConfigFilename)
		if err != nil {
			cmd.Printf("Couldn't read configuration file '%s'.\n", DefaultConfiguration.ConfigFilename)
			os.Exit(1)
		}
	} else {
		//config, _ = getConfigValuesForCommand(DefaultConfiguration.ConfigFilename)
		cmd.Printf("Error: Please specify cert to import in first argument to command. Optionally, specify a config_file as the second argument.\n")
		os.Exit(1)
	}

	serverCertificate, _, _ := getCertsFromPem(config.ServerCertFilename)
	serverCertificateInfo := getCertInfo(serverCertificate)
	if !serverCertificateInfo.IsCA {
		cmd.Printf("Your server certificate is not a CA. You cannot add any more mailboxes. Please generate a new server certificate with the `init` command.\n")
		return
	}

	// Check that imported certificate was signed by Server Certificate
	importCertificate, _, _ := getCertsFromPem(importCert)
	sigErr := importCertificate.CheckSignatureFrom(serverCertificate)
	if sigErr != nil {
		cmd.Printf("Error: import certificate is not signed by the server certificate; %s\n", sigErr)
		return
	}
	preexisting_certInfo := getCertInfo(importCertificate)

	// Check that hostname matches
	if preexisting_certInfo.domains[0] != serverCertificateInfo.domains[0] {
		cmd.Printf("Hostname of mailbox doesn't match hostname of server certificate. Please create a new mailbox certificate so that they match, or regenerate the server certificate.\n")
		return
	}

	// Print out details of cert.
	cmd.Printf("Here are the details of your mailbox:\n")
	cmd.Printf("Hostname: %s\n", preexisting_certInfo.domains[0])
	cmd.Printf("Mailbox Name: %s\n", preexisting_certInfo.mailbox)
	cmd.Printf("Full Name/Blurb: %s\n", preexisting_certInfo.blurb)
	cmd.Printf("Fingerprint: %s\n", preexisting_certInfo.fingerprint)

	reader := bufio.NewReader(os.Stdin)

	// Read mailbox blurb/full-name
	var admin = false
	for {
		cmd.Printf("Should mailbox be an admin (able to change server configuration)? Y/N? ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n")
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.ToUpper(strings.TrimSpace(input))
		if input == "Y" || input == "YES" {
			admin = true
		} else if input == "N" || input == "NO" {
			admin = false
		} else {
			cmd.Printf("Error: Please enter Y/N.\n")
			continue
		}
		break
	}

	// Load mailboxes and check if mailbox with name already exists
	mailboxes, loadErr := loadMailboxes(config)
	if loadErr != nil {
		cmd.Printf("Couldn't load mailbox.list file: %s\n", loadErr)
		return
	}
	if mailboxes.Exists(preexisting_certInfo.mailbox) {
		cmd.Printf("Mailbox with the name '%s' already exists. Aborting without saving configuration...\n", preexisting_certInfo.mailbox)
		return
	}

	// Prompt whether to copy gembox
	var copyGembox = false
	for {
		cmd.Printf("Do you want to copy a gembox to the mailboxes directory? Y/N? ")
		input, readErr := reader.ReadString('\n')
		if readErr == io.EOF {
			cmd.Printf("Aborting without saving configuration...\n") // TODO: Remove previously saved files
			os.Exit(1)
		} else if readErr != nil {
			cmd.Printf("Error: could not read input.\n")
			continue
		}
		input = strings.ToUpper(strings.TrimSpace(input))
		if input == "Y" || input == "YES" {
			copyGembox = true
		} else if input == "N" || input == "NO" {
			copyGembox = false
		} else {
			cmd.Printf("Please type one of the following options: Y or N.\n")
			continue
		}

		break
	}

	// Prompt for gembox file to copy
	if copyGembox {
		for {
			cmd.Printf("Enter filepath of gembox to copy: ")
			input, readErr := reader.ReadString('\n')
			if readErr == io.EOF {
				cmd.Printf("Aborting without saving configuration...\n") // TODO: Remove previously saved files
				os.Exit(1)
			} else if readErr != nil {
				cmd.Printf("Error: could not read input.\n")
				continue
			}
			input = strings.TrimSpace(input)
			if input == "" {
				cmd.Printf("Please enter a filepath.\n")
				continue
			} else if input == "cancel" {
				// Cancel trying to copy gembox file
				break
			} else {
				// Try to open the file
				bytes, readErr := os.ReadFile(input)
				if errors.Is(readErr, os.ErrNotExist) || errors.Is(readErr, fs.ErrNotExist) {
					cmd.Printf("File does not exist. Type in a new file or 'cancel' to Cancel.\n")
					continue
				}

				// If able to open, then save the file.
				newGemboxLocation := filepath.Join(config.MailboxDirectory, preexisting_certInfo.mailbox+".gembox")
				writeErr := os.WriteFile(newGemboxLocation, bytes, 0660)
				if writeErr != nil {
					cmd.PrintErrf("Could not write gembox file as '%s': %s\n", newGemboxLocation, writeErr)
					continue
				}
			}

			break
		}
	}

	// Add mailbox to mailbox.list file
	mailbox := CreateMailBox(MailboxListType_User, preexisting_certInfo, "", false, false, config) // TODO: Ask if should be admin mailbox
	if admin {
		mailbox.T = MailboxListType_Admin
	}
	mailboxes.AddMailBox(&mailbox)
	mailboxes.saveMailboxes()

	// Append mailbox to mailbox.list file
	/*appendErr := appendToMailboxList(MailboxListItem{MailboxListType_User, preexisting_certInfo.mailbox, preexisting_certInfo.blurb, preexisting_certInfo.fingerprint, preexisting_certInfo.domains[0], "", "", "", time.Time{}}, config, false)
	if appendErr != nil {
		// TODO: The mailbox list is now out of sync with the actual number of mailboxes. What do I do about this?
		cmd.PrintErrf("Could not append new mailbox to mailbox list.\n")
		os.Exit(1)
	}*/
}
