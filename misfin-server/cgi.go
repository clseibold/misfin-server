package main

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"fmt"
	"io"
	"net"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"time"
)

func RunCgi(scriptPath, hostname string, port string, clientCert *x509.Certificate, clientCertInfo CertInfo, URL *url.URL, path string, query string, conn net.Conn) {
	remote_ip, remote_port, _ := net.SplitHostPort(conn.RemoteAddr().String())

	// Set up environment variables
	env := os.Environ()
	env = append(env, "GATEWAY_INTERFACE=CGI/1.1")
	env = append(env, "SERVER_PROTOCOL=GEMINI")
	env = append(env, "SCHEME=gemini")
	env = append(env, "REQUEST_METHOD=")
	env = append(env, "SERVER_SOFTWARE=Misfin-server/"+Version)
	env = append(env, fmt.Sprintf("SERVER_NAME=%s", hostname))
	env = append(env, fmt.Sprintf("SERVER_PORT=%s", port))
	env = append(env, fmt.Sprintf("REAL_LISTEN_PORT=%s", port))
	env = append(env, fmt.Sprintf("REMOTE_ADDR=%s", remote_ip))
	env = append(env, fmt.Sprintf("REMOTE_PORT=%s", remote_port))
	env = append(env, fmt.Sprintf("REMOTE_HOST=%s", conn.RemoteAddr().String())) // TODO
	env = append(env, "SCRIPT_NAME="+scriptPath)                                 // TODO

	if clientCert != nil {
		env = append(env, fmt.Sprintf("AUTH_TYPE=CERTIFICATE"))
		env = append(env, fmt.Sprintf("TLS_CLIENT_HASH=%s", clientCertInfo.fingerprint)) // TODO: JetForce prepends with "SHA256:"
		env = append(env, fmt.Sprintf("TLS_CLIENT_NOT_BEFORE=%s", clientCert.NotBefore.Format(time.RFC3339)))
		env = append(env, fmt.Sprintf("TLS_CLIENT_NOT_AFTER=%s", clientCert.NotAfter.Format(time.RFC3339)))
		env = append(env, fmt.Sprintf("TLS_CLIENT_SERIAL_NUMBER=%s", clientCert.SerialNumber.String()))
		env = append(env, fmt.Sprintf("TLS_CLIENT_SERIAL=%s", clientCert.SerialNumber.String()))
		// // // // env = append(env, fmt.Sprintf("TLS_CLIENT_AUTHORIZED=%s", clientCertSerialNumber)) // TODO

		env = append(env, fmt.Sprintf("REMOTE_IDENT=%s", clientCertInfo.fingerprint))
		env = append(env, fmt.Sprintf("REMOTE_USER=%s", clientCertInfo.mailbox))
	}

	env = append(env, fmt.Sprintf("REQUEST_URI=%s", URL.String()))
	env = append(env, fmt.Sprintf("GEMINI_URL=%s", URL.String()))
	env = append(env, fmt.Sprintf("REQUEST_DOMAIN=%s", hostname))
	env = append(env, fmt.Sprintf("PATH_INFO=%s", path)) // TODO: Just use the path after the prefix for the route to the script
	env = append(env, fmt.Sprintf("REQUEST_PATH=%s", path))
	env = append(env, fmt.Sprintf("QUERY_STRING=%s", query))

	if tlsConn, isTls := conn.(*tls.Conn); isTls {
		env = append(env, fmt.Sprintf("SSL_TLS_SNI=%s", tlsConn.ConnectionState().ServerName))
		env = append(env, fmt.Sprintf("TLS_CIPHER=%s", tls.CipherSuiteName(tlsConn.ConnectionState().CipherSuite)))
		env = append(env, fmt.Sprintf("TLS_VERSION=%s", tls.VersionName(tlsConn.ConnectionState().Version)))
	}

	// Execute the CGI script
	cmd := exec.Command(scriptPath)
	cmd.Env = env
	cmd.Stdout = conn
	cmd.Stderr = os.Stderr
	cmd.Dir = filepath.Dir(scriptPath)

	// Run the command
	err := cmd.Run()
	if err != nil {
		fmt.Println("Error executing CGI script:", err)
		conn.Write([]byte("42 CGI error.\r\n"))
		return
	}
}

func RunScgi(scriptPath, hostname string, port string, clientCert *x509.Certificate, clientCertInfo CertInfo, URL *url.URL, path string, query string, conn net.Conn) {
	remote_ip, remote_port, _ := net.SplitHostPort(conn.RemoteAddr().String())

	// Prepare SCGI request
	headers := map[string]string{
		"GATEWAY_INTERFACE":      "CGI/1.1",
		"CONTENT_LENGTH":         "0",
		"SCGI":                   "1",
		"SERVER_PROTOCOL":        "GEMINI",
		"SCHEME":                 "gemini",
		"REQUEST_METHOD":         "",
		"SERVER_SOFTWARE":        "Misfin-server/" + Version,
		"SERVER_NAME":            hostname,
		"SERVER_PORT":            port, // port being forwarded from router
		"REAL_LISTEN_PORT":       port, // listenPort
		"REMOTE_ADDR":            remote_ip,
		"REMOTE_PORT":            remote_port,
		"REMOTE_HOST":            conn.RemoteAddr().String(),
		"SCRIPT_NAME":            scriptPath, // TODO
		"GEMINI_SCRIPT_FILENAME": scriptPath,
		"GEMINI_DOCUMENT_ROOT":   filepath.Dir(scriptPath),

		"REQUEST_URI":     URL.String(),
		"GEMINI_URL":      URL.String(),
		"REQUEST_DOMAIN":  hostname,
		"GEMINI_URL_PATH": path,
		"PATH_INFO":       path, // TODO: Just use the path after the prefix for the route to the script
		"REQUEST_PATH":    path,
		"QUERY_STRING":    query,
	}

	if clientCert != nil {
		headers["AUTH_TYPE"] = "CERTIFICATE"
		headers["TLS_CLIENT_HASH"] = clientCertInfo.fingerprint
		headers["TLS_CLIENT_NOT_BEFORE"] = clientCert.NotBefore.Format(time.RFC3339)
		headers["TLS_CLIENT_NOT_AFTER"] = clientCert.NotAfter.Format(time.RFC3339)
		headers["TLS_CLIENT_SERIAL_NUMBER"] = clientCert.SerialNumber.String()
		headers["TLS_CLIENT_SERIAL"] = clientCert.SerialNumber.String()
		headers["REMOTE_IDENT"] = clientCertInfo.fingerprint
		headers["REMOTE_USER"] = clientCertInfo.mailbox
	}

	if tlsConn, isTls := conn.(*tls.Conn); isTls {
		headers["SSL_TLS_SNI"] = tlsConn.ConnectionState().ServerName
		headers["TLS_CIPHER"] = tls.CipherSuiteName(tlsConn.ConnectionState().CipherSuite)
		headers["TLS_VERSION"] = tls.VersionName(tlsConn.ConnectionState().Version)
	}

	// Create SCGI request
	var buf bytes.Buffer
	for k, v := range headers {
		buf.WriteString(k)
		buf.WriteByte(0)
		buf.WriteString(v)
		buf.WriteByte(0)
	}
	headerLength := buf.Len()
	fullRequest := fmt.Sprintf("%d:%s,", headerLength, buf.String())

	// Connect to SCGI server
	scgiConn, err := net.Dial("tcp", "localhost:4000") // Assuming SCGI server is on localhost:4000
	if err != nil {
		fmt.Println("Error connecting to SCGI server:", err)
		conn.Write([]byte("42 SCGI connection error.\r\n"))
		return
	}
	defer scgiConn.Close()

	// Send SCGI request
	_, err = scgiConn.Write([]byte(fullRequest))
	if err != nil {
		fmt.Println("Error sending SCGI request:", err)
		conn.Write([]byte("42 SCGI request error.\r\n"))
		return
	}

	// Read response from SCGI server and write to client
	_, err = io.Copy(conn, scgiConn)
	if err != nil {
		fmt.Println("Error reading SCGI response:", err)
		conn.Write([]byte("42 SCGI response error.\r\n"))
		return
	}
}

func RunFastCgi(scriptPath, hostname string, port string, clientCert *x509.Certificate, clientCertInfo CertInfo, URL *url.URL, path string, query string, conn net.Conn) {
	remote_ip, remote_port, _ := net.SplitHostPort(conn.RemoteAddr().String())

	// Prepare FastCGI parameters
	params := map[string]string{
		"GATEWAY_INTERFACE": "CGI/1.1",
		"SERVER_PROTOCOL":   "GEMINI",
		"SCHEME":            "gemini",
		"REQUEST_METHOD":    "",
		"SERVER_SOFTWARE":   "Misfin-server/" + Version,
		"SERVER_NAME":       hostname,
		"SERVER_PORT":       port,
		"REAL_LISTEN_PORT":  port,
		"REMOTE_ADDR":       remote_ip,
		"REMOTE_PORT":       remote_port,
		"REMOTE_HOST":       conn.RemoteAddr().String(),
		"SCRIPT_NAME":       scriptPath,
		"SCRIPT_FILENAME":   scriptPath,
		"REQUEST_URI":       URL.String(),
		"GEMINI_URL":        URL.String(),
		"REQUEST_DOMAIN":    hostname,
		"PATH_INFO":         path,
		"REQUEST_PATH":      path,
		"QUERY_STRING":      query,
	}

	if clientCert != nil {
		params["AUTH_TYPE"] = "CERTIFICATE"
		params["TLS_CLIENT_HASH"] = clientCertInfo.fingerprint
		params["TLS_CLIENT_NOT_BEFORE"] = clientCert.NotBefore.Format(time.RFC3339)
		params["TLS_CLIENT_NOT_AFTER"] = clientCert.NotAfter.Format(time.RFC3339)
		params["TLS_CLIENT_SERIAL_NUMBER"] = clientCert.SerialNumber.String()
		params["REMOTE_IDENT"] = clientCertInfo.fingerprint
		params["REMOTE_USER"] = clientCertInfo.mailbox
	}

	if tlsConn, isTls := conn.(*tls.Conn); isTls {
		params["SSL_TLS_SNI"] = tlsConn.ConnectionState().ServerName
		params["TLS_CIPHER"] = tls.CipherSuiteName(tlsConn.ConnectionState().CipherSuite)
		params["TLS_VERSION"] = tls.VersionName(tlsConn.ConnectionState().Version)
	}

	// Connect to FastCGI server
	fcgiConn, err := net.Dial("tcp", "localhost:9000")
	if err != nil {
		fmt.Println("Error connecting to FastCGI server:", err)
		conn.Write([]byte("42 FastCGI connection error.\r\n"))
		return
	}
	defer fcgiConn.Close()

	// Construct FastCGI request
	var buf bytes.Buffer
	writeRecord := func(recType uint8, content []byte) {
		buf.WriteByte(1)                       // version
		buf.WriteByte(recType)                 // type
		buf.WriteByte(0)                       // requestIdB1
		buf.WriteByte(1)                       // requestIdB0
		buf.WriteByte(byte(len(content) >> 8)) // contentLengthB1
		buf.WriteByte(byte(len(content)))      // contentLengthB0
		buf.WriteByte(0)                       // paddingLength
		buf.WriteByte(0)                       // reserved
		buf.Write(content)
	}

	// Begin request
	writeRecord(1, []byte{0, 1, 0, 0, 0, 0, 0, 0})

	// Params
	var paramsBuf bytes.Buffer
	for k, v := range params {
		paramsBuf.WriteByte(byte(len(k)))
		paramsBuf.WriteByte(byte(len(v)))
		paramsBuf.WriteString(k)
		paramsBuf.WriteString(v)
	}
	writeRecord(4, paramsBuf.Bytes())
	writeRecord(4, []byte{}) // empty params to signal end

	// Write FastCGI request to server
	_, err = fcgiConn.Write(buf.Bytes())
	if err != nil {
		fmt.Println("Error writing FastCGI request:", err)
		conn.Write([]byte("42 FastCGI request error.\r\n"))
		return
	}

	// Read FastCGI response
	var respBuf bytes.Buffer
	for {
		header := make([]byte, 8)
		_, err := io.ReadFull(fcgiConn, header)
		if err != nil {
			if err == io.EOF {
				break
			}
			fmt.Println("Error reading FastCGI response header:", err)
			conn.Write([]byte("42 FastCGI response error.\r\n"))
			return
		}

		contentLength := int(header[4])<<8 | int(header[5])
		paddingLength := int(header[6])

		content := make([]byte, contentLength)
		_, err = io.ReadFull(fcgiConn, content)
		if err != nil {
			fmt.Println("Error reading FastCGI response content:", err)
			conn.Write([]byte("42 FastCGI response error.\r\n"))
			return
		}

		if header[1] == 6 { // STDOUT
			respBuf.Write(content)
		}

		if paddingLength > 0 {
			padding := make([]byte, paddingLength)
			_, err = io.ReadFull(fcgiConn, padding)
			if err != nil {
				fmt.Println("Error reading FastCGI response padding:", err)
				conn.Write([]byte("42 FastCGI response error.\r\n"))
				return
			}
		}
	}

	// Write response to client
	_, err = conn.Write(respBuf.Bytes())
	if err != nil {
		fmt.Println("Error writing FastCGI response to client:", err)
		conn.Write([]byte("42 FastCGI response error.\r\n"))
		return
	}
}

func TestFastCGIServer() {
	listener, err := net.Listen("tcp", "localhost:9000")
	if err != nil {
		fmt.Println("Error starting FastCGI server:", err)
		return
	}
	defer listener.Close()

	fmt.Println("FastCGI server listening on localhost:9000")

	for {
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println("Error accepting connection:", err)
			continue
		}

		go handleFastCGIConnection(conn)
	}
}

func handleFastCGIConnection(conn net.Conn) {
	defer conn.Close()

	// Read FastCGI records
	for {
		header := make([]byte, 8)
		_, err := io.ReadFull(conn, header)
		if err != nil {
			if err == io.EOF {
				return
			}
			fmt.Println("Error reading FastCGI header:", err)
			return
		}

		contentLength := int(header[4])<<8 | int(header[5])
		paddingLength := int(header[6])

		content := make([]byte, contentLength)
		_, err = io.ReadFull(conn, content)
		if err != nil {
			fmt.Println("Error reading FastCGI content:", err)
			return
		}

		if header[1] == 4 { // PARAMS
			params := parseParams(content)
			if query, ok := params["QUERY_STRING"]; ok {
				// Send response
				response := fmt.Sprintf("20 text/gemini\r\nHello World: %s", query)
				writeRecord(conn, 6, []byte(response))               // STDOUT
				writeRecord(conn, 6, []byte{})                       // Empty STDOUT to signal end
				writeRecord(conn, 3, []byte{0, 0, 0, 0, 0, 0, 0, 0}) // END_REQUEST
				return
			}
		}

		// Skip padding
		if paddingLength > 0 {
			padding := make([]byte, paddingLength)
			_, err = io.ReadFull(conn, padding)
			if err != nil {
				fmt.Println("Error reading FastCGI padding:", err)
				return
			}
		}
	}
}

func parseParams(content []byte) map[string]string {
	params := make(map[string]string)
	for len(content) > 0 {
		var keyLength, valueLength int
		if content[0]>>7 == 0 {
			keyLength = int(content[0])
			content = content[1:]
		} else {
			keyLength = int(content[0]&0x7f)<<24 | int(content[1])<<16 | int(content[2])<<8 | int(content[3])
			content = content[4:]
		}
		if content[0]>>7 == 0 {
			valueLength = int(content[0])
			content = content[1:]
		} else {
			valueLength = int(content[0]&0x7f)<<24 | int(content[1])<<16 | int(content[2])<<8 | int(content[3])
			content = content[4:]
		}
		key := string(content[:keyLength])
		value := string(content[keyLength : keyLength+valueLength])
		params[key] = value
		content = content[keyLength+valueLength:]
	}
	return params
}

func writeRecord(conn net.Conn, recType uint8, content []byte) {
	record := make([]byte, 8+len(content))
	record[0] = 1 // version
	record[1] = recType
	record[2] = 0 // requestIdB1
	record[3] = 1 // requestIdB0
	record[4] = byte(len(content) >> 8)
	record[5] = byte(len(content))
	record[6] = 0 // paddingLength
	record[7] = 0 // reserved
	copy(record[8:], content)
	conn.Write(record)
}
