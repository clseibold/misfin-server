package main

import (
	"bytes"
	"crypto"
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/elliptic"
	crypto_rand "crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/asn1"
	"encoding/hex"
	"encoding/pem"
	"fmt"
	"math/big"
	"os"
	"time"
)

// OID_USER_ID represents the Object Identifier for the userId attribute
// in X.500 directory services. It is commonly used to store a user's
// login name or username in X.509 certificates.
var OID_USER_ID = asn1.ObjectIdentifier{0, 9, 2342, 19200300, 100, 1, 1}

// OID_CN represents the Object Identifier for the Common Name (CN) attribute
// in X.500 directory services. It is commonly used to store the name of an entity
// (e.g., a person, organization, or domain) in X.509 certificates.
var OID_CN = asn1.ObjectIdentifier{2, 5, 4, 3}

var OID_MISFIN = "2.25.276466666638671594291857646440529523315.0" // 2.25.276466666638671594291857646440529523315.0

type CertInfo struct {
	blurb       string
	domains     []string
	mailbox     string
	mailAddress string
	fingerprint string
	IsCA        bool
}

// NOTE: Assumes that cert and private key are in a single (pem) file.
func getCertsFromPem(certFilename string) (*x509.Certificate, crypto.PrivateKey, []byte) {
	file, readErr := os.ReadFile(certFilename)
	if readErr != nil {
		panic(fmt.Errorf("could not read certificate file %q: %v", certFilename, readErr))
	}

	pemBlock, rest := pem.Decode(file)
	var certificate *x509.Certificate
	var privateKey crypto.PrivateKey
	var privateKeyPemBytes []byte
	for pemBlock != nil {
		var parseErr error
		if pemBlock.Type == "CERTIFICATE" {
			certificate, parseErr = x509.ParseCertificate(pemBlock.Bytes)
		} else if pemBlock.Type == "RSA PRIVATE KEY" {
			privateKeyPemBytes = pemBlock.Bytes
			privateKey, parseErr = x509.ParsePKCS1PrivateKey(pemBlock.Bytes)
		} else if pemBlock.Type == "EC PRIVATE KEY" {
			privateKeyPemBytes = pemBlock.Bytes
			privateKey, parseErr = x509.ParseECPrivateKey(pemBlock.Bytes)
		} else if pemBlock.Type == "PRIVATE KEY" {
			privateKeyPemBytes = pemBlock.Bytes
			privateKey, parseErr = x509.ParsePKCS8PrivateKey(pemBlock.Bytes)
		}
		if parseErr != nil {
			panic(parseErr)
		}
		pemBlock, rest = pem.Decode(rest)
	}

	return certificate, privateKey, privateKeyPemBytes
}

// NOTE: Assumes that cert and private key are in a single (pem) file.
func getCertsFromPemData(certData []byte) (*x509.Certificate, crypto.PrivateKey, []byte) {
	pemBlock, rest := pem.Decode(certData)
	var certificate *x509.Certificate
	var privateKey crypto.PrivateKey
	var privateKeyPemBytes []byte
	for pemBlock != nil {
		var parseErr error
		if pemBlock.Type == "CERTIFICATE" {
			certificate, parseErr = x509.ParseCertificate(pemBlock.Bytes)
		} else if pemBlock.Type == "RSA PRIVATE KEY" {
			privateKeyPemBytes = pemBlock.Bytes
			privateKey, parseErr = x509.ParsePKCS1PrivateKey(pemBlock.Bytes)
		} else if pemBlock.Type == "EC PRIVATE KEY" {
			privateKeyPemBytes = pemBlock.Bytes
			privateKey, parseErr = x509.ParseECPrivateKey(pemBlock.Bytes)
		} else if pemBlock.Type == "PRIVATE KEY" {
			privateKeyPemBytes = pemBlock.Bytes
			privateKey, parseErr = x509.ParsePKCS8PrivateKey(pemBlock.Bytes)
		}
		if parseErr != nil {
			panic(parseErr)
		}
		pemBlock, rest = pem.Decode(rest)
	}

	return certificate, privateKey, privateKeyPemBytes
}

// Returns Common Name (aka. blurb), mailbox, hostname(s), and mailAddress
func getCertInfo(certificate *x509.Certificate) CertInfo {
	commonName := certificate.Subject.CommonName // aka. blurb
	domains := certificate.DNSNames              // aka. hostname
	mailbox := ""
	for _, n := range certificate.Subject.Names {
		if n.Type.String() == "0.9.2342.19200300.100.1.1" {
			mailbox = n.Value.(string)
		}
	}
	mailAddress := mailbox + "@"
	if len(domains) > 0 {
		mailAddress = mailbox + "@" + domains[0]
	}

	h := sha256.New()
	h.Write(certificate.Raw)
	fingerprint := hex.EncodeToString(h.Sum(nil))

	return CertInfo{commonName, domains, mailbox, mailAddress, fingerprint, certificate.IsCA}
}

type KeyType int

const (
	KeyType_ECDSA KeyType = iota
	KeyType_ED25519
	KeyType_RSA
)

// If creating Server Certificate, set is_ca to true, serverCert to nil, and serverPrivateKey to nil
// Returns pem data of certificate/privatekey pair
func generateCertAndPrivateKey(is_ca bool, serverCert *x509.Certificate, serverPrivateKey any, keyType KeyType, cert_mailbox_blurb string, cert_mailbox_name string, cert_hostname string, pemBuffer *bytes.Buffer) (*x509.Certificate, error) {
	// Generate random serial number for Cert
	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := crypto_rand.Int(crypto_rand.Reader, serialNumberLimit)
	if err != nil {
		//cmd.PrintErrf("Error generating serial number for certificate: %s\n", err)
		return nil, err
	}

	// Create Certificate
	var privateKey any
	var publicKey any
	//var signatureAlgorithm x509.SignatureAlgorithm
	//var publicKeyAlgorithm x509.PublicKeyAlgorithm
	if keyType == KeyType_ECDSA {
		var key *ecdsa.PrivateKey
		key, err = ecdsa.GenerateKey(elliptic.P256(), crypto_rand.Reader)
		privateKey = key
		publicKey = &key.PublicKey
		//signatureAlgorithm = x509.ECDSAWithSHA256
		//publicKeyAlgorithm = x509.ECDSA
	} else if keyType == KeyType_ED25519 {
		publicKey, privateKey, err = ed25519.GenerateKey(crypto_rand.Reader)
		//signatureAlgorithm = x509.PureEd25519
		//publicKeyAlgorithm = x509.Ed25519
	} else if keyType == KeyType_RSA {
		var key *rsa.PrivateKey
		key, err = rsa.GenerateKey(crypto_rand.Reader, 2048) // TODO: In the far future, change this to 3072
		publicKey = key.PublicKey
		//signatureAlgorithm = x509.SHA256WithRSA
		//publicKeyAlgorithm = x509.RSA
	}
	if err != nil {
		fmt.Printf("Error generating private key: %s\n", err)
		return nil, err
	}

	subject := pkix.Name{
		CommonName: cert_mailbox_blurb,
		ExtraNames: []pkix.AttributeTypeAndValue{{Type: OID_USER_ID, Value: string(cert_mailbox_name)}},
	}
	issuer := subject
	if !is_ca {
		issuer = serverCert.Subject
	}

	/*blah := &x509.OID{}
	blah.UnmarshalText([]byte("Testing"))*/

	certificate := &x509.Certificate{
		IsCA:         is_ca,
		DNSNames:     []string{cert_hostname},
		Subject:      subject,
		Issuer:       issuer,
		SerialNumber: serialNumber,
		NotBefore:    time.Now().UTC(),
		NotAfter:     time.Now().UTC().Add(time.Hour * 24 * 365 * 200), // 200 years
		//SignatureAlgorithm:    signatureAlgorithm,
		//PublicKeyAlgorithm:    publicKeyAlgorithm,
		BasicConstraintsValid: true,
		//ExtraExtensions:       []pkix.Extension{misfinExtension},
		Policies: []x509.OID{},
	}
	var parent *x509.Certificate
	var signerPrivateKey any
	//var publicKey any

	if !is_ca {
		// Mailbox cert: Signed by server certificate CA
		if serverCert == nil {
			panic("Server Cert cannot be nil when creating a mailbox certificate.\n")
		} else if !serverCert.IsCA {
			panic("The Server Cert passed in is not marked as a CA.\n")
		} else if serverPrivateKey == nil {
			panic("Server Private Key cannot be nil when creating a mailbox certificate.\n")
		}
		parent = serverCert
		signerPrivateKey = serverPrivateKey

		// Create Certificate Signing Request to use as the public key for the new cert
		csrTemplate := &x509.CertificateRequest{Subject: subject}
		csr_DER, csrErr := x509.CreateCertificateRequest(crypto_rand.Reader, csrTemplate, privateKey)
		if csrErr != nil {
			return nil, err
		}
		csr, csrErr := x509.ParseCertificateRequest(csr_DER)
		if csrErr != nil {
			return nil, err
		}
		publicKey = csr.PublicKey
	} else {
		// Self-signed: creating the server certificate as a CA. Use itself as parent cert, and its own private key as the signer
		parent = certificate
		signerPrivateKey = privateKey
	}

	certBytes_DER, createErr := x509.CreateCertificate(crypto_rand.Reader, certificate, parent, publicKey, signerPrivateKey) // NOTE: When making mailboxes, privateKey should be parent's private key!
	if createErr != nil {
		//cmd.PrintErrf("Error creating certificate: %s\n", createErr)
		fmt.Printf("Could not create certificate: %s.\n", createErr)
		fmt.Printf("Requested Private Key Type: %T\nSignature Algorithm of Certificate: %s\n", privateKey, certificate.SignatureAlgorithm)
		return nil, err
	}

	finalCertificate, parseErr := x509.ParseCertificate(certBytes_DER)
	if parseErr != nil {
		fmt.Printf("Could not parse certificate.\n")
		return nil, err
	}

	// Convert private key to DER bytes
	privateKeyBytes_DER, err := x509.MarshalPKCS8PrivateKey(privateKey)
	if err != nil {
		fmt.Printf("Could not marshal private key.\n")
		return nil, err
	}
	//privateKeyBytes_DER := x509.MarshalPKCS1PrivateKey(privateKey)

	// Convert cert to pem format and save it
	certBlock := pem.Block{Type: "CERTIFICATE", Bytes: certBytes_DER}
	privBlock := pem.Block{Type: "PRIVATE KEY", Bytes: privateKeyBytes_DER}
	encodeErr := pem.Encode(pemBuffer, &certBlock)
	if encodeErr != nil {
		//cmd.PrintErrf("Could not encode certificate to PEM format.\n")
		fmt.Printf("Could not encode certificate to PEM format.\n")
		return nil, err
	}
	encodeErr = pem.Encode(pemBuffer, &privBlock)
	if encodeErr != nil {
		fmt.Printf("Could not encode certificate to PEM format.\n")
		//cmd.PrintErrf("Could not encode certificate to PEM format.\n")
		return nil, err
	}

	return finalCertificate, nil
}
