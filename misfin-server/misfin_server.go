package main

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/ed25519"
	crypto_rand "crypto/rand"
	"crypto/rsa"
	"encoding/base64"
	"fmt"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	"gitlab.com/clseibold/misfin-server/gemmail"
	"gitlab.com/clseibold/misfin-server/misfin_client"
)

func handleMisfinRequest(conn tlsconn, URL *url.URL, misfinC bool, messageBody string, serverContext *ServerContext, writeTimeout time.Duration, ip string) {
	tlsState := conn.ConnectionState()
	recipientMailbox := URL.User.Username()
	recipientHostname := URL.Hostname()

	// Check that the recipient mailbox exists on this server
	if recipientHostname != serverContext.serverCertInfo.domains[0] { // TODO: Check all domains
		// Incorrect hostname
		fmt.Printf("received mail for wrong hostname\n")
		responseDomainNotServiced := "53 This mailserver doesn't serve mail for the hostname you provided.\r\n"
		conn.Write([]byte(responseDomainNotServiced))
		return
	}
	if !serverContext.mailboxes.Exists(recipientMailbox) && recipientMailbox != serverContext.serverCertInfo.mailbox {
		fmt.Printf("received mail for mailbox that doesn't exist\n")
		responseMailboxDoesntExist := "51 The mailbox you are trying to send to doesn't exist, and the mailserver won't accept your message.\r\n"
		conn.Write([]byte(responseMailboxDoesntExist))
		return
	}

	// Parse the message as gemmail
	var gmail gemmail.GemMail
	if !misfinC {
		gmail = gemmail.ParseGemMail_MisfinB(messageBody)
	} else {
		gmail = gemmail.ParseGemMail_MisfinC(messageBody)
	}

	// Get recipient's mailbox
	mailbox := serverContext.mailboxes.Get(recipientMailbox)

	// Detect blank gemmails. If they are blank, then they are probably being used to verify fingerprints. Don't save these to the gembox
	if checkBlankGemmail(gmail) {
		fmt.Printf("Received blank verification gemmail.\n")
		responseSuccess := "20 " + mailbox.CertInfo.fingerprint + "\r\n"
		conn.Write([]byte(responseSuccess))
		return
	}

	// Check that a client certificate was used.
	if len(tlsState.PeerCertificates) == 0 {
		// Error: no cert
		fmt.Printf("no cert\n")
		responseCertificateRequired := "60 This mailserver doesn't accept anonymous mail, and you need to repeat your request with a certificate.\r\n"
		conn.Write([]byte(responseCertificateRequired))
		return
	}

	// Make sure that the sender has the USER_ID field in their cert
	senderCert := tlsState.PeerCertificates[0]
	senderCertInfo := getCertInfo(senderCert)
	if !verifySenderCert(conn, senderCertInfo) {
		return
	}

	fmt.Printf("\n|> Incoming to %s from %s (%s)\n", recipientMailbox, senderCertInfo.blurb, senderCertInfo.mailAddress)
	fmt.Printf("Fingerprint is %s\n", senderCertInfo.fingerprint)
	if misfinC {
		fmt.Printf("Misfin(C) Request\n")
	} else {
		fmt.Printf("Misfin(B) Request\n")
	}
	fmt.Printf("Message:\n")
	fmt.Printf("%s\n", messageBody)

	if writeTimeout != 0 {
		err := conn.SetWriteDeadline(time.Now().Add(writeTimeout))
		if err != nil {
			fmt.Printf("could not set socket write timeout: %s", err)
			return
		}
	}

	// Add the sender line at the end of the list of sender lines and add a timestamp, both of which are the responsibility of the receiving mailserver, as per the misfin spec, sections 4.1 and 4.3
	gmail.PrependSender(senderCertInfo.mailAddress, senderCertInfo.blurb)
	gmail.PrependTimestamp(time.Now().UTC())
	gmail.AddTag("Unread")
	gmail.AddTag("Inbox")
	gmail.SetID()

	// If the recipient mailbox is the same as the (last and only) sender, and there is a recipients line, then the gemmail should be tagged as Sent.
	if len(gmail.Senders) == 1 && mailbox.CertInfo.mailAddress == senderCertInfo.mailAddress && len(gmail.Receivers) > 0 {
		gmail.AddTag("Sent")
		gmail.RemoveTag("Unread")
	}

	// Verify sender's fingerprint.
	// NOTE: This MUST be after the checkBlankGemmail call so that this doesn't go in an infinite loop!
	if !verifyFingerprint(serverContext, conn, senderCertInfo) {
		fmt.Printf("Could not verify fingerprint.\n")
		return
	}

	// Append signature of message's full (misfin(C)) string to the message's body only if it is not a forwarded message
	// And only for use in the Newsfin system
	if len(gmail.Senders) == 1 && mailbox.NewsfinGroup {
		body_hash := gmail.SHA256()
		var signature []byte
		var sign_err error
		switch v := serverContext.serverPrivateKey.(type) {
		case *ecdsa.PrivateKey:
			signature, sign_err = v.Sign(crypto_rand.Reader, body_hash, crypto.SHA256)
		case ed25519.PrivateKey:
			signature, sign_err = v.Sign(crypto_rand.Reader, body_hash, crypto.SHA256)
		case *rsa.PrivateKey:
			signature, sign_err = v.Sign(crypto_rand.Reader, body_hash, crypto.SHA256)
		}
		if sign_err != nil {
			panic(sign_err)
		}
		encodedSignature := base64.StdEncoding.EncodeToString(signature)
		gmail.GemtextLines = append(gmail.GemtextLines, gemmail.GemMailLineText("Injector Signature: "+encodedSignature))
	} else if len(gmail.Senders) > 1 && mailbox.NewsfinGroup {
		//injector, _ := gmail.GetInjector()

		// Parse signature from Newsfin message and verify it.
		signature := ""
		for i := len(gmail.GemtextLines) - 1; i >= 0; i-- {
			line := gmail.GemtextLines[i].String()
			if strings.HasPrefix(line, "Injector Signature:") {
				signature = strings.TrimSpace(strings.TrimPrefix(line, "Injector Signature:"))
				break
			}
		}

		if signature == "" {
			// ERROR
		} else {
			// Verify the signature by checking the second sender with the received public cert. If cert is not in tofu_certs store, contact the mailserver.
		}
	}

	// If sending to Mailinglist, check whether subscription requests are closed if sender is subscribing.
	// If not trying to subscribe, then check if user is allowed to send message to mailinglist.
	// Also check that a user is not trying to forward a message to the mailinglist.
	msgCommand, _ := checkMessageCommand(gmail)
	if mailbox.Mailinglist || mailbox.NewsfinGroup {
		sub, subscribed := mailbox.List.Subscribers[senderCertInfo.mailAddress]
		if msgCommand == MailinglistCommand_Subscribe || msgCommand == MailinglistCommand_Subscribe_NoReply {
			// Check that subs function is not closed (nobody can subscribe)
			if mailbox.List.SubsFunction == MailingListSubs_Closed {
				fmt.Printf("%s (%s) unsuccessful subscribe - %s mailinglist subs function is closed; it doesn't accept subscribe requests.\n", senderCertInfo.mailAddress, senderCertInfo.blurb, mailbox.CertInfo.blurb)
				responseSubsClosed := "61 You cannot subscribe to this mailinglist. Mailinglist's subscriptions are closed.\r\n"
				conn.Write([]byte(responseSubsClosed))
				return
			} else if mailbox.List.SubsFunction == MailingListSubs_AcceptServerMembers {
				// If "accept-server-members", then only allow server members to subscribe
				if !senderFromThisServer(serverContext, senderCertInfo) {
					responseSubsClosed := "61 You cannot subscribe to this mailinglist. Mailinglist's subscriptions are open only to those with mailboxes on this server.\r\n"
					conn.Write([]byte(responseSubsClosed))
					return
				}
			}
			/*else if mailbox.List.SubsFunction == MailingListSubs_Prompt {
				fmt.Printf("%s (%s) unsuccessful subscribe - %s mailinglist subs function is in prompt mode; must manually accept subscriber requests.\n", senderCertInfo.mailAddress, senderCertInfo.blurb, mailbox.CertInfo.blurb)
				responseSubsClosed := "61 Your subscription request has been sent.\r\n"
				conn.Write([]byte(responseSubsClosed))
				return
			}*/
			// Note: Actual subscription gets handled in handleMailinglist() function called later
		} else {
			addMailboxHolder := ""
			if _, mailboxHolderInReceivers := gmail.Receivers[mailbox.Gbox.MailboxAddress]; !gmail.ContainsSender(mailbox.Gbox.MailboxAddress) && !mailboxHolderInReceivers {
				addMailboxHolder = mailbox.Gbox.MailboxAddress
			}
			_, isReply := mailbox.Gbox.GetThreadWithSubjectAndParticipantsHash(string(gmail.Subject), gmail.GetParticipantsHash(addMailboxHolder))

			if mailbox.List.SendThreadPermission == MailingListSend_Subs && msgCommand == MailinglistCommand_Unsubscribe { // Allow message if Subs-only and is a sub. Unsubscribe messages should only work if person is a sub
				if !subscribed {
					fmt.Printf("%s (%s) not subscribed to mailinglist %s yet.\n", senderCertInfo.mailbox, senderCertInfo.mailAddress, mailbox.CertInfo.blurb)
					responseUnauthorized := "61 You are not subscribed to this mailinglist yet.\r\n"
					conn.Write([]byte(responseUnauthorized))
					return
				}
			} else if msgCommand != MailinglistCommand_None {
				// Allow other mailinglist commands to go through
				// Unless it is sub-only or writer-only and user is not a sub, and the command is not the "info" command
				if msgCommand != MailinglistCommand_Info && (mailbox.List.SendThreadPermission == MailingListSend_Subs || mailbox.List.SendThreadPermission == MailingListSend_Writers) && !subscribed {
					fmt.Printf("%s (%s) not subscribed to mailinglist %s yet.\n", senderCertInfo.mailbox, senderCertInfo.mailAddress, mailbox.CertInfo.blurb)
					responseUnauthorized := "61 You are not subscribed to this mailinglist yet.\r\n"
					conn.Write([]byte(responseUnauthorized))
					return
				}
			} else if msgCommand == MailinglistCommand_None && ((!isReply && !subscribed && mailbox.List.SendThreadPermission == MailingListSend_Subs) || (isReply && !subscribed && mailbox.List.SendReplyPermission == MailingListSend_Subs)) {
				// Only allow message in subs-only mode if user is a sub.
				fmt.Printf("%s (%s) not subscribed to mailinglist %s yet.\n", senderCertInfo.mailbox, senderCertInfo.mailAddress, mailbox.CertInfo.blurb)
				responseUnauthorized := "61 You are not subscribed to this mailinglist yet.\r\n"
				conn.Write([]byte(responseUnauthorized))
				return
			} else if (!isReply && mailbox.List.SendThreadPermission == MailingListSend_Writers) || (isReply && mailbox.List.SendReplyPermission == MailingListSend_Writers) {
				// If writers-only, allow message if the message is being forwarded from a federated address and the sender is a writer
				// Also allow message if the sub is a writer, admin, or incoming-forwarder
				if !subscribed || (sub.T != MailboxListType_Mailinglist_Admin && sub.T != MailboxListType_Mailinglist_Writer && sub.T != MailboxListType_Mailinglist_IncomingForwarder) {
					if sub.T == MailboxListType_Mailinglist_Federated && len(gmail.Senders) > 1 {
						// Go down the list and check if any address is a writer:
						hasOneWriter := false
						for i := 1; i < len(gmail.Senders); i++ {
							writer := gmail.Senders[i].Address
							writer_sub, writerIsSub := mailbox.List.Subscribers[writer]
							if writerIsSub && (writer_sub.T == MailboxListType_Mailinglist_Writer || writer_sub.T == MailboxListType_Mailinglist_Admin || writer_sub.T != MailboxListType_Mailinglist_IncomingForwarder) {
								hasOneWriter = true
							}
						}

						if !hasOneWriter {
							fmt.Printf("%s (%s) not allowed to write to mailinglist %s.\n", senderCertInfo.mailbox, senderCertInfo.mailAddress, mailbox.CertInfo.blurb)
							responseUnauthorized := "61 Not allowed to write to this mailinglist. Must have writer or admin in senders list.\r\n"
							conn.Write([]byte(responseUnauthorized))
							return
						}
					} else {
						fmt.Printf("%s (%s) not allowed to write to mailinglist %s.\n", senderCertInfo.mailbox, senderCertInfo.mailAddress, mailbox.CertInfo.blurb)
						responseUnauthorized := "61 You are not allowed to write to this mailinglist. Only writers or admins allowed.\r\n"
						conn.Write([]byte(responseUnauthorized))
						return
					}
				}
			} else if mailbox.List.SendThreadPermission == MailingListSend_Open {
				// Mailinglist is open, allow through
				// How do these people handle replies?
				// TODO: Add the sender to the recipients list if they are not a sub so they can get replies?
				if !subscribed {
					gmail.AddReceiver(senderCertInfo.mailAddress)
				}
			}
		}

		// Check that the user is not trying to forward a message to the mailinglist by ensuring the senders list is empty
		// Except for those allowed to make forwards (federated users, admins, writers, and incoming-forwarders)
		allowForward := (sub.T == MailboxListType_Mailinglist_Federated || sub.T == MailboxListType_Mailinglist_Admin || sub.T == MailboxListType_Mailinglist_Writer || sub.T == MailboxListType_Mailinglist_IncomingForwarder)
		if !allowForward && len(gmail.Senders) > 1 {
			response := "40 Forwarding mails to a mailinglist is not allowed.\r\n"
			conn.Write([]byte(response))
			return
		}
	}

	// Send a success code back, with the recipient mailbox's fingerprint
	responseSuccess := "20 " + mailbox.CertInfo.fingerprint + "\r\n"
	conn.Write([]byte(responseSuccess))

	// Check for duplicate messages, especially for mailinglists that are federated with each other to prevent mailing loops
	// Do this check for every mailbox, to handle cases where a mailbox is subscribed to two mailinglists that are federated with each other.
	// NOTE: This assumes a mailbox doesn't receive two messages from same sender with the same timestamp but that are different/separate mails (with different contents). The chances of this are low, but this may need reconsideration in the future.
	unlock := false
	duplicate := false
	if len(gmail.Timestamps) > 0 && len(gmail.Senders) >= 1 {
		unlock = true
		mailbox.Mutex.Lock()
		fmt.Printf("Checking if mail is duplicate: %s (%s).\n", gmail.Senders[len(gmail.Senders)-1], gmail.Timestamps[len(gmail.Timestamps)-1])
		for _, mail := range mailbox.Gbox.Mails {
			origSender := mail.Senders[len(mail.Senders)-1] // NOTE: Original sender is always the very last sender listed
			if len(mail.Timestamps) > 0 {
				timestamp := mail.Timestamps[len(mail.Timestamps)-1] // NOTE: Original/first timestamp is always the very last timestamp
				if gmail.Timestamps[len(gmail.Timestamps)-1] == timestamp && gmail.Senders[len(gmail.Senders)-1] == origSender {
					// Already received mail, it's a duplicate. Don't forward it anywhere. Mail doesn't get saved to mailbox either.
					fmt.Printf("Duplicate mail received. %s (%s).\n", gmail.Senders[len(gmail.Senders)-1], gmail.Timestamps[len(gmail.Timestamps)-1])
					duplicate = true
					break
				}
			}
		}
	}

	// Make sure unlock and mailbox save happens even when there's a panic
	defer func() {
		if unlock {
			mailbox.Mutex.Unlock()
		}
		serverContext.saveChan <- recipientMailbox
	}()

	// Only forward and save message if it's not a duplicate
	if !duplicate {
		// Handle GMAP Requests
		var save = true
		if mailbox.CertInfo.mailbox == "gmap" {
			handleGMAPRequest(serverContext, mailbox, senderCertInfo, gmail)
			save = false
		}

		// Handle mailinglist feature
		save = handleMailinglist(serverContext, mailbox, senderCertInfo, &gmail)

		// Append gemmail to mailbox's gembox file, as long as it's not a subscribe message, and send a request over the saveChan to save the mailbox
		if save && msgCommand != MailinglistCommand_Subscribe && msgCommand != MailinglistCommand_Subscribe_NoReply && msgCommand != MailinglistCommand_Unsubscribe {
			mailbox.Gbox.AppendGemMail(gmail)
		}
	}
	/*if unlock {
		mailbox.Mutex.Unlock()
	}
	serverContext.saveChan <- recipientMailbox*/
	fmt.Printf("\n")
}

// Returns true if verified that cert has a hostname, a USER_ID field, and a CN
func verifySenderCert(conn tlsconn, senderCertInfo CertInfo) bool {
	if len(senderCertInfo.domains) <= 0 {
		fmt.Printf("cert invalid - no domain/hostname\n")
		responseCertInvalid_NoDomain := "62 Certificate is invalid, no domain is provided.\r\n"
		conn.Write([]byte(responseCertInvalid_NoDomain))
		return false
	}
	/*if senderCertInfo.domains[0] == "localhost" || strings.HasPrefix(senderCertInfo.domains[0], "127.") || strings.HasPrefix(senderCertInfo.domains[0], "192.") { // TODO: Check all domains
		fmt.Printf("cert invalid - hostname is a private/local IP Address (%q)\n", senderCertInfo.domains[0])
		responseCertInvalid_PrivateIP := "62 Certificate is invalid, hostname is a private/local IP Address.\r\n"
		conn.Write([]byte(responseCertInvalid_PrivateIP))
		return false
	}*/
	if senderCertInfo.mailbox == "" {
		fmt.Printf("cert invalid - no USER_ID (%q).\n", senderCertInfo.mailbox)
		responseCertInvalid_NoUSERID := "62 Certificate is invalid, no USER_ID field.\r\n"
		conn.Write([]byte(responseCertInvalid_NoUSERID))
		return false
	}
	if senderCertInfo.blurb == "" {
		fmt.Printf("cert invalid - no CN (%q).\n", senderCertInfo.blurb)
		responseCertInvalid_NOCN := "62 Certificate is invalid, no CommonName field.\r\n"
		conn.Write([]byte(responseCertInvalid_NOCN))
		return false
	}

	// TODO: Check cert expiration and start date

	return true
}

// Checks if the sender is a mailbox on this server
func senderFromThisServer(serverContext *ServerContext, senderCertInfo CertInfo) bool {
	return serverContext.mailboxes.Exists(senderCertInfo.mailbox) && senderCertInfo.domains[0] == serverContext.serverCertInfo.domains[0]
}
func mailboxFromThisServer(serverContext *ServerContext, hostname string, mailboxName string) bool {
	return serverContext.mailboxes.Exists(mailboxName) && hostname == serverContext.serverCertInfo.domains[0]
}

// Returns true if verified
// TODO: Handle slow down response
func verifyFingerprint(serverContext *ServerContext, conn tlsconn, senderCertInfo CertInfo) bool {
	serverMember := senderFromThisServer(serverContext, senderCertInfo)
	if serverMember && serverContext.mailboxes.Get(senderCertInfo.mailbox).CertInfo.fingerprint != senderCertInfo.fingerprint {
		fmt.Printf("cert invalid - cert fingerprint for mailbox on this mailserver has changed (%q). Should be %q, got %q.\n", senderCertInfo.blurb, serverContext.mailboxes.Get(senderCertInfo.mailbox).CertInfo.fingerprint, senderCertInfo.fingerprint)
		responseCertFingerprintIncorrect := "63 Your certificate fingerprint has changed.\r\n"
		conn.Write([]byte(responseCertFingerprintIncorrect))
		return false
	} else if !serverMember {
		fingerprint, ok := serverContext.TOFUMap[senderCertInfo.mailAddress]
		if ok {
			// If in TOFU Map, check that the fingerprints match
			if fingerprint != senderCertInfo.fingerprint {
				// If fingerprints don't match, then try to get the fingerprint from the server again in case if it changed.
				return sendFingerprintVerification(serverContext, conn, senderCertInfo, true)
			}
		} else {
			// If not in TOFU Map, verify them by sending blank gemmail to their address and checking the returned fingerprint
			return sendFingerprintVerification(serverContext, conn, senderCertInfo, false)
		}
	}

	return true
}

func sendFingerprintVerification(serverContext *ServerContext, conn tlsconn, senderCertInfo CertInfo, fingerprintChanged bool) bool {
	// TODO: How do redirects work here? A redirect implies a different fingerprint
	misfinClient := misfin_client.DefaultClient
	server_cert, _ := os.ReadFile(serverContext.config.ServerCertFilename)                                             // TODO: This file should have already been opened and saved in config struct
	resp, err := misfinClient.SendWithCert("misfin://"+senderCertInfo.mailAddress, server_cert, server_cert, "\n\n\n") // NOTE: The message needs to contain 3 empty lines for misfin(C)
	if err != nil || resp == nil || resp.Status != 20 {
		// Invalid request, try again with a Misfin(B) request
		if resp != nil && (resp.Status == 59 || resp.Status == 40 || resp.Status == 60) {
			misfinClient = misfin_client.DefaultClient_MisfinB
			resp, err = misfinClient.SendWithCert("misfin://"+senderCertInfo.mailAddress, server_cert, server_cert, "") // NOTE: No empty lines for misfin(B)
			if err != nil || resp == nil || resp.Status != 20 {
				fmt.Printf("temp failure - cannot verify certificate because server cannot contact their mailserver (%q). This is being let through for now.\n", senderCertInfo.mailAddress)
			} else if resp.Status == 20 && strings.TrimSpace(resp.Meta) != senderCertInfo.fingerprint {
				fmt.Printf("cert invalid - sender's mailserver sent back different fingerprint (%q). Expected %q, got %q.\n", senderCertInfo.blurb, senderCertInfo.fingerprint, resp.Meta)
				responseCertInvalid_DifferentFingerprint := "63 Certificate is invalid. Your mailserver sent back different fingerprint for your mailbox.\r\n"
				conn.Write([]byte(responseCertInvalid_DifferentFingerprint))
				return false
			} else {
				serverContext.TOFUMap[senderCertInfo.mailAddress] = senderCertInfo.fingerprint
				serverContext.saveChan <- "tofu"
			}
		} else {
			if fingerprintChanged {
				fmt.Printf("temp failure - cannot verify changed certificate because server cannot contact their mailserver (%q).\n", senderCertInfo.mailAddress)
				conn.Write([]byte("62 Certificate not verifiable. Cannot contact mailserver to get changed fingerprint.\r\n"))
			} else {
				fmt.Printf("temp failure - cannot verify certificate because server cannot contact their mailserver (%q).\n", senderCertInfo.mailAddress)
				conn.Write([]byte("62 Certificate is invalid. Cannot contact mailserver to get fingerprint.\r\n"))
			}
			return false
		}
	} else if resp.Status == 20 && strings.TrimSpace(resp.Meta) != senderCertInfo.fingerprint {
		fmt.Printf("cert invalid - sender's mailserver sent back different fingerprint (%q). Expected %q, got %q.\n", senderCertInfo.blurb, senderCertInfo.fingerprint, resp.Meta)
		responseCertInvalid_DifferentFingerprint := "63 Certificate is invalid. Your mailserver sent back different fingerprint for your mailbox.\r\n"
		conn.Write([]byte(responseCertInvalid_DifferentFingerprint))
		return false
	} else {
		serverContext.TOFUMap[senderCertInfo.mailAddress] = senderCertInfo.fingerprint
		serverContext.saveChan <- "tofu"
	}

	return true
}

// The only difference between this and verifyFingerprint is this sends back error response codes that work for gemini (that is, 63 codes in misfin are changed to 62 codes for gemini)
func verifyFingerprint_gemini(serverContext *ServerContext, conn tlsconn, senderCertInfo CertInfo) bool {
	serverMember := senderFromThisServer(serverContext, senderCertInfo)
	if serverMember && serverContext.mailboxes.Get(senderCertInfo.mailbox).CertInfo.fingerprint != senderCertInfo.fingerprint {
		fmt.Printf("cert invalid - cert fingerprint for mailbox on this mailserver has changed (%q). Should be %q, got %q.\n", senderCertInfo.blurb, serverContext.mailboxes.Get(senderCertInfo.mailbox).CertInfo.fingerprint, senderCertInfo.fingerprint)
		responseCertFingerprintIncorrect := "62 Your certificate fingerprint has changed.\r\n"
		conn.Write([]byte(responseCertFingerprintIncorrect))
		return false
	} else if !serverMember {
		fingerprint, ok := serverContext.TOFUMap[senderCertInfo.mailAddress]
		if ok {
			// If in TOFU Map, check that the fingerprints match
			if fingerprint != senderCertInfo.fingerprint {
				fmt.Printf("temp failure - known certificate fingerprint changed (%q). Should be %q, got %q.\n", senderCertInfo.blurb, fingerprint, senderCertInfo.fingerprint)
				responseCertFingerprintIncorrect := "62 Your certificate fingerprint has changed.\r\n"
				conn.Write([]byte(responseCertFingerprintIncorrect))
				return false
			}
		} else {
			// If not in TOFU Map, verify them by sending blank gemmail to their address and checking the returned fingerprint
			// TODO: How do redirects work here? A redirect implies a different fingerprint
			misfinClient := misfin_client.DefaultClient
			server_cert, _ := os.ReadFile(serverContext.config.ServerCertFilename)                                             // TODO: This file should have already been opened and saved in config struct
			resp, err := misfinClient.SendWithCert("misfin://"+senderCertInfo.mailAddress, server_cert, server_cert, "\n\n\n") // NOTE: The message needs to contain 3 empty lines
			if err != nil || resp == nil || resp.Status != 20 {
				// Invalid request, try again with a Misfin(B) request
				if resp != nil && (resp.Status == 59 || resp.Status == 40 || resp.Status == 60) {
					misfinClient = misfin_client.DefaultClient_MisfinB
					resp, err = misfinClient.SendWithCert("misfin://"+senderCertInfo.mailAddress, server_cert, server_cert, "")
					if err != nil || resp == nil || resp.Status != 20 {
						fmt.Printf("temp failure - cannot verify certificate because server cannot contact their mailserver (%q). This is being let through for now.\n", senderCertInfo.mailAddress)
					} else if resp.Status == 20 && strings.TrimSpace(resp.Meta) != senderCertInfo.fingerprint {
						fmt.Printf("cert invalid - sender's mailserver sent back different fingerprint (%q). Expected %q, got %q.\n", senderCertInfo.blurb, senderCertInfo.fingerprint, resp.Meta)
						responseCertInvalid_DifferentFingerprint := "62 Certificate is invalid. Your mailserver sent back different fingerprint for your mailbox.\r\n"
						conn.Write([]byte(responseCertInvalid_DifferentFingerprint))
						return false
					} else {
						serverContext.TOFUMap[senderCertInfo.mailAddress] = senderCertInfo.fingerprint
						serverContext.saveChan <- "tofu"
					}
				} else {
					fmt.Printf("temp failure - cannot verify certificate because server cannot contact their mailserver (%q). This is being let through for now.\n", senderCertInfo.mailAddress)
				}
				/*responseCannotVerifyCertWithMailserver := "40 Cannot verify certificate because server cannot contact your mailserver.\r\n"
				conn.Write([]byte(responseCannotVerifyCertWithMailserver))
				return*/
				// TODO: Let through for now.
			} else if resp.Status == 20 && strings.TrimSpace(resp.Meta) != senderCertInfo.fingerprint {
				fmt.Printf("cert invalid - sender's mailserver sent back different fingerprint (%q). Expected %q, got %q.\n", senderCertInfo.blurb, senderCertInfo.fingerprint, resp.Meta)
				responseCertInvalid_DifferentFingerprint := "62 Certificate is invalid. Your mailserver sent back different fingerprint for your mailbox.\r\n"
				conn.Write([]byte(responseCertInvalid_DifferentFingerprint))
				return false
			} else {
				serverContext.TOFUMap[senderCertInfo.mailAddress] = senderCertInfo.fingerprint
				serverContext.saveChan <- "tofu"
			}
		}
	}

	return true
}

// Returns true if blank
// TODO: Check subject line too?!
func checkBlankGemmail(gmail gemmail.GemMail) bool {
	var builder strings.Builder
	for _, l := range gmail.GemtextLines {
		builder.WriteString(l.String())
	}
	return strings.TrimSpace(string(gmail.Subject)) == "" && (len(gmail.GemtextLines) == 0 || strings.TrimSpace(builder.String()) == "")
}

type MailinglistCommand int

const MailinglistCommand_None MailinglistCommand = 0
const MailinglistCommand_Subscribe MailinglistCommand = 1
const MailinglistCommand_Unsubscribe MailinglistCommand = 2
const MailinglistCommand_Info MailinglistCommand = 3
const MailinglistCommand_Subscribe_NoReply MailinglistCommand = 4
const MailinglistCommand_Exclude_Tag MailinglistCommand = 5
const MailinglistCommand_INCLUDE_Tag MailinglistCommand = 6
const MailinglistCommand_HELP MailinglistCommand = 7
const MailinglistCommand_REPLIES_ALL MailinglistCommand = 8
const MailinglistCommand_REPLIES_NONE MailinglistCommand = 9
const MailinglistCommand_REPLIES_OWN MailinglistCommand = 10

func checkMessageCommand(gmail gemmail.GemMail) (MailinglistCommand, string) {
	cmdString := strings.TrimPrefix(strings.TrimSpace(strings.ToUpper(string(gmail.Subject))), "# ")
	text := gemmail.GemMailLineText("")
	textLine := false
	if len(gmail.GemtextLines) > 0 {
		text, textLine = gmail.GemtextLines[0].(gemmail.GemMailLineText)
	}

	if strings.HasPrefix(cmdString, "UNSUBSCRIBE") {
		return MailinglistCommand_Unsubscribe, string(gmail.Subject)[len("UNSUBSCRIBE"):]
	} else if strings.HasPrefix(cmdString, "SUBSCRIBE NO-REPLY") {
		return MailinglistCommand_Subscribe_NoReply, string(gmail.Subject)[len("SUBSCRIBE NO-REPLY"):]
	} else if strings.HasPrefix(cmdString, "SUBSCRIBE") {
		return MailinglistCommand_Subscribe, string(gmail.Subject)[len("SUBSCRIBE"):]
	} else if textLine && strings.TrimSpace(strings.ToLower(string(text))) == "subscribe" {
		return MailinglistCommand_Subscribe, "" // TODO
	} else if strings.HasPrefix(cmdString, "INFO") {
		return MailinglistCommand_Info, string(gmail.Subject)[len("INFO"):]
	} else if strings.HasPrefix(cmdString, "EXCLUDE LABEL") {
		return MailinglistCommand_Exclude_Tag, string(gmail.Subject)[len("EXCLUDE LABEL"):]
	} else if strings.HasPrefix(cmdString, "EXCLUDE TAG") {
		return MailinglistCommand_Exclude_Tag, string(gmail.Subject)[len("EXCLUDE TAG"):]
	} else if strings.HasPrefix(cmdString, "INCLUDE LABEL") {
		return MailinglistCommand_INCLUDE_Tag, string(gmail.Subject)[len("INCLUDE LABEL"):]
	} else if strings.HasPrefix(cmdString, "INCLUDE TAG") {
		return MailinglistCommand_INCLUDE_Tag, string(gmail.Subject)[len("INCLUDE TAG"):]
	} else if cmdString == "HELP" {
		return MailinglistCommand_HELP, ""
	} else if strings.HasPrefix(cmdString, "REPLIES ALL") {
		return MailinglistCommand_REPLIES_ALL, string(gmail.Subject)[len("REPLIES ALL"):]
	} else if strings.HasPrefix(cmdString, "REPLIES OWN") {
		return MailinglistCommand_REPLIES_OWN, string(gmail.Subject)[len("REPLIES OWN"):]
	} else if strings.HasPrefix(cmdString, "REPLIES NONE") {
		return MailinglistCommand_REPLIES_NONE, string(gmail.Subject)[len("REPLIES NONE"):]
	}

	return MailinglistCommand_None, ""
}

// Returns bool whether to save the mail to the mailbox
func handleMailinglist(serverContext *ServerContext, mailbox *MailBox, senderCertInfo CertInfo, gmail *gemmail.GemMail) bool {
	if !mailbox.Mailinglist && !mailbox.NewsfinGroup {
		return true
	}

	// Get mailinglist's certificate file data
	ml_cert, _ := os.ReadFile(mailbox.List.CertificateFilepath)

	gmailCommand, cmdArgs := checkMessageCommand(*gmail)

	// Handle Subscribe Message
	if gmailCommand == MailinglistCommand_Subscribe || gmailCommand == MailinglistCommand_Subscribe_NoReply {
		// TODO: Handle subscription prompt mode - send message that subscription request has been received, but not accepted yet, and don't add user to subscriptions.
		// TODO: Have a way for admins to receive and accept subscription requests.

		subType := MailboxListType_Mailinglist_User

		// Finish step 2 of Federation process if step 1 was already completed
		if _, doFederation := mailbox.List.ToFederate.Get(senderCertInfo.mailAddress); doFederation {
			subType = MailboxListType_Mailinglist_Federated
			mailbox.List.ToFederate.Remove(senderCertInfo.mailAddress)
		}

		fmt.Printf("Adding %s to subscribers list for mailbox %s.\n", senderCertInfo.mailAddress, mailbox.CertInfo.mailAddress)
		mailbox.List.Subscribe(MailboxListItem{subType, senderCertInfo.mailbox, senderCertInfo.blurb, senderCertInfo.fingerprint, senderCertInfo.domains[0], MailingListReceiveReplies_All, make(map[string]struct{}), "", "", "", "", "", false, time.Now().UTC()})

		// Printing Subscribers
		fmt.Printf("Subscribers List: ")
		for address, s := range mailbox.List.Subscribers {
			fmt.Printf("%s (%s), ", address, s.Blurb)
		}
		fmt.Printf("\n")

		// Send response back (if not a NoReply message)
		if gmailCommand == MailinglistCommand_Subscribe {
			mailinglistName := mailbox.CertInfo.blurb
			mailinglistName_Upper := strings.ToUpper(mailbox.CertInfo.blurb)
			if !strings.HasSuffix(mailinglistName_Upper, "MAILINGLIST") && !strings.HasSuffix(mailinglistName_Upper, "MAILING LIST") && !strings.HasSuffix(mailinglistName_Upper, "MAIL LIST") && !strings.HasSuffix(mailinglistName_Upper, "MAILLIST") && !strings.HasSuffix(mailinglistName_Upper, "NEWSLETTER") && !strings.HasSuffix(mailinglistName_Upper, "NEWS LIST") && !strings.HasSuffix(mailinglistName_Upper, "NEWS LETTER") && !strings.HasSuffix(mailinglistName_Upper, "NEWSLIST") {
				if mailbox.NewsfinGroup {
					mailinglistName = strings.TrimSpace(mailinglistName) + " Group"
				} else if mailbox.List.SendThreadPermission == MailingListSend_Writers {
					mailinglistName = strings.TrimSpace(mailinglistName) + " Newsletter"
				} else {
					mailinglistName = strings.TrimSpace(mailinglistName) + " Mailinglist"
				}
			}
			var subscribedMsgBuilder strings.Builder

			// Title
			subscribedMsgBuilder.WriteString("# Welcome to the ")
			subscribedMsgBuilder.WriteString(mailinglistName)
			subscribedMsgBuilder.WriteString("\n")

			// Subscribed message
			if mailbox.List.SendThreadPermission == MailingListSend_Subs || mailbox.List.SendThreadPermission == MailingListSend_Open {
				if mailbox.NewsfinGroup {
					subscribedMsgBuilder.WriteString("You have successfully subscribed to this group. Simply send an email to this group's address and it will be forwarded to all subscribers.\n")
				} else {
					subscribedMsgBuilder.WriteString("You have successfully subscribed to this mailinglist. Simply send an email to this mailinglist address and it will be forwarded to all subscribers.\n")
				}
			} else if mailbox.List.SendReplyPermission == MailingListSend_Subs || mailbox.List.SendReplyPermission == MailingListSend_Open {
				if mailbox.NewsfinGroup {
					subscribedMsgBuilder.WriteString("You have successfully subscribed to this group. Simply send a reply gemmail to this group address and it will be forwarded to all subscribers.\n")
				} else {
					subscribedMsgBuilder.WriteString("You have successfully subscribed to this mailinglist/newsletter. Simply send a reply gemmail to this mailinglist/newsletter address and it will be forwarded to all subscribers.\n")
				}
			} else if mailbox.List.SendThreadPermission == MailingListSend_Writers || mailbox.List.SendReplyPermission == MailingListSend_Writers {
				if mailbox.NewsfinGroup {
					subscribedMsgBuilder.WriteString("You have successfully subscribed to this group. Only writers can send to this group, but you will receive all gemmails from mailinglist writers.\n")
				} else {
					subscribedMsgBuilder.WriteString("You have successfully subscribed to this mailinglist/newsletter. Only writers can send to this mailinglist, but you will receive all gemmails from mailinglist writers.\n")
				}
			}

			// Description
			if mailbox.List.Description != "" {
				subscribedMsgBuilder.WriteString("\nDescription: ")
				subscribedMsgBuilder.WriteString(mailbox.List.Description)
				subscribedMsgBuilder.WriteString("\n")
			}

			// How to send messages with subjects
			subscribedMsgBuilder.WriteString("\nSend gemmails with subjects by placing a gemtext title (lines prepended with '# ') to the beginning of the message. You can add a label to the gemmail by prepending it to the subject in between square brackets, '[like_so]'.\n")

			// How to unsubscribe
			subscribedMsgBuilder.WriteString("You can always unsubscribe by sending a gemmail with the message subject \"Unsubscribe\" to this address.\n")

			// How to view archives
			if serverContext.config.GeminiServerPermission == GeminiPermission_Public && mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_Open {
				fmt.Fprintf(&subscribedMsgBuilder, "\nYou can view this mailinglist's open archives via Gemini using the link below:\n=> gemini://%s:%s/mailinglist/%s Open Mailinglist Archives via Gemini\n", serverContext.serverCertInfo.domains[0], serverContext.config.Port, mailbox.CertInfo.mailbox)
			} else if serverContext.config.GeminiServerPermission == GeminiPermission_Public && mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_Subs {
				fmt.Fprintf(&subscribedMsgBuilder, "\nAs a sub, you can view this mailinglist's archives via Gemini using the link below:\n=> gemini://%s:%s/mailinglist/%s Mailinglist Archives via Gemini\n", serverContext.serverCertInfo.domains[0], serverContext.config.Port, mailbox.CertInfo.mailbox)
			} else if serverContext.config.GeminiServerPermission == GeminiPermission_MemberOnly && (mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_Subs || mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_Open) {
				// If gemini server only accessible by members, then check that sub is member of this server
				if senderFromThisServer(serverContext, senderCertInfo) {
					fmt.Fprintf(&subscribedMsgBuilder, "\nAs a member of this mailserver, you can view this mailinglist's archives via Gemini using the link below:\n=> gemini://%s:%s/mailinglist/%s Mailinglist Archives via Gemini\n", serverContext.serverCertInfo.domains[0], serverContext.config.Port, mailbox.CertInfo.mailbox)
				}
			}

			// Commands
			subscribedMsgBuilder.WriteString("\n## Commands\nSend a mail to the mailinglist with the command in the subject:\n")
			subscribedMsgBuilder.WriteString(`* info - get info on the mailinglist
* help - get this help page
* exclude label <label_name> - stop receiving messages with the label <label_name>
* include label <label_name> - start receiving messages with the label <label_name> again
* replies all - receive all replies.
* replies own - receive replies only to your own threads and threads you've sent mails to.
* replies none - do not receive any replies.
* unsubscribe - unsubscribe from the mailinglist`)
			subscribedMsgBuilder.WriteString("\n")

			subscribedMsg := gemmail.CreateGemMailFromBody(subscribedMsgBuilder.String())
			sendMessageToSender(serverContext, mailbox, senderCertInfo, ml_cert, subscribedMsg)
		}
		return false
	} else if gmailCommand == MailinglistCommand_Unsubscribe {
		// Handle unsubscribing
		fmt.Printf("Removing %s from subscribers list for mailbox %s.\n", senderCertInfo.mailAddress, mailbox.CertInfo.mailAddress)
		//mailbox.List.Subscribe(MailboxListItem{MailboxListType_Mailinglist_User, senderCertInfo.mailbox, senderCertInfo.blurb, senderCertInfo.fingerprint, senderCertInfo.domains[0], "", "", "", time.Now().UTC()})
		mailbox.List.Unsubscribe(senderCertInfo.mailAddress)

		mailinglistName := mailbox.CertInfo.blurb
		mailinglistName_Upper := strings.ToUpper(mailbox.CertInfo.blurb)
		if !strings.HasSuffix(mailinglistName_Upper, "MAILINGLIST") && !strings.HasSuffix(mailinglistName_Upper, "MAILING LIST") && !strings.HasSuffix(mailinglistName_Upper, "MAIL LIST") && !strings.HasSuffix(mailinglistName_Upper, "MAILLIST") && !strings.HasSuffix(mailinglistName_Upper, "NEWSLETTER") && !strings.HasSuffix(mailinglistName_Upper, "NEWS LIST") && !strings.HasSuffix(mailinglistName_Upper, "NEWS LETTER") && !strings.HasSuffix(mailinglistName_Upper, "NEWSLIST") {
			if mailbox.NewsfinGroup {
				mailinglistName = strings.TrimSpace(mailinglistName) + " Group"
			} else if mailbox.List.SendThreadPermission == MailingListSend_Writers {
				mailinglistName = strings.TrimSpace(mailinglistName) + " Newsletter"
			} else {
				mailinglistName = strings.TrimSpace(mailinglistName) + " Mailinglist"
			}
		}
		unsubscribeMsgString := "# Unsubscribed from " + mailinglistName + "\nYou have been unsubscribed from this mailinglist/newsletter."
		unsubscribeMsg := gemmail.CreateGemMailFromBody(unsubscribeMsgString)
		sendMessageToSender(serverContext, mailbox, senderCertInfo, ml_cert, unsubscribeMsg)
		return false
	} else if gmailCommand == MailinglistCommand_Info { // You don't need to be a sub to use this command
		var infoMsgBuilder strings.Builder
		infoMsgBuilder.WriteString("# Info on ")
		infoMsgBuilder.WriteString(mailbox.CertInfo.blurb)
		infoMsgBuilder.WriteString("\n\n")

		if mailbox.List.Description != "" {
			infoMsgBuilder.WriteString("Description: ")
			infoMsgBuilder.WriteString(mailbox.List.Description)
			infoMsgBuilder.WriteString("\n\n")
		}

		if mailbox.List.SendThreadPermission == MailingListSend_Open {
			infoMsgBuilder.WriteString("Who Can Send Mails on Mailinglist: Anyone (Subscription not needed)\n")
		} else if mailbox.List.SendThreadPermission == MailingListSend_Subs {
			infoMsgBuilder.WriteString("Who Can Send Mails on Mailinglist: Subs only\n")
		} else if mailbox.List.SendThreadPermission == MailingListSend_Writers {
			infoMsgBuilder.WriteString("Who Can Send Mails on Mailinglist: Designated Writers and Admins only\n")
		}
		if mailbox.List.SendThreadPermission == MailingListSend_Open {
			infoMsgBuilder.WriteString("Who Can Send Replies on Mailinglist: Anyone (Subscription not needed)\n")
		} else if mailbox.List.SendThreadPermission == MailingListSend_Subs {
			infoMsgBuilder.WriteString("Who Can Send Replies on Mailinglist: Subs only\n")
		} else if mailbox.List.SendThreadPermission == MailingListSend_Writers {
			infoMsgBuilder.WriteString("Who Can Send Replies on Mailinglist: Designated Writers and Admins only\n")
		}
		if mailbox.List.SubsFunction == MailingListSubs_AcceptAll {
			infoMsgBuilder.WriteString("Who can Subscribe: Anybody\n")
		} else if mailbox.List.SubsFunction == MailingListSubs_Closed {
			infoMsgBuilder.WriteString("Who can Subscribe: Nobody\n")
		} else if mailbox.List.SubsFunction == MailingListSubs_Prompt {
			infoMsgBuilder.WriteString("Who can Subscribe: Anybody, requires confirmation by Admin\n")
		}

		var tempBuilder strings.Builder
		printFederation := false
		tempBuilder.WriteString("Federated with the following: ")
		i := 0
		for addr, sub := range mailbox.List.Subscribers {
			if sub.T == MailboxListType_Mailinglist_Federated {
				if i > 0 {
					tempBuilder.WriteString(", ")
				}
				tempBuilder.WriteString(addr)
				i += 1
				printFederation = true
			}
		}
		if printFederation {
			fmt.Fprintf(&infoMsgBuilder, "%s\n\n", tempBuilder.String())
		}

		fmt.Fprintf(&infoMsgBuilder, "Unsubscribe to this mailinglist/newsletter by sending a gemmail with the message subject \"Unsubscribe\" to %s via misfin. You can also send a gemmail with the subject \"info\" to get more info on the mailinglist, or \"help\" to see a list of mailinglist commands and user settings to configure.\n\n", mailbox.CertInfo.mailAddress)

		infoMsg := gemmail.CreateGemMailFromBody(infoMsgBuilder.String())
		sendMessageToSender(serverContext, mailbox, senderCertInfo, ml_cert, infoMsg)
		return false
	} else if gmailCommand == MailinglistCommand_Exclude_Tag {
		// Add label exclusion to subscriber data
		_, isSubbed := mailbox.List.Subscribers[senderCertInfo.mailAddress]
		if !isSubbed {
			return false
		}

		labelToExclude, _, _ := strings.Cut(strings.TrimSpace(cmdArgs), " ")
		mailbox.List.Subscribers[senderCertInfo.mailAddress].ExcludeLabels[labelToExclude] = struct{}{}
		return false
	} else if gmailCommand == MailinglistCommand_INCLUDE_Tag {
		// Add label exclusion to subscriber data
		_, isSubbed := mailbox.List.Subscribers[senderCertInfo.mailAddress]
		if !isSubbed {
			return false
		}

		labelToInclude, _, _ := strings.Cut(strings.TrimSpace(cmdArgs), " ")
		delete(mailbox.List.Subscribers[senderCertInfo.mailAddress].ExcludeLabels, labelToInclude)
		return false
	} else if gmailCommand == MailinglistCommand_REPLIES_ALL {
		// Check if subbed
		sub_copy, isSubbed := mailbox.List.Subscribers[senderCertInfo.mailAddress]
		if !isSubbed {
			return false
		}

		sub_copy.ReceiveReplies = MailingListReceiveReplies_All
		mailbox.List.Subscribers[senderCertInfo.mailAddress] = sub_copy
		return false
	} else if gmailCommand == MailinglistCommand_REPLIES_OWN {
		// Check if subbed
		sub_copy, isSubbed := mailbox.List.Subscribers[senderCertInfo.mailAddress]
		if !isSubbed {
			return false
		}

		sub_copy.ReceiveReplies = MailingListReceiveReplies_OwnPosts
		mailbox.List.Subscribers[senderCertInfo.mailAddress] = sub_copy
		return false
	} else if gmailCommand == MailinglistCommand_REPLIES_NONE {
		// Check if subbed
		sub_copy, isSubbed := mailbox.List.Subscribers[senderCertInfo.mailAddress]
		if !isSubbed {
			return false
		}

		sub_copy.ReceiveReplies = MailingListReceiveReplies_None
		mailbox.List.Subscribers[senderCertInfo.mailAddress] = sub_copy
		return false
	} else if gmailCommand == MailinglistCommand_HELP {
		var infoMsgBuilder strings.Builder
		infoMsgBuilder.WriteString("# Help on ")
		infoMsgBuilder.WriteString(mailbox.CertInfo.blurb)
		infoMsgBuilder.WriteString("\n\n")

		infoMsgBuilder.WriteString("## Commands\n")
		infoMsgBuilder.WriteString("Send a mail to the mailinglist with the command in the subject:\n")
		infoMsgBuilder.WriteString("* info - get info on the mailinglist\n")
		infoMsgBuilder.WriteString("* help - get this help page\n")
		infoMsgBuilder.WriteString("* exclude label <label_name> - stop receiving messages with the label <label_name>\n")
		infoMsgBuilder.WriteString("* include label <label_name> - start receiving messages with the label <label_name> again\n")
		infoMsgBuilder.WriteString("* replies all - receive all replies.\n")
		infoMsgBuilder.WriteString("* replies own - receive replies only to your own threads and threads you've sent mails to.\n")
		infoMsgBuilder.WriteString("* replies none - do not receive any replies.\n")
		infoMsgBuilder.WriteString("* unsubscribe - unsubscribe from the mailinglist\n")

		infoMsg := gemmail.CreateGemMailFromBody(infoMsgBuilder.String())
		sendMessageToSender(serverContext, mailbox, senderCertInfo, ml_cert, infoMsg)
		return false
	} else {
		// Handle message's label
		label := ""
		trimmedSubject := strings.TrimLeft(string(gmail.Subject), "\t ")
		if strings.HasPrefix(trimmedSubject, "[") {
			trimmedSubject = strings.TrimPrefix(trimmedSubject, "[")
			indx := strings.IndexRune(trimmedSubject, ']')
			if indx != -1 { // There's a label
				label = trimmedSubject[0:indx]
				gmail.AddTag("_" + label)
			}
		}

		// Forward email to (subbed) recipients
		for address, sub := range mailbox.List.Subscribers {
			// Skip the person who originally sent the message
			if sub.Fingerprint == senderCertInfo.fingerprint {
				continue
			}
			// Don't send to incoming forwarders. They don't receive messages, they only forward to the mailinglist.
			if sub.T == MailboxListType_Mailinglist_IncomingForwarder || sub.IncomingWriter {
				continue
			}
			// If subscriber is part of the senders list, skip them (because they've already received the mail)
			if gmail.ContainsSender(address) {
				continue
			}

			// Check that sub is not excluding the label of this mail
			if _, excluded := sub.ExcludeLabels[label]; excluded {
				continue
			}

			go sendMessageToSub(serverContext, mailbox, address, sub, ml_cert, *gmail)
		}
	}

	return true
}

func handleGMAPRequest(serverContext *ServerContext, gmapMailbox *MailBox, senderCertInfo CertInfo, gmail gemmail.GemMail) {
	// Get gmap's certificate file data
	//ml_cert, _ := os.ReadFile(gmapMailbox.Cert)

}

// Handles sending messages back to sender via the passed-in mailbox. If the sender is a mailbox on this server, then use a different process
func sendMessageToSender(serverContext *ServerContext, fromMailbox *MailBox, senderCertInfo CertInfo, certFile []byte, gmail gemmail.GemMail) {
	if senderFromThisServer(serverContext, senderCertInfo) {
		// TODO: When implemented, handle forwarded mailboxes or mailboxes with active redirects

		clonedGmail := gmail.Clone(false)
		clonedGmail.PrependSender(fromMailbox.CertInfo.mailAddress, fromMailbox.CertInfo.blurb)
		clonedGmail.PrependTimestamp(time.Now().UTC())
		clonedGmail.AddTag("Unread")
		clonedGmail.AddTag("Inbox")
		clonedGmail.SetID()

		serverContext.mailboxes.Get(senderCertInfo.mailbox).Gbox.AppendGemMail(clonedGmail)
		serverContext.saveChan <- senderCertInfo.mailbox
	} else {
		if len(certFile) == 0 {
			panic("Mailinglist certificate not set (is empty).")
		}

		address := senderCertInfo.mailAddress
		redirectAttempt := 0 // Keep track of how many times redirected. Don't redirect after 5th attempt
		misfinB := false
		misfinClient := misfin_client.DefaultClient
		for {
			msg := ""
			if misfinB {
				msg = gmail.String_MisfinB()
			} else {
				msg = gmail.String()
			}
			resp, err := misfinClient.SendWithCert("misfin://"+address, certFile, certFile, msg)
			if err != nil || resp == nil {
				fmt.Printf("Failed to send subscribed gemmail back to %s. Failed with err %v\n", address, err)
			} else if redirectAttempt < 6 && (resp.Status == misfin_client.StatusRedirect || resp.Status == misfin_client.StatusRedirectPermanent) {
				// Handle redirects (stopping at 5th redirect attempt)
				address = strings.TrimPrefix(strings.TrimPrefix(strings.TrimSpace(resp.Meta), "misfin://"), "misfin:")
				redirectAttempt += 1
				time.Sleep(226 * time.Millisecond)
				// TODO: If permanent redirect, change sub's new address and save it?
				continue
			} else if resp.Status == 44 { // Slow Down request: sleep, then try again
				num, _ := strconv.ParseInt(resp.Meta, 10, 64)
				if num == 0 {
					num = 1
				}
				time.Sleep((time.Duration(num) * time.Second) + time.Millisecond)
				redirectAttempt += 1
				continue
			} else if resp.Status != 20 {
				// Invalid requst, try with Misfin(B) client
				if (resp.Status == 59 || resp.Status == 40 || resp.Status == 60) && !misfinB {
					misfinClient = misfin_client.DefaultClient_MisfinB
					misfinB = true
					redirectAttempt = 0
					time.Sleep(226 * time.Millisecond)
					continue
				} else {
					fmt.Printf("Failed to send subscribed gemmail back to %s. Failed with misfin error %v: %s\n", address, resp.Status, resp.Meta)
				}
			}

			break
		}
	}
}

func sendMessageToSub(serverContext *ServerContext, fromMailbox *MailBox, sub_address string, sub MailboxListItem, certFile []byte, gmail gemmail.GemMail) {
	addMailboxHolder := ""
	if _, mailboxHolderInReceivers := gmail.Receivers[fromMailbox.Gbox.MailboxAddress]; !gmail.ContainsSender(fromMailbox.Gbox.MailboxAddress) && !mailboxHolderInReceivers {
		addMailboxHolder = fromMailbox.Gbox.MailboxAddress
	}

	// Check if the recipient/subscriber is a mailbox on this server
	if mailboxFromThisServer(serverContext, sub.Hostname, sub.Name) {
		// TODO: When implemented, handle forwarded mailboxes or mailboxes with active redirects

		clonedGmail := gmail.Clone(false)
		clonedGmail.PrependSender(fromMailbox.CertInfo.mailAddress, fromMailbox.CertInfo.blurb)
		clonedGmail.PrependTimestamp(time.Now().UTC())
		clonedGmail.AddTag("Unread")
		clonedGmail.AddTag("Inbox")
		clonedGmail.SetID()

		// Check for duplicate message in mailbox
		subMailbox := serverContext.mailboxes.Get(sub.Name)
		duplicate := false
		if len(clonedGmail.Timestamps) > 0 && len(clonedGmail.Senders) >= 1 {
			//fmt.Printf("Checking if mail is duplicate: %s (%s).\n", clonedGmail.Senders[len(clonedGmail.Senders)-1], clonedGmail.Timestamps[len(clonedGmail.Timestamps)-1])
			for _, mail := range subMailbox.Gbox.Mails {
				origSender := mail.Senders[len(mail.Senders)-1] // NOTE: Original sender is always the very last sender listed
				if len(mail.Timestamps) > 0 {
					timestamp := mail.Timestamps[len(mail.Timestamps)-1] // NOTE: Original/first timestamp is always the very last timestamp
					if clonedGmail.Timestamps[len(clonedGmail.Timestamps)-1] == timestamp && clonedGmail.Senders[len(clonedGmail.Senders)-1] == origSender {
						// Already received mail, it's a duplicate. Don't save it to mailbox.
						//fmt.Printf("Duplicate mail received. %s (%s).\n", clonedGmail.Senders[len(clonedGmail.Senders)-1], clonedGmail.Timestamps[len(clonedGmail.Timestamps)-1])
						duplicate = true
						break
					}
				}
			}
		}

		// Save if not duplicate
		if !duplicate {
			if fromMailbox.List.Subscribers[subMailbox.CertInfo.mailAddress].ReceiveReplies == MailingListReceiveReplies_None {
				// If replies set to none and message is a reply to a thread, then skip
				if _, isExistingThread := fromMailbox.Gbox.GetThreadWithSubjectAndParticipantsHash(string(gmail.Subject), gmail.GetParticipantsHash(addMailboxHolder)); isExistingThread {
					return
				}
			} else if fromMailbox.List.Subscribers[sub_address].ReceiveReplies == MailingListReceiveReplies_OwnPosts {
				// Only receive replies to threads you are involved in
				if thread, isExistingThread := fromMailbox.Gbox.GetThreadWithSubjectAndParticipantsHash(string(gmail.Subject), gmail.GetParticipantsHash(addMailboxHolder)); isExistingThread {
					// Check if sub is involved in thread
					if !thread.ContainsSender(sub_address) {
						return
					}
				}
			}

			serverContext.mailboxes.Get(sub.Name).Gbox.AppendGemMail(clonedGmail)
			serverContext.saveChan <- sub.Name
		}
	} else {
		if fromMailbox.List.Subscribers[sub_address].ReceiveReplies == MailingListReceiveReplies_None {
			// If replies set to none and message is a reply to a thread, then skip
			if _, isExistingThread := fromMailbox.Gbox.GetThreadWithSubjectAndParticipantsHash(string(gmail.Subject), gmail.GetParticipantsHash(addMailboxHolder)); isExistingThread {
				return
			}
		} else if fromMailbox.List.Subscribers[sub_address].ReceiveReplies == MailingListReceiveReplies_OwnPosts {
			// Only receive replies to threads you are involved in
			if thread, isExistingThread := fromMailbox.Gbox.GetThreadWithSubjectAndParticipantsHash(string(gmail.Subject), gmail.GetParticipantsHash(addMailboxHolder)); isExistingThread {
				// Check if sub is involved in thread
				if !thread.ContainsSender(sub_address) {
					return
				}
			}
		}

		redirectAttempt := 0 // Keep track of how many times redirected. Don't redirect after 5th attempt
		misfinB := false
		misfinClient := misfin_client.DefaultClient
		for {
			msg := ""
			if misfinB {
				msg = gmail.String_MisfinB()
			} else {
				msg = gmail.String()
			}
			resp, err := misfinClient.SendWithCert("misfin://"+sub_address, certFile, certFile, msg)
			if err != nil || resp == nil {
				fmt.Printf("Failed to send gemmail to sub %s on mailinglist %s. Failed with err %v\n", sub_address, fromMailbox.CertInfo.mailAddress, err)
			} else if redirectAttempt < 6 && (resp.Status == misfin_client.StatusRedirect || resp.Status == misfin_client.StatusRedirectPermanent) {
				// Handle redirects (stopping at 5th redirect attempt)
				sub_address = strings.TrimPrefix(strings.TrimPrefix(strings.TrimSpace(resp.Meta), "misfin://"), "misfin:")
				redirectAttempt += 1
				// TODO: If permanent redirect, change sub's new address and fingerprint and save it?
				time.Sleep(226 * time.Millisecond)
				continue
			} else if resp.Status == 44 { // Slow Down request: sleep, then try again
				num, _ := strconv.ParseInt(resp.Meta, 10, 64)
				if num == 0 {
					num = 1
				}
				time.Sleep((time.Duration(num) * time.Second) + time.Millisecond)
				redirectAttempt += 1
				continue
			} else if resp.Status != 20 {
				if (resp.Status == 59 || resp.Status == 40 || resp.Status == 60) && !misfinB {
					misfinClient = misfin_client.DefaultClient_MisfinB
					misfinB = true
					redirectAttempt = 0
					time.Sleep(226 * time.Millisecond)
					continue
				} else {
					fmt.Printf("Failed to send gemmail to sub %s on mailinglist %s. Failed with misfin error %v\n", sub_address, fromMailbox.CertInfo.mailAddress, resp.Status)
				}
			}

			break
		}
	}
}
