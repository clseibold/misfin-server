package main

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"time"
)

type Config struct {
	ConfigFilename                   string
	ServerCertFilename               string
	ServerCertFile                   []byte
	Port                             string
	BindAddress                      string
	MailboxDirectory                 string // Contains mailboxes and mailbox index file
	GeminiServerPermission           GeminiPermission
	GeminiUserRegistrationPermission GeminiRegistrationPermission
	MaxConcurrentConnections         int
	RateLimitDuration                time.Duration // THe rate-limit duration for non-server-members
	RateLimitDuration_Member         time.Duration // The rate-limit duration for server members
	SkylabDirectory                  string
	PrevVersion                      string // The server version that last ran with the config
}

type GeminiPermission int

const (
	GeminiPermission_Disabled GeminiPermission = iota // TODO: Add internal so that gemini requests only work from localhost/local ips
	GeminiPermission_AdminOnly
	GeminiPermission_MemberOnly
	GeminiPermission_Public
)

type GeminiRegistrationPermission int

const (
	GeminiRegistrationPermission_Disabled  GeminiRegistrationPermission = iota // Don't allow registrations
	GeminiRegistrationPermission_AdminOnly                                     // Only Admins can register users from the gemini frontend
	GeminiRegistrationPermission_Public    GeminiRegistrationPermission = 4    // Anyone can register
)

var DefaultConfiguration = Config{"./misfinserver.conf", "admin.pem", []byte{}, "1958", "0.0.0.0", "./mailboxes/", GeminiPermission_Disabled, GeminiRegistrationPermission_Disabled, 2000, time.Millisecond * 225, time.Millisecond * 50, "", "0.5.2"}
var UserConfiguration = DefaultConfiguration
var FlagsConfiguration = Config{}

// Make sure to set config filename // TODO: Return errors on malformed file format
func parseConfig(data string, config *Config) {
	lines := strings.FieldsFunc(data, func(r rune) bool { return r == '\n' })

	for _, line := range lines {
		parts := strings.SplitN(line, ":", 2)
		field := strings.TrimSpace(parts[0])
		value := strings.TrimSpace(parts[1])

		if field == "port" {
			config.Port = value
		} else if field == "bind-address" {
			config.BindAddress = value
		} else if field == "mailbox-dir" {
			config.MailboxDirectory = value
		} else if field == "server-cert" {
			config.ServerCertFilename = value
		} else if field == "gemini-permission" {
			if value == "disabled" {
				config.GeminiServerPermission = GeminiPermission_Disabled
			} else if value == "admin-only" {
				config.GeminiServerPermission = GeminiPermission_AdminOnly
			} else if value == "member-only" {
				config.GeminiServerPermission = GeminiPermission_MemberOnly
			} else if value == "public" {
				config.GeminiServerPermission = GeminiPermission_Public
			}
		} else if field == "gemini-user-registration-permission" {
			if value == "disabled" {
				config.GeminiUserRegistrationPermission = GeminiRegistrationPermission_Disabled
			} else if value == "admin-only" {
				config.GeminiUserRegistrationPermission = GeminiRegistrationPermission_AdminOnly
			} else if value == "public" {
				config.GeminiUserRegistrationPermission = GeminiRegistrationPermission_Public
			}
		} else if field == "max-concurrent-connections" {
			num, parseErr := strconv.ParseInt(value, 10, 64)
			if parseErr != nil {
				continue
			}
			config.MaxConcurrentConnections = int(num)
		} else if field == "rate-limit-duration" {
			num, parseErr := strconv.ParseInt(value, 10, 64)
			if parseErr != nil {
				continue
			}
			config.RateLimitDuration = time.Duration(num) * time.Millisecond
		} else if field == "rate-limit-duration-members" {
			num, parseErr := strconv.ParseInt(value, 10, 64)
			if parseErr != nil {
				continue
			}
			config.RateLimitDuration_Member = time.Duration(num) * time.Millisecond
		} else if field == "version" {
			// The server version that last ran with the config
			config.PrevVersion = value
		} else if field == "skylab-directory" {
			config.SkylabDirectory = value
		}
	}
}

// Saves to the ConfigFilename in the passed in config value
func saveConfig(config Config) {
	var builder strings.Builder
	builder.WriteString("port: ")
	builder.WriteString(config.Port)
	builder.WriteString("\n")

	builder.WriteString("bind-address: ")
	builder.WriteString(config.BindAddress)
	builder.WriteString("\n")

	builder.WriteString("mailbox-dir: ")
	builder.WriteString(config.MailboxDirectory)
	builder.WriteString("\n")

	builder.WriteString("server-cert: ")
	builder.WriteString(config.ServerCertFilename)
	builder.WriteString("\n")

	builder.WriteString("gemini-permission: ")
	if config.GeminiServerPermission == GeminiPermission_Disabled {
		builder.WriteString("disabled")
	} else if config.GeminiServerPermission == GeminiPermission_AdminOnly {
		builder.WriteString("admin-only")
	} else if config.GeminiServerPermission == GeminiPermission_MemberOnly {
		builder.WriteString("member-only")
	} else if config.GeminiServerPermission == GeminiPermission_Public {
		builder.WriteString("public")
	}
	builder.WriteString("\n")

	builder.WriteString("gemini-user-registration-permission: ")
	if config.GeminiUserRegistrationPermission == GeminiRegistrationPermission_Disabled {
		builder.WriteString("disabled")
	} else if config.GeminiUserRegistrationPermission == GeminiRegistrationPermission_AdminOnly {
		builder.WriteString("admin-only")
	} else if config.GeminiUserRegistrationPermission == GeminiRegistrationPermission_Public {
		builder.WriteString("public")
	}
	builder.WriteString("\n")

	builder.WriteString("max-concurrent-connections: ")
	fmt.Fprintf(&builder, "%d", config.MaxConcurrentConnections)
	builder.WriteString("\n")

	builder.WriteString("rate-limit-duration: ")
	fmt.Fprintf(&builder, "%d", config.RateLimitDuration/time.Millisecond)
	builder.WriteString("\n")

	builder.WriteString("rate-limit-duration-members: ")
	fmt.Fprintf(&builder, "%d", config.RateLimitDuration_Member/time.Millisecond)
	builder.WriteString("\n")

	if config.SkylabDirectory != "" {
		builder.WriteString("skylab-directory: ")
		fmt.Fprintf(&builder, "%s", config.SkylabDirectory)
		builder.WriteString("\n")
	}

	// Always save with the current version
	builder.WriteString("version: ")
	builder.WriteString(Version)
	builder.WriteString("\n")

	writeErr := os.WriteFile(config.ConfigFilename, []byte(builder.String()), 0660)
	if writeErr != nil {
		panic(writeErr)
	}
}

func handleConfigValues(config *Config) {
	homeDir, _ := os.UserHomeDir()
	if runtime.GOOS == "windows" {
		if strings.HasPrefix(config.ServerCertFilename, "~") {
			config.ServerCertFilename = filepath.Join(homeDir, strings.TrimPrefix(config.ServerCertFilename, "~"))
		}
		if strings.HasPrefix(config.MailboxDirectory, "~") {
			config.MailboxDirectory = filepath.Join(homeDir, strings.TrimPrefix(config.MailboxDirectory, "~"))
		}
	}

	// Open server cert
	config.ServerCertFile, _ = os.ReadFile(config.ServerCertFilename)
}

// Input configFilename, or empty string for default.
// Returns config values based on set flags or defaults. If a flag was not set, then that field uses the value from the user's defaults
func getConfigValuesForCommand(configFilename string) (Config, error) {
	if configFilename == "" {
		configFilename = DefaultConfiguration.ConfigFilename
		file, readErr := os.ReadFile(configFilename)
		if readErr != nil {
			return Config{}, readErr
		}
		parseConfig(string(file), &UserConfiguration)
	} else {
		file, readErr := os.ReadFile(configFilename)
		if readErr != nil {
			return Config{}, readErr
		}
		parseConfig(string(file), &UserConfiguration)
	}
	var result Config

	if FlagsConfiguration.Port == "" {
		result.Port = UserConfiguration.Port
	} else {
		result.Port = FlagsConfiguration.Port
	}

	if FlagsConfiguration.BindAddress == "" {
		result.BindAddress = UserConfiguration.BindAddress
	} else {
		result.BindAddress = FlagsConfiguration.BindAddress
	}

	if FlagsConfiguration.MailboxDirectory == "" {
		result.MailboxDirectory = UserConfiguration.MailboxDirectory
	} else {
		result.MailboxDirectory = FlagsConfiguration.MailboxDirectory
	}

	if FlagsConfiguration.ServerCertFilename == "" {
		result.ServerCertFilename = UserConfiguration.ServerCertFilename
	} else {
		result.ServerCertFilename = FlagsConfiguration.ServerCertFilename
	}

	result.GeminiServerPermission = UserConfiguration.GeminiServerPermission
	result.GeminiUserRegistrationPermission = UserConfiguration.GeminiUserRegistrationPermission
	result.MaxConcurrentConnections = UserConfiguration.MaxConcurrentConnections
	result.RateLimitDuration = UserConfiguration.RateLimitDuration
	result.RateLimitDuration_Member = UserConfiguration.RateLimitDuration_Member
	result.SkylabDirectory = UserConfiguration.SkylabDirectory
	result.PrevVersion = UserConfiguration.PrevVersion

	result.ConfigFilename = configFilename
	handleConfigValues(&result)
	return result, nil
}
