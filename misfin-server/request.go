package main

import (
	"crypto/md5"
	"crypto/sha256"
	"crypto/x509"
	"encoding/hex"
	"fmt"
	"io"
	"net/url"
	"path"
	"strings"

	"gitlab.com/clseibold/misfin-server/gemmail"
)

type ProtocolType int

const ProtocolType_Gemini ProtocolType = 0   // Gemini + Titan
const ProtocolType_Misfin_B ProtocolType = 3 // Misfin(B) + Misfin(C) + Gemini
const ProtocolType_Misfin_C ProtocolType = 8 // Misfin(B) + Misfin(C) + Gemini
const ProtocolType_Admin ProtocolType = 4    // Gemini + Titan
const ProtocolType_Unknown ProtocolType = 7

type Request struct {
	// Request Data
	Type          ProtocolType      // TODO: Change this to ProtocolType instead
	Upload        bool              // true if request supports reading uploaded data
	fromRoute     string            // What route the request came from
	requestString string            // Path for Nex, URL for Gopher, Gemini, and Misfin. Always excludes the query
	_rawQuery     string            // The raw query (always escaped, without '?', or '\t' for gopher). Spartan does not use this, but uses the Data field instead.
	_queryLimit   int64             // Used by Spartan servers to limit data for queries. If 0, use the default of 1024.
	GlobString    string            // Set to the path components of the glob part of the route
	readwriter    io.ReadWriter     // Reads from and Writes to the connection (net or tls)
	params        map[string]string // Stores route params and their values

	// Upload Data
	DataSize int64  // Expected Size for upload (-1 for any size until connection close)
	_data    []byte // Used to cache raw uploaded data. For NPS, the data is already read before calling the handler. If proxying from Spartan servers, the data is set to a query string, if used.
	DataMime string // Used for Titan and other protocols that support upload with mimetype. For Spartan, it is usually empty or "text/plain" if proxying to Spartan servers from servers that use queries.

	// More Request Data
	UserCert     *x509.Certificate // Used for protocols that support TLS Client Certs (Misfin and Gemini)
	UserCertInfo CertInfo
	IP           string

	// Response Data
	servePath   string // Used internally to serve static files/directories.
	headerSent  bool   // Stores whether the response header has already been sent (for protocols that have this)
	convertMode bool   // When turned on, converts from given format to default format of protocol

	// Proxying Data
	proxyRoute       string // Used to proxy routes from one server to another, or within the same server.
	proxiedUnder     string // The path being proxied under. This is necessary to proxy gemini and nex over to gopher, because gopher requires links be absolute to originating server.
	proxiedFromRoute string // The route being proxied from

	Proxied bool
	Server  *ServerContext

	// Misfin Request Info
	gemmail gemmail.GemMail
}

type RequestHandler func(request Request)

// Get Titan or Spartan raw data. // TODO
func (r *Request) GetUploadData() ([]byte, error) { // TODO: Return error when there's no data received after a timeout.
	if !r.Upload {
		return []byte{}, fmt.Errorf("current page does not allow upload")
	}

	return r._getUploadData()
}

// TODO
func (r *Request) _getUploadData() ([]byte, error) {
	// If data has already been read, then return that. This is useful for multiple calls of this method, and for proxying to Spartan servers (where query strings get set to the data).
	if len(r._data) > 0 {
		return r._data, nil
	}

	if r.DataSize == 0 {
		// A delete request in Titan. No data upload in Spartan.
		return []byte{}, nil
	}

	uploadData := make([]byte, r.DataSize)
	buf := make([]byte, 1)
	for i := 0; i < int(r.DataSize); i++ {
		n, err := r.readwriter.Read(buf)
		if err == io.EOF && n <= 0 {
			switch r.Type {
			case ProtocolType_Misfin_B, ProtocolType_Misfin_C, ProtocolType_Admin, ProtocolType_Gemini: // Titan
				fmt.Printf("Titan Error: Couldn't read data\n")
				responseBadRequest := "40 Failed to read data.\r\n"
				r.readwriter.Write([]byte(responseBadRequest))
				return []byte{}, err
			}
		} else if err != nil && err != io.EOF {
			switch r.Type {
			case ProtocolType_Misfin_B, ProtocolType_Misfin_C, ProtocolType_Admin, ProtocolType_Gemini: // Titan
				fmt.Printf("Titan Error: Couldn't read data\n")
				responseBadRequest := "40 Failed to read data.\r\n"
				r.readwriter.Write([]byte(responseBadRequest))
				return []byte{}, err
			}
		}

		uploadData[i] = buf[0]
	}
	//_, err := r.readwriter.Read(titanData)
	//titanData, err := io.ReadAll(io.LimitReader(r.readwriter, r.DataSize))

	r._data = uploadData
	return uploadData, nil
}

// Takes checksum hash of IP and returns it in a hex string
func (r *Request) IPHash() string {
	hasher := sha256.New()
	hasher.Write([]byte(r.IP))
	return hex.EncodeToString(hasher.Sum(nil))
}

// MD5 Hash of User Cert - usually used for Gemini and Titan. Returns empty string when there's no user cert.
func (r *Request) UserCert_MD5Hash() string {
	if r.UserCert == nil {
		return ""
	}
	return fmt.Sprintf("%x", md5.Sum(r.UserCert.Raw))
}

func (r *Request) UserCertHash_Gemini() string {
	return r.UserCert_MD5Hash()
}

// SHA256 Hash of User Cert - usually used for Misfin. Returns empty string when there's no user cert.
func (r *Request) UserCert_SHA256Hash() string {
	h := sha256.New()
	h.Write(r.UserCert.Raw)
	return hex.EncodeToString(h.Sum(nil))
}

func (r *Request) UserCertHash_Misfin() string {
	return r.UserCert_SHA256Hash()
}

// Misfin, Gopher, Gemini, Titan, and Nex

// TODO: Reconsider how this works...
// Gets the requested hostname for protocols that use those in requests
// If not used in request, then gets the hostname of the server.
func (r *Request) Hostname() string {
	if r.Type == ProtocolType_Admin || r.Type == ProtocolType_Gemini || r.Type == ProtocolType_Misfin_B || r.Type == ProtocolType_Misfin_C {
		URL, err := url.Parse(r.requestString)
		if err != nil {
			return ""
		}
		return URL.Hostname()
	} else { // TODO: Get server's hostname
		//return r.Server.Hostname
		return r.Server.Hostname()
	}
}

var defaultPorts = map[ProtocolType]string{
	ProtocolType_Gemini:   "1965",
	ProtocolType_Admin:    "1995",
	ProtocolType_Misfin_B: "1958",
	ProtocolType_Misfin_C: "1958",
}

// Hostname + Port. Port is excluded if it's the default of the protocol.
func (r *Request) Host() string {
	if r.Server.Port() != defaultPorts[r.Type] {
		return r.Hostname()
	} else {
		return r.Hostname() + ":" + r.Server.Port()
	}
}

// Gets the requested path, removing hostname and scheme information for protocols that use those in requests
// Path is unescaped if part of URL (for protocols that use URL requests). Unescaping is unnecessary for Nex and Gopher.
func (r *Request) Path() string { // TODO: Escaped vs. Unescaped
	if r.Type == ProtocolType_Admin || r.Type == ProtocolType_Gemini {
		URL, err := url.Parse(r.requestString)
		if err != nil {
			return ""
		}
		return URL.Path
	} else if r.Type == ProtocolType_Misfin_B || r.Type == ProtocolType_Misfin_C {
		return ""
	}
	// TODO: How does Gopher handle this?

	return r.requestString
}

// Gets the requested path, removing hostname and scheme information for protocols that use those in requests
// Path is escaped if part of URL (for protocols that use URL requests). NOTE: Nex and Gopher requests are not escaped.
func (r *Request) RawPath() string {
	if r.Type == ProtocolType_Admin || r.Type == ProtocolType_Gemini {
		URL, err := url.Parse(r.requestString)
		if err != nil {
			return ""
		}
		return URL.EscapedPath()
	} else if r.Type == ProtocolType_Misfin_B || r.Type == ProtocolType_Misfin_C {
		return ""
	}

	return r.requestString
}

// Gets the escaped query for protocols that support queries in requests.
// If Spartan, it gets the uploaded data (if there is any) as text and escapes it.
// If data is longer than Spartan Query Limit, then this method will return an error.
func (r *Request) RawQuery() (string, error) {
	return r._rawQuery, nil
}

// Gets the query for protocols that support queries in requests. The returned query is unescaped.
// If Spartan, it gets the *raw* uploaded data (if there is any) as text. To get it query escaped, use request.RawQuery()
// If data is longer than Spartan Query Limit, then this method will return an error.
func (r *Request) Query() (string, error) {
	rawQuery, _ := r.RawQuery()
	q, err := url.QueryUnescape(rawQuery)
	if err != nil {
		return "", err
	}
	return q, nil
}

// Gets the query for protocols that support queries in requests
/*
func (r *Request) RawQuery() string {
	if r.Type == ProtocolType_Admin || r.Type == ProtocolType_Gemini {
		URL, err := url.Parse(r.requestString)
		if err != nil {
			return ""
		}
		return URL.RawQuery
	} else if r.Type == ProtocolType_Nex {
		return ""
	} else if r.Type == ProtocolType_Gopher {
		// Check for query using "?" first. If found, everything after it is part of the query (including any tab characters)
		urlQuery := strings.IndexRune(r.requestString, '?')
		traditionalQuery := strings.IndexRune(r.requestString, '\t')
		if urlQuery != -1 && (traditionalQuery == -1 || urlQuery < traditionalQuery) { // ? comes first, so everything after it is the query
			URL, err := url.Parse(r.requestString)
			if err != nil {
				return ""
			}
			return URL.RawQuery
		} else { // Tab comes first, so '?' is part of the query
			// Traditionally, gopher queries use a tab after the request selector
			_, query, _ := strings.Cut(r.requestString, "\t")
			return query
		}
	}

	// TODO: How does Gopher handle this?
	return ""
}
*/

// NOTE: Should almost never be used
func (r *Request) Fragment() string {
	if r.Type == ProtocolType_Admin || r.Type == ProtocolType_Gemini || r.Type == ProtocolType_Misfin_B || r.Type == ProtocolType_Misfin_C {
		URL, err := url.Parse(r.requestString)
		if err != nil {
			return ""
		}
		return URL.Fragment
	} else {
		parts := strings.SplitAfterN(r.requestString, "#", 2)
		if len(parts) <= 1 {
			return ""
		} else {
			f, err := url.QueryUnescape(parts[1])
			if err != nil {
				return ""
			} else {
				return f
			}
		}
	}
}

// NOTE: Should almost never be used
func (r *Request) RawFragment() string {
	if r.Type == ProtocolType_Admin || r.Type == ProtocolType_Gemini || r.Type == ProtocolType_Misfin_B || r.Type == ProtocolType_Misfin_C {
		URL, err := url.Parse(r.requestString)
		if err != nil {
			return ""
		}
		return URL.EscapedFragment()
	} else {
		parts := strings.SplitAfterN(r.requestString, "#", 2)
		if len(parts) <= 1 {
			return ""
		} else {
			return parts[1]
		}
	}
}

// Returns empty string if param not in route
func (r *Request) GetParam(name string) string {
	p, ok := r.params[name]
	if ok {
		return p
	}
	return ""
}

/*
// Proxy Handler - handles proxying routes from one server to another, or to a route on the same server
func proxyHandler(request Request) {
	//fmt.Printf("Glob: %s\n", request.GlobString)

	// TODO: As a security mechanism, make sure we do not get infinite proxying when having two servers that proxy each other.
	// Ideally, if we find that this request is proxying a route that proxies back to the originating server, just redirect to that page.
	// Technically, the request header limit also limites the depth of the proxying, but we should still have this here just in case
	// (and for protocols in the future that might have a much larger header length limit).

	proxyRoute := request.proxyRoute
	var server *Server = nil
	if strings.HasPrefix(proxyRoute, "$") {
		trimmed := strings.TrimPrefix(proxyRoute, "$")
		if strings.HasPrefix(trimmed, "/") {
			// If starts with $ but there's no server name ("$/"), assume the current server
			proxyRoute = trimmed
		} else {
			parts := strings.SplitN(trimmed, "/", 2)
			destServerName := parts[0]
			proxyRoute = parts[1]
			handle := request.Server.SIS().FindServerByName(destServerName)
			server = handle.GetServer()
		}
	}
	if server == nil {
		server = request.Server.GetServer()
	}
	proxyRoute = strings.TrimPrefix(proxyRoute, "/")

	fmt.Printf("Proxied from server '%s' to server '%s'\n", request.Server.Name(), server.Name)

	resultRouteString := InsertParamsIntoRouteString(proxyRoute, request.params, request.GlobString, false)

	// TODO: Check escaping of params
	node, globString, params := server.Router.Search(resultRouteString)
	if node != nil && node.Handler != nil {
		proxyRequest := request
		switch server.Type {
		case ProtocolType_Nex, ProtocolType_Admin, ProtocolType_Gemini, ProtocolType_Gopher, ProtocolType_Spartan:
			proxyRequest.requestString = resultRouteString
			// TODO
		}

		// NOTE: When proxying *to* a Spartan server, since spartan doesn't have proper query strings because
		// they act like normal data uploads, when the query or upload data is requested in the spartan
		// server, it will first check if _data is non-empty, and if so, it will use that as the data upload.
		// NOTE: However, when proxying *from* a Spartan server, do not get the query, since that will start
		// reading the data upload. Instead, we want to delay the data upload to when the handler requests it.
		if request.Type != ProtocolType_Spartan { // Get query when not proxying *from* a spartan server.
			query, _ := request.Query()
			proxyRequest._data = []byte(query)
			proxyRequest.DataMime = "text/plain"
		}

		//proxyRequest.requestString = // TODO: How do I do this?
		proxyRequest.GlobString = globString
		proxyRequest.params = params
		// proxyRequest.Data = ???
		proxyRequest.servePath = node.servePath
		proxyRequest.convertMode = true
		proxyRequest.proxyRoute = node.proxyRoute
		proxyRequest.fromRoute = node.GetRoutePath()
		proxyRequest.proxiedFromRoute = request.fromRoute
		fmt.Printf("Proxied from route '%s' to route '%s'\n", proxyRequest.proxiedFromRoute, proxyRequest.fromRoute)

		// If gopherItemType of node was assigned, then use that. Otherwise, continue to use the ProxyRoute's assigned gopher type
		if node.gopherItemType != 0 {
			proxyRequest.gopherItemType = node.gopherItemType
		}

		// TODO
		if request.Proxied {
			proxyRequest.proxiedUnder = path.Join(request.proxiedUnder, strings.TrimSuffix(request.RawPath(), request.GlobString))
			//fmt.Printf("Proxying Under: %s\nOrig Request: %s\n", proxyRequest.proxiedUnder, proxyRequest.origRequest)
		} else {
			proxyRequest.proxiedUnder = "/" + strings.TrimSuffix(request.RawPath(), request.GlobString)
			//proxyRequest.origRequest = "/" + path.Join(request.origRequest, request.Path())
			//fmt.Printf("Proxying Under: %s\nOrig Request: %s\n", proxyRequest.proxiedUnder, proxyRequest.origRequest)
		}
		proxyRequest.Proxied = true
		// proxyRequest.Server // TODO: For now, the proxyRequest.Server is the source Server, not the one being proxied. Look more into this, or perhaps add another field as proxyRequest.ProxyServer
		node.Handler(proxyRequest)
	} else {
		request.NotFound("Proxy route not found.")
	}
}
*/

// Reverse will insert params into components starting with ":". Otherwise, params will be inserted into components starting with "$"
func InsertParamsIntoRouteString(route string, params map[string]string, globString string, reverse bool) string {
	resultRouteString := "/"
	parts := strings.Split(route, "/")
	for _, component := range parts {
		if !reverse && strings.HasPrefix(component, "$") {
			param := params[strings.TrimPrefix(component, "$")]
			resultRouteString = path.Join(resultRouteString, param)
		} else if reverse && strings.HasPrefix(component, ":") {
			param := params[strings.TrimPrefix(component, ":")]
			resultRouteString = path.Join(resultRouteString, param)
		} else if component == "*" {
			if !strings.HasSuffix(resultRouteString, "/") {
				resultRouteString = resultRouteString + "/" + globString
			} else {
				resultRouteString = resultRouteString + globString
			}
		} else {
			resultRouteString = path.Join(resultRouteString, component)
		}
	}

	return resultRouteString
}
