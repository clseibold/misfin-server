package main

import (
	"bytes"
	"crypto/x509"
	"fmt"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"time"
	"unicode"

	"gitlab.com/clseibold/misfin-server/gemmail"
	"gitlab.com/clseibold/misfin-server/misfin_client"
)

// ----- CGI Environment Variables -----
// SERVER_PROTOCOL = "Gemini"
// SERVER_SOFTWARE = [Server Software Name]
// REMOTE_ADDR = public-facing address to access server
// REAL_LISTEN_ADDR = address listening on
// REQUEST_URI
// GEMINI_URL = full url with scheme and hostname
// SERVER_NAME = server hostname
// REQUEST_DOMAIN = domain requested
// REQUEST_PORT = port in request url
// SCHEME = scheme being used (gemini, titan, etc.)
// SERVER_PORT = port of server
// SCRIPT_NAME
// QUERY_STRING = query string of request url
// PATH_INFO (and REQUEST_PATH) = the path in the request url
// TLS_CLIENT_HASH = client cert
// REMOTE_IDENT = client cert's fingerprints of cert and public key
// REMOTE_USER = client cert's user field
// LANG
// REAL_LISTEN_PORT = port listening on
// REMOTE_PORT = port used to access server from internet
// SSL_TLS_SNI = SNI domain used

func handleGeminiRequest(conn tlsconn, URL *url.URL, serverContext *ServerContext, writeTimeout time.Duration, ip string) {
	if URL.Fragment != "" {
		fmt.Printf("invalid request url: Includes fragment\n")
		responseBadURL := "59 URL invalid: Fragments are rejected. Ask the client developer to strip them when sending gemini requests.\r\n"
		conn.Write([]byte(responseBadURL))
		return
	}

	if writeTimeout != 0 {
		err := conn.SetWriteDeadline(time.Now().Add(writeTimeout))
		if err != nil {
			fmt.Printf("could not set socket write timeout: %s", err)
			return
		}
	}

	// Check if client certificate is required
	tlsState := conn.ConnectionState()
	hasCertificate := len(tlsState.PeerCertificates) > 0
	if serverContext.config.GeminiServerPermission != GeminiPermission_Public {
		// If not public gemini server, then require a cert
		if !hasCertificate {
			// Error: no cert
			fmt.Printf("no cert\n")
			responseCertificateRequired := "60 Client certificate required.\r\n"
			conn.Write([]byte(responseCertificateRequired))
			return
		}
	}

	// TODO: Allow any certificate for public mailinglists
	var viewerCert *x509.Certificate = nil
	var viewerCertInfo CertInfo
	var member bool
	var admin bool
	if hasCertificate {
		viewerCert = tlsState.PeerCertificates[0]
		viewerCertInfo = getCertInfo(viewerCert)
		// Verify certificate
		if !verifySenderCert(conn, viewerCertInfo) {
			return
		}

		if !verifyFingerprint_gemini(serverContext, conn, viewerCertInfo) {
			return
		}

		member = senderFromThisServer(serverContext, viewerCertInfo)
		// If not public, only allow member and server root certificates through
		if serverContext.config.GeminiServerPermission != GeminiPermission_Public && !member && viewerCertInfo.fingerprint != serverContext.serverCertInfo.fingerprint {
			responseCertNotAuthorized := "61 Cert not from a mailbox on this mailserver.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}

		// If admin only, check that mailbox is an admin or root mailbox
		viewerMailbox := serverContext.mailboxes.Get(viewerCertInfo.mailbox)
		if serverContext.config.GeminiServerPermission == GeminiPermission_AdminOnly {
			if viewerMailbox.T != MailboxListType_Admin && viewerMailbox.T != MailboxListType_Root {
				responseCertNotAuthorized := "61 Cert not from an admin mailbox on this mailserver.\r\n"
				conn.Write([]byte(responseCertNotAuthorized))
				return
			} else {
				admin = true
			}
		}

		// Check that member fingerprint has not changed
		if member && viewerCertInfo.fingerprint != viewerMailbox.CertInfo.fingerprint {
			responseCertFingerprintIncorrect := fmt.Sprintf("62 Your certificate fingerprint has changed. Expected '%s', got '%s'.\r\n", viewerMailbox.CertInfo.fingerprint, viewerCertInfo.fingerprint)
			conn.Write([]byte(responseCertFingerprintIncorrect))
			return
		}
	} else {
		if serverContext.config.GeminiServerPermission != GeminiPermission_Public {
			responseCertRequired := "60 The gemini interface to this mailserver is member-only. If you are a member, please enable your certificate.\r\n"
			conn.Write([]byte(responseCertRequired))
			return
		}
	}

	if URL.Path == "" || URL.Path == "/" {
		if hasCertificate && member {
			if URL.RawQuery == "login" {
				// Redirect back to homepage without query
				serverContext.IPRateLimit_ExpectRedirectPath(ip, "/")
				responseRedirect := "30 /\r\n"
				conn.Write([]byte(responseRedirect))
				return
			}
			MemberRootPage(conn, serverContext, viewerCert, viewerCertInfo)
			return
		} else {
			if URL.RawQuery == "login" {
				responseCertRequired := "60 Enable your mailbox certificate.\r\n"
				conn.Write([]byte(responseCertRequired))
				return
			}
			RootPage(conn, serverContext)
			return
		}
	} else if URL.Path == "/~skylab" || strings.HasPrefix(URL.Path, "/~skylab") {
		if serverContext.config.SkylabDirectory == "" {
			conn.Write([]byte("51 Skylab not set up.\r\n"))
			return
		}
		skylabScript := filepath.Join(serverContext.config.SkylabDirectory, serverContext.serverCertInfo.domains[0], "skylab") // TODO: Make this work for Windows as well
		query, _ := url.QueryUnescape(URL.RawQuery)
		serverContext.IPRateLimit_ExpectRedirectPath(ip, "$skylab")
		RunCgi(skylabScript, serverContext.serverCertInfo.domains[0], serverContext.config.Port, viewerCert, viewerCertInfo, URL, URL.Path, query, conn)
	} else if URL.Path == "/gmap_setup" {
		var builder strings.Builder
		builder.WriteString("# GMAP Setup Info\n")
		builder.WriteString("Hostname: ")
		builder.WriteString(serverContext.serverCertInfo.domains[0])
		builder.WriteString("\n")

		builder.WriteString("Port: ")
		builder.WriteString(serverContext.config.Port)
		builder.WriteString("\n")

		builder.WriteString("GMAP Address: ")
		builder.WriteString("gmap@")
		builder.WriteString(serverContext.serverCertInfo.domains[0])
		builder.WriteString("\n")

		builder.WriteString("\n## Clients that Support GMAP\n")
		builder.WriteString("=> gemini://satch.xyz/skylab/ Skylab\n")

		builder.WriteString("\n## What is GMAP\n")
		builder.WriteString("GMAP stands for Gemini-Misfin Access Protocol and is a protocol for message-retrieval from misfin servers via Gemini. It was specced by satch. You can find the informal spec here:\n")
		builder.WriteString("=> gemini://satch.xyz/skylab/gmap.gmi The Gemini-Misfin Access Protocol (GMAP)\n")

		conn.Write([]byte("20 text/gemini\r\n"))
		conn.Write([]byte(builder.String()))
	} else if URL.Path == "/archive" {
		if hasCertificate && member {
			MemberArchivePage(conn, serverContext, viewerCert, viewerCertInfo)
			return
		} else {
			responseNotFound := "51 Not Found.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}
	} else if URL.Path == "/sent" {
		if hasCertificate && member {
			MemberSentPage(conn, serverContext, viewerCert, viewerCertInfo)
			return
		} else {
			responseNotFound := "51 Not Found.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}
	} else if URL.Path == "/trash" {
		if hasCertificate && member {
			MemberTrashPage(conn, serverContext, viewerCert, viewerCertInfo)
			return
		} else {
			responseNotFound := "51 Not Found.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}
	} else if URL.Path == "/register" {
		// Only accessible if registrations are allowed
		if serverContext.config.GeminiUserRegistrationPermission == GeminiRegistrationPermission_Disabled {
			conn.Write([]byte("40 Access denied. User registrations are disabled.\r\n"))
			return
		} else if serverContext.config.GeminiUserRegistrationPermission == GeminiRegistrationPermission_AdminOnly {
			if !hasCertificate || !member {
				responseCertRequired := "60 Admin restricted. Enable admin certificate.\r\n"
				conn.Write([]byte(responseCertRequired))
				return
			} else {
				senderMailbox := serverContext.mailboxes.Get(viewerCertInfo.mailbox)
				if senderMailbox == nil || (senderMailbox.T != MailboxListType_Admin && senderMailbox.T != MailboxListType_Root) {
					conn.Write([]byte("61 Admin restricted. Your cert is not allowed.\r\n"))
					return
				}
			}
		}

		if URL.RawQuery == "" {
			// Ask whether to store cert on server?
			conn.Write([]byte("10 What certificate key type do you want to register: ECDSA, ED25519, or RSA? \r\n")) // Request input
			return
		} else {
			query, _ := url.QueryUnescape(URL.RawQuery)
			query = strings.ToUpper(query)
			if query == "ECDSA" {
				serverContext.IPRateLimit_ExpectRedirectPath(ip, "/register/ecdsa")
				conn.Write([]byte("30 /register/ecdsa\r\n"))
			} else if query == "RSA" {
				serverContext.IPRateLimit_ExpectRedirectPath(ip, "/register/rsa")
				conn.Write([]byte("30 /register/rsa\r\n"))
			} else if query == "ED25519" {
				serverContext.IPRateLimit_ExpectRedirectPath(ip, "/register/ed25519")
				conn.Write([]byte("30 /register/ed25519\r\n"))
			} else {
				conn.Write([]byte("10 Invalid key type. What certificate key type do you want to register: ECDSA, ED25519, or RSA? \r\n")) // Request input
				return
			}
		}
		//} else if URL.Path == "/register/" {
	} else if match, err := path.Match("/register/*", URL.Path); err == nil && match {
		keyTypeString := strings.ToUpper(strings.TrimSuffix(strings.TrimPrefix(URL.Path, "/register/"), "/"))
		var keyType KeyType
		if keyTypeString == "RSA" {
			keyType = KeyType_RSA
		} else if keyTypeString == "ED25519" {
			keyType = KeyType_ED25519
		} else if keyTypeString == "ECDSA" {
			keyType = KeyType_ECDSA
		}

		// Only accessible if registrations are allowed
		if serverContext.config.GeminiUserRegistrationPermission == GeminiRegistrationPermission_Disabled {
			conn.Write([]byte("40 Access denied. User registrations are disabled.\r\n"))
			return
		} else if serverContext.config.GeminiUserRegistrationPermission == GeminiRegistrationPermission_AdminOnly {
			if !hasCertificate || !member {
				responseCertRequired := "60 Admin restricted. Enable admin certificate.\r\n"
				conn.Write([]byte(responseCertRequired))
				return
			} else {
				senderMailbox := serverContext.mailboxes.Get(viewerCertInfo.mailbox)
				if senderMailbox == nil || (senderMailbox.T != MailboxListType_Admin && senderMailbox.T != MailboxListType_Root) {
					conn.Write([]byte("61 Admin restricted. Your cert is not allowed.\r\n"))
					return
				}
			}
		}

		if URL.RawQuery == "" {
			// Ask whether to store cert on server?
			conn.Write([]byte("10 Enter mailbox name, a space, and then the blurb (or Full Name): \r\n")) // Request input
			return
		} else {
			query, _ := url.QueryUnescape(URL.RawQuery)
			parts := strings.SplitN(query, " ", 2)
			if len(parts) < 2 {
				conn.Write([]byte("10 Enter mailbox name, a space, and then the blurb (or Full Name): \r\n")) // Request input
				return
			}
			mailbox := strings.TrimSpace(parts[0])
			blurb := strings.TrimSpace(parts[1])
			if mailbox == "" || blurb == "" {
				conn.Write([]byte("10 Enter mailbox name, a space, and then the blurb (or Full Name): \r\n")) // Request input
				return
			}

			// Check if name already exists and check for disallowed mailbox names
			if serverContext.mailboxes.Exists(mailbox) || (!admin && (mailbox == "admin" || mailbox == "support" || mailbox == "mail" || mailbox == "help" || mailbox == "user" || mailbox == "mod" || mailbox == "email")) {
				conn.Write([]byte("40 Mailbox name already exists.\r\n"))
				return
			}

			containsWhitespace := strings.ContainsFunc(mailbox, func(r rune) bool {
				return unicode.IsSpace(r)
			})
			if containsWhitespace {
				conn.Write([]byte("40 Mailbox name cannot contain whitespace.\r\n"))
				return
			}

			// Create cert, store on server, and then send cert to user
			pemBuffer := bytes.NewBuffer([]byte{})
			var certificate *x509.Certificate
			var err error
			certificate, err = generateCertAndPrivateKey(false, serverContext.serverCertificate, serverContext.serverPrivateKey, keyType, blurb, mailbox, serverContext.serverCertInfo.domains[0], pemBuffer)
			if err != nil {
				conn.Write([]byte("40 Couldn't generate cert.\r\n"))
				return
			}

			// Save the user's .pem file to the server's Mailbox directory
			certFilepath := filepath.Join(serverContext.config.MailboxDirectory, mailbox+".pem")
			writeErr := os.WriteFile(certFilepath, pemBuffer.Bytes(), 0660)
			if writeErr != nil {
				conn.Write([]byte("40 Couldn't write cert file on server.\r\n"))
				return
			}

			// Get Cert Info to get the fingerprint
			certInfo := getCertInfo(certificate)

			// Add mailbox to mailbox.list file
			filename := filepath.Join(serverContext.config.MailboxDirectory, mailbox+".gembox")
			userMailbox := CreateMailBox(MailboxListType_User, certInfo, filename, false, false, *serverContext.config)
			/*if admin {
				userMailbox.T = MailboxListType_Admin
			}*/
			serverContext.mailboxes.AddMailBox(&userMailbox)
			serverContext.mailboxes.saveMailboxes()
			serverContext.mailboxes.Get(mailbox).SaveGemBox()

			// Send the cert back to the user
			var page strings.Builder
			page.WriteString("# Mailbox Created\n")
			fmt.Fprintf(&page, "Blurb/Name: %s\n", certInfo.blurb)
			fmt.Fprintf(&page, "Mailbox name: %s\n", mailbox)
			fmt.Fprintf(&page, "Address: %s\n", certInfo.mailAddress)
			fmt.Fprintf(&page, "## Certificate\n")
			fmt.Fprintf(&page, "```\n")
			page.WriteString(string(pemBuffer.Bytes()))
			fmt.Fprintf(&page, "\n```\n")
			fmt.Fprintf(&page, "\nImport the cert using your browser, or copy and paste into a file named %s.pem. In Lagrange, you can open the Identity panel and select Import. Your misfin certificate will automatically be parsed from the page.\n", mailbox)
			fmt.Fprintf(&page, "=> / Homepage\n")
			fmt.Fprintf(&page, "=> /?login Login\n")

			conn.Write([]byte("20 text/gemini\r\n"))
			conn.Write([]byte(page.String()))
			return
		}

		// Request info: Mailbox name, Blurb/full name, admin?
		//return
	} else if URL.Path == "/msg/" || URL.Path == "/msg" {
		serverContext.IPRateLimit_ExpectRedirectPath(ip, "/")
		conn.Write([]byte("30 /\r\n"))
	} else if match, err := path.Match("/msg/*", URL.Path); err == nil && match {
		msgID := path.Base(URL.Path)
		/*num, err := strconv.Atoi(msgNumString)
		if err != nil {
			response := "50 Invalid number."
			conn.Write([]byte(response))
			return
		}*/

		if !hasCertificate {
			responseCertNotAuthorized := "60 Cert required for message access.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}

		if member {
			MemberMessagePage(conn, serverContext, viewerCert, viewerCertInfo, msgID)
			return
		} else {
			responseNotFound := "51 Page not found.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}
	} else if match, err := path.Match("/thread", URL.Path); err == nil && match {
		query, query_err := url.QueryUnescape(URL.RawQuery)
		if query_err != nil {
			responseBadQuery := "59 Bad query.\r\n"
			conn.Write([]byte(responseBadQuery))
			return
		}
		if !hasCertificate {
			responseCertNotAuthorized := "60 Cert required for message access.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}

		if member {
			parts := strings.Split(query, "|")
			subject := parts[0]
			if len(parts) > 2 {
				subject = strings.Join(parts[:len(parts)-2], "|")
			}
			participantsHash := parts[len(parts)-1]
			MemberThreadPage(conn, serverContext, viewerCert, viewerCertInfo, subject, participantsHash)
			return
		} else {
			responseNotFound := "51 Page not found.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}
	} else if match, err := path.Match("/msgid/*", URL.Path); err == nil && match { // GMAP get raw message (with message id)
		if URL.Path == "/msgid/" { // No id provided
			responseNotFound := "51 Page not found.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}

		msgID := path.Base(URL.Path)
		if !hasCertificate {
			responseCertNotAuthorized := "60 Cert required for message access.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}

		if member {
			MemberMessageGMAP(conn, serverContext, viewerCert, viewerCertInfo, msgID)
			return
		} else {
			responseNotFound := "51 Page not found.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}
	} else if match, err := path.Match("/msg/*/tag/*", URL.Path); err == nil && match { // non-GMAP - tag a message
		if !hasCertificate {
			responseCertNotAuthorized := "60 Cert required for message access.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}
		if !member {
			responseNotFound := "51 Page not found.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}

		viewerMailbox := serverContext.mailboxes.Get(viewerCertInfo.mailbox)
		tagName := path.Base(URL.Path)
		if tagName == "tag" || tagName == "" {
			response := "51 Not Found."
			conn.Write([]byte(response))
			return
		}
		msgID := strings.TrimSuffix(strings.TrimPrefix(path.Dir(URL.Path), "/msg/"), "/tag")

		// Toggle tag on message ID
		gmail, _ := viewerMailbox.Gbox.GetGemMailWithID(msgID)
		if gmail == nil {
			responseNotFound := "51 Page not found.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}
		if gmail.HasTag(tagName) {
			gmail.RemoveTag(tagName)
		} else {
			gmail.AddTag(tagName)
		}
		// Have the mailbox saved
		serverContext.saveChan <- viewerCertInfo.mailbox
		serverContext.IPRateLimit_ExpectRedirectPath(ip, fmt.Sprintf("/msg/%s", msgID))
		conn.Write([]byte(fmt.Sprintf("30 /msg/%s\r\n", msgID)))
	} else if strings.HasPrefix(URL.Path, "/msg/") && strings.HasSuffix(URL.Path, "/delete") {
		msgID := strings.TrimSuffix(strings.TrimPrefix(URL.Path, "/msg/"), "/delete")
		if !hasCertificate {
			responseCertNotAuthorized := "60 Cert required for message access.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}
		if !member {
			responseNotFound := "51 Page not found.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}

		query, _ := url.QueryUnescape(URL.RawQuery)
		if query == "" {
			conn.Write([]byte("10 Enter YES to delete: \r\n"))
		} else if strings.ToUpper(query) == "YES" || strings.ToUpper(query) == "Y" {
			MemberMessageDelete(conn, serverContext, viewerCert, viewerCertInfo, msgID, ip)
		}
		return
	} else if tagMatch, err := path.Match("/tag/*", URL.Path); err == nil && tagMatch { // GMAP: Get all messages or messages with a specific tag
		if !hasCertificate {
			responseCertNotAuthorized := "60 Cert required for message access.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}
		if !member {
			responseNotFound := "51 Page not found.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}

		viewerMailbox := serverContext.mailboxes.Get(viewerCertInfo.mailbox)
		if URL.Path == "/tag/" { // Return list of all message IDs
			conn.Write([]byte("20 text/plain\r\n"))
			for i, gmail := range viewerMailbox.Gbox.Mails {
				if i > 0 {
					conn.Write([]byte(","))
				}
				conn.Write([]byte(gmail.ID))
			}
			return
		}

		tagName := path.Base(URL.Path)
		query, _ := url.QueryUnescape(URL.RawQuery)
		if query == "" {
			conn.Write([]byte("20 text/plain\r\n"))
			gemmails := viewerMailbox.Gbox.GetGemMailsWithTag(tagName)
			i := 0
			for _, gmail := range gemmails {
				if gmail.HasTag("Trash") && tagName != "Trash" {
					continue
				}
				if i > 0 {
					conn.Write([]byte(","))
				}
				conn.Write([]byte(gmail.ID))
				i++
			}
		} else {
			// Add/Update tag on message ID
			msgIDs := strings.Split(query, ",")
			for _, msgID := range msgIDs {
				gmail, _ := viewerMailbox.Gbox.GetGemMailWithID(strings.TrimSpace(msgID))
				if gmail == nil {
					/*responseNotFound := "51 Page not found.\r\n"
					conn.Write([]byte(responseNotFound))
					return*/
					continue
				}
				gmail.AddTag(tagName)
			}
			// Have the mailbox saved
			serverContext.saveChan <- viewerCertInfo.mailbox
			conn.Write([]byte("20 text/plain\r\n"))
		}
	} else if tagMatch, err := path.Match("/untag/*", URL.Path); err == nil && tagMatch { // GMAP: untag a message
		if !hasCertificate {
			responseCertNotAuthorized := "60 Cert required for message access.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}
		if !member {
			responseNotFound := "51 Page not found.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}

		viewerMailbox := serverContext.mailboxes.Get(viewerCertInfo.mailbox)
		if URL.Path == "/untag/" { // Return list of all message IDs
			responseNotFound := "51 Page not found.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}

		tagName := path.Base(URL.Path)
		query, _ := url.QueryUnescape(URL.RawQuery)
		if query == "" {
			responseNotFound := "51 Page not found.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		} else {
			// Add/Update tag on message ID
			msgIDs := strings.Split(query, ",")
			for _, msgID := range msgIDs {
				gmail, _ := viewerMailbox.Gbox.GetGemMailWithID(strings.TrimSpace(msgID))
				if gmail == nil {
					/*responseNotFound := "51 Page not found.\r\n"
					conn.Write([]byte(responseNotFound))
					return*/
					continue
				}
				gmail.RemoveTag(tagName)
			}
			// Have the mailbox saved
			serverContext.saveChan <- viewerCertInfo.mailbox
			conn.Write([]byte("20 text/plain\r\n"))
		}
	} else if tagMatch, err := path.Match("/tag/*/*", URL.Path); err == nil && tagMatch { // GMAP get messages with tag that have been received from a certain date
		if !hasCertificate {
			responseCertNotAuthorized := "60 Cert required for message access.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}
		if !member {
			responseNotFound := "51 Page not found.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}

		// Get all messages with the tag, received from a given timestamp
		timestampString := path.Base(URL.Path) // TODO: This is bugged if the last element of path is empty
		if timestampString == "" {
			responseNotFound := "51 Page not found.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}
		afterTimestamp, err := time.Parse(time.RFC3339, timestampString)
		if err != nil { // Timestamp parsing error
			conn.Write([]byte("40 Couldn't parse timestamp."))
			return
		}

		tagName := strings.TrimPrefix(path.Dir(URL.Path), "/tag/")
		if tagName == "" {
			responseNotFound := "51 Page not found.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}

		viewerMailbox := serverContext.mailboxes.Get(viewerCertInfo.mailbox)
		conn.Write([]byte("20 text/plain\r\n"))
		gemmails := viewerMailbox.Gbox.GetGemMailsWithTag(tagName)
		i := 0
		for _, gmail := range gemmails {
			if !gmail.Timestamps[0].Value.After(afterTimestamp) {
				continue
			}
			if gmail.HasTag("Trash") && tagName != "Trash" {
				continue
			}
			if i > 0 {
				conn.Write([]byte(","))
			}
			conn.Write([]byte(gmail.ID))
			i++
		}
	} else if deleteMatch, err := path.Match("/delete", URL.Path); err == nil && deleteMatch { // GMAP Delete (only if msg has "Trash" tag)
		if !hasCertificate {
			responseCertNotAuthorized := "60 Cert required for message access.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}
		if !member {
			responseNotFound := "51 Not Found.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}

		query, _ := url.QueryUnescape(URL.RawQuery)
		if query == "" {
			responseNotFound := "51 Not Found.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		} else {
			msgID := query
			viewerMailbox := serverContext.mailboxes.Get(viewerCertInfo.mailbox)
			gmail, _ := viewerMailbox.Gbox.GetGemMailWithID(msgID)
			if gmail == nil || !gmail.HasTag("Trash") {
				responseNotFound := "51 Not Found\r\n"
				conn.Write([]byte(responseNotFound))
				return
			}

			// Permanently delete message
			viewerMailbox.Mutex.Lock()
			viewerMailbox.Gbox.RemoveGemMailByID(msgID)
			viewerMailbox.Mutex.Unlock()
			serverContext.saveChan <- viewerMailbox.CertInfo.mailbox
			conn.Write([]byte("20 text/plain\r\n"))
		}
	} else if URL.Path == "/fetch" {
		if hasCertificate {
			if member {
				// Read gembox and give it back to client
				senderMailbox := serverContext.mailboxes.Get(viewerCertInfo.mailbox)
				conn.Write([]byte("20 text/gembox\r\n"))
				conn.Write([]byte(senderMailbox.Gbox.String()))
				return
			} else {
				responseCertNotAuthorized := "61 Cert not from a mailbox on this mailserver.\r\n"
				conn.Write([]byte(responseCertNotAuthorized))
				return
			}
		} else {
			responseCertRequired := "60 Enable your mailbox certificate.\r\n"
			conn.Write([]byte(responseCertRequired))
			return
		}
	} else if URL.Path == "/fetch_c" {
		if hasCertificate {
			if member {
				// Read gembox and give it back to client
				senderMailbox := serverContext.mailboxes.Get(viewerCertInfo.mailbox)
				conn.Write([]byte("20 text/gembox\r\n"))
				conn.Write([]byte(senderMailbox.Gbox.String_MisfinC_old()))
				return
			} else {
				responseCertNotAuthorized := "61 Cert not from a mailbox on this mailserver.\r\n"
				conn.Write([]byte(responseCertNotAuthorized))
				return
			}
		} else {
			responseCertRequired := "60 Enable your mailbox certificate.\r\n"
			conn.Write([]byte(responseCertRequired))
			return
		}
	} else if URL.Path == "/fetch_b" {
		if hasCertificate {
			if member {
				// Read gembox and give it back to client
				senderMailbox := serverContext.mailboxes.Get(viewerCertInfo.mailbox)
				conn.Write([]byte("20 text/gembox\r\n"))
				conn.Write([]byte(senderMailbox.Gbox.String_MisfinB_old()))
				return
			} else {
				responseCertNotAuthorized := "61 Cert not from a mailbox on this mailserver.\r\n"
				conn.Write([]byte(responseCertNotAuthorized))
				return
			}
		} else {
			responseCertRequired := "60 Enable your mailbox certificate.\r\n"
			conn.Write([]byte(responseCertRequired))
			return
		}
	} else if ml_mailinglistMatch, err := path.Match("/mailinglist/*", URL.Path); err == nil && ml_mailinglistMatch {
		requestedMLName := path.Base(URL.Path)

		// Check that mailbox exists and is a mailinglist
		if !serverContext.mailboxes.Exists(requestedMLName) {
			responseNotFound := "51 Mailinglist doesn't exist.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}

		mailbox := serverContext.mailboxes.Get(requestedMLName)
		if !mailbox.Mailinglist && !mailbox.NewsfinGroup {
			responseNotFound := "51 Mailinglist doesn't exist.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}

		// If subscribing is closed and there's no archives, then disable mailinglist page completely
		if mailbox.List.SubsFunction == MailingListSubs_Closed && mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_Disabled {
			responseCertNotAuthorized := "40 Mailinglist page disabled. It is not accepting subs or allowing archive viewing.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}

		// If subscribing is closed but the archives are viewable by subs, then require a cert and only allow subs (and admins & writers) to see the mailinglist page
		if mailbox.List.SubsFunction == MailingListSubs_Closed && (mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_Subs || mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_AdminOnly) {
			if !hasCertificate {
				responseCertNotAuthorized := "60 Mailinglist page restricted to subs. Must enable certificate.\r\n"
				conn.Write([]byte(responseCertNotAuthorized))
				return
			}

			sub, subscribed := mailbox.List.Subscribers[viewerCertInfo.mailAddress]
			if !subscribed {
				responseCertNotAuthorized := "61 Not a member of this private mailinglist.\r\n"
				conn.Write([]byte(responseCertNotAuthorized))
				return
			}

			// Check that sub fingerprint has not changed
			if subscribed && viewerCertInfo.fingerprint != sub.Fingerprint {
				responseCertFingerprintIncorrect := fmt.Sprintf("62 Your certificate fingerprint has changed. Expected '%s', got '%s'.\r\n", sub.Fingerprint, viewerCertInfo.fingerprint)
				conn.Write([]byte(responseCertFingerprintIncorrect))
				return
			}

			// If subscribing is closed but the archives are viewable by admins only, then require a cert and only allow admins to see the mailinglist page
			if mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_AdminOnly && sub.T != MailboxListType_Mailinglist_Admin {
				responseCertNotAuthorized := "61 Mailinglist page restricted to admins. You are not an admin.\r\n"
				conn.Write([]byte(responseCertNotAuthorized))
				return
			}
		}

		// Else, if anyone can subscribe, or if subs is set to prompt admins, or if the archives are public, then the mailinglist page is viewable for everyone.
		// Whether the archives will show on the mailinglist page will depend solely on the archive view permission.
		showArchive := mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_Open
		if mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_Disabled {
			showArchive = false
		}
		if mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_Subs || mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_AdminOnly {
			if !hasCertificate {
				showArchive = false
			} else {
				sub, subscribed := mailbox.List.Subscribers[viewerCertInfo.mailAddress]
				showArchive = subscribed
				if mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_AdminOnly {
					showArchive = sub.T == MailboxListType_Mailinglist_Admin
				}

				// Check that sub fingerprint has not changed
				if subscribed && viewerCertInfo.fingerprint != sub.Fingerprint {
					responseCertFingerprintIncorrect := fmt.Sprintf("62 Your certificate fingerprint has changed. Expected '%s', got '%s'.\r\n", sub.Fingerprint, viewerCertInfo.fingerprint)
					conn.Write([]byte(responseCertFingerprintIncorrect))
					return
				}
			}
		}

		MailingListPage(conn, serverContext, mailbox, viewerCert, viewerCertInfo, showArchive)
		return
	} else if ml_fetchAddress, err := path.Match("/mailinglist/*/fetch", URL.Path); err == nil && ml_fetchAddress {
		requestedMLName := path.Base(strings.TrimSuffix(URL.Path, "/fetch"))

		// Check that mailbox exists and is a mailinglist
		if !serverContext.mailboxes.Exists(requestedMLName) {
			responseNotFound := "51 Mailinglist doesn't exist.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}

		mailbox := serverContext.mailboxes.Get(requestedMLName)
		if !mailbox.Mailinglist && !mailbox.NewsfinGroup {
			responseNotFound := "51 Mailinglist doesn't exist.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}

		// Check Archive View Permissions
		// If disabled, disallow everyone
		if mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_Disabled {
			responseCertNotAuthorized := "40 Mailinglist archive view disabled.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}
		sub, subscribed := mailbox.List.Subscribers[viewerCertInfo.mailAddress]

		// Check that sub fingerprint has not changed
		if subscribed && viewerCertInfo.fingerprint != sub.Fingerprint {
			responseCertFingerprintIncorrect := fmt.Sprintf("62 Your certificate fingerprint has changed. Expected '%s', got '%s'.\r\n", sub.Fingerprint, viewerCertInfo.fingerprint)
			conn.Write([]byte(responseCertFingerprintIncorrect))
			return
		}

		// If not open to everyone, make sure user has a certificate enabled
		if mailbox.List.ArchiveViewPermission != MailingListArchiveViewPermission_Open && !hasCertificate {
			responseCertNotAuthorized := "60 Mailinglist archive not public. Must enable certificate.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}

		// If mailinglist archive view is subs-only, check that viewerCert is a member
		if mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_Subs && !subscribed /*|| mailbox.List.SubsFunction == MailingListSubs_Prompt*/ {
			responseCertNotAuthorized := "61 Not a member of this private mailinglist.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}

		// If mailinglist archive view is admin-only, check that the veiwerCert is an admin
		if mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_AdminOnly && sub.T != MailboxListType_Mailinglist_Admin {
			responseCertNotAuthorized := "61 Mailinglist archive restricted to admins. You are not an admin.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}

		// Send back mailinglist's gembox
		conn.Write([]byte("20 text/gembox\r\n"))
		conn.Write([]byte(mailbox.Gbox.String()))
		return
	} else if ml_fetchAddress, err := path.Match("/mailinglist/*/fetch_b", URL.Path); err == nil && ml_fetchAddress {
		requestedMLName := path.Base(strings.TrimSuffix(URL.Path, "/fetch_b"))

		// Check that mailbox exists and is a mailinglist
		if !serverContext.mailboxes.Exists(requestedMLName) {
			responseNotFound := "51 Mailinglist doesn't exist.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}

		mailbox := serverContext.mailboxes.Get(requestedMLName)
		if !mailbox.Mailinglist && !mailbox.NewsfinGroup {
			responseNotFound := "51 Mailinglist doesn't exist.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}

		// Check Archive View Permissions
		// If disabled, disallow everyone
		if mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_Disabled {
			responseCertNotAuthorized := "40 Mailinglist archive view disabled.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}
		sub, subscribed := mailbox.List.Subscribers[viewerCertInfo.mailAddress]

		// Check that sub fingerprint has not changed
		if subscribed && viewerCertInfo.fingerprint != sub.Fingerprint {
			responseCertFingerprintIncorrect := fmt.Sprintf("62 Your certificate fingerprint has changed. Expected '%s', got '%s'.\r\n", sub.Fingerprint, viewerCertInfo.fingerprint)
			conn.Write([]byte(responseCertFingerprintIncorrect))
			return
		}

		// If not open to everyone, make sure user has a certificate enabled
		if mailbox.List.ArchiveViewPermission != MailingListArchiveViewPermission_Open && !hasCertificate {
			responseCertNotAuthorized := "60 Mailinglist archive not public. Must enable certificate.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}

		// If mailinglist archive view is subs-only, check that viewerCert is a member
		if mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_Subs && !subscribed /*|| mailbox.List.SubsFunction == MailingListSubs_Prompt*/ {
			responseCertNotAuthorized := "61 Not a member of this private mailinglist.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}

		// If mailinglist archive view is admin-only, check that the veiwerCert is an admin
		if mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_AdminOnly && sub.T != MailboxListType_Mailinglist_Admin {
			responseCertNotAuthorized := "61 Mailinglist archive restricted to admins. You are not an admin.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}

		// Send back mailinglist's gembox
		conn.Write([]byte("20 text/gembox\r\n"))
		conn.Write([]byte(mailbox.Gbox.String_MisfinB_old()))
		return
	} else if ml_msgAddress, err := path.Match("/mailinglist/*/msg/*", URL.Path); err == nil && ml_msgAddress {
		requestedMLName := strings.TrimSuffix(strings.TrimPrefix(path.Dir(URL.Path), "/mailinglist/"), "/msg")

		// Check that mailbox exists and is a mailinglist
		if !serverContext.mailboxes.Exists(requestedMLName) {
			fmt.Printf("%s\n", requestedMLName)
			responseNotFound := "51 Mailinglist doesn't exist.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}

		mailbox := serverContext.mailboxes.Get(requestedMLName)
		if !mailbox.Mailinglist && !mailbox.NewsfinGroup {
			fmt.Printf("%s\n", requestedMLName)
			responseNotFound := "51 Mailinglist doesn't exist.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}

		// Check Archive View Permissions
		// If disabled, disallow everyone
		if mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_Disabled {
			responseCertNotAuthorized := "40 Mailinglist archive view disabled.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}
		sub, subscribed := mailbox.List.Subscribers[viewerCertInfo.mailAddress]

		// Check that sub fingerprint has not changed
		if subscribed && viewerCertInfo.fingerprint != sub.Fingerprint {
			responseCertFingerprintIncorrect := fmt.Sprintf("62 Your certificate fingerprint has changed. Expected '%s', got '%s'.\r\n", sub.Fingerprint, viewerCertInfo.fingerprint)
			conn.Write([]byte(responseCertFingerprintIncorrect))
			return
		}

		// If not open to everyone, make sure user has a certificate enabled
		if mailbox.List.ArchiveViewPermission != MailingListArchiveViewPermission_Open && !hasCertificate {
			responseCertNotAuthorized := "60 Mailinglist archive not public. Must enable certificate.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}

		// If mailinglist archive view is subs-only, check that viewerCert is a member
		if mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_Subs && !subscribed /*|| mailbox.List.SubsFunction == MailingListSubs_Prompt*/ {
			responseCertNotAuthorized := "61 Not a member of this mailinglist.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}

		// If mailinglist archive view is admin-only, check that the veiwerCert is an admin
		if mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_AdminOnly && sub.T != MailboxListType_Mailinglist_Admin {
			responseCertNotAuthorized := "61 Mailinglist archive restricted to admins. You are not an admin.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}

		msgID := path.Base(URL.Path)
		MailingListMsgPage(conn, serverContext, mailbox, viewerCert, viewerCertInfo, msgID)
		return
	} else if ml_threadAddress, err := path.Match("/mailinglist/*/thread", URL.Path); err == nil && ml_threadAddress {
		requestedMLName := strings.TrimSuffix(strings.TrimSuffix(strings.TrimPrefix(URL.Path, "/mailinglist/"), "/thread"), "/thread/")
		query, query_err := url.QueryUnescape(URL.RawQuery)
		if query_err != nil {
			responseBadQuery := "59 Bad query.\r\n"
			conn.Write([]byte(responseBadQuery))
			return
		}

		// Check that mailbox exists and is a mailinglist
		if !serverContext.mailboxes.Exists(requestedMLName) {
			fmt.Printf("%s\n", requestedMLName)
			responseNotFound := "51 Mailinglist doesn't exist.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}

		mailbox := serverContext.mailboxes.Get(requestedMLName)
		if !mailbox.Mailinglist && !mailbox.NewsfinGroup {
			fmt.Printf("%s\n", requestedMLName)
			responseNotFound := "51 Mailinglist doesn't exist.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}

		// Check Archive View Permissions
		// If disabled, disallow everyone
		if mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_Disabled {
			responseCertNotAuthorized := "40 Mailinglist archive view disabled.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}
		sub, subscribed := mailbox.List.Subscribers[viewerCertInfo.mailAddress]

		// Check that sub fingerprint has not changed
		if subscribed && viewerCertInfo.fingerprint != sub.Fingerprint {
			responseCertFingerprintIncorrect := fmt.Sprintf("62 Your certificate fingerprint has changed. Expected '%s', got '%s'.\r\n", sub.Fingerprint, viewerCertInfo.fingerprint)
			conn.Write([]byte(responseCertFingerprintIncorrect))
			return
		}

		// If not open to everyone, make sure user has a certificate enabled
		if mailbox.List.ArchiveViewPermission != MailingListArchiveViewPermission_Open && !hasCertificate {
			responseCertNotAuthorized := "60 Mailinglist archive not public. Must enable certificate.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}

		// If mailinglist archive view is subs-only, check that viewerCert is a member
		if mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_Subs && !subscribed /*|| mailbox.List.SubsFunction == MailingListSubs_Prompt*/ {
			responseCertNotAuthorized := "61 Not a member of this mailinglist.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}

		// If mailinglist archive view is admin-only, check that the veiwerCert is an admin
		if mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_AdminOnly && sub.T != MailboxListType_Mailinglist_Admin {
			responseCertNotAuthorized := "61 Mailinglist archive restricted to admins. You are not an admin.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}

		parts := strings.Split(query, "|")
		subject := parts[0]
		if len(parts) > 2 {
			subject = strings.Join(parts[:len(parts)-2], "|")
		}
		participantsHash := parts[len(parts)-1]
		MailingListThreadPage(conn, serverContext, mailbox, viewerCert, viewerCertInfo, subject, participantsHash)
		return
	} else if ml_msgAddressDel, err := path.Match("/mailinglist/*/msg/*/delete", URL.Path); err == nil && ml_msgAddressDel {
		requestedMLName := strings.TrimSuffix(strings.TrimPrefix(path.Dir(strings.TrimSuffix(URL.Path, "/delete")), "/mailinglist/"), "/msg")

		// Check that mailbox exists and is a mailinglist
		if !serverContext.mailboxes.Exists(requestedMLName) {
			fmt.Printf("%s\n", requestedMLName)
			responseNotFound := "51 Mailinglist doesn't exist.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}

		mailbox := serverContext.mailboxes.Get(requestedMLName)
		if !mailbox.Mailinglist && !mailbox.NewsfinGroup {
			fmt.Printf("%s\n", requestedMLName)
			responseNotFound := "51 Mailinglist doesn't exist.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}

		// Check that client cert is enabled and that the user is an admin of the mailinglist
		if !hasCertificate {
			responseCertNotAuthorized := "60 Restricted to admins. Enable your certificate.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}
		userMailbox := serverContext.mailboxes.Get(viewerCertInfo.mailbox)
		if userMailbox.T != MailboxListType_Admin {
			sub, subscribed := mailbox.List.Subscribers[viewerCertInfo.mailAddress]
			if !subscribed {
				responseCertNotAuthorized := "61 Not a member of this mailinglist.\r\n"
				conn.Write([]byte(responseCertNotAuthorized))
				return
			} else if sub.T != MailboxListType_Mailinglist_Admin {
				responseCertNotAuthorized := "61 Not an admin of this mailinglist.\r\n"
				conn.Write([]byte(responseCertNotAuthorized))
				return
			}

			// Check that sub fingerprint has not changed
			if subscribed && viewerCertInfo.fingerprint != sub.Fingerprint {
				responseCertFingerprintIncorrect := fmt.Sprintf("62 Your certificate fingerprint has changed. Expected '%s', got '%s'.\r\n", sub.Fingerprint, viewerCertInfo.fingerprint)
				conn.Write([]byte(responseCertFingerprintIncorrect))
				return
			}
		}

		msgID := path.Base(strings.TrimSuffix(URL.Path, "/delete"))
		query, _ := url.QueryUnescape(URL.RawQuery)
		if query == "" {
			conn.Write([]byte("10 Enter YES to delete: \r\n"))
		} else if strings.ToUpper(query) == "YES" || strings.ToUpper(query) == "Y" {
			MailingListMsgDelete(conn, serverContext, mailbox, viewerCert, viewerCertInfo, msgID, ip)
		}
		return
	} else if ml_msgAddress, err := path.Match("/mailinglist/*/federate", URL.Path); err == nil && ml_msgAddress {
		requestedMLName := strings.TrimSuffix(strings.TrimPrefix(URL.Path, "/mailinglist/"), "/federate")

		// Check that mailbox exists and is a mailinglist
		if !serverContext.mailboxes.Exists(requestedMLName) {
			fmt.Printf("%s\n", requestedMLName)
			responseNotFound := "51 Mailinglist doesn't exist.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}

		mailbox := serverContext.mailboxes.Get(requestedMLName)
		if !mailbox.Mailinglist && !mailbox.NewsfinGroup {
			fmt.Printf("%s\n", requestedMLName)
			responseNotFound := "51 Mailinglist doesn't exist.\r\n"
			conn.Write([]byte(responseNotFound))
			return
		}

		// Check that client cert is enabled and that the user is an admin of the mailinglist
		if !hasCertificate {
			responseCertNotAuthorized := "60 Restricted to admins. Enable your certificate.\r\n"
			conn.Write([]byte(responseCertNotAuthorized))
			return
		}
		if viewerCert == nil {
			conn.Write([]byte("60 Must be authorized with client certificate.\r\n"))
			return
		}
		userMailbox := serverContext.mailboxes.Get(viewerCertInfo.mailbox)
		if userMailbox.T != MailboxListType_Admin {
			sub, subscribed := mailbox.List.Subscribers[viewerCertInfo.mailAddress]
			if !subscribed {
				responseCertNotAuthorized := "61 Not a member of this mailinglist.\r\n"
				conn.Write([]byte(responseCertNotAuthorized))
				return
			} else if sub.T != MailboxListType_Mailinglist_Admin {
				responseCertNotAuthorized := "61 Not an admin of this mailinglist.\r\n"
				conn.Write([]byte(responseCertNotAuthorized))
				return
			}

			// Check that sub fingerprint has not changed
			if subscribed && viewerCertInfo.fingerprint != sub.Fingerprint {
				responseCertFingerprintIncorrect := fmt.Sprintf("62 Your certificate fingerprint has changed. Expected '%s', got '%s'.\r\n", sub.Fingerprint, viewerCertInfo.fingerprint)
				conn.Write([]byte(responseCertFingerprintIncorrect))
				return
			}
		}

		query, _ := url.QueryUnescape(URL.RawQuery)
		query = strings.TrimSpace(query)
		if query == "" {
			conn.Write([]byte("10 Enter the address to federate with: \r\n"))
		} else if strings.ContainsAny(query, " \t\r\n") {
			conn.Write([]byte("10 Enter the address to federate with. It cannot contain whitespace: \r\n"))
		} else {
			if subInfo, subbed := mailbox.List.Subscribers[query]; subbed && subInfo.T == MailboxListType_Mailinglist_Federated {
				conn.Write([]byte("20 text/gemini\r\n# Already Federated\r\n\r\nThe address is already federated with this mailbox.\r\n"))
				return
			}

			// Do Subscribe message to begin federation process
			gmail := gemmail.CreateGemMailFromBody("")
			gmail.Subject = "Subscribe No-Reply"

			ml_cert, _ := os.ReadFile(mailbox.List.CertificateFilepath)
			redirectAttempt := 0 // Keep track of how many times redirected. Don't redirect after 5th attempt
			misfinB := false
			misfinClient := misfin_client.DefaultClient
			for {
				msg := ""
				if misfinB {
					msg = gmail.String_MisfinB()
				} else {
					msg = gmail.String()
				}
				resp, err := misfinClient.SendWithCert("misfin://"+query, ml_cert, ml_cert, msg)
				if err != nil || resp == nil {
					conn.Write([]byte("40 Could not send subscription message to address for federation.\r\n"))
					return
				} else if redirectAttempt < 5 && (resp.Status == misfin_client.StatusRedirect || resp.Status == misfin_client.StatusRedirectPermanent) {
					// Handle redirects (stopping at 5th redirect attempt)
					query = strings.TrimPrefix(strings.TrimPrefix(strings.TrimSpace(resp.Meta), "misfin://"), "misfin:")
					redirectAttempt += 1
					// TODO: If permanent redirect, change sub's new address and fingerprint and save it?
					continue
				} else if resp.Status != 20 {
					if (resp.Status == 59 || resp.Status == 40 || resp.Status == 60) && !misfinB {
						misfinClient = misfin_client.DefaultClient_MisfinB
						misfinB = true
						redirectAttempt = 0
						continue
					} else {
						//fmt.Printf("Failed to send gemmail to sub %s on mailinglist %s. Failed with misfin error %v\n", query, mailbox.CertInfo.mailAddress, resp.Status)
						conn.Write([]byte("40 Could not send subscription message to address for federation.\r\n"))
						return
					}
				}
				break
			}

			// Check if address federating with has already subscribed, and if so, automatically complete step 2 of the federation process.
			if newSubInfo, subbed := mailbox.List.Subscribers[query]; subbed {
				newSubInfo.T = MailboxListType_Mailinglist_Federated
				mailbox.List.Subscribers[query] = newSubInfo
				serverContext.saveChan <- mailbox.CertInfo.mailbox
				conn.Write([]byte("20 text/gemini\r\n# Federation Steps 1-2 Complete\r\n\r\nBoth steps 1 and 2 of the federation process have been completed. The two addresses are now federated.\r\n"))
			} else {
				mailbox.List.ToFederate.Set(query, struct{}{})
				conn.Write([]byte("20 text/gemini\r\n# Federation Step 1 Completed\r\n\r\nStep 1 of the federation process has completed. Once the other server begins the federation process, the final step will automatically be completed by this server. Do not restart this server until the other server has completed the federation process. You can now close this page.\r\n"))
			}
		}
	} else {
		responseNotFound := "51 Page not found.\r\n"
		conn.Write([]byte(responseNotFound))
		return
	}
}

func RootPage(conn tlsconn, serverContext *ServerContext) {
	conn.Write([]byte("20 text/gemini\r\n"))

	var builder strings.Builder
	builder.WriteString("# ")
	builder.WriteString(serverContext.serverCertInfo.domains[0]) // TODO: Server title
	builder.WriteString(" Gemini Mail Interface\n\n")
	// TODO: Server description

	builder.WriteString("Users who have a mailbox on this server can login with their mailbox certificate using the link below: \n")

	builder.WriteString("=> /?login Login with mailbox certificate\n")
	if serverContext.config.GeminiUserRegistrationPermission != GeminiRegistrationPermission_Disabled {
		builder.WriteString("=> /register Register new mailbox\n")
	}
	builder.WriteString("=> /gmap_setup GMAP Setup Info\n")
	builder.WriteString("\n")

	// Mailinglist/Newsletters list
	var first = true
	for _, mbox := range serverContext.mailboxes.mailboxes {
		if !mbox.Mailinglist {
			continue
		}

		// If mailinglist subbing is closed and archive is disabled, then don't show it
		if mbox.List.SubsFunction == MailingListSubs_Closed && mbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_Disabled {
			continue
		}

		// Show link to mailinglist
		if first {
			builder.WriteString("## Public Mailinglists/Newsletters\n")
			first = false
		}
		fmt.Fprintf(&builder, "=> /mailinglist/%s %s\n", url.PathEscape(mbox.CertInfo.mailbox), mbox.CertInfo.blurb)
	}

	// Newsfin Groups list
	first = true
	for _, mbox := range serverContext.mailboxes.mailboxes {
		if !mbox.NewsfinGroup {
			continue
		}

		// If mailinglist subbing is closed and archive is disabled, then don't show it
		if mbox.List.SubsFunction == MailingListSubs_Closed && mbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_Disabled {
			continue
		}

		// Show link to mailinglist
		if first {
			builder.WriteString("## Public Newsfin Groups\n")
			first = false
		}
		fmt.Fprintf(&builder, "=> /mailinglist/%s %s: %s\n", url.PathEscape(mbox.CertInfo.mailbox), mbox.CertInfo.mailbox, mbox.CertInfo.blurb)
	}

	conn.Write([]byte(builder.String()))
}

func MemberRootPage(conn tlsconn, serverContext *ServerContext, viewerCert *x509.Certificate, viewerCertInfo CertInfo) {
	userMailbox := serverContext.mailboxes.Get(viewerCertInfo.mailbox)
	conn.Write([]byte("20 text/gemini\r\n"))

	var builder strings.Builder

	// Main header
	builder.WriteString("# ")
	builder.WriteString(viewerCertInfo.mailbox)
	builder.WriteString("'s Mailbox\n")

	builder.WriteString("Your Misfin Address: ")
	builder.WriteString(userMailbox.CertInfo.mailAddress)
	builder.WriteString("\n\n")

	// Navigation? Gemini Fetch Address (to download gembox)
	if userMailbox.T == MailboxListType_Admin {
		builder.WriteString("=> /admin/ Admin Dashboard\n")
	}
	builder.WriteString("=> /fetch Gemini Fetch Address (download gembox using client cert)\n")
	builder.WriteString("=> /gmap_setup GMAP Setup Info\n\n")
	builder.WriteString("=> /archive Archive\n")
	builder.WriteString("=> /sent Sent\n")
	builder.WriteString("=> /trash Trash\n")
	builder.WriteString("\n")

	// Messages
	builder.WriteString("## Inbox Messages\n")

	inboxThreads := userMailbox.Gbox.GetThreadsWithTag("Inbox")
	lastTime := time.Time{}
	for index := len(inboxThreads) - 1; index >= 0; index-- {
		thread := inboxThreads[index]
		if thread.Length() == 0 {
			continue
		} else if thread.Length() == 1 {
			// Display as just one message, if it's not trashed
			gmail := thread.Mails[0]
			if gmail.HasTag("Trash") {
				continue
			}

			// Add new line if on new day from last message.
			if year1, month1, _ := gmail.Timestamps[0].Value.Date(); lastTime.Year() != year1 || lastTime.Month() != month1 {
				builder.WriteString("\n")
			}
			lastTime = gmail.Timestamps[0].Value

			subject := "[No Subject]"
			if string(gmail.Subject) != "" {
				subject = string(gmail.Subject)
			}
			if gmail.HasTag("Unread") {
				subject = "* " + subject
			}
			timestamp := ""
			if len(gmail.Timestamps) > 0 {
				timestamp = gmail.Timestamps[0].Value.Format(time.DateOnly)
			}
			senderAddress := ""
			if len(gmail.Senders) > 0 {
				senderAddress = gmail.Senders[0].Address
			}
			builder.WriteString(fmt.Sprintf("=> /msg/%s %s %s (From %s)\n", gmail.ID, timestamp, subject, senderAddress))
		} else {
			// Display as a thread, with the last message's date as the date // TODO
			gmail := thread.Mails[len(thread.Mails)-1]

			// Add new line if on new day from last message.
			if year1, month1, _ := gmail.Timestamps[0].Value.Date(); lastTime.Year() != year1 || lastTime.Month() != month1 {
				builder.WriteString("\n")
			}
			lastTime = gmail.Timestamps[0].Value

			// TODO: Handle Trash

			subject := "[No Subject]"
			if string(thread.Subject) != "" {
				subject = "(" + strconv.Itoa(thread.Length()) + ") " + string(thread.Subject)
			}
			if thread.HasTag("Unread") { // NOTE: Has to go through all gemmails in thread to check for tag
				subject = "* " + subject
			}
			timestamp := ""
			if len(gmail.Timestamps) > 0 {
				timestamp = gmail.Timestamps[0].Value.Format(time.DateOnly)
			}
			senderAddress := ""
			if len(gmail.Senders) > 0 {
				senderAddress = gmail.Senders[0].Address
			}
			builder.WriteString(fmt.Sprintf("=> /thread?%s%s %s %s (Last from %s)\n", url.QueryEscape(thread.Subject), url.QueryEscape("|"+thread.GetParticipantsHash()), timestamp, subject, senderAddress))
		}
	}

	/*
		inboxMails := userMailbox.Gbox.GetGemMailsWithTag("Inbox")
		for index := len(inboxMails) - 1; index >= 0; index-- {
			gmail := inboxMails[index]
			if gmail.HasTag("Trash") {
				continue
			}
			subject := "[No Subject]"
			if string(gmail.Subject) != "" {
				subject = string(gmail.Subject)
			}
			if gmail.HasTag("Unread") {
				subject = "* " + subject
			}
			timestamp := ""
			if len(gmail.Timestamps) > 0 {
				timestamp = gmail.Timestamps[0].Value.Format(time.DateOnly)
			}
			senderAddress := ""
			if len(gmail.Senders) > 0 {
				senderAddress = gmail.Senders[0].Address
			}
			builder.WriteString(fmt.Sprintf("=> /msg/%s %s %s (From %s)\n", gmail.ID, timestamp, subject, senderAddress))
		}
	*/

	// Mailinglist/Newsletters list
	var first = true
	for _, mbox := range serverContext.mailboxes.mailboxes {
		if !mbox.Mailinglist {
			continue
		}

		// Mailinglist page disabled if subscribing is closed and archive is disabled, or if subs is closed and archive is admin-only and user is not admin
		sub, subscribed := mbox.List.Subscribers[userMailbox.CertInfo.mailAddress]
		if mbox.List.SubsFunction == MailingListSubs_Closed && mbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_Disabled {
			continue
		}
		if mbox.List.SubsFunction == MailingListSubs_Closed && (mbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_AdminOnly || mbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_Subs) {
			if !subscribed {
				continue
			} else if mbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_AdminOnly && sub.T != MailboxListType_Mailinglist_Admin {
				continue
			}
		}

		// Show link to mailinglist
		if first {
			builder.WriteString("## Mailinglists/Newsletters\n")
			first = false
		}
		fmt.Fprintf(&builder, "=> /mailinglist/%s %s\n", url.PathEscape(mbox.CertInfo.mailbox), mbox.CertInfo.blurb)
	}

	// Newsfin Groups
	first = true
	for _, mbox := range serverContext.mailboxes.mailboxes {
		if !mbox.NewsfinGroup {
			continue
		}

		// Mailinglist page disabled if subscribing is closed and archive is disabled, or if subs is closed and archive is admin-only and user is not admin
		sub, subscribed := mbox.List.Subscribers[userMailbox.CertInfo.mailAddress]
		if mbox.List.SubsFunction == MailingListSubs_Closed && mbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_Disabled {
			continue
		}
		if mbox.List.SubsFunction == MailingListSubs_Closed && (mbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_AdminOnly || mbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_Subs) {
			if !subscribed {
				continue
			} else if mbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_AdminOnly && sub.T != MailboxListType_Mailinglist_Admin {
				continue
			}
		}

		// Show link to mailinglist
		if first {
			builder.WriteString("## Newsfin Groups\n")
			first = false
		}
		fmt.Fprintf(&builder, "=> /mailinglist/%s %s: %s\n", url.PathEscape(mbox.CertInfo.mailbox), mbox.CertInfo.mailbox, mbox.CertInfo.blurb)
	}

	conn.Write([]byte(builder.String()))
}

func MemberArchivePage(conn tlsconn, serverContext *ServerContext, viewerCert *x509.Certificate, viewerCertInfo CertInfo) {
	senderMailbox := serverContext.mailboxes.Get(viewerCertInfo.mailbox)
	conn.Write([]byte("20 text/gemini\r\n"))

	var builder strings.Builder

	// Main header
	builder.WriteString("# ")
	builder.WriteString(viewerCertInfo.mailbox)
	builder.WriteString("'s Mailbox\n")

	builder.WriteString("Your Misfin Address: ")
	builder.WriteString(senderMailbox.CertInfo.mailAddress)
	builder.WriteString("\n\n")

	// Navigation? Gemini Fetch Address (to download gembox)
	builder.WriteString("=> /fetch Gemini Fetch Address (download gembox using client cert)\n")
	builder.WriteString("=> / Inbox\n")
	builder.WriteString("=> /sent Sent\n")
	builder.WriteString("=> /trash Trash\n")
	builder.WriteString("\n")

	// Messages
	builder.WriteString("## Archive Messages\n")

	inboxMails := senderMailbox.Gbox.GetGemMailsWithTag("Archive")
	for index := len(inboxMails) - 1; index >= 0; index-- {
		gmail := inboxMails[index]
		if gmail.HasTag("Trash") {
			continue
		}
		subject := "[No Subject]"
		if string(gmail.Subject) != "" {
			subject = string(gmail.Subject)
		}
		if gmail.HasTag("Unread") {
			subject = "* " + subject
		}
		builder.WriteString(fmt.Sprintf("=> /msg/%s %s %s (From %s)\n", gmail.ID, gmail.Timestamps[0].Value.Format(time.DateOnly), subject, gmail.Senders[0].Address))
	}

	conn.Write([]byte(builder.String()))
}

func MemberSentPage(conn tlsconn, serverContext *ServerContext, viewerCert *x509.Certificate, viewerCertInfo CertInfo) {
	senderMailbox := serverContext.mailboxes.Get(viewerCertInfo.mailbox)
	conn.Write([]byte("20 text/gemini\r\n"))

	var builder strings.Builder

	// Main header
	builder.WriteString("# ")
	builder.WriteString(viewerCertInfo.mailbox)
	builder.WriteString("'s Mailbox\n")

	builder.WriteString("Your Misfin Address: ")
	builder.WriteString(senderMailbox.CertInfo.mailAddress)
	builder.WriteString("\n\n")

	// Navigation? Gemini Fetch Address (to download gembox)
	builder.WriteString("=> /fetch Gemini Fetch Address (download gembox using client cert)\n")
	builder.WriteString("=> / Inbox\n")
	builder.WriteString("=> /archive Archive\n")
	builder.WriteString("=> /trash Trash\n")
	builder.WriteString("\n")

	// Messages
	builder.WriteString("## Sent Messages\n")

	inboxMails := senderMailbox.Gbox.GetGemMailsWithTag("Sent")
	for index := len(inboxMails) - 1; index >= 0; index-- {
		gmail := inboxMails[index]
		if gmail.HasTag("Trash") {
			continue
		}
		subject := "[No Subject]"
		if string(gmail.Subject) != "" {
			subject = string(gmail.Subject)
		}
		if gmail.HasTag("Unread") {
			subject = "* " + subject
		}
		recipient := ""
		if len(gmail.Receivers) > 0 {
			for first := range gmail.Receivers {
				recipient = first
				break
			}
		} else {
			continue
		}
		builder.WriteString(fmt.Sprintf("=> /msg/%s %s %s (Sent to %s)\n", gmail.ID, gmail.Timestamps[0].Value.Format(time.DateOnly), subject, recipient))
	}

	conn.Write([]byte(builder.String()))
}

func MemberTrashPage(conn tlsconn, serverContext *ServerContext, viewerCert *x509.Certificate, viewerCertInfo CertInfo) {
	senderMailbox := serverContext.mailboxes.Get(viewerCertInfo.mailbox)
	conn.Write([]byte("20 text/gemini\r\n"))

	var builder strings.Builder

	// Main header
	builder.WriteString("# ")
	builder.WriteString(viewerCertInfo.mailbox)
	builder.WriteString("'s Mailbox\n")

	builder.WriteString("Your Misfin Address: ")
	builder.WriteString(senderMailbox.CertInfo.mailAddress)
	builder.WriteString("\n\n")

	// Navigation? Gemini Fetch Address (to download gembox)
	builder.WriteString("=> /fetch Gemini Fetch Address (download gembox using client cert)\n")
	builder.WriteString("=> / Inbox\n")
	builder.WriteString("=> /archive Archive\n")
	builder.WriteString("=> /sent Sent\n")
	builder.WriteString("\n")

	// Messages
	builder.WriteString("## Trashed Messages\n")

	inboxMails := senderMailbox.Gbox.GetGemMailsWithTag("Trash")
	for index := len(inboxMails) - 1; index >= 0; index-- {
		gmail := inboxMails[index]
		subject := "[No Subject]"
		if string(gmail.Subject) != "" {
			subject = string(gmail.Subject)
		}
		if gmail.HasTag("Unread") {
			subject = "* " + subject
		}
		builder.WriteString(fmt.Sprintf("=> /msg/%s %s %s (From %s)\n", gmail.ID, gmail.Timestamps[0].Value.Format(time.DateOnly), subject, gmail.Senders[0].Address))
	}

	conn.Write([]byte(builder.String()))
}

func MemberMessagePage(conn tlsconn, serverContext *ServerContext, viewerCert *x509.Certificate, viewerCertInfo CertInfo, msgID string) {
	viewerMailbox := serverContext.mailboxes.Get(viewerCertInfo.mailbox)
	gmail, _ := viewerMailbox.Gbox.GetGemMailWithID(msgID)
	if gmail == nil {
		responseNotFound := "51 Message not found.\r\n"
		conn.Write([]byte(responseNotFound))
		return
	}

	conn.Write([]byte("20 text/gemini\r\n"))

	var builder strings.Builder
	subject := "[No Subject]"
	if string(gmail.Subject) != "" {
		subject = string(gmail.Subject)
	}
	fmt.Fprintf(&builder, "# %s\n\n", subject)

	//fmt.Fprintf(&builder, "Message ID: %s\n", gmail.ID)

	originalSender := gmail.Senders[0]
	if len(gmail.Senders) >= 2 {
		originalSender = gmail.Senders[len(gmail.Senders)-1]

		fmt.Fprintf(&builder, "=> misfin:%s From: %s (%s)\n", originalSender.Address, originalSender.Address, originalSender.Blurb)

		fmt.Fprintf(&builder, "Forwarder(s): ")
		for i := 0; i < len(gmail.Senders)-1; i++ {
			sender := gmail.Senders[i]
			if i > 0 {
				fmt.Fprintf(&builder, ", ")
			}
			fmt.Fprintf(&builder, "%s (%s)", sender.Address, sender.Blurb)
		}
		fmt.Fprintf(&builder, "\n")
	} else {
		fmt.Fprintf(&builder, "=> misfin:%s From: %s (%s)\n", originalSender.Address, originalSender.Address, originalSender.Blurb)
	}

	if len(gmail.Timestamps) > 0 {
		fmt.Fprintf(&builder, "Received on %s\n", gmail.Timestamps[0].Value.Format(time.DateTime))
	}

	if len(gmail.Receivers) > 0 {
		fmt.Fprintf(&builder, "Recipients: ")
		for r := range gmail.Receivers {
			fmt.Fprintf(&builder, "%s ", strings.TrimSpace(r))
		}
		fmt.Fprintf(&builder, "\n")
	}

	fmt.Fprintf(&builder, "\n")
	first := true
	for _, l := range gmail.GemtextLines {
		if first && l.String() == "" {
			continue
		}
		fmt.Fprintf(&builder, "%s\n", l.String())
		first = false
	}

	// Navigation Footer
	fmt.Fprintf(&builder, "\n")
	if len(gmail.Receivers) == 0 {
		fmt.Fprintf(&builder, "=> misfin:%s?%s Reply\n", gmail.Senders[0].Address, url.PathEscape("# "+subject+"\n\n"))
	} else {
		fmt.Fprintf(&builder, "=> misfin:%s?%s Reply to Sender\n", gmail.Senders[0].Address, url.PathEscape("# "+subject+"\n\n"))
		fmt.Fprintf(&builder, "=> misfin:")
		firstDone := false
		for _, p := range gmail.GetParticipants("") {
			if p == viewerMailbox.CertInfo.mailAddress || p == "" {
				continue
			}
			if firstDone {
				builder.WriteString(",")
			}
			builder.WriteString(p)
			firstDone = true
		}
		fmt.Fprintf(&builder, "?%s Reply All\n", url.PathEscape("# "+subject+"\n\n"))
	}
	if !gmail.HasTag("Unread") {
		fmt.Fprintf(&builder, "=> /msg/%s/tag/Unread Mark Unread\n", gmail.ID)
	}
	if !gmail.HasTag("Sent") && !gmail.HasTag("Drafts") {
		if !gmail.HasTag("Archive") {
			fmt.Fprintf(&builder, "=> /msg/%s/tag/Archive Archive\n", gmail.ID)
		} else {
			fmt.Fprintf(&builder, "=> /msg/%s/tag/Inbox Move to Inbox\n", gmail.ID)
		}
	}
	if gmail.HasTag("Trash") {
		fmt.Fprintf(&builder, "=> /msg/%s/tag/Trash Restore from Trash\n", gmail.ID)
		fmt.Fprintf(&builder, "=> /msg/%s/delete Permenately Delete\n", gmail.ID)
	} else {
		fmt.Fprintf(&builder, "=> /msg/%s/tag/Trash Move to Trash\n", gmail.ID)
	}
	fmt.Fprintf(&builder, "=> / Back to Inbox\n")

	conn.Write([]byte(builder.String()))

	// Remove "Unread" tag and save mailbox
	viewerMailbox.Mutex.Lock()
	gmail.RemoveTag("Unread")
	viewerMailbox.Mutex.Unlock()
	serverContext.saveChan <- viewerMailbox.CertInfo.mailbox
}

func MemberThreadPage(conn tlsconn, serverContext *ServerContext, viewerCert *x509.Certificate, viewerCertInfo CertInfo, subject string, participantsHash string) {
	viewerMailbox := serverContext.mailboxes.Get(viewerCertInfo.mailbox)
	thread, exists := viewerMailbox.Gbox.GetThreadWithSubjectAndParticipantsHash(subject, participantsHash)
	if !exists {
		responseNotFound := "51 Thread not found.\r\n"
		conn.Write([]byte(responseNotFound))
		return
	}

	conn.Write([]byte("20 text/gemini\r\n"))

	var builder strings.Builder
	fmt.Fprintf(&builder, "# Thread: %s\n\n", subject)

	fmt.Fprintf(&builder, "=> / Back to Inbox\n")
	fmt.Fprintf(&builder, "=> misfin:")
	firstDone := false
	for _, p := range thread.Participants {
		if p == viewerMailbox.CertInfo.mailAddress || p == "" {
			continue
		}
		if firstDone {
			builder.WriteString(",")
		}
		builder.WriteString(p)
		firstDone = true
	}
	fmt.Fprintf(&builder, "?%s Reply All\n", url.PathEscape("# "+subject+"\n\n"))

	fmt.Fprintf(&builder, "\n---\n\n")

	for i := len(thread.Mails) - 1; i >= 0; i-- {
		gmail := thread.Mails[i]

		originalSender := gmail.Senders[0]
		if len(gmail.Senders) >= 2 {
			originalSender = gmail.Senders[len(gmail.Senders)-1]

			fmt.Fprintf(&builder, "=> misfin:%s From: %s (%s)\n", originalSender.Address, originalSender.Address, originalSender.Blurb)

			fmt.Fprintf(&builder, "Forwarder(s): ")
			for i := 0; i < len(gmail.Senders)-1; i++ {
				sender := gmail.Senders[i]
				if i > 0 {
					fmt.Fprintf(&builder, ", ")
				}
				fmt.Fprintf(&builder, "%s (%s)", sender.Address, sender.Blurb)
			}
			fmt.Fprintf(&builder, "\n")
		} else {
			fmt.Fprintf(&builder, "=> misfin:%s From: %s (%s)\n", originalSender.Address, originalSender.Address, originalSender.Blurb)
		}

		if len(gmail.Timestamps) > 0 {
			fmt.Fprintf(&builder, "Received on %s\n", gmail.Timestamps[0].Value.Format(time.DateTime))
		}

		if len(gmail.Receivers) > 0 {
			fmt.Fprintf(&builder, "Recipients: ")
			for r := range gmail.Receivers {
				fmt.Fprintf(&builder, "%s ", r)
			}
			fmt.Fprintf(&builder, "\n")
		}

		fmt.Fprintf(&builder, "\n")
		first := true
		for _, l := range gmail.GemtextLines {
			if first && l.String() == "" {
				continue
			}
			fmt.Fprintf(&builder, "%s\n", l.String())
			first = false
		}

		// Navigation Footer
		fmt.Fprintf(&builder, "\n")
		if !gmail.HasTag("Unread") {
			fmt.Fprintf(&builder, "=> /msg/%s/tag/Unread Mark Unread\n", gmail.ID)
		}
		if !gmail.HasTag("Archive") {
			fmt.Fprintf(&builder, "=> /msg/%s/tag/Archive Archive\n", gmail.ID)
		} else {
			fmt.Fprintf(&builder, "=> /msg/%s/tag/Inbox Move to Inbox\n", gmail.ID)
		}
		if gmail.HasTag("Trash") {
			fmt.Fprintf(&builder, "=> /msg/%s/tag/Trash Restore from Trash\n", gmail.ID)
			fmt.Fprintf(&builder, "=> /msg/%s/delete Permenately Delete\n", gmail.ID)
		} else {
			fmt.Fprintf(&builder, "=> /msg/%s/tag/Trash Move to Trash\n", gmail.ID)
		}

		fmt.Fprintf(&builder, "\n---\n\n")

		// Remove "Unread" tag for every message in thread
		viewerMailbox.Mutex.Lock()
		gmail.RemoveTag("Unread")
		viewerMailbox.Mutex.Unlock()
	}

	// Save mailbox
	serverContext.saveChan <- viewerMailbox.CertInfo.mailbox

	conn.Write([]byte(builder.String()))
}

func MemberMessageGMAP(conn tlsconn, serverContext *ServerContext, viewerCert *x509.Certificate, viewerCertInfo CertInfo, ID string) {
	viewerMailbox := serverContext.mailboxes.Get(viewerCertInfo.mailbox)

	// Check that message exists in mailbox
	gmail, _ := viewerMailbox.Gbox.GetGemMailWithID(ID)
	if gmail == nil {
		responseNotFound := "51 Message not found.\r\n"
		conn.Write([]byte(responseNotFound))
		return
	}

	conn.Write([]byte("20 text/plain\r\n"))
	conn.Write([]byte(gmail.String()))
}

func MemberMessageDelete(conn tlsconn, serverContext *ServerContext, viewerCert *x509.Certificate, viewerCertInfo CertInfo, ID string, ip string) {
	viewerMailbox := serverContext.mailboxes.Get(viewerCertInfo.mailbox)
	viewerMailbox.Mutex.Lock()
	viewerMailbox.Gbox.RemoveGemMailByID(ID)
	viewerMailbox.Mutex.Unlock()
	serverContext.saveChan <- viewerMailbox.CertInfo.mailbox

	serverContext.IPRateLimit_ExpectRedirectPath(ip, "/")
	conn.Write([]byte("30 /\r\n"))
}

func MailingListPage(conn tlsconn, serverContext *ServerContext, mailinglistMailbox *MailBox, viewerCert *x509.Certificate, viewerCertInfo CertInfo, showArchive bool) {
	//mailbox := mailboxes.Get()
	conn.Write([]byte("20 text/gemini\r\n"))

	var builder strings.Builder
	builder.WriteString("# ")
	if mailinglistMailbox.NewsfinGroup {
		builder.WriteString(mailinglistMailbox.CertInfo.mailbox)
		builder.WriteString(": ")
	}
	builder.WriteString(mailinglistMailbox.CertInfo.blurb)
	//builder.WriteString("'s Mailbox\n\n")
	builder.WriteString("\n\n")

	if viewerCert != nil {
		userMailbox := serverContext.mailboxes.Get(viewerCertInfo.mailbox)
		if sub, subExists := mailinglistMailbox.List.Subscribers[viewerCertInfo.mailAddress]; (subExists && sub.T == MailboxListType_Mailinglist_Admin) || userMailbox.T == MailboxListType_Admin {
			fmt.Fprintf(&builder, "=> /mailinglist/%s/federate Federate with a Mailbox Address\n\n", mailinglistMailbox.CertInfo.mailbox)
		}
	}

	// Description
	if mailinglistMailbox.List.Description != "" {
		fmt.Fprintf(&builder, "Description: %s\n\n", mailinglistMailbox.List.Description)
	}

	if mailinglistMailbox.List.SendThreadPermission == MailingListSend_Open {
		builder.WriteString("Who Can Send New Threads: Anyone (Subscription not needed)\n")
	} else if mailinglistMailbox.List.SendThreadPermission == MailingListSend_Subs {
		builder.WriteString("Who Can Send New Threads: Subs only\n")
	} else if mailinglistMailbox.List.SendThreadPermission == MailingListSend_Writers {
		builder.WriteString("Who Can Send New Threads: Designated Writers and Admins only\n")
	}
	if mailinglistMailbox.List.SendReplyPermission == MailingListSend_Open {
		builder.WriteString("Who Can Send Reply Mails: Anyone (Subscription not needed)\n")
	} else if mailinglistMailbox.List.SendReplyPermission == MailingListSend_Subs {
		builder.WriteString("Who Can Send Reply Mails: Subs only\n")
	} else if mailinglistMailbox.List.SendReplyPermission == MailingListSend_Writers {
		builder.WriteString("Who Can Send Reply Mails: Designated Writers and Admins only\n")
	}
	if mailinglistMailbox.List.SubsFunction == MailingListSubs_AcceptAll {
		builder.WriteString("Who can Subscribe: Anybody\n")
	} else if mailinglistMailbox.List.SubsFunction == MailingListSubs_AcceptServerMembers {
		builder.WriteString("Who can Subscribe: Members of this server\n")
	} else if mailinglistMailbox.List.SubsFunction == MailingListSubs_Closed {
		builder.WriteString("Who can Subscribe: Nobody\n")
	} else if mailinglistMailbox.List.SubsFunction == MailingListSubs_Prompt {
		builder.WriteString("Who can Subscribe: Anybody, requires confirmation by Admin\n")
	}

	// Federated addresses
	if !mailinglistMailbox.NewsfinGroup { // NOTE: Don't print federation if it's a newsfin group, just because the list would probably be very long
		var tempBuilder strings.Builder
		printFederation := false
		tempBuilder.WriteString("Federated with the following: ")
		i := 0
		for addr, sub := range mailinglistMailbox.List.Subscribers {
			if sub.T == MailboxListType_Mailinglist_Federated {
				if i > 0 {
					tempBuilder.WriteString(", ")
				}
				tempBuilder.WriteString(addr)
				i += 1
				printFederation = true
			}
		}
		if printFederation {
			fmt.Fprintf(&builder, "%s\n", tempBuilder.String())
		}
	}

	// How to subscribe (if available)
	/*
		if mailinglistMailbox.List.SubsFunction == MailingListSubs_AcceptAll || mailinglistMailbox.List.SubsFunction == MailingListSubs_AcceptServerMembers || mailinglistMailbox.List.SubsFunction == MailingListSubs_Prompt {
			fmt.Fprintf(&builder, "\nSubscribe to this mailinglist/newsletter by sending a gemmail with the message subject \"Subscribe\" to %s via misfin.\n", mailinglistMailbox.CertInfo.mailAddress)
		}
	*/

	builder.WriteString("\nSend gemmails with subjects by placing a gemtext title (lines prepended with '# ') to the beginning of the message. You can add a label to the gemmail by prepending it to the subject in between square brackets, '[like_so]'.\n\n")

	// Navigation
	builder.WriteString("=> / Home Page\n")
	if showArchive {
		fmt.Fprintf(&builder, "=> /mailinglist/%s/fetch Gemini Fetch Address (download gembox using client cert)\n", url.PathEscape(mailinglistMailbox.CertInfo.mailbox))
	}
	if mailinglistMailbox.List.SubsFunction == MailingListSubs_AcceptAll || mailinglistMailbox.List.SubsFunction == MailingListSubs_AcceptServerMembers || mailinglistMailbox.List.SubsFunction == MailingListSubs_Prompt {
		fmt.Fprintf(&builder, "=> misfin:%s?%s Subscribe\n", mailinglistMailbox.CertInfo.mailAddress, url.PathEscape("# Subscribe\n"))
	}
	if mailinglistMailbox.List.SendThreadPermission != MailingListSend_Writers || mailinglistMailbox.List.SendReplyPermission != MailingListSend_Writers {
		if mailinglistMailbox.NewsfinGroup {
			fmt.Fprintf(&builder, "=> misfin:%s Send mail to Newsfin Group\n", mailinglistMailbox.CertInfo.mailAddress)
		} else {
			fmt.Fprintf(&builder, "=> misfin:%s Send mail to mailinglist\n", mailinglistMailbox.CertInfo.mailAddress)
		}
	}
	builder.WriteString("\n")

	// Messages
	if showArchive {
		builder.WriteString("## Messages\n")

		threads := mailinglistMailbox.Gbox.GetThreads()
		lastTime := time.Time{}
		for index := len(threads) - 1; index >= 0; index-- {
			thread := threads[index]
			if thread.Length() == 0 {
				continue
			} else if thread.Length() == 1 {
				// Display as just one message
				gmail := thread.Mails[0]

				// Add new line if on new day from last message.
				if year1, month1, _ := gmail.Timestamps[0].Value.Date(); lastTime.Year() != year1 || lastTime.Month() != month1 {
					builder.WriteString("\n")
				}
				lastTime = gmail.Timestamps[0].Value

				subject := "[No Subject]"
				if string(gmail.Subject) != "" {
					subject = string(gmail.Subject)
				}
				timestamp := ""
				if len(gmail.Timestamps) > 0 {
					timestamp = gmail.Timestamps[0].Value.Format(time.DateOnly)
				}
				senderAddress := ""
				senderBlurb := ""
				if len(gmail.Senders) > 0 {
					senderAddress = gmail.Senders[len(gmail.Senders)-1].Address
					senderBlurb = gmail.Senders[len(gmail.Senders)-1].Blurb
				}

				//fmt.Fprintf(&builder, "=> /mailinglist/%s/msg/%s %s %s (From %s)\n", url.PathEscape(mailinglistMailbox.CertInfo.mailbox), gmail.ID, timestamp, subject, senderAddress)
				fmt.Fprintf(&builder, "=> misfin:%s %s (%s)\n", senderAddress, senderAddress, senderBlurb)
				fmt.Fprintf(&builder, "%s — %s", subject, gmail.TruncatedBodyString(350))
				fmt.Fprintf(&builder, "=> /mailinglist/%s/msg/%s %s View Post\n\n", url.PathEscape(mailinglistMailbox.CertInfo.mailbox), gmail.ID, timestamp)
			} else {
				// Display as a thread
				gmail_last := thread.Mails[len(thread.Mails)-1]
				gmail_first := thread.Mails[0]

				// Add new line if on new day from last message.
				if year1, month1, _ := gmail_first.Timestamps[0].Value.Date(); lastTime.Year() != year1 || lastTime.Month() != month1 {
					builder.WriteString("\n")
				}
				lastTime = gmail_first.Timestamps[0].Value

				subject := "[No Subject]"
				if string(gmail_first.Subject) != "" {
					subject = string(thread.Subject)
				}
				timestamp := ""
				if len(gmail_first.Timestamps) > 0 {
					timestamp = gmail_first.Timestamps[0].Value.Format(time.DateOnly)
				}
				senderAddress := ""
				senderBlurb := ""
				if len(gmail_first.Senders) > 0 {
					senderAddress = gmail_first.Senders[len(gmail_first.Senders)-1].Address
					senderBlurb = gmail_first.Senders[len(gmail_first.Senders)-1].Blurb
				}
				/*lastTimestamp := ""
				if len(gmail_last.Timestamps) > 0 {
					lastTimestamp = gmail_last.Timestamps[0].Value.Format(time.DateOnly)
				}*/
				gmailLast_SenderAddress := ""
				if len(gmail_last.Senders) > 0 {
					gmailLast_SenderAddress = gmail_last.Senders[len(gmail_last.Senders)-1].Address
				}

				/*fmt.Fprintf(&builder, "=> /mailinglist/%s/thread?%s %s %s (From %s)\n", url.PathEscape(mailinglistMailbox.CertInfo.mailbox), url.QueryEscape(thread.Subject), timestamp, subject, senderAddress)
				fmt.Fprintf(&builder, "%s", gmail_first.TruncatedBodyString(350))
				fmt.Fprintf(&builder, "Last updated on %s from %s\n\n", lastTimestamp, lastSenderAddress)*/

				fmt.Fprintf(&builder, "=> misfin:%s %s (%s)\n", senderAddress, senderAddress, senderBlurb)
				fmt.Fprintf(&builder, "%s — %s", subject, gmail_first.TruncatedBodyString(350))
				fmt.Fprintf(&builder, "=> /mailinglist/%s/thread?%s%s %s %d replies · Last updated by %s\n\n", url.PathEscape(mailinglistMailbox.CertInfo.mailbox), url.QueryEscape(thread.Subject), url.QueryEscape("|"+thread.GetParticipantsHash()), timestamp, thread.Length()-1, gmailLast_SenderAddress)
			}
		}
	}

	conn.Write([]byte(builder.String()))
}

func MailingListMsgPage(conn tlsconn, serverContext *ServerContext, mailinglistMailbox *MailBox, viewerCert *x509.Certificate, viewerCertInfo CertInfo, msgID string) {
	gmail, _ := mailinglistMailbox.Gbox.GetGemMailWithID(msgID)
	if gmail == nil {
		responseNotFound := "51 Message not found.\r\n"
		conn.Write([]byte(responseNotFound))
		return
	}

	conn.Write([]byte("20 text/gemini\r\n"))

	var builder strings.Builder
	subject := "[No Subject]"
	if string(gmail.Subject) != "" {
		subject = string(gmail.Subject)
	}
	fmt.Fprintf(&builder, "# %s\n\n", subject)

	lastSender := gmail.Senders[0]
	originalSender := lastSender
	if len(gmail.Senders) >= 3 {
		originalSender = gmail.Senders[len(gmail.Senders)-1]
		secondSender := gmail.Senders[len(gmail.Senders)-2] // First forwarder

		fmt.Fprintf(&builder, "=> misfin:%s From: %s (%s)\n", originalSender.Address, originalSender.Address, originalSender.Blurb)
		fmt.Fprintf(&builder, "First Forwarder: %s (%s)\n", secondSender.Address, secondSender.Blurb)
		fmt.Fprintf(&builder, "Last Forwarder: %s (%s)\n", lastSender.Address, lastSender.Blurb)
	} else if len(gmail.Senders) == 2 {
		originalSender = gmail.Senders[len(gmail.Senders)-1]

		fmt.Fprintf(&builder, "=> misfin:%s From: %s (%s)\n", originalSender.Address, originalSender.Address, originalSender.Blurb)
		fmt.Fprintf(&builder, "Forwarder: %s (%s)\n", lastSender.Address, lastSender.Blurb)
	} else {
		fmt.Fprintf(&builder, "=> misfin:%s From: %s (%s)\n", lastSender.Address, lastSender.Address, lastSender.Blurb)
	}

	if len(gmail.Timestamps) > 0 {
		fmt.Fprintf(&builder, "Received on %s\n", gmail.Timestamps[0].Value.Format(time.DateTime))
	}

	if len(gmail.Receivers) > 0 {
		fmt.Fprintf(&builder, "Recipients: ")
		for r := range gmail.Receivers {
			fmt.Fprintf(&builder, "%s ", r)
		}
		fmt.Fprintf(&builder, "\n")
	}

	fmt.Fprintf(&builder, "\n")
	first := true
	for _, l := range gmail.GemtextLines {
		if first && l.String() == "" {
			continue
		}
		fmt.Fprintf(&builder, "%s\n", l.String())
		first = false
	}

	// Navigation Footer (Delete link for admins of mailinglist only)
	fmt.Fprintf(&builder, "\n")
	if viewerCert != nil {
		userMailbox := serverContext.mailboxes.Get(viewerCertInfo.mailbox)
		sub, subscribed := mailinglistMailbox.List.Subscribers[viewerCertInfo.mailAddress]
		if (subscribed && sub.T == MailboxListType_Mailinglist_Admin) || userMailbox.T == MailboxListType_Admin {
			fmt.Fprintf(&builder, "=> /mailinglist/%s/msg/%s/delete Delete Message from Archive\n", mailinglistMailbox.CertInfo.mailbox, gmail.ID)
		}
	}
	fmt.Fprintf(&builder, "=> misfin:%s?%s Reply to Thread\n", mailinglistMailbox.CertInfo.mailAddress, url.PathEscape("# "+subject+"\n\n"))
	fmt.Fprintf(&builder, "=> /mailinglist/%s Archive Home\n", mailinglistMailbox.CertInfo.mailbox)

	conn.Write([]byte(builder.String()))
}

func MailingListThreadPage(conn tlsconn, serverContext *ServerContext, mailinglistMailbox *MailBox, viewerCert *x509.Certificate, viewerCertInfo CertInfo, subject string, participantsHash string) {
	thread, exists := mailinglistMailbox.Gbox.GetThreadWithSubjectAndParticipantsHash(subject, participantsHash)
	if !exists {
		responseNotFound := "51 Thread not found.\r\n"
		conn.Write([]byte(responseNotFound))
		return
	}

	conn.Write([]byte("20 text/gemini\r\n"))

	var builder strings.Builder
	fmt.Fprintf(&builder, "# Thread: %s\n\n", subject)

	fmt.Fprintf(&builder, "=> /mailinglist/%s Archive Home\n", mailinglistMailbox.CertInfo.mailbox)
	fmt.Fprintf(&builder, "=> misfin:%s?%s Reply to Thread\n", mailinglistMailbox.CertInfo.mailAddress, url.PathEscape("# "+subject+"\n\n"))

	fmt.Fprintf(&builder, "\n---\n\n")

	//for i := len(thread.Mails) - 1; i >= 0; i-- {
	for i := 0; i < len(thread.Mails); i++ {
		gmail := thread.Mails[i]

		lastSender := gmail.Senders[0]
		originalSender := lastSender
		if len(gmail.Senders) >= 3 {
			originalSender = gmail.Senders[len(gmail.Senders)-1]
			secondSender := gmail.Senders[len(gmail.Senders)-2] // First forwarder

			fmt.Fprintf(&builder, "=> misfin:%s From: %s (%s)\n", originalSender.Address, originalSender.Address, originalSender.Blurb)
			fmt.Fprintf(&builder, "First Forwarder: %s (%s)\n", secondSender.Address, secondSender.Blurb)
			fmt.Fprintf(&builder, "Last Forwarder: %s (%s)\n", lastSender.Address, lastSender.Blurb)
		} else if len(gmail.Senders) == 2 {
			originalSender = gmail.Senders[len(gmail.Senders)-1]

			fmt.Fprintf(&builder, "=> misfin:%s From: %s (%s)\n", originalSender.Address, originalSender.Address, originalSender.Blurb)
			fmt.Fprintf(&builder, "Forwarder: %s (%s)\n", lastSender.Address, lastSender.Blurb)
		} else {
			fmt.Fprintf(&builder, "=> misfin:%s From: %s (%s)\n", lastSender.Address, lastSender.Address, lastSender.Blurb)
		}

		if len(gmail.Timestamps) > 0 {
			fmt.Fprintf(&builder, "Received on %s\n", gmail.Timestamps[0].Value.Format(time.DateTime))
		}

		if len(gmail.Receivers) > 0 {
			fmt.Fprintf(&builder, "Recipients: ")
			for r := range gmail.Receivers {
				fmt.Fprintf(&builder, "%s ", r)
			}
			fmt.Fprintf(&builder, "\n")
		}

		fmt.Fprintf(&builder, "\n")
		first := true
		for _, l := range gmail.GemtextLines {
			if first && l.String() == "" {
				continue
			}
			fmt.Fprintf(&builder, "%s\n", l.String())
			first = false
		}

		// Navigation Footer (Delete link for admins of mailinglist only)
		fmt.Fprintf(&builder, "\n")
		if viewerCert != nil {
			userMailbox := serverContext.mailboxes.Get(viewerCertInfo.mailbox)
			sub, subscribed := mailinglistMailbox.List.Subscribers[viewerCertInfo.mailAddress]
			if (subscribed && sub.T == MailboxListType_Mailinglist_Admin) || userMailbox.T == MailboxListType_Admin {
				fmt.Fprintf(&builder, "=> /mailinglist/%s/msg/%s/delete Delete Message from Archive\n", mailinglistMailbox.CertInfo.mailbox, gmail.ID)
			}
		}

		fmt.Fprintf(&builder, "\n---\n\n")
	}

	conn.Write([]byte(builder.String()))
}

// Make sure not to call this if the viewerCert is not an admin of the mailinglist
func MailingListMsgDelete(conn tlsconn, serverContext *ServerContext, mailinglistMailbox *MailBox, viewerCert *x509.Certificate, viewerCertInfo CertInfo, msgID string, ip string) {
	// Make sure viewer is logged in at least and is admin of mailinglist
	if viewerCert == nil {
		conn.Write([]byte("61 Not authorized.\r\n"))
		return
	}
	userMailbox := serverContext.mailboxes.Get(viewerCertInfo.mailbox)
	sub, subscribed := mailinglistMailbox.List.Subscribers[viewerCertInfo.mailAddress]
	if (subscribed && sub.T != MailboxListType_Mailinglist_Admin) || (!subscribed && userMailbox.T != MailboxListType_Admin) {
		conn.Write([]byte("61 Not authorized.\r\n"))
		return
	}

	// Remove the mail and save the gembox
	mailinglistMailbox.Mutex.Lock()
	mailinglistMailbox.Gbox.RemoveGemMailByID(msgID)
	mailinglistMailbox.Mutex.Unlock()
	serverContext.saveChan <- mailinglistMailbox.CertInfo.mailbox

	// Redirect back to mailinglist homepage
	serverContext.IPRateLimit_ExpectRedirectPath(ip, fmt.Sprintf("/mailinglist/%s", mailinglistMailbox.CertInfo.mailbox))
	conn.Write([]byte("30 /mailinglist/"))
	conn.Write([]byte(mailinglistMailbox.CertInfo.mailbox))
	conn.Write([]byte("\r\n"))
}

func AdminRoot(conn tlsconn, serverContext *ServerContext, viewerCert *x509.Certificate, viewerCertInfo CertInfo, ip string) {
	// Make sure viewer is server admin
	if viewerCert == nil {
		conn.Write([]byte("61 Not authorized.\r\n"))
		return
	}
	userMailbox := serverContext.mailboxes.Get(viewerCertInfo.mailbox)
	if userMailbox.T != MailboxListType_Admin && userMailbox.T != MailboxListType_Root {
		conn.Write([]byte("61 Not authorized.\r\n"))
		return
	}
	conn.Write([]byte("20 text/gemini\r\n"))

	var builder strings.Builder
	builder.WriteString("# Admin\n\n")
	builder.WriteString("=> /admin/users User Mailboxes\n")
	builder.WriteString("=> /admin/register Register New Mailbox\n")
	builder.WriteString("=> /admin/mailinglists Mailinglists & Newsletters\n")
	builder.WriteString("=> /admin/create_mailinglist Create New Mailinglist\n")
	builder.WriteString("=> /admin/newsfin_groups Newsfin Groups\n")
	builder.WriteString("=> /admin/create_newsfin_group Create New Newsfin Group\n")

	builder.WriteString("## Mailinglists & Newsletters\n")

	builder.WriteString("## Newsfin Groups\n")

	// General misfin-server settings: bind-address, rate-limit-duration, rate-limit-duration-members, max-concurrent-connections, gemini-user-registration-permission, gemini-permission,
	// Create new mailinglist/user/newsletter
	// List of users w/ links to user pages, w/ settings on user pages: type
	// List of mailinglists w/ links to mailinglist pages: send, subs-function, archive-view-permission, description
	// Mailinglist members permissions: type, receive-replies, exclude labels
	// Way to update server
}
