package main

import (
	"fmt"
	"path"
	"strings"
	"sync/atomic"

	"github.com/gammazero/deque"
)

// Uses a tree to store routes, and matches using a breadth-first search algorithm.
type Router struct {
	routes          RouteNode
	NotFoundHandler RequestHandler
}

type RouteListItem struct {
	route string
	node  *RouteNode
}

// Flattens routes tree into a list of routes. Returns an array of strings.
func (router *Router) GetRoutesList() []RouteListItem {
	return constructRouteList("/", &router.routes)
}

func constructRouteList(s string, node *RouteNode) []RouteListItem {
	currentPath := path.Join(s, node.Component)
	result := make([]RouteListItem, 0, 5)
	if node.Handler != nil {
		result = append(result, RouteListItem{currentPath, node})
	}

	current := node.Children
	for current != nil {
		list := constructRouteList(currentPath, current)
		result = append(result, list...)
		current = current.Next
	}

	return result
}

func (router *Router) PrintRouteTree() {
	PrintRoute(&router.routes, 0)
}

func PrintRoute(node *RouteNode, indent int) {
	for i := 0; i < indent; i++ {
		fmt.Print(" ")
	}
	fmt.Printf("%s/", node.Component)
	if node.Handler != nil {
		fmt.Printf(" [with Handler %p]", node.Handler)
	}
	if node.servePath != "" {
		fmt.Printf(" serving %s", node.servePath)
	}
	fmt.Printf("\n")
	currentChild := node.Children
	for currentChild != nil {
		PrintRoute(currentChild, indent+2)
		currentChild = currentChild.Next
	}
}

// TODO: Handle paths that end in "index.gmi" for gemini or "index" for nex?
func (router *Router) addRoute(p string, handler RequestHandler, servePath string, proxyRoute string, gopherItemType rune) {
	p = path.Clean("/" + p)
	parts := strings.Split(p, "/")
	if parts[0] == "" {
		parts = parts[1:]
	}

	// Check if root
	if parts[0] == "" && len(parts) == 1 {
		if router.routes.Handler == nil {
			router.routes.Handler = handler
			router.routes.servePath = servePath
		}
	}

	//currentParent := &router.routes
	currentNode := &router.routes
	for i, component := range parts {
		if component == "" || component == ".." || component == "." { // TODO
			continue
		}

		param := strings.HasPrefix(component, ":")

		// Check if component already exists in children, and use that
		searchedNode := currentNode.searchChild_DoNotMatchParam(component)
		if searchedNode != nil {
			if i == len(parts)-1 {
				// Attach handler to it if last component of p
				searchedNode.Handler = handler
				searchedNode.servePath = servePath
				searchedNode.proxyRoute = proxyRoute
				searchedNode.gopherItemType = gopherItemType

				// Attach handler to parent as well
				if component == "*" && currentNode.Handler == nil {
					currentNode.Handler = handler
					currentNode.servePath = servePath
					currentNode.proxyRoute = proxyRoute
					currentNode.gopherItemType = gopherItemType
				}
			}

			currentNode = searchedNode
		} else {
			// Otherwise, create the child and add it
			// Note: "Composite Literals" are created on the heap in golang, apparently.
			var child *RouteNode
			if i == len(parts)-1 {
				child = &RouteNode{component, handler, servePath, nil, nil, nil, param, proxyRoute, gopherItemType, false, atomic.Int64{}, atomic.Int64{}}
				// Attach handler to parent as well
				if component == "*" && currentNode.Handler == nil {
					currentNode.Handler = handler
					currentNode.servePath = servePath
					currentNode.proxyRoute = proxyRoute
					currentNode.gopherItemType = gopherItemType
				}
			} else {
				child = &RouteNode{component, nil, servePath, nil, nil, nil, param, proxyRoute, gopherItemType, false, atomic.Int64{}, atomic.Int64{}}
			}
			currentNode.addChild(child)
			//currentParent = currentNode
			currentNode = child
		}
	}
}

// Uses breadth-first search. Returns node, globString (if exists), and params map
// TODO: Make sure to collect list of params
// TODO: Differentiate between "/blah/" and "/blah"?
func (router *Router) Search(p string) (*RouteNode, string, map[string]string) {
	globDirectory := false
	if strings.HasSuffix(p, "/") && p != "/" {
		globDirectory = true
	}

	p = path.Clean("/" + p)
	parts := strings.Split(p, "/") // TODO: Handle empty part at end.
	if parts[0] == "" {
		parts = parts[1:]
	}

	//fmt.Printf("Parts: %#v\n", parts)

	type SearchInfo struct {
		globStartIndex int
		*RouteNode
	}

	var q *deque.Deque[SearchInfo] = deque.New[SearchInfo]() // TODO
	q.PushBack(SearchInfo{0, &router.routes})

	// Check if root
	if parts[0] == "" && len(parts) == 1 {
		return q.PopFront().RouteNode, "", nil
	}

	// TODO: Get map of params
	//globStartIndex := len(parts)
	params := make(map[string]string, 7)
	for i, component := range parts {
		//fmt.Printf("Checking Component #%d: %s\n", i, component)
		initialNum := q.Len()
		if q.Len() == 0 {
			// TODO: No matches?
			return nil, "", nil
		}

		// First child that has a handler and first child that has a handler and no param are tracked so that if this is the last component of the path we are searching for, they are returned.
		var firstChildHandler *RouteNode
		var firstChildHandlerNotParam *RouteNode
		for j := 0; j < initialNum; j++ {
			current := q.PopFront()

			// TODO: Push back glob in case we're not at last component
			if current.Children == nil && current.Component == "*" {
				q.PushBack(current)
			}

			children := current.searchChildren(component)
			for _, child := range children {
				globStartIndex := len(parts)
				if child.Component == "*" {
					globStartIndex = i
					//fmt.Printf("Found Glob at %d\n", globStartIndex)
				}
				q.PushBack(SearchInfo{globStartIndex, child})
				//fmt.Printf("Pushed child: %s\n", child.Component)
				if child.Handler != nil && firstChildHandler == nil {
					firstChildHandler = child
				}
				if child.param {
					params[strings.TrimPrefix(child.Component, ":")] = component // TODO: This is hacky.
				}
				if !child.param {
					if child.Handler != nil && firstChildHandlerNotParam == nil {
						firstChildHandlerNotParam = child
					}
				}
			}
		}

		// Check for first match that has a handler and that's not a param. Otherwise, use first match that has a handler.
		if i == len(parts)-1 {
			// Construct globString
			/*globString := path.Join(parts[globStartIndex:]...)
			if globString != "" && globDirectory {
				globString = globString + "/"
			}*/

			// Last path component
			if q.Len() == 0 {
				// TODO: No matches?
				return nil, "", nil
			} else {
				// Return match that meets the first/earliest of these requirements: 1. Exact match with no globs and not a param, 2. Param match with no globs, 3. highest glob start with no param, 4. highest glob start with param, 5. first match
				firstMatch := SearchInfo{0, nil}
				firstMatch_NoGlobNotParam := SearchInfo{0, nil}
				firstMatch_NoGlob := SearchInfo{0, nil}
				highestGlobStartIndex := -1
				matchHighestGlobStartNotParam := SearchInfo{0, nil}
				matchHighestGlobStart := SearchInfo{0, nil}

				for q.Len() != 0 {
					currentMatch := q.PopFront()
					if currentMatch.Handler == nil {
						continue
					}
					if firstMatch.RouteNode == nil {
						firstMatch = currentMatch
					}
					if currentMatch.globStartIndex > highestGlobStartIndex && currentMatch.globStartIndex != len(parts) {
						highestGlobStartIndex = currentMatch.globStartIndex
						matchHighestGlobStart = currentMatch
						if currentMatch.matchNotParam(component) {
							matchHighestGlobStartNotParam = currentMatch
						}
					} else if currentMatch.globStartIndex == len(parts) { // if no glob
						if firstMatch_NoGlobNotParam.RouteNode == nil && currentMatch.matchNotParam(component) {
							firstMatch_NoGlobNotParam = currentMatch
						}
						if firstMatch_NoGlob.RouteNode == nil {
							firstMatch_NoGlob = currentMatch
						}
					}
				}
				match := firstMatch
				if firstMatch_NoGlobNotParam.RouteNode != nil { // Exact match with no globs or params
					match = firstMatch_NoGlobNotParam
				} else if firstMatch_NoGlob.RouteNode != nil {
					match = firstMatch_NoGlob
				} else if matchHighestGlobStartNotParam.RouteNode != nil {
					match = matchHighestGlobStartNotParam
				} else if matchHighestGlobStart.RouteNode != nil {
					match = matchHighestGlobStart
				}

				globString := path.Join(parts[match.globStartIndex:]...)
				if globString != "" && globDirectory {
					globString = globString + "/"
				}
				return match.RouteNode, globString, params
			}
		}
	}

	return nil, "", nil
}

type RouteNode struct {
	Component string         // If equal to "*", then it's a blob and matches with everything.
	Handler   RequestHandler // If nil, no handler
	servePath string
	Parent    *RouteNode
	Children  *RouteNode // Linked List of Children

	// Linked List Info
	Next *RouteNode

	// Other Info
	param              bool // Param Type
	proxyRoute         string
	gopherItemType     rune         // Used for gopher routes and gopher proxies
	via_config         bool         // Set to true if loaded from a routes config list.
	visits             atomic.Int64 // How many times the route has been visited
	currentConnections atomic.Int64 // Number of currently-running connections to route
}

// Returns if component is a match.
func (node *RouteNode) match(component string) bool {
	if node.param { // TODO: Handle param type?
		return true
	}
	if node.Component == "*" {
		return true
	}
	return node.Component == component
}

// Returns if component is a match.
func (node *RouteNode) matchNotParam(component string) bool {
	return node.Component == component
}

// Searches the Children Linked List of a node.
func (node *RouteNode) searchChild(component string) *RouteNode {
	if node.Children == nil {
		return nil
	}
	current := node.Children
	for {
		if current.match(component) {
			return current
		}

		if current.Next == nil {
			break
		} else {
			current = current.Next
		}
	}

	return nil
}

// Searches the Children Linked List of a node. Doesn't match params, which is used when adding routes so that named routes don't get combined into param routes.
func (node *RouteNode) searchChild_DoNotMatchParam(component string) *RouteNode {
	if node.Children == nil {
		return nil
	}
	current := node.Children
	for {
		if current.matchNotParam(component) {
			return current
		}

		if current.Next == nil {
			break
		} else {
			current = current.Next
		}
	}

	return nil
}

// Returns array of all matches from the Children Linked List
func (node *RouteNode) searchChildren(component string) []*RouteNode {
	if node.Children == nil {
		return nil
	}

	result := make([]*RouteNode, 0, 5)
	current := node.Children
	for {
		if current.match(component) {
			// Add to list
			result = append(result, current)
		}

		if current.Next == nil {
			break
		} else {
			current = current.Next
		}
	}

	return result
}

// Adds a child to the end of the linked list
func (node *RouteNode) addChild(child *RouteNode) { // TODO: Don't add if component already exists in children?
	child.Parent = node
	if node.Children == nil {
		node.Children = child
		return
	}
	current := node.Children
	for current.Next != nil {
		current = current.Next
	}

	current.Next = child
}

func (node *RouteNode) ChildrenLength() int {
	result := 0
	current := node.Children
	for current != nil {
		result += 1
		current = current.Next
	}
	return result
}

// Gets array of route nodes from root to last component
func (node *RouteNode) GetRouteAncestry() []*RouteNode {
	ancestry := make([]*RouteNode, 0, 5)
	ancestry = append(ancestry, node)
	current := node.Parent
	for current != nil {
		ancestry = append(ancestry, current)
		current = current.Parent
	}

	return ancestry
}

// Gets the route path string
func (node *RouteNode) GetRoutePath() string {
	p := node.Component
	current := node.Parent
	for current != nil {
		p = path.Join(current.Component, p)
		current = current.Parent
	}

	return p
}
