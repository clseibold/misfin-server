package main

import (
	"bufio"
	"bytes"
	"context"
	"crypto"
	"crypto/sha256"
	"crypto/tls"
	"crypto/x509"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"net"
	"net/url"
	"os"
	"path/filepath"
	"runtime"
	"runtime/debug"
	"strconv"
	"strings"
	"sync"
	"time"

	cmap "github.com/orcaman/concurrent-map/v2"
	"gitlab.com/clseibold/misfin-server/gemmail"
	"golang.org/x/sync/semaphore"
)

// tcpKeepAliveListener sets TCP keep-alive timeouts on accepted
// connections. It's used by Run so dead TCP connections (e.g.
// closing laptop mid-download) eventually go away.
type tcpKeepAliveListener struct {
	*net.TCPListener
}

func (ln tcpKeepAliveListener) Accept() (c net.Conn, err error) {
	if c, err = ln.AcceptTCP(); err != nil {
		return
	} else if err = c.(*net.TCPConn).SetKeepAlive(true); err != nil {
		return
	}
	// Ignore error from setting the KeepAlivePeriod as some systems, such as
	// OpenBSD, do not support setting TCP_USER_TIMEOUT on IPPROTO_TCP
	_ = c.(*net.TCPConn).SetKeepAlivePeriod(3 * time.Minute)

	return
}

func loadTOFUMap(serverContext *ServerContext) error {
	file, readErr := os.ReadFile(filepath.Join(serverContext.config.MailboxDirectory, "tofu_store.list"))
	if readErr != nil {
		return readErr
	}
	lines := strings.FieldsFunc(string(file), func(r rune) bool { return r == '\n' })
	for _, line := range lines {
		parts := strings.SplitN(line, ":", 2)
		if len(parts) == 2 {
			address := strings.TrimSpace(parts[0])
			fingerprint := strings.TrimSpace(parts[1])

			if address != "" && fingerprint != "" {
				serverContext.TOFUMap[address] = fingerprint
			}
		}
	}

	return nil
}

func saveTOFUMap(serverContext *ServerContext) {
	var builder strings.Builder

	for address, fingerprint := range serverContext.TOFUMap {
		builder.WriteString(address)
		builder.WriteString(": ")
		builder.WriteString(fingerprint)
		builder.WriteString("\n")
	}

	writeErr := os.WriteFile(filepath.Join(serverContext.config.MailboxDirectory, "tofu_store.list"), []byte(builder.String()), 0660)
	if writeErr != nil {
		panic(writeErr)
	}
}

type RateLimitListItem struct {
	prevTime     time.Time
	redirectPath string
}

type ServerContext struct {
	config                *Config
	mailboxes             MailBoxMap
	TOFUMap               map[string]string
	serverCertificate     *x509.Certificate
	serverPrivateKey      crypto.PrivateKey
	serverPrivateKeyBytes []byte
	serverCertInfo        CertInfo
	ipBlocklist           cmap.ConcurrentMap[string, struct{}]
	ipRateLimitList       cmap.ConcurrentMap[string, RateLimitListItem]

	saveChan chan string
}

func (context *ServerContext) Hostname() string {
	return context.serverCertInfo.domains[0]
}

func (context *ServerContext) Port() string {
	return context.config.Port
}

func initServerContext(config *Config) (*ServerContext, error) {
	doSaveMailboxes := false
	// 0.5.10c changed the "send" mailinglist permission to "send-thread", so save this change on upgrade
	if config.PrevVersion == "0.5.9c" {
		doSaveMailboxes = true
	}
	if config.PrevVersion != Version {
		fmt.Printf("Upgrading from version %s to version %s.\n", config.PrevVersion, Version)
		saveConfig(*config)
	}

	mailboxes, loadErr := loadMailboxes(*config)
	if loadErr != nil {
		return nil, loadErr
	}
	if doSaveMailboxes {
		mailboxes.saveMailboxes()
	}

	certificate, privateKey, privateKey_bytes := getCertsFromPem(config.ServerCertFilename)
	serverCertInfo := getCertInfo(certificate)

	TOFUMap := make(map[string]string)
	serverContext := &ServerContext{config, mailboxes, TOFUMap, certificate, privateKey, privateKey_bytes, serverCertInfo, cmap.New[struct{}](), cmap.New[RateLimitListItem](), make(chan string, mailboxes.Length()*2)}
	loadTOFUMap(serverContext)
	return serverContext, nil
}

// IP Banning Functions

func (context *ServerContext) BlockIP(ip string) {
	context.ipBlocklist.Set(ip, struct{}{})
}
func (context *ServerContext) AllowIP(ip string) {
	context.ipBlocklist.Remove(ip)
}
func (context *ServerContext) IsIPBlocked(ip string) bool {
	return context.ipBlocklist.Has(ip)
}

// Rate Limiting Functions

// If not rate-limited, set ip to current time and return false. If last access time is within duration, set ip to current time and return true. Otherwise, set ip to current time and return false
func (s *ServerContext) IsIPRateLimited(ip string, isServerMember bool) bool {
	hasher := sha256.New()
	hasher.Write([]byte(ip))
	ipHash := hex.EncodeToString(hasher.Sum(nil))
	currentTime := time.Now().UTC()

	// If Client Cert is from a Server-Member, reduce the rate-limit restrictions
	rateLimitDuration := s.config.RateLimitDuration
	if isServerMember {
		rateLimitDuration = s.config.RateLimitDuration_Member
	}

	if rl, has := s.ipRateLimitList.Get(ipHash); has {
		if currentTime.After(rl.prevTime.Add(rateLimitDuration)) {
			// Outside of duration, no rate limit
			s.ipRateLimitList.Set(ipHash, RateLimitListItem{currentTime, rl.redirectPath})
			return false
		} else {
			// Rate limited, require ip to wait 1 whole second before requesting again by setting the ip's "previous access time" to the future
			//delta := time.Second - s.RateLimitDuration
			s.ipRateLimitList.Set(ipHash, RateLimitListItem{currentTime, rl.redirectPath})
			return true
		}
	} else {
		s.ipRateLimitList.Set(ipHash, RateLimitListItem{currentTime, ""})
		return false
	}
}

func (s *ServerContext) IPRateLimit_IsExpectingRedirect(ip string) bool {
	hasher := sha256.New()
	hasher.Write([]byte(ip))
	ipHash := hex.EncodeToString(hasher.Sum(nil))

	if rl, has := s.ipRateLimitList.Get(ipHash); has {
		if rl.redirectPath != "" {
			return true
		}
	}
	return false
}
func (s *ServerContext) IpRateLimit_GetExpectedRedirectPath(ip string) string {
	hasher := sha256.New()
	hasher.Write([]byte(ip))
	ipHash := hex.EncodeToString(hasher.Sum(nil))

	if rl, has := s.ipRateLimitList.Get(ipHash); has {
		return rl.redirectPath
	}
	return ""
}

// When a redirection occurs, let server know to expect another request immediately following the previous one so that the rate-limiting can be bypassed
func (s *ServerContext) IPRateLimit_ExpectRedirectPath(ip string, redirectPath string) {
	hasher := sha256.New()
	hasher.Write([]byte(ip))
	ipHash := hex.EncodeToString(hasher.Sum(nil))

	if rl, has := s.ipRateLimitList.Get(ipHash); has {
		s.ipRateLimitList.Set(ipHash, RateLimitListItem{rl.prevTime, redirectPath})
	}
}
func (s *ServerContext) IPRateLimit_ClearRedirectPath(ip string) {
	hasher := sha256.New()
	hasher.Write([]byte(ip))
	ipHash := hex.EncodeToString(hasher.Sum(nil))

	if rl, has := s.ipRateLimitList.Get(ipHash); has {
		s.ipRateLimitList.Set(ipHash, RateLimitListItem{rl.prevTime, ""})
	}
}

// Removes all IPs that are passed the rate-limit duration.
// There's no need to call this too often, but do not wait so long that the ip list becomes too big.
func (s *ServerContext) CleanupRateLimiting() {
	fmt.Printf("[Misfin-Server] Cleaning up rate limiting map\n")
	currentTime := time.Now().UTC()
	items := s.ipRateLimitList.IterBuffered()
	for item := range items {
		prevTime := item.Val.prevTime
		redirectPath := item.Val.redirectPath
		if currentTime.After(prevTime.Add(s.config.RateLimitDuration)) && currentTime.After(prevTime.Add(s.config.RateLimitDuration_Member)) && redirectPath == "" {
			s.ipRateLimitList.Remove(item.Key)
		}
	}
}

// Starts a goroutine that cleans up the ipRateLimitList map every 6 hours.
func (s *ServerContext) startRateLimitingCleanerLoop() {
	timer := time.NewTicker(time.Hour * 6)
	go func() {
		for {
			<-timer.C
			s.CleanupRateLimiting()
		}
	}()
}

func RunServer(_config Config) {
	serverContext, err := initServerContext(&_config)
	if err != nil {
		panic(err)
	}

	/*if checkServerAlreadyRunning(config) {
		fmt.Printf("Server using configuration file '%s' is already running. Please stop the server first.\n", config.ConfigFilename)
		os.Exit(1)
	}

	// Create lockfile to know when server is running
	lockfile := filepath.Join(config.MailboxDirectory, "server.running")
	os.WriteFile(lockfile, []byte{}, 0660)*/

	// Print banner
	fmt.Printf("----- Server Certificate Info (%s) -----\n", serverContext.config.ServerCertFilename)
	fmt.Printf("Misfin-Server Version: %s\n", Version)
	fmt.Printf("Common Name: %s\n", serverContext.serverCertInfo.blurb)
	fmt.Printf("Domains: %v\n", serverContext.serverCertInfo.domains)
	fmt.Printf("Mailbox: %s\n", serverContext.serverCertInfo.mailbox)
	fmt.Printf("Fingerprint: %s\n", serverContext.serverCertInfo.fingerprint)
	fmt.Printf("Receiving for %s (%s), bound to %s\n", serverContext.serverCertInfo.blurb, serverContext.serverCertInfo.mailAddress, serverContext.config.BindAddress)

	// Rate Limiting Cleaner Loop and Gembox Save handler
	serverContext.startRateLimitingCleanerLoop()
	go gemboxSaveHandler(serverContext)

	// Setup TLS Config and Listener
	tlsConfig := &tls.Config{
		MinVersion: tls.VersionTLS12,
		ClientAuth: tls.RequestClientCert,
	}
	tlsConfig.Certificates = make([]tls.Certificate, 1)
	defaultCert := tls.Certificate{Certificate: [][]byte{serverContext.serverCertificate.Raw}, PrivateKey: serverContext.serverPrivateKey, Leaf: serverContext.serverCertificate}
	tlsConfig.Certificates[0] = defaultCert

	// SNI Handling
	strictSni := true
	tlsConfig.GetCertificate = func(chi *tls.ClientHelloInfo) (*tls.Certificate, error) {
		if chi.ServerName == serverContext.serverCertInfo.domains[0] || chi.ServerName == serverContext.serverCertInfo.domains[0]+":1958" {
			return &defaultCert, nil
		} else if strictSni {
			return nil, fmt.Errorf("strict SNI enabled - No certificate found for domain: %q, closing connection", chi.ServerName)
		} else {
			return &defaultCert, nil
		}
	}

	// Support logging TLS keys for debugging
	keylogfile := os.Getenv("SSLKEYLOGFILE")
	if keylogfile != "" {
		w, err := os.OpenFile(keylogfile, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0660)
		if err == nil {
			tlsConfig.KeyLogWriter = w
			defer w.Close()
		}
	}

	// TODO: tcp4 and tcp6 options, also the bind address for ipv6 is different (especially if trying to listen on all interfaces with ipv6; should be something like `[::1]`?)
	listener, listenErr := net.Listen("tcp", serverContext.config.BindAddress+":"+serverContext.config.Port)
	if listenErr != nil {
		panic(listenErr)
	}

	keepAliveListener := &tcpKeepAliveListener{listener.(*net.TCPListener)}
	tlsListener := tls.NewListener(keepAliveListener, tlsConfig)
	defer tlsListener.Close()

	// Loop to accept incoming connections
	var bufpool sync.Pool
	bufpool.New = func() interface{} { return bufio.NewReaderSize(nil, 2048) }
	MaxConcurrentConnections := 2000
	connChan := make(chan *tls.Conn, MaxConcurrentConnections)

	var sem *semaphore.Weighted
	if MaxConcurrentConnections == -1 { // No Limit - semaphores released almost as soon as they are acquired
		sem = semaphore.NewWeighted(int64(runtime.GOMAXPROCS(0)))
	} else {
		sem = semaphore.NewWeighted(int64(MaxConcurrentConnections))
	}

	acceptLoop := func() {
		for {
			// Wait until there's an open space for the connection
			sem.Acquire(context.Background(), 1)
			conn, err := tlsListener.Accept() // NOTE: Blocking (unless listener is closed)
			if errors.Is(err, net.ErrClosed) {
				sem.Release(1)
				break
			} else if err != nil {
				fmt.Printf("accept error: %v\n", err)
				sem.Release(1)
				continue
			}

			ip, _, _ := net.SplitHostPort(conn.RemoteAddr().String())
			if serverContext.IsIPBlocked(ip) {
				conn.Close()
				sem.Release(1)
				continue
			}

			tc, ok := conn.(*tls.Conn)
			if !ok {
				fmt.Printf("non-tls connection")
				conn.Close()
				sem.Release(1)
				continue
			}

			// Manually do handshake here for now.
			tc.Handshake()

			// If not expecting redirect, do rate limiting here. Otherwise, wait to do rate limiting until we have read the header of the request (which occurs in the goroutine for the connection)
			if !serverContext.IPRateLimit_IsExpectingRedirect(ip) {
				isServerMember := false
				tlsState := tc.ConnectionState()
				if len(tlsState.PeerCertificates) > 0 {
					senderCert := tlsState.PeerCertificates[0]
					senderCertInfo := getCertInfo(senderCert)
					if senderFromThisServer(serverContext, senderCertInfo) {
						isServerMember = true
					}
				}

				if serverContext.IsIPRateLimited(ip, isServerMember) {
					// Rate limited, return a 44 slow down error
					conn.Write([]byte("44 Rate limited.\r\n"))
					conn.Close()
					sem.Release(1)
					continue
				}
			}

			// Send the connection to the main loop
			connChan <- tc
		}
	}

	go acceptLoop()

	//mainloop:
	for {
		var tlsConn *tls.Conn
		//ok := true
		received := 0
		//hasConn := false

		/*select {
		case _, ok = <-s.msg:
			received++
		default:
		}*/
		select {
		case tlsConn = <-connChan:
			received++
			//hasConn = true
		default:
		}

		// Haven't received anything, block the loop until one is received
		if received == 0 {
			select {
			//case _, ok = <-s.msg:
			case tlsConn = <-connChan:
				//hasConn = true
			}
		}
		// If need to shut down
		/*if !ok {
			fmt.Printf("[Nex:%s] Shutting down.\n", s.Name)
			if hasConn {
				tlsConn.Close()
			}
			tlsListener.Close()
			break mainloop
		}*/

		// If no max number of concurrent connections specified, then release 1 from the semaphor before the goroutine starts
		if MaxConcurrentConnections == -1 {
			sem.Release(1)
		}

		// NOTE: handshake happens on first Write to the connection (tc)
		go handleRequest(tlsConn, &bufpool, serverContext, time.Minute*1, 0)
	}
}

// Handles saving gbox whenever new messages come in
func gemboxSaveHandler(serverContext *ServerContext) {
	for {
		identifier := <-serverContext.saveChan
		if identifier == "tofu" {
			saveTOFUMap(serverContext)
		} else {
			mailbox := serverContext.mailboxes.Get(identifier)
			if mailbox != nil {
				mailbox.SaveGemBox()
			}
		}
	}
}

// tlsconn wraps every necessary method from *tls.Conn, so it can be stubbed.
type tlsconn interface {
	net.Conn
	ConnectionState() tls.ConnectionState
}

// Set readTimeout to 0 for no timeout
// TODO: Split this into handleMisfinRequest, handleGeminiRequest, and handleTitanRequest
func handleRequest(conn tlsconn, bufpool *sync.Pool, serverContext *ServerContext, readTimeout time.Duration, writeTimeout time.Duration) {
	defer conn.Close()

	defer func() {
		if r := recover(); r != nil {
			panicString := ""
			switch r := r.(type) {
			case string:
				panicString = r
			}

			panicString = panicString + "\n\n" + string(debug.Stack())
			panicString = strings.ReplaceAll(panicString, "\n```", "\n\\```")
			if strings.HasPrefix(panicString, "```") {
				panicString = "\\" + panicString
			}

			fromMailbox := serverContext.mailboxes.rootMailbox
			if fromMailbox == nil {
				return
			}
			gmail := gemmail.CreateGemMailFromBody("# Misfin Server Error\n```\n" + panicString + "\n```\n")

			for _, m := range serverContext.mailboxes.mailboxes {
				if m.T == MailboxListType_Admin { // TODO: Add maintainers here too
					clonedGmail := gmail.Clone(false)
					clonedGmail.PrependSender(fromMailbox.CertInfo.mailAddress, fromMailbox.CertInfo.blurb)
					clonedGmail.PrependTimestamp(time.Now().UTC())
					clonedGmail.AddTag("Unread")
					clonedGmail.AddTag("Inbox")
					clonedGmail.SetID()

					m.Mutex.Lock()
					serverContext.mailboxes.Get(m.CertInfo.mailbox).Gbox.AppendGemMail(clonedGmail)
					serverContext.saveChan <- m.CertInfo.mailbox
					m.Mutex.Unlock()
				}
			}
		}
	}()

	if readTimeout != 0 {
		err := conn.SetReadDeadline(time.Now().Add(readTimeout))
		if err != nil {
			fmt.Printf("Could not set socket read deadline; %v\n", err)
			// NOTE: Don't return here, just continue with the request
		}
	}

	reader := bufpool.Get().(*bufio.Reader)
	defer bufpool.Put(reader)
	reader.Reset(conn)

	// Read the request
	var request []byte
	var overflow_Misfin bool
	var requestHeaderOverflow bool

	delim := []byte("\r\n")
	delim_misfinB := []byte(" ")
	buf := make([]byte, 1)
	for {
		n, err := reader.Read(buf)
		if err == io.EOF && n <= 0 {
			// Error
			fmt.Printf("error reading request\n")
			responseBadURL := "59 Request invalid.\r\n"
			conn.Write([]byte(responseBadURL))
			return
		} else if err != nil && err != io.EOF {
			// Error
			fmt.Printf("unknown error reading request header; %v\n", err)
			return
		}

		request = append(request, buf...)
		// Parse request string until a CRLF (for gemini and titan) or a space (for misfin). In misfin, everything after the space is the metadata and message body, ending with another CRLF
		if bytes.HasSuffix(request, delim) {
			request = request[:len(request)-len(delim)]
			break
		} else if bytes.HasSuffix(request, delim_misfinB) {
			request = request[:len(request)-len(delim_misfinB)]
			break
		}

		if len(request) > 2048-2 {
			requestHeaderOverflow = true
			break
		}
	}

	// Check overflows
	if requestHeaderOverflow {
		fmt.Printf("request length too long\n")
		responseLengthTooLong := "59 Request length too long. Total length cannot be more than 1KB for Misfin(C)'s request header, 1KB for Gemini, and 2KB for Misfin(AB)'s request+metadata+message.\r\n"
		conn.Write([]byte(responseLengthTooLong))
		return
	}

	//fmt.Printf("request: %s\r\n", request)

	// Check for a tab, which will determine weather this request header is a Misfin(C) request or a Misfin(B) request.
	// Misfin C Format: misfin://<MAILBOX>@<HOSTNAME><TAB><CONTENT-LENGTH><CRLF><MESSAGE>
	// Misfin B Format: misfin://<MAILBOX>@<HOSTNAME><SPACE><MESSAGE><CRLF>
	requestHeader, contentLengthStr, isMisfinC := strings.Cut(string(request), "\t")

	// Get the URL
	URL, err := url.Parse(requestHeader)
	if err != nil {
		fmt.Printf("invalid request url: %s\n", err)
		responseBadURL := "59 URL invalid.\r\n"
		conn.Write([]byte(responseBadURL))
		return
	}
	if URL.Scheme == "" {
		URL.Scheme = "misfin"
	}

	// If expecting redirect, do rate-limiting now that we have read the header of the request to check against
	ip, _, _ := net.SplitHostPort(conn.RemoteAddr().String())
	if serverContext.IPRateLimit_IsExpectingRedirect(ip) {
		// Check if Server Member
		isServerMember := false
		tlsState := conn.ConnectionState()
		if len(tlsState.PeerCertificates) > 0 {
			senderCert := tlsState.PeerCertificates[0]
			senderCertInfo := getCertInfo(senderCert)
			if senderFromThisServer(serverContext, senderCertInfo) {
				isServerMember = true
			}
		}

		if serverContext.IpRateLimit_GetExpectedRedirectPath(ip) == URL.EscapedPath() {
			// Allow the redirect through and clear the expected redirect path
			serverContext.IPRateLimit_ClearRedirectPath(ip)
		} else if serverContext.IpRateLimit_GetExpectedRedirectPath(ip) == "$skylab" && strings.HasPrefix(URL.EscapedPath(), "/~skylab") {
			// Allow the redirect through and clear the expected redirect path
			serverContext.IPRateLimit_ClearRedirectPath(ip)
		} else if serverContext.IsIPRateLimited(ip, isServerMember) {
			// Rate limited, return a 44 slow down error
			conn.Write([]byte("44 Rate limited.\r\n"))
			return
		}
	}

	if URL.Scheme != "misfin" && URL.Scheme != "gemini" {
		fmt.Printf("non-misfin and non-gemini scheme: %s\n", requestHeader)
		responseBadSchema := "59 Scheme invalid. Please use the misfin or gemini schemes, depending on the type of request you are trying to send.\r\n"
		conn.Write([]byte(responseBadSchema))
		return
	} else if URL.Scheme == "gemini" {
		if serverContext.config.GeminiServerPermission == GeminiPermission_Disabled {
			fmt.Printf("invalid request url: %s\n", err)
			responseBadURL := "59 URL invalid: Gemini requests disabled.\r\n"
			conn.Write([]byte(responseBadURL))
			return
		}

		handleGeminiRequest(conn, URL, serverContext, writeTimeout, ip)
	} else if URL.Scheme == "misfin" {
		// Convert contentlength string to integer
		var contentLength int64
		if isMisfinC {
			var parseErr error
			contentLength, parseErr = strconv.ParseInt(contentLengthStr, 10, 64)
			if parseErr != nil {
				fmt.Printf("error reading request\n")
				responseBadURL := "59 Request invalid.\r\n"
				conn.Write([]byte(responseBadURL))
				return
			}
			if contentLength > 16384 {
				fmt.Printf("request length too long\n")
				responseLengthTooLong := "59 Request length too long. Total metadata+message length cannot be more than 16KB for Misfin(C), or 2KB for Misfin(AB).\r\n"
				conn.Write([]byte(responseLengthTooLong))
				return
			}
		}

		// Read the rest of the data (misfin metadata and message body)
		// Read until CRLF for misfin(B) or until content length for misfin(C)
		var messageBody []byte
		buf2 := make([]byte, 1)
		for {
			n, err := reader.Read(buf2)
			if err == io.EOF && n <= 0 {
				// Error
				fmt.Printf("error reading request\n")
				responseBadURL := "59 Request invalid.\r\n"
				conn.Write([]byte(responseBadURL))
				return
			} else if err != nil && err != io.EOF {
				// Error
				fmt.Printf("unknown error reading message body; %v\n", err)
				return
			}

			messageBody = append(messageBody, buf2...)
			// Read until a CRLF (delim) for Misfin(B)
			if !isMisfinC && bytes.HasSuffix(messageBody, delim) {
				messageBody = messageBody[:len(messageBody)-len(delim)]
				break
			} else if isMisfinC && int64(len(messageBody)) == contentLength { // Or the content length for Misfin(C)
				break
			}

			// Make sure size is not over 16KB
			// TODO: This size limit needs to exclude the metadata for Misfin(C)
			if (isMisfinC && (len(messageBody) > 16384)) || (!isMisfinC && (len(messageBody) > 2048-2)) {
				overflow_Misfin = true
				break
			}
		}

		if overflow_Misfin {
			fmt.Printf("request length too long\n")
			responseLengthTooLong := "59 Request length too long. Total metadata+message length cannot be more than 16KB for Misfin(C), or 2KB for Misfin(AB).\r\n"
			conn.Write([]byte(responseLengthTooLong))
			return
		}

		fmt.Printf("Address: %s\nContent Length: %s\n", requestHeader, contentLengthStr)

		/*tlsState := conn.ConnectionState()
		var senderCert *x509.Certificate = nil
		var senderCertInfo CertInfo
		if len(tlsState.PeerCertificates) > 1 {
			senderCert = tlsState.PeerCertificates[0]
			senderCertInfo = getCertInfo(senderCert)
		}

		// Set ProtocolType and get message header (and message body for misfin(B))
		requestType := ProtocolType_Misfin_B
		if isMisfinC {
			requestType = ProtocolType_Misfin_C
		} else {

		}
		gmail := gemmail.CreateGemMailFromBody("") // Switch this to ParseMetadata_MisfinC()
		request := Request{requestType, true, "", URL.String(), "", 16384, "", conn, make(map[string]string), -1, []byte{}, "", senderCert, senderCertInfo, ip, "", false, false, "", "", "", false, serverContext, gmail}
		*/

		handleMisfinRequest(conn, URL, isMisfinC, string(messageBody), serverContext, writeTimeout, ip)
	} else if URL.Scheme == "titan" {
		fmt.Printf("invalid request url: Titan not supported yet.\n")
		responseBadURL := "59 Request invalid: Titan not supported yet.\r\n"
		conn.Write([]byte(responseBadURL))
		return
	}

	// Check that URL is misfin scheme
}
