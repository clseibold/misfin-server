package main

import (
	"os"
	"strings"
	"time"

	cmap "github.com/orcaman/concurrent-map/v2"
)

type ResendInfo struct {
	to []string
}

// A file of addresses
type MailingList struct {
	Filepath              string
	CertificateFilepath   string
	Description           string
	Subscribers           map[string]MailboxListItem // Address -> MailboxListItem
	SendThreadPermission  MailingListSendPermission
	SendReplyPermission   MailingListSendPermission
	SubsFunction          MailingListSubsFunction
	ArchiveViewPermission MailingListArchiveViewPermission

	ToFederate cmap.ConcurrentMap[string, struct{}]

	//ResendMails map[string]ResendInfo
}

// Which people can send
type MailingListSendPermission int

const MailingListSend_Subs MailingListSendPermission = 0    // Only subs can send; this is the default
const MailingListSend_Open MailingListSendPermission = 1    // Anyone can send, but non-subs will only receive replies
const MailingListSend_Writers MailingListSendPermission = 2 // Only writers can send; this is useful for newsletters

type MailingListSubsFunction int

const MailingListSubs_AcceptAll MailingListSubsFunction = 0           // auto-accepts all sub requests; this is the default
const MailingListSubs_Closed MailingListSubsFunction = 1              // auto-denies all sub requests
const MailingListSubs_Prompt MailingListSubsFunction = 2              // prompts admin to accept sub requests
const MailingListSubs_AcceptServerMembers MailingListSubsFunction = 3 // auto-accepts all sub requests from mailbox members of this server

type MailingListArchiveViewPermission int

const MailingListArchiveViewPermission_Subs MailingListArchiveViewPermission = 0      // Only mailinglist subs can view mailinglist archive, this is the default
const MailingListArchiveViewPermission_Disabled MailingListArchiveViewPermission = 1  // Nobody can view mailinglist archive
const MailingListArchiveViewPermission_AdminOnly MailingListArchiveViewPermission = 2 // Only mailinglist admins can view mailinglist archive
const MailingListArchiveViewPermission_Open MailingListArchiveViewPermission = 3      // Everyone can view mailinglist archive (it's public)

// Pass this into a mailbox
func CreateMailingList(filename string, certFilepath string) MailingList {
	return MailingList{filename, certFilepath, "", make(map[string]MailboxListItem), MailingListSend_Subs, MailingListSend_Subs, MailingListSubs_AcceptAll, MailingListArchiveViewPermission_Subs, cmap.New[struct{}]()}
}

func (list *MailingList) Subscribe(item MailboxListItem) {
	address := item.Name + "@" + item.Hostname
	list.Subscribers[address] = item
}

func (list *MailingList) Unsubscribe(address string) {
	delete(list.Subscribers, address)
}

func (list *MailingList) ReloadMailingList() error {
	// Reopen file and parse list
	mailboxList, readErr := os.ReadFile(list.Filepath)
	if readErr != nil {
		return readErr
	}

	lines := strings.FieldsFunc(string(mailboxList), func(r rune) bool { return r == '\n' })

	clear(list.Subscribers)
	var currentMailboxListItem MailboxListItem
	for _, line := range lines {
		if strings.HasPrefix(line, "[") && strings.HasSuffix(strings.TrimSpace(line), "]") {
			// New Mailbox Definition found, add the previous mailbox to the list and move to new mailbox
			if currentMailboxListItem.Name != "" {
				address := currentMailboxListItem.Name + "@" + currentMailboxListItem.Hostname
				list.Subscribers[address] = currentMailboxListItem
			}

			// Create new MailboxListItem
			name := strings.TrimSuffix(strings.TrimPrefix(strings.TrimSpace(line), "["), "]")
			currentMailboxListItem = MailboxListItem{Name: name, ExcludeLabels: make(map[string]struct{})}
		} else {
			parts := strings.SplitN(line, ":", 2)
			field := strings.TrimSpace(parts[0])
			value := strings.TrimSpace(parts[1])

			if field == "type" {
				if value == "admin" {
					currentMailboxListItem.T = MailboxListType_Mailinglist_Admin
				} else if value == "user" {
					currentMailboxListItem.T = MailboxListType_Mailinglist_User
				} else if value == "writer" {
					currentMailboxListItem.T = MailboxListType_Mailinglist_Writer
				} else if value == "incoming-writer" { // One-way writer. Doesn't get sent messages from mailinglist
					currentMailboxListItem.T = MailboxListType_Mailinglist_Writer
					currentMailboxListItem.IncomingWriter = true
				} else if value == "federated" { // Two-way with forwarding allowed
					currentMailboxListItem.T = MailboxListType_Mailinglist_Federated
				} else if value == "incoming-forwarder" { // One-way with forwarding allowed. Doesn't get sent messages from mailinglist
					currentMailboxListItem.T = MailboxListType_Mailinglist_IncomingForwarder
				}
			} else if field == "blurb" {
				currentMailboxListItem.Blurb = value
			} else if field == "fingerprint" {
				currentMailboxListItem.Fingerprint = value
			} else if field == "hostname" {
				currentMailboxListItem.Hostname = value
			} else if field == "join-date" && value != "" {
				currentMailboxListItem.JoinDate, _ = time.Parse(time.RFC3339, value)
			} else if field == "receive-replies" {
				if value == "all" {
					currentMailboxListItem.ReceiveReplies = MailingListReceiveReplies_All
				} else if value == "own-posts" {
					currentMailboxListItem.ReceiveReplies = MailingListReceiveReplies_OwnPosts
				} else if value == "none" {
					currentMailboxListItem.ReceiveReplies = MailingListReceiveReplies_None
				}
			} else if field == "exclude-labels" {
				parts := strings.Split(value, ",")
				for _, part := range parts {
					part = strings.TrimSpace(part)
					labelToExclude, _, _ := strings.Cut(part, " ")
					currentMailboxListItem.ExcludeLabels[labelToExclude] = struct{}{}
				}
			}
		}
	}

	// Make sure to append last subscriber
	if currentMailboxListItem.Name != "" {
		address := currentMailboxListItem.Name + "@" + currentMailboxListItem.Hostname
		list.Subscribers[address] = currentMailboxListItem
	}

	return nil
}

func (list *MailingList) SaveMailingList() error {
	// Save list to file
	var builder strings.Builder
	var err error
	for _, sub := range list.Subscribers {
		builder.WriteString("\n[")
		_, err = builder.WriteString(sub.Name)
		builder.WriteString("]\n")
		if err != nil {
			return err
		}

		fields := []string{"type", "blurb", "fingerprint", "hostname", "join-date", "receive-replies", "exclude-labels"}
		for _, field := range fields {
			var value = ""
			if field == "type" {
				if sub.T == MailboxListType_Mailinglist_Admin {
					value = "admin"
				} else if sub.T == MailboxListType_Mailinglist_User {
					value = "user"
				} else if sub.T == MailboxListType_Mailinglist_Writer {
					value = "writer"
					if sub.IncomingWriter {
						value = "incoming-writer"
					}
				} else if sub.T == MailboxListType_Mailinglist_Federated { // Two-way with forwarding allowed.
					value = "federated"
				} else if sub.T == MailboxListType_Mailinglist_IncomingForwarder { // One-way with forwarding allowed. Doesn't get sent messages from mailinglist
					value = "incoming-forwarder"
				}
			} else if field == "blurb" {
				value = sub.Blurb
			} else if field == "fingerprint" {
				value = sub.Fingerprint
			} else if field == "hostname" {
				value = sub.Hostname
			} else if field == "join-date" {
				value = sub.JoinDate.Format(time.RFC3339)
			} else if field == "receive-replies" {
				if sub.ReceiveReplies == MailingListReceiveReplies_All {
					value = "all"
				} else if sub.ReceiveReplies == MailingListReceiveReplies_None {
					value = "none"
				} else if sub.ReceiveReplies == MailingListReceiveReplies_OwnPosts {
					value = "own-posts"
				}
			}

			if field == "exclude-labels" && (sub.ExcludeLabels == nil || len(sub.ExcludeLabels) == 0) {
				continue
			}

			builder.WriteString(field)
			_, err = builder.WriteString(": ")
			if err != nil {
				return err
			}

			if field == "exclude-labels" {
				value = ""
				i := 0
				for label := range sub.ExcludeLabels {
					if i > 0 {
						builder.WriteString(",")
					}
					_, err := builder.WriteString(label)
					if err != nil {
						return err
					}
					i++
				}
			}

			_, err = builder.WriteString(value)
			if err != nil {
				return err
			}
			_, err = builder.WriteString("\n")
			if err != nil {
				return err
			}
		}
	}

	err = os.WriteFile(list.Filepath, []byte(builder.String()), 0660)
	if err != nil {
		return err
	}

	return nil
}
