package main

import (
	"errors"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"gitlab.com/clseibold/misfin-server/gembox"
)

// MailboxListType represents the type of mailbox in the system.
type MailboxListType int

// Constants defining various mailbox list types
const (
	MailboxListType_Root MailboxListType = iota
	MailboxListType_Admin
	MailboxListType_User
	MailboxListType_Mailinglist
	MailboxListType_NewsfinGroup
	MailboxListType_Mailinglist_Admin             // Used for Mailinglist and Newsfin Group subscribers lists
	MailboxListType_Mailinglist_User              // Used for Mailinglist subscribers lists
	MailboxListType_Mailinglist_Writer            // Used for Mailinglist subscribers lists
	MailboxListType_Mailinglist_Federated         // Used for Mailinglist subscribers lists, allows federation between mailinglists on two different servers. Two-way with forwarding allowed.
	MailboxListType_Mailinglist_IncomingForwarder // One-way with forwarding allowed. Doesn't get sent messages from mailinglist
)

// MailinglistReceiveReplies defines whether a subscriber to a mailinglist will receive replies.
type MailingListReceiveReplies int

// Constants for MailingListReceiveReplies
const (
	MailingListReceiveReplies_All MailingListReceiveReplies = iota // Default
	MailingListReceiveReplies_OwnPosts
	MailingListReceiveReplies_None
)

// MailboxListItem represents an item in the mailboxes list.
type MailboxListItem struct {
	T              MailboxListType
	Name           string
	Blurb          string
	Fingerprint    string
	Hostname       string
	ReceiveReplies MailingListReceiveReplies // Used for subs of mailinglists
	ExcludeLabels  map[string]struct{}       // For subs of mailinglists

	// Used only for mailbox.list file
	List_Description            string
	List_Send_Thread_Permission string
	List_Send_Reply_Permission  string
	List_Subs_Function          string
	Archive_View_Permission     string
	IncomingWriter              bool

	// Used only for mailinglist .list files (to track mailinglist subscribers)
	JoinDate time.Time

	//GemboxFilepath string // Optional - assumed to be in mailboxes directory
}

// createEmptyMailboxesMap creates an empty MailBoxMap stored in the Mailbox Directory of the given configuration.
func createEmptyMailboxesMap(config Config) MailBoxMap {
	mailboxListFilepath := filepath.Join(config.MailboxDirectory, "mailbox.list")
	return MakeMailBoxMap(mailboxListFilepath)
}

// loadMailboxes reads the mailboxes list file and loads all of its mailboxes into a MailBoxMap.
func loadMailboxes(config Config) (MailBoxMap, error) {
	mailboxListFilepath := filepath.Join(config.MailboxDirectory, "mailbox.list")
	mailboxList, readErr := os.ReadFile(mailboxListFilepath)
	if readErr != nil {
		return MailBoxMap{}, readErr
	}

	lines := strings.FieldsFunc(string(mailboxList), func(r rune) bool { return r == '\n' })

	var list []MailboxListItem
	var currentMailboxListItem MailboxListItem
	for _, line := range lines {
		if strings.HasPrefix(line, "[") && strings.HasSuffix(strings.TrimSpace(line), "]") {
			// New Mailbox Definition found, add the previous mailbox to the list and move to new mailbox
			if currentMailboxListItem.Name != "" {
				list = append(list, currentMailboxListItem)
			}

			name := strings.TrimSuffix(strings.TrimPrefix(strings.TrimSpace(line), "["), "]")
			currentMailboxListItem = MailboxListItem{Name: name}
		} else {
			parts := strings.SplitN(line, ":", 2)
			field := strings.TrimSpace(parts[0])
			value := strings.TrimSpace(parts[1])

			if field == "type" {
				if value == "root" {
					currentMailboxListItem.T = MailboxListType_Root
				} else if value == "admin" {
					currentMailboxListItem.T = MailboxListType_Admin
				} else if value == "user" {
					currentMailboxListItem.T = MailboxListType_User
				} else if value == "mailinglist" {
					currentMailboxListItem.T = MailboxListType_Mailinglist
				} else if value == "newsfin-group" {
					currentMailboxListItem.T = MailboxListType_NewsfinGroup
				}
			} else if field == "blurb" {
				currentMailboxListItem.Blurb = value
			} else if field == "fingerprint" {
				currentMailboxListItem.Fingerprint = value
			} else if field == "hostname" {
				currentMailboxListItem.Hostname = value
			} else if field == "send" || field == "send-thread" {
				currentMailboxListItem.List_Send_Thread_Permission = value
			} else if field == "send-reply" {
				currentMailboxListItem.List_Send_Reply_Permission = value
			} else if field == "subs-function" {
				currentMailboxListItem.List_Subs_Function = value
			} else if field == "description" {
				currentMailboxListItem.List_Description = value
			} else if field == "archive-view-permission" {
				currentMailboxListItem.Archive_View_Permission = value
			}
		}
	}

	// Make sure to append last mailbox
	if currentMailboxListItem.Name != "" {
		list = append(list, currentMailboxListItem)
	}

	// mailboxes := MakeMailBoxMap()
	// serverMailBox := CreateMailBox(serverCertInfo, filepath.Join(config.MailboxDirectory, serverCertInfo.mailbox+".gembox"), false)
	// mailboxes.AddMailBox(&serverMailBox)

	mailboxes := MakeMailBoxMap(mailboxListFilepath)
	for _, item := range list {
		address := item.Name + "@" + item.Hostname
		certInfo := CertInfo{blurb: item.Blurb, domains: []string{item.Hostname}, mailbox: item.Name, mailAddress: address, fingerprint: item.Fingerprint, IsCA: item.T == MailboxListType_Root}
		gembox := filepath.Join(config.MailboxDirectory, item.Name+".gembox")
		mailbox := CreateMailBox(item.T, certInfo, gembox, item.T == MailboxListType_Mailinglist, item.T == MailboxListType_NewsfinGroup, config) // NOTE: Conversions from previous versions happen inside this function

		// Set mailinglist stuff
		if item.T == MailboxListType_Mailinglist || item.T == MailboxListType_NewsfinGroup { // TODO
			mailbox.List.Description = item.List_Description

			if item.List_Send_Thread_Permission == "open" {
				mailbox.List.SendThreadPermission = MailingListSend_Open
			} else if item.List_Send_Thread_Permission == "subs" || item.List_Send_Thread_Permission == "" {
				mailbox.List.SendThreadPermission = MailingListSend_Subs
			} else if item.List_Send_Thread_Permission == "writers" {
				mailbox.List.SendThreadPermission = MailingListSend_Writers
			}

			if item.List_Send_Reply_Permission == "open" {
				mailbox.List.SendReplyPermission = MailingListSend_Open
			} else if item.List_Send_Reply_Permission == "subs" || item.List_Send_Reply_Permission == "" {
				mailbox.List.SendReplyPermission = MailingListSend_Subs
			} else if item.List_Send_Reply_Permission == "writers" {
				mailbox.List.SendReplyPermission = MailingListSend_Writers
			} else if item.List_Send_Reply_Permission == "" { // If send-reply setting not set in mailboxes list, set it to the same as send-thread
				mailbox.List.SendReplyPermission = mailbox.List.SendThreadPermission
			}

			if item.List_Subs_Function == "accept-all" || item.List_Subs_Function == "" {
				mailbox.List.SubsFunction = MailingListSubs_AcceptAll
			} else if item.List_Subs_Function == "accept-server-members" {
				mailbox.List.SubsFunction = MailingListSubs_AcceptServerMembers
			} else if item.List_Subs_Function == "closed" {
				mailbox.List.SubsFunction = MailingListSubs_Closed
			} else if item.List_Subs_Function == "prompt" {
				mailbox.List.SubsFunction = MailingListSubs_Prompt
			}

			if item.Archive_View_Permission == "open" {
				mailbox.List.ArchiveViewPermission = MailingListArchiveViewPermission_Open
			} else if item.Archive_View_Permission == "subs" || item.Archive_View_Permission == "" {
				mailbox.List.ArchiveViewPermission = MailingListArchiveViewPermission_Subs
			} else if item.Archive_View_Permission == "admin-only" {
				mailbox.List.ArchiveViewPermission = MailingListArchiveViewPermission_AdminOnly
			} else if item.Archive_View_Permission == "disabled" {
				mailbox.List.ArchiveViewPermission = MailingListArchiveViewPermission_Disabled
			}
		}

		mailboxes.AddMailBox(&mailbox)
	}

	// All existing mailboxes have now been converted, so change config Version to current version
	config.PrevVersion = Version

	return mailboxes, nil
}

// MailBoxMap is a map of mailboxes keyed by mailbox name
type MailBoxMap struct {
	//serverMailBox *MailBox
	MailboxListFilepath string
	mailboxes           map[string]*MailBox
	rootMailbox         *MailBox
}

// MakeMailBoxMap creates a new MailBoxMap given the mailboxes list filepath.
func MakeMailBoxMap(mailboxListFilepath string) MailBoxMap {
	return MailBoxMap{mailboxListFilepath, make(map[string]*MailBox), nil}
}
func (m *MailBoxMap) Get(mailbox string) *MailBox {
	return m.mailboxes[mailbox]
}
func (m *MailBoxMap) Exists(mailbox string) bool {
	_, ok := m.mailboxes[mailbox]
	return ok
}
func (m *MailBoxMap) AddMailBox(mailbox *MailBox) {
	m.mailboxes[mailbox.CertInfo.mailbox] = mailbox
	if mailbox.T == MailboxListType_Root {
		m.rootMailbox = mailbox
	}
}
func (m *MailBoxMap) RemoveMailBox(mailbox MailBox) {
	if mailbox.T == MailboxListType_Root {
		m.rootMailbox = nil
	}
	delete(m.mailboxes, mailbox.CertInfo.mailbox)
}
func (m *MailBoxMap) RemoveMailBox_mailbox(mailbox string) {
	if m.mailboxes[mailbox].T == MailboxListType_Root {
		m.rootMailbox = nil
	}
	delete(m.mailboxes, mailbox)
}
func (m *MailBoxMap) Length() int {
	return len(m.mailboxes)
}

// savemailboxes saves the current state of mailboxes to the mailbox list file.
func (m *MailBoxMap) saveMailboxes() error {
	// Save list to file
	var builder strings.Builder
	var err error
	for _, mailbox := range m.mailboxes {
		builder.WriteString("\n[")
		_, err = builder.WriteString(mailbox.CertInfo.mailbox)
		builder.WriteString("]\n")
		if err != nil {
			return err
		}

		fields := []string{"type", "blurb", "fingerprint", "hostname", "send-thread", "send-reply", "subs-function", "archive-view-permission", "description"}
		for _, field := range fields {
			var value = ""
			if field == "type" {
				if mailbox.Mailinglist {
					value = "mailinglist"
				} else if mailbox.NewsfinGroup {
					value = "newsfin-group"
				} else if mailbox.CA {
					value = "root"
				} else if mailbox.T == MailboxListType_Admin {
					value = "admin"
				} else {
					value = "user"
				}
			} else if field == "blurb" {
				value = mailbox.CertInfo.blurb
			} else if field == "fingerprint" {
				value = mailbox.CertInfo.fingerprint
			} else if field == "hostname" {
				value = mailbox.CertInfo.domains[0]
			} else if field == "send-thread" {
				if !mailbox.Mailinglist && !mailbox.NewsfinGroup {
					continue
				}
				if mailbox.List.SendThreadPermission == MailingListSend_Subs {
					value = "subs"
				} else if mailbox.List.SendThreadPermission == MailingListSend_Open {
					value = "open"
				} else if mailbox.List.SendThreadPermission == MailingListSend_Writers {
					value = "writers"
				}
			} else if field == "send-reply" {
				if !mailbox.Mailinglist && !mailbox.NewsfinGroup {
					continue
				}
				if mailbox.List.SendReplyPermission == MailingListSend_Subs {
					value = "subs"
				} else if mailbox.List.SendReplyPermission == MailingListSend_Open {
					value = "open"
				} else if mailbox.List.SendReplyPermission == MailingListSend_Writers {
					value = "writers"
				}
			} else if field == "subs-function" {
				if !mailbox.Mailinglist && !mailbox.NewsfinGroup {
					continue
				}
				if mailbox.List.SubsFunction == MailingListSubs_AcceptServerMembers {
					value = "accept-server-members"
				} else if mailbox.List.SubsFunction == MailingListSubs_AcceptAll {
					value = "accept-all"
				} else if mailbox.List.SubsFunction == MailingListSubs_Closed {
					value = "closed"
				} else if mailbox.List.SubsFunction == MailingListSubs_Prompt {
					value = "prompt"
				}
			} else if field == "archive-view-permission" {
				if mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_Open {
					value = "open"
				} else if mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_Subs {
					value = "subs"
				} else if mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_AdminOnly {
					value = "admin-only"
				} else if mailbox.List.ArchiveViewPermission == MailingListArchiveViewPermission_Disabled {
					value = "disabled"
				}
			} else if field == "description" {
				if !mailbox.Mailinglist && !mailbox.NewsfinGroup {
					continue
				}
				if mailbox.List.Description == "" {
					continue // Skip
				}
				value = mailbox.List.Description
			}

			_, err = builder.WriteString(field)
			if err != nil {
				return err
			}
			_, err = builder.WriteString(": ")
			if err != nil {
				return err
			}
			_, err = builder.WriteString(value)
			if err != nil {
				return err
			}
			_, err = builder.WriteString("\n")
			if err != nil {
				return err
			}
		}
	}

	err = os.WriteFile(m.MailboxListFilepath, []byte(builder.String()), 0660)
	if err != nil {
		return err
	}

	// TODO: Do I save the gemboxes here as well?

	return nil
}

// MailBox represents a mailbox in the system.
type MailBox struct {
	T              MailboxListType
	CertFilepath   string
	CertInfo       CertInfo
	GemboxFilepath string // NOTE: Must join with MailboxDirectory.
	Mailinglist    bool
	NewsfinGroup   bool
	CA             bool // Used for Certificate Authority of main Server Cert, in multi-mailbox environments. Should only be the server certificate.
	Gbox           *gembox.GemBox
	List           MailingList
	load_misfinC   bool // Whether the gembox should be loaded in misfinC format or not.
	Mutex          *sync.RWMutex
}

// createMailBox creates a new MailBox and loads its gembox.
func CreateMailBox(T MailboxListType, certInfo CertInfo, gemboxFilename string, mailinglist bool, newsfingroup bool, config Config) MailBox {
	if gemboxFilename == "" { // Default gemboxFilename if one doesn't exist
		gemboxFilename = filepath.Join(config.MailboxDirectory, certInfo.mailbox+".gembox")
	}
	certFilepath := filepath.Join(config.MailboxDirectory, certInfo.mailbox+".pem")
	result := MailBox{T, certFilepath, certInfo, gemboxFilename, mailinglist, newsfingroup, certInfo.IsCA, gembox.NewEmptyGembox(certInfo.mailbox), MailingList{}, strings.HasSuffix(config.PrevVersion, "c"), &sync.RWMutex{}}
	if mailinglist {
		mailinglistFilename := filepath.Join(config.MailboxDirectory, certInfo.mailbox+".list")
		result.List = CreateMailingList(mailinglistFilename, certFilepath)
	} else if newsfingroup {
		mailinglistFilename := filepath.Join(config.MailboxDirectory, certInfo.mailbox+".list")
		result.List = CreateMailingList(mailinglistFilename, certFilepath)
	}

	result.ReloadGemBox(config) // NOTE: Conversions from previous versions happen inside this function

	// Save the converted Gembox
	if config.PrevVersion != Version {
		result.SaveGemBox()
	}

	return result
}

// ReloadGemBox opens the gembox file and reloads its contents.
func (mailbox *MailBox) ReloadGemBox(config Config) {
	// Load Gembox
	gemboxFile, readErr := os.ReadFile(mailbox.GemboxFilepath)
	gemboxFileString := string(gemboxFile)
	if errors.Is(readErr, os.ErrNotExist) || errors.Is(readErr, fs.ErrNotExist) {
		//fmt.Printf("gembox file %q will be created when first misfin mail comes in.\n", mailbox.GemboxFilepath)
		gemboxFileString = ""
	} else if readErr != nil {
		fmt.Printf("unable to read gembox file; %v\n", readErr)
		gemboxFileString = ""
	}

	if gemboxFileString != "" {
		if config.PrevVersion != Version && (config.PrevVersion == "0.5.3c" || config.PrevVersion == "0.5.2c" || !strings.HasSuffix(config.PrevVersion, "c")) { // Parse in old gembox format
			if mailbox.load_misfinC {
				*mailbox.Gbox = gembox.ParseGemBox_MisfinC_old(mailbox.CertInfo.mailbox, gemboxFileString)
			} else {
				*mailbox.Gbox = gembox.ParseGemBox_MisfinB_old(mailbox.CertInfo.mailbox, gemboxFileString)
			}
		} else { // 0.5.4c was the first version to use this format.
			// Load in new gembox format
			*mailbox.Gbox = gembox.ParseGemBox(mailbox.CertInfo.mailbox, gemboxFileString)
		}
	}

	// If any version before 0.5.8c, then make sure all mails in mailbox are tagged with Inbox, or Sent if the gemmail has one sender that is the mailbox holder and there's one or more recipients.
	doSave := false
	if gemboxFileString != "" && (config.PrevVersion == "0.5.8c" || config.PrevVersion == "0.5.7c" || config.PrevVersion == "0.5.6c" || config.PrevVersion == "0.5.5c" || config.PrevVersion == "0.5.4c" || config.PrevVersion == "0.5.3c" || config.PrevVersion == "0.5.2c" || !strings.HasSuffix(config.PrevVersion, "c")) {
		mailbox.Mutex.Lock()
		for i := range mailbox.Gbox.Mails {
			mail := &mailbox.Gbox.Mails[i]
			if !mail.HasTag("Inbox") && !mail.HasTag("Archive") && !mail.HasTag("Sent") && !mail.HasTag("Drafts") {
				mail.AddTag("Inbox")
				doSave = true
			}

			// If the recipient mailbox is the same as the (last and only) sender, and there is a recipients line, then the gemmail should be tagged as Sent.
			if len(mail.Senders) == 1 && mailbox.CertInfo.mailAddress == mail.Senders[0].Address && len(mail.Receivers) > 0 {
				mail.AddTag("Sent")
				mail.RemoveTag("Unread")
				doSave = true
			}

			if len(mail.Timestamps) <= 0 {
				fmt.Printf("Warning: Mail %s has 0 timestamps.\n", mail.ID)
			}
			if len(mail.Senders) <= 0 {
				fmt.Printf("Warning: Mail %s has 0 senders.\n", mail.ID)
			}
		}
		mailbox.Mutex.Unlock()
	}

	if mailbox.Mailinglist || mailbox.NewsfinGroup {
		// Reload the mailinglist addresses list, ignore error (if, for example, the list file doesn't exist)
		mailbox.List.ReloadMailingList() // TODO: Handle returned error
	}

	if doSave {
		mailbox.SaveGemBox()
	}

	if mailbox.Gbox.MailboxAddress == "" {
		mailbox.Gbox.MailboxAddress = mailbox.CertInfo.mailAddress
		mailbox.Gbox.MailboxBlurb = mailbox.CertInfo.blurb
		mailbox.Gbox.Fingerprint = mailbox.CertInfo.fingerprint
		mailbox.SaveGemBox()
		mailbox.ReloadGemBox(config) // NOTE: Makes sure threads are all correctly organized since adding the MailboxAddress to the Gembox file.
	}
}

// SaveGemBox saves the current state of the mailbox's gembox to file. This should only be called after Gembox
// and Mailinglist have been loaded.
func (mailbox *MailBox) SaveGemBox() {
	mailbox.Mutex.RLock()
	err := os.WriteFile(mailbox.GemboxFilepath, []byte(mailbox.Gbox.String()), 0660)
	if err != nil {
		fmt.Printf("failed to write file %s\n", mailbox.GemboxFilepath)
	}
	mailbox.Mutex.RUnlock()

	if mailbox.Mailinglist || mailbox.NewsfinGroup {
		// Save the mailinglist addresses list also
		mailbox.List.SaveMailingList()
	}
}
