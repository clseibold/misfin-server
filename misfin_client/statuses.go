// Source: https://github.com/makew0rld/go-gemini/blob/master/gemini.go
// Copyright (c) 2020, makeworld - See License info for file in LICENSE-CLIENT
// Copyright (c) 2023, Christian Lee Seibold
//
// 9-28-2023 Christian Lee Seibold - Made small changes to add status codes from misfin spec.
//

package misfin_client

import "fmt"

// Gemini status codes as defined in the Gemini spec Appendix 1.
const (
	// Reserved, must not be send by a Misfin server
	StatusInput = 10

	// Message has been delivered
	StatusSuccess = 20

	StatusRedirect          = 30
	StatusRedirectTemporary = 30
	StatusRedirectPermanent = 31

	StatusTemporaryFailure = 40
	StatusUnavailable      = 41
	StatusCGIError         = 42
	StatusProxyError       = 43
	StatusSlowDown         = 44
	StatusMailboxFull      = 45

	StatusPermanentFailure = 50
	// Mailbox does not exist. Mailserver won't accept your message.
	StatusNotFound = 51
	// Mailbox once existed, but doesn't anymore.
	StatusGone = 52
	// The mailserver doesn't serve mail for the hostname you provided.
	StatusDomainNotServiced   = 53
	StatusProxyRequestRefused = 53
	StatusBadRequest          = 59

	// Mailserver doesn't accept anonymous mail.
	StatusClientCertificateRequired = 60
	// Certificate was validated, but you are now allowed to send mail to that mailbox.
	StatusCertificateNotAuthorised = 61
	StatusCertificateNotValid      = 62
	// "YOU'RE A LIAR" - Certificate matches an identity the mailserver recognizes, but the fingerprint has changed, so it is rejecting your message.
	StatusCertificateChanged = 63
	// The mailserver needs you to complete a task to confirm that you are a legitimate sender. Not usually used.
	StatusCertificateProveIt = 64
)

var statusText = map[int]string{
	StatusInput: "Input",

	StatusSuccess: "Success",

	// StatusRedirect:       "Redirect - Temporary"
	StatusRedirectTemporary: "Redirect - Temporary",
	StatusRedirectPermanent: "Redirect - Permanent",

	StatusTemporaryFailure: "Temporary Failure",
	StatusUnavailable:      "Server Unavailable",
	StatusCGIError:         "CGI Error",
	StatusProxyError:       "Proxy Error",
	StatusSlowDown:         "Slow Down",
	StatusMailboxFull:      "Mailbox Full",

	StatusPermanentFailure:    "Permanent Failure",
	StatusNotFound:            "Not Found", // aka. Doesn't Exist
	StatusGone:                "Gone",
	StatusProxyRequestRefused: "Domain Not Serviced", // aka. Proxy Request Refused
	StatusBadRequest:          "Bad Request",

	StatusClientCertificateRequired: "Client Certificate Required",
	StatusCertificateNotAuthorised:  "Certificate Not Authorised",
	StatusCertificateNotValid:       "Certificate Not Valid",
	StatusCertificateChanged:        "Certificate Changed",
	StatusCertificateProveIt:        "Prove It",
}

// StatusText returns a text for the Gemini status code. It returns the empty
// string if the code is unknown.
func StatusText(code int) string {
	return statusText[code]
}

// SimplifyStatus simplify the response status by ommiting the detailed second digit of the status code.
func SimplifyStatus(status int) int {
	return (status / 10) * 10
}

// IsStatusValid checks whether an int status is covered by the spec.
// Note that:
//     A client SHOULD deal with undefined status codes
//     between '10' and '69' per the default action of the initial digit.
func IsStatusValid(status int) bool {
	_, found := statusText[status]
	return found
}

// StatusInRange returns true if the status has a valid first digit.
// This means it can be handled even if it's not defined by the spec,
// because it has a known category
func StatusInRange(status int) bool {
	if status < 10 || status > 69 {
		return false
	}
	return true
}

// CleanStatus returns the status code as is, unless it's invalid but still in range
// Then it returns the status code with the second digit zeroed. So 51 returns 51,
// but 22 returns 20.
//
// This corresponds with the spec:
//     A client SHOULD deal with undefined status codes
//     between '10' and '69' per the default action of the initial digit.
func CleanStatus(status int) int {
	// All the functions come together!
	if !IsStatusValid(status) && StatusInRange(status) {
		return SimplifyStatus(status)
	}
	return status
}

type Error struct {
	Err    error
	Status int
}

func (e Error) Error() string {
	return fmt.Sprintf("Status %d: %v", e.Status, e.Err)
}

func (e Error) Unwrap() error {
	return e.Err
}
