# Misfin-Server TODO

## General

- What happens when a forwarder adds something to the contents? How does this interact with duplicate-message detection?

- [x] gembox: handle threading (organize gemmails into threads based on subject title, and ordered by received-date)
      - Threads have the same subject title.
- [x] Finish Receive-Replies mailinglist setting
- [ ] Switch to sqlite database for storing mailbox messages (or at least keeping track of gemmail files)
- [x] Make threading more advanced
      - [x] Keep track of thread's subject, last sender, and recipients
      - [x] Messages of the same subject where the last sender is in the recipients of the message should be part of the same thread (this is used for groups)
      - Don't do any of this for mailinglist mailboxes

- [ ] Server should reload on SIGHUP (which is common for daemons in modern usage now)

- [ ] Load in message body only when needed - this allows the server to reject messages before the entire message body is downloaded.
  - [ ] Load up to first new line (or end of message) to detect blank gemmail
  - [ ] Verification of sender's fingerprint can be done before the entire message body is downloaded.
  - [ ] Check if mail is duplicate relies only on header information, so that could be done before the entire message body as well. (Add new error code for duplicate mails?)

- [ ] Netnews-like system on top of misfin - Newsfin.

## Mailinglists

- [x] Mailinglist Info command
- [ ] Ability for a user to not get replies to posts
- [ ] Message sent to mailinglist, with an additional recipient that's not subbed to the mailinglist. Make sure replies take this into account
- [ ] If mailinglist is set to anybody can post to it without subbing, handle how replies are handled - probably just have the mailinglist add the sender as a recipient so clients will send replies to them _and_ the mailinglist.
- [x] Labels/tags for mailinglist posts.
  - [x] Ability for a user to not receive posts of a specific label/tag
  - [ ] Configuration to require labels on all mails
  - [ ] Configuration to restrict labels to a specific set. New command to list this set of labels.
  - [ ] Restrict certain labels to admins or writers only.
- [ ] Mailinglist Digests

- [ ] CGI /cgi/env - prints out all environment variables that are given to cgi scripts
  - REQUEST_PATH vs. PATH_INFO
- [ ] Way to configure routes to use a cgi exe/script
