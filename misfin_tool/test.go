package main

import (
	"crypto"
	"crypto/sha256"
	"crypto/x509"
	"encoding/hex"
	"encoding/pem"
	"fmt"
	"os"
)

type CertInfo struct {
	blurb       string
	domains     []string
	mailbox     string
	mailAddress string
	fingerprint string
	IsCA        bool
}

func main() {
	/*cert, _, _ := getCertsFromPem("mailboxes/clseibold.pem")
	certInfo := getCertInfo(cert)
	fmt.Printf("Fingerprint: %s\n", certInfo.fingerprint)*/
}

// NOTE: Assumes that cert and private key are in a single (pem) file.
func getCertsFromPem(certFilename string) (*x509.Certificate, crypto.PrivateKey, []byte) {
	file, readErr := os.ReadFile(certFilename)
	if readErr != nil {
		panic(fmt.Errorf("could not read certificate file %q: %v", certFilename, readErr))
	}

	pemBlock, rest := pem.Decode(file)
	var certificate *x509.Certificate
	var privateKey crypto.PrivateKey
	var privateKeyPemBytes []byte
	for pemBlock != nil {
		var parseErr error
		if pemBlock.Type == "CERTIFICATE" {
			certificate, parseErr = x509.ParseCertificate(pemBlock.Bytes)
		} else if pemBlock.Type == "RSA PRIVATE KEY" {
			privateKeyPemBytes = pemBlock.Bytes
			privateKey, parseErr = x509.ParsePKCS1PrivateKey(pemBlock.Bytes)
		} else if pemBlock.Type == "EC PRIVATE KEY" {
			privateKeyPemBytes = pemBlock.Bytes
			privateKey, parseErr = x509.ParseECPrivateKey(pemBlock.Bytes)
		} else if pemBlock.Type == "PRIVATE KEY" {
			privateKeyPemBytes = pemBlock.Bytes
			privateKey, parseErr = x509.ParsePKCS8PrivateKey(pemBlock.Bytes)
		}
		if parseErr != nil {
			panic(parseErr)
		}
		pemBlock, rest = pem.Decode(rest)
	}

	return certificate, privateKey, privateKeyPemBytes
}

// Returns Common Name (aka. blurb), mailbox, hostname(s), and mailAddress
func getCertInfo(certificate *x509.Certificate) CertInfo {
	commonName := certificate.Subject.CommonName // aka. blurb
	domains := certificate.DNSNames              // aka. hostname
	mailbox := ""
	for _, n := range certificate.Subject.Names {
		if n.Type.String() == "0.9.2342.19200300.100.1.1" {
			mailbox = n.Value.(string)
		}
	}
	mailAddress := mailbox + "@" + domains[0]

	h := sha256.New()
	h.Write(certificate.Raw)
	fingerprint := hex.EncodeToString(h.Sum(nil))

	return CertInfo{commonName, domains, mailbox, mailAddress, fingerprint, certificate.IsCA}
}
