module gitlab.com/clseibold/misfin-server

go 1.23

require (
	github.com/djherbis/times v1.6.0
	github.com/gabriel-vasile/mimetype v1.4.6
	github.com/gammazero/deque v0.2.1
	github.com/orcaman/concurrent-map/v2 v2.0.1
	github.com/spf13/cobra v1.8.1
	golang.org/x/crypto v0.28.0
	golang.org/x/net v0.30.0
	golang.org/x/sync v0.8.0
	golang.org/x/text v0.19.0
)

require (
	github.com/inconshreveable/mousetrap v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	golang.org/x/sys v0.26.0 // indirect
)
