# New GemBox Format

A Gembox is a collection of gemmails along with metadata information on each message and on the gembox, to be used as storage only, and *never* during message transmission over Misfin(B) or Misfin(C). Gemboxes can be used as the storage backend for misfin clients and servers, although one quickly runs into the limitations of a single-file format (i.e., file locking, linear read and write). Gemboxes are therefore *more* useful when used for exporting, importing, and archiving mailboxes.

Gembox metadata includes created tags, a mailbox address of the user, and a description, all of which are optional. Metadata on messages includes the standard metadata for misfin as well as the (optional) metadata required for GMAP. This includes senders, recipients, received-timestamps, as well as tags. Every tag gets a timestamp of its last modification date and its current value (true/false).

All timestamps should be in ISO 8601, the same standard used by both GMAP and Misfin.

The mailbox name in the gembox metadata should be assumed to be the original recipient of every message in the mailbox not tagged with "Drafts" or "Sent".

Message IDs should only be used to distinguish messages within the gembox, and for use with GMAP. They are not guaranteed to be unique outside of the gembox file. During import, these Message IDs should be replaced by ones that are unique within the destination mailbox.

## Gembox Metadata

A Gembox may start with the optional gembox metadata. None of these fields are required, except for "CreatedTags" if any optional tags are attached to any messages. Fields include the following:
* "CreatedTags" - comma-delimited list of optional tags that MAY be attached to messages. New tags should always be appended to this list.
* "Mailbox" - address and optional blurb of mailbox, in the format "address@hostname Optional Blurb" or just "address@hostname"
* "Fingerprint" - the SHA256 hash of the mailbox's cert, as a hexadecimal number without octet separators
* "Description" - Description of gembox file

All fields get this format:
```
FieldName: Value [, Additional Value]
```

An example of a Gembox Metadata section is this:
```
Mailbox: clseibold@auragem.letz.dev Christian Lee Seibold
CreatedTags: Inbox.sub_folder, Inbox.sub_folder_2
```

## Delimiting Messages

Every message begins with its Message ID in brackets. This Message ID is unique to each message within the gembox, but uniqueness is not guaranteed outside of the gembox.
The Message ID is followed by the following (optional) fields:
* "Senders" - a list of senders in the format "address@hostname Blurb", delimited by a comma, ordered from most recent to least recent (original).
* "Recipients" - a list of recipient addresses, delimited by a comma.
* "Timestamps" - a list of ISO 8601 received timestamps that correspond to the senders. The last timestamp always corresponds to the datetime received from the least recent (original) sender.

After those three fields, tags may be listed as fields with a timestamp value followed by a true/false on whether that tag applies to the message or not. The timestamps are used for syncing clients via GMAP by determining the last time that tag was toggled on that particular message. The only tags that are allowed are the default GMAP tags and those listed in the CreatedTags Gembox Metadata field.
An example follows:
```
Inbox: 2024-02-11T06:10:00Z false
Archive: 2024-02-11T06:10:00Z true
```

Tags that are not listed are assumed to have never been tagged or untagged on the message, and therefore have no last modification date and are always assumed to be "false".

The above shows that the message was moved from Inbox to Archive on February 11, 2024, at 6:10 AM. GMAP clients can get a list of "events" since a particular datetime, and the server should respond with this message getting tagged with "Archive" and untagged with "Inbox" only if the given datetime is less than or equal to the datetime of the last modification datetime of both of those tags.

The body always starts where the Subject is. No metadata fields can follow the Subject. Subjects always begin with the *first* "#" linetype. If a subject is empty, place a "#" on a line by itself. The subject line is *required*.

A full message should look like the following, with a Message ID of "lhasf324h3gk3j3guh3hkh3uiuh33kh3u" and an empty subject line:
```
[lhasf324h3gk3j3guh3hkh3uiuh33kh3u]
Senders: misfin@auragem.letz.dev AuraGem Misfin Mail List, clseibold@hashnix.club Christian Lee Seibold
Timestamps: 2024-02-11T06:11:00Z, 2024-02-11T06:10:00Z
Inbox: 2024-02-11T06:11:00Z true
Unread: 2024-02-11T06:11:00Z true
Inbox.sub_folder: 2024-02-11T06:11:00Z true
#

This is the body of the message.

\[ escaped bracket line here.
```

If messages contain lines that start with square brackets, they must be escaped with "\\[". The next message always starts with a square bracket ("["). Fields that appear in messages do not need to be escaped because no fields can follow the Subject line.

## Example of full Gembox File

```
Mailbox: clseibold@auragem.letz.dev Christian Lee Seibold
CreatedTags: Inbox.sub_folder, Inbox.sub_folder_2
[lhasf324h3gk3j3guh3hkh3uiuh33kh3u]
Senders: misfin@auragem.letz.dev AuraGem Misfin Mail List, clseibold@hashnix.club Christian Lee Seibold
Timestamps: 2024-02-11T06:11:00Z, 2024-02-11T06:10:00Z
Inbox: 2024-02-11T06:11:00Z true
Unread: 2024-02-11T06:11:00Z true
Inbox.sub_folder: 2024-02-11T06:11:00Z true
#

This is the body of the message.

\[ escaped bracket line here.
[hsdf32lh3hkjg23ftykj23u3ugiiug3r8]
Senders: misfin@auragem.letz.dev AuraGem Misfin Mail List, clseibold@hashnix.club Christian Lee Seibold
Timestamps: 2024-02-11T06:13:00Z, 2024-02-11T06:12:00Z
Inbox: 2024-02-11T06:14:00Z false
Archive: 2024-02-11T06:14:00Z true
Unread: 2024-02-11T06:13:00Z true
Inbox.sub_folder: 2024-02-11T06:14:00Z false
# Example Subject

This is the body of this second message.

Not_A_Tag_or_Field: This is part of the message body still!

```
