package gemmail

import (
	"bufio"
	"crypto"
	"crypto/ecdsa"
	"crypto/ed25519"
	crypto_rand "crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"slices"
	"strings"
	"time"

	"golang.org/x/crypto/blake2b"
)

type GemMail struct {
	ID           string              // MD5 Hash of timestamp and orig. sender
	Subject      GemMailLineHeading1 // First level-1 heading of gemmail body (not included in the GemtextLines array)
	Senders      []GemMailLineSender
	Receivers    map[string]struct{}
	Timestamps   []GemMailLineTimestamp
	GemtextLines []GemMailLine // NOTE: Does not include first level-1 heading (the Subject)
	Tags         map[string]Tag
}

type Tag struct {
	Name                        string
	Value                       bool // whether tag is enabled or disabled
	Last_modification_timestamp time.Time
}

type GemMailLine interface {
	String() string
	gemmailline()
}

type GemMailLineSender struct {
	Address string
	Blurb   string
}

func (sender *GemMailLineSender) Clone() GemMailLineSender {
	result := GemMailLineSender{}
	result.Address = strings.Clone(sender.Address)
	result.Blurb = strings.Clone(sender.Blurb)
	return result
}

// type GemMailLineReceivers []string // Array of addresses
type GemMailLineTimestamp struct {
	Value time.Time
}

// LineLink is a link line.
type GemMailLineLink struct {
	URL  string
	Name string
}

func (link *GemMailLineLink) Clone() GemMailLineLink {
	result := GemMailLineLink{}
	result.URL = strings.Clone(link.URL)
	result.Name = strings.Clone(link.Name)
	return result
}

// LinePreformattingToggle is a preformatting toggle line.
type GemMailLinePreformattingToggle string

func (performattedToggle *GemMailLinePreformattingToggle) Clone() GemMailLinePreformattingToggle {
	return GemMailLinePreformattingToggle(strings.Clone(string(*performattedToggle)))
}

// LinePreformattedText is a preformatted text line.
type GemMailLinePreformattedText string

func (preformattedText *GemMailLinePreformattedText) Clone() GemMailLinePreformattedText {
	return GemMailLinePreformattedText(strings.Clone(string(*preformattedText)))
}

// LineHeading1 is a first-level heading line. This is equivalent to the gemmail's Subject
type GemMailLineHeading1 string

func (heading1 *GemMailLineHeading1) Clone() GemMailLineHeading1 {
	return GemMailLineHeading1(strings.Clone(string(*heading1)))
}

// LineHeading2 is a second-level heading line.
type GemMailLineHeading2 string

func (heading2 *GemMailLineHeading2) Clone() GemMailLineHeading2 {
	return GemMailLineHeading2(strings.Clone(string(*heading2)))
}

// LineHeading3 is a third-level heading line.
type GemMailLineHeading3 string

func (heading3 *GemMailLineHeading3) Clone() GemMailLineHeading3 {
	return GemMailLineHeading3(strings.Clone(string(*heading3)))
}

// LineListItem is an unordered list item line.
type GemMailLineListItem string

func (listItem *GemMailLineListItem) Clone() GemMailLineListItem {
	return GemMailLineListItem(strings.Clone(string(*listItem)))
}

// LineQuote is a quote line.
type GemMailLineQuote string

func (quote *GemMailLineQuote) Clone() GemMailLineQuote {
	return GemMailLineQuote(strings.Clone(string(*quote)))
}

// LineText is a text line.
type GemMailLineText string

func (lineText *GemMailLineText) Clone() GemMailLineText {
	return GemMailLineText(strings.Clone(string(*lineText)))
}

func (l GemMailLineSender) String() string {
	if l.Blurb != "" {
		//return fmt.Sprintf("< %s %s", l.Address, l.Blurb)
		return fmt.Sprintf("%s %s", l.Address, l.Blurb)
	}
	//return fmt.Sprintf("< %s", l.Address)
	return l.Address
}

/*
	func (l GemMailLineReceivers) String() string {
		var builder strings.Builder
		fmt.Fprintf(&builder, ":")
		for _, r := range l {
			fmt.Fprintf(&builder, " %s", r)
		}
		return builder.String()
	}
*/
func (l GemMailLineTimestamp) String() string {
	//return fmt.Sprintf("@ %s", l.Value.Format(time.RFC3339))
	return l.Value.Format(time.RFC3339)
}
func (l GemMailLineLink) String() string {
	if l.Name != "" {
		return fmt.Sprintf("=> %s %s", l.URL, l.Name)
	}
	return fmt.Sprintf("=> %s", l.URL)
}
func (l GemMailLinePreformattingToggle) String() string {
	return fmt.Sprintf("```%s", string(l))
}
func (l GemMailLinePreformattedText) String() string {
	return string(l)
}
func (l GemMailLineHeading1) String() string {
	return fmt.Sprintf("# %s", string(l))
}
func (l GemMailLineHeading2) String() string {
	return fmt.Sprintf("## %s", string(l))
}
func (l GemMailLineHeading3) String() string {
	return fmt.Sprintf("### %s", string(l))
}
func (l GemMailLineListItem) String() string {
	return fmt.Sprintf("* %s", string(l))
}
func (l GemMailLineQuote) String() string {
	return fmt.Sprintf("> %s", string(l))
}
func (l GemMailLineText) String() string {
	return string(l)
}

func (l GemMailLineSender) gemmailline() {}

// func (l GemMailLineReceivers) gemmailline()           {}
func (l GemMailLineTimestamp) gemmailline()           {}
func (l GemMailLineLink) gemmailline()                {}
func (l GemMailLinePreformattingToggle) gemmailline() {}
func (l GemMailLinePreformattedText) gemmailline()    {}
func (l GemMailLineHeading1) gemmailline()            {}
func (l GemMailLineHeading2) gemmailline()            {}
func (l GemMailLineHeading3) gemmailline()            {}
func (l GemMailLineListItem) gemmailline()            {}
func (l GemMailLineQuote) gemmailline()               {}
func (l GemMailLineText) gemmailline()                {}

// -----

// Create gemmail from body. After setting timestamp and sender, run gemmail.SetID() if you intend to add the message to a gembox or store it in memory.
func CreateGemMailFromBody(body string) GemMail {
	return ParseGemMail_MisfinB(body) // NOTE: Intentionally not using MisfinC Parser because the body should not contian the 3 metadata lines
}

func generateID(timestamp time.Time, sender string) string {
	timeStr := timestamp.Format("2006-01-02T15:04:05")
	hash := blake2b.Sum256([]byte(timeStr + sender))
	return fmt.Sprintf("%x", hash)
}

func ParseGemMail_MisfinB(gemmail string) GemMail {
	spacetab := " \t"
	var result GemMail
	result.Senders = make([]GemMailLineSender, 0)
	result.Receivers = make(map[string]struct{})
	result.Timestamps = make([]GemMailLineTimestamp, 0)
	result.GemtextLines = make([]GemMailLine, 0)
	result.Tags = make(map[string]Tag)

	scanner := bufio.NewScanner(strings.NewReader(gemmail))
	// TODO: scanner.Err()

	var pre bool = false
	var gotSubject bool = false
	for scanner.Scan() {
		line := strings.TrimSpace(scanner.Text())
		var gemmailLine GemMailLine
		if strings.HasPrefix(line, "```") {
			pre = !pre
			line = line[3:]
			gemmailLine = GemMailLinePreformattingToggle(line)
			result.GemtextLines = append(result.GemtextLines, gemmailLine)
		} else if pre {
			gemmailLine = GemMailLinePreformattedText(line)
			result.GemtextLines = append(result.GemtextLines, gemmailLine)
		} else if strings.HasPrefix(line, "<") {
			parts := strings.SplitN(strings.TrimSpace(strings.TrimPrefix(line, "<")), " ", 2)
			if len(parts) == 1 { // TODO: Error out if length is 0
				gemmailLine = GemMailLineSender{parts[0], ""}
			} else {
				gemmailLine = GemMailLineSender{parts[0], parts[1]}
			}

			result.Senders = append(result.Senders, gemmailLine.(GemMailLineSender))
		} else if strings.HasPrefix(line, ":") {
			parts := strings.Split(strings.TrimSpace(strings.TrimPrefix(line, ":")), " ")
			for _, r := range parts {
				result.Receivers[r] = struct{}{}
			}
		} else if strings.HasPrefix(line, "@") {
			trimmed := strings.TrimSpace(strings.TrimPrefix(line, "@"))
			timestamp, err := time.Parse(time.RFC3339, trimmed)
			if err != nil {
				// TODO
				continue
			}
			gemmailLine = GemMailLineTimestamp{timestamp}
			result.Timestamps = append(result.Timestamps, gemmailLine.(GemMailLineTimestamp))
		} else if strings.HasPrefix(line, "=>") {
			line = line[2:]
			line = strings.TrimLeft(line, spacetab)
			split := strings.IndexAny(line, spacetab)
			if split == -1 {
				// text is a URL
				gemmailLine = GemMailLineLink{URL: line}
			} else {
				url := line[:split]
				name := line[split:]
				name = strings.TrimLeft(name, spacetab)
				gemmailLine = GemMailLineLink{url, name}
			}
			result.GemtextLines = append(result.GemtextLines, gemmailLine)
		} else if strings.HasPrefix(line, "* ") {
			line = line[2:]
			line = strings.TrimLeft(line, spacetab)
			gemmailLine = GemMailLineListItem(line)
			result.GemtextLines = append(result.GemtextLines, gemmailLine)
		} else if strings.HasPrefix(line, "###") {
			line = line[3:]
			line = strings.TrimLeft(line, spacetab)
			gemmailLine = GemMailLineHeading3(line)
			result.GemtextLines = append(result.GemtextLines, gemmailLine)
		} else if strings.HasPrefix(line, "##") {
			line = line[2:]
			line = strings.TrimLeft(line, spacetab)
			gemmailLine = GemMailLineHeading2(line)
			result.GemtextLines = append(result.GemtextLines, gemmailLine)
		} else if strings.HasPrefix(line, "#") {
			line = line[1:]
			line = strings.TrimLeft(line, spacetab)
			gemmailLine = GemMailLineHeading1(line)

			if !gotSubject {
				gotSubject = true
				result.Subject = gemmailLine.(GemMailLineHeading1)
			} else {
				result.GemtextLines = append(result.GemtextLines, gemmailLine)
			}
		} else if strings.HasPrefix(line, ">") {
			line = line[1:]
			line = strings.TrimLeft(line, spacetab)
			gemmailLine = GemMailLineQuote(line)
			result.GemtextLines = append(result.GemtextLines, gemmailLine)
		} else {
			gemmailLine = GemMailLineText(line)
			result.GemtextLines = append(result.GemtextLines, gemmailLine)
		}
	}

	// Construct ID
	if len(result.Timestamps) > 0 && len(result.Senders) > 0 {
		result.ID = generateID(result.Timestamps[len(result.Timestamps)-1].Value, result.Senders[len(result.Senders)-1].String())
	}

	return result
}

// Parses metadata in misfin(C) format without the gemmail's body, and returns a new GemMail
func ParseMetadata_MisfinC(metadata string) GemMail {
	var result GemMail
	result.Senders = make([]GemMailLineSender, 0)
	result.Receivers = make(map[string]struct{})
	result.Timestamps = make([]GemMailLineTimestamp, 0)
	result.GemtextLines = make([]GemMailLine, 0)
	result.Tags = make(map[string]Tag)

	scanner := bufio.NewScanner(strings.NewReader(metadata))
	// TODO: scanner.Err()

	var i = 0
	for scanner.Scan() {
		line := strings.TrimSpace(scanner.Text())
		var gemmailLine GemMailLine
		if i == 0 {
			// Senders
			senders := strings.Split(strings.TrimSpace(strings.TrimPrefix(line, "<")), ",")
			for _, s := range senders {
				// Skip empty strings
				s := strings.TrimPrefix(s, " ")
				if s == "" {
					continue
				}
				parts := strings.SplitN(s, " ", 2)
				if len(parts) == 1 { // TODO: Error out if length is 0
					gemmailLine = GemMailLineSender{parts[0], ""}
				} else {
					gemmailLine = GemMailLineSender{parts[0], parts[1]}
				}
				result.Senders = append(result.Senders, gemmailLine.(GemMailLineSender))
			}
		} else if i == 1 {
			// Recipients
			parts := strings.Split(strings.TrimSpace(strings.TrimPrefix(line, ":")), ",")
			for _, r := range parts {
				r := strings.TrimSpace(r)
				if r == "" {
					continue
				}
				result.Receivers[r] = struct{}{}
			}
		} else if i == 2 {
			// Timestamps
			timestamps := strings.Split(strings.TrimSpace(strings.TrimPrefix(line, "<")), ",")
			for _, t := range timestamps {
				// Skip empty strings
				t := strings.TrimSpace(t)
				if t == "" {
					continue
				}
				timestamp, err := time.Parse(time.RFC3339, t)
				if err != nil {
					continue
				}
				gemmailLine = GemMailLineTimestamp{timestamp}
				result.Timestamps = append(result.Timestamps, gemmailLine.(GemMailLineTimestamp))
			}
		}

		i += 1
	}

	// Construct ID
	if len(result.Timestamps) > 0 && len(result.Senders) > 0 {
		result.ID = generateID(result.Timestamps[len(result.Timestamps)-1].Value, result.Senders[len(result.Senders)-1].String())
	}

	return result
}

// When using the new Misfin(C) format, where the first 3 lines are set as senders, recipients, and timestamps
func ParseGemMail_MisfinC(gemmail string) GemMail {
	spacetab := " \t"
	var result GemMail
	result.Senders = make([]GemMailLineSender, 0)
	result.Receivers = make(map[string]struct{})
	result.Timestamps = make([]GemMailLineTimestamp, 0)
	result.GemtextLines = make([]GemMailLine, 0)
	result.Tags = make(map[string]Tag)

	scanner := bufio.NewScanner(strings.NewReader(gemmail))
	// TODO: scanner.Err()

	var pre bool = false
	var gotSubject bool = false
	var i = 0
	for scanner.Scan() {
		line := strings.TrimSpace(scanner.Text())
		var gemmailLine GemMailLine
		if i == 0 {
			// Senders
			senders := strings.Split(strings.TrimSpace(strings.TrimPrefix(line, "<")), ",")
			for _, s := range senders {
				// Skip empty strings
				s := strings.TrimPrefix(s, " ")
				if s == "" {
					continue
				}
				parts := strings.SplitN(s, " ", 2)
				if len(parts) == 1 { // TODO: Error out if length is 0
					gemmailLine = GemMailLineSender{parts[0], ""}
				} else {
					gemmailLine = GemMailLineSender{parts[0], parts[1]}
				}
				result.Senders = append(result.Senders, gemmailLine.(GemMailLineSender))
			}
		} else if i == 1 {
			// Recipients
			parts := strings.Split(strings.TrimSpace(strings.TrimPrefix(line, ":")), ",")
			for _, r := range parts {
				r := strings.TrimSpace(r)
				if r == "" {
					continue
				}
				result.Receivers[r] = struct{}{}
			}
		} else if i == 2 {
			// Timestamps
			timestamps := strings.Split(strings.TrimSpace(strings.TrimPrefix(line, "<")), ",")
			for _, t := range timestamps {
				// Skip empty strings
				t := strings.TrimSpace(t)
				if t == "" {
					continue
				}
				timestamp, err := time.Parse(time.RFC3339, t)
				if err != nil {
					continue
				}
				gemmailLine = GemMailLineTimestamp{timestamp}
				result.Timestamps = append(result.Timestamps, gemmailLine.(GemMailLineTimestamp))
			}
		} else if strings.HasPrefix(line, "[") {
			// Escape lines starting with '['
			gemmailLine = GemMailLineText("\\" + line)
			result.GemtextLines = append(result.GemtextLines, gemmailLine)
		} else if strings.HasPrefix(line, "```") {
			pre = !pre
			line = line[3:]
			gemmailLine = GemMailLinePreformattingToggle(line)
			result.GemtextLines = append(result.GemtextLines, gemmailLine)
		} else if pre {
			gemmailLine = GemMailLinePreformattedText(line)
			result.GemtextLines = append(result.GemtextLines, gemmailLine)
		} else if strings.HasPrefix(line, "<") {
			/*parts := strings.SplitN(strings.TrimSpace(strings.TrimPrefix(line, "<")), " ", 2)
			if len(parts) == 1 { // TODO: Error out if length is 0
				gemmailLine = GemMailLineSender{parts[0], ""}
			} else {
				gemmailLine = GemMailLineSender{parts[0], parts[1]}
			}

			result.Senders = append(result.Senders, gemmailLine.(GemMailLineSender))*/
		} else if strings.HasPrefix(line, ":") {
			/*parts := strings.Split(strings.TrimSpace(strings.TrimPrefix(line, ":")), " ")
			for _, r := range parts {
				result.Receivers[r] = struct{}{}
			}*/
		} else if strings.HasPrefix(line, "@") {
			/*trimmed := strings.TrimSpace(strings.TrimPrefix(line, "@"))
			timestamp, err := time.Parse(time.RFC3339, trimmed)
			if err != nil {
				// TODO
				continue
			}
			gemmailLine = GemMailLineTimestamp{timestamp}
			result.Timestamps = append(result.Timestamps, gemmailLine.(GemMailLineTimestamp))*/
		} else if strings.HasPrefix(line, "=>") {
			line = line[2:]
			line = strings.TrimLeft(line, spacetab)
			split := strings.IndexAny(line, spacetab)
			if split == -1 {
				// text is a URL
				gemmailLine = GemMailLineLink{URL: line}
			} else {
				url := line[:split]
				name := line[split:]
				name = strings.TrimLeft(name, spacetab)
				gemmailLine = GemMailLineLink{url, name}
			}
			result.GemtextLines = append(result.GemtextLines, gemmailLine)
		} else if strings.HasPrefix(line, "* ") {
			line = line[2:]
			line = strings.TrimLeft(line, spacetab)
			gemmailLine = GemMailLineListItem(line)
			result.GemtextLines = append(result.GemtextLines, gemmailLine)
		} else if strings.HasPrefix(line, "###") {
			line = line[3:]
			line = strings.TrimLeft(line, spacetab)
			gemmailLine = GemMailLineHeading3(line)
			result.GemtextLines = append(result.GemtextLines, gemmailLine)
		} else if strings.HasPrefix(line, "##") {
			line = line[2:]
			line = strings.TrimLeft(line, spacetab)
			gemmailLine = GemMailLineHeading2(line)
			result.GemtextLines = append(result.GemtextLines, gemmailLine)
		} else if strings.HasPrefix(line, "#") {
			line = line[1:]
			line = strings.TrimLeft(line, spacetab)
			gemmailLine = GemMailLineHeading1(line)

			if !gotSubject {
				gotSubject = true
				result.Subject = gemmailLine.(GemMailLineHeading1)
			} else {
				result.GemtextLines = append(result.GemtextLines, gemmailLine)
			}
		} else if strings.HasPrefix(line, ">") {
			line = line[1:]
			line = strings.TrimLeft(line, spacetab)
			gemmailLine = GemMailLineQuote(line)
			result.GemtextLines = append(result.GemtextLines, gemmailLine)
		} else {
			gemmailLine = GemMailLineText(line)
			result.GemtextLines = append(result.GemtextLines, gemmailLine)
		}

		i += 1
	}

	// Construct ID
	if len(result.Timestamps) > 0 && len(result.Senders) > 0 {
		result.ID = generateID(result.Timestamps[len(result.Timestamps)-1].Value, result.Senders[len(result.Senders)-1].String())
	}

	return result
}

func remove(slice []GemMailLineSender, s int) []GemMailLineSender {
	return append(slice[:s], slice[s+1:]...)
}

// The original sender of a message is the address that has created and sent the message from their client to a server.
func (gemmail *GemMail) GetOriginalSender() (GemMailLineSender, bool) {
	if len(gemmail.Senders) < 1 {
		return GemMailLineSender{}, false
	}
	return gemmail.Senders[len(gemmail.Senders)-1], true
}

// The injector of a message is the first server that has forwarded the message, which should be the second-to-last server listed in the senders list.
func (gemmail *GemMail) GetInjector() (GemMailLineSender, bool) {
	if len(gemmail.Senders) < 2 {
		return GemMailLineSender{}, false
	}
	return gemmail.Senders[len(gemmail.Senders)-2], true
}

// The immediate sender is the most recent sender, which should be the first server listed in the sender list
func (gemmail *GemMail) GetImmediateSender() (GemMailLineSender, bool) {
	if len(gemmail.Senders) < 1 {
		return GemMailLineSender{}, false
	}
	return gemmail.Senders[0], true
}

func (gemmail *GemMail) GetParticipants(addMailboxHolder string) []string {
	participants := []string{}
	participants = append(participants, gemmail.Senders[0].Address)
	for r := range gemmail.Receivers {
		if r == "" {
			continue
		}
		participants = append(participants, r)
	}

	if addMailboxHolder != "" {
		participants = append(participants, addMailboxHolder)
	}

	slices.Sort(participants)
	return participants
}

func (gemmail *GemMail) GetParticipantsHash(addMailboxHolder string) string {
	participants := gemmail.GetParticipants(addMailboxHolder)

	h := sha256.New()
	for _, p := range participants {
		h.Write([]byte(p))
	}
	return hex.EncodeToString(h.Sum(nil))
}

func (gemmail *GemMail) AddTag(tag string) {
	// Mutually exclusive folder tags // TODO: Optional folder tags?
	if tag == "Inbox" {
		gemmail.RemoveTag("Archive")
		gemmail.RemoveTag("Sent")
		gemmail.RemoveTag("Drafts")
	} else if tag == "Archive" {
		gemmail.RemoveTag("Inbox")
		gemmail.RemoveTag("Sent")
		gemmail.RemoveTag("Drafts")
	} else if tag == "Sent" {
		gemmail.RemoveTag("Inbox")
		gemmail.RemoveTag("Archive")
		gemmail.RemoveTag("Drafts")
	} else if tag == "Drafts" {
		gemmail.RemoveTag("Inbox")
		gemmail.RemoveTag("Archive")
		gemmail.RemoveTag("Sent")
	}
	gemmail.Tags[tag] = Tag{tag, true, time.Now().UTC()}
}

func (gemmail *GemMail) RemoveTag(tag string) {
	//delete(gemmail.Tags, tag)
	if _, exists := gemmail.Tags[tag]; exists {
		gemmail.Tags[tag] = Tag{tag, false, time.Now().UTC()}
	}
}

// Whether gemmail has tag and it's enabled.
func (gemmail *GemMail) HasTag(tag string) bool {
	t, exists := gemmail.Tags[tag]
	return exists && t.Value
}

func (gemmail *GemMail) SetTag(tag string, value bool, last_modification_timestamp time.Time) {
	gemmail.Tags[tag] = Tag{tag, value, last_modification_timestamp}
}

// TODO: This could pose a little bit of a problem.
func (gemmail *GemMail) SetID() {
	if len(gemmail.Timestamps) == 0 {
		panic("Gemmail must have a timestamp set.")
	} else if len(gemmail.Senders) == 0 {
		panic("Gemmail must have a sender set.")
	}

	// Construct ID
	gemmail.ID = generateID(gemmail.Timestamps[len(gemmail.Timestamps)-1].Value, gemmail.Senders[len(gemmail.Senders)-1].String())
}

func (gemmail *GemMail) ContainsSender(address string) bool {
	for _, sender := range gemmail.Senders {
		if sender.Address == address {
			return true
		}
	}
	return false
}

// Clones gemmail. This is necessary if you want to make modifications only within a specific scope (particularly for loops)
func (gemmail *GemMail) Clone(cloneTags bool) GemMail {
	result := GemMail{}

	result.Senders = make([]GemMailLineSender, len(gemmail.Senders))
	result.Receivers = make(map[string]struct{})
	result.Timestamps = make([]GemMailLineTimestamp, len(gemmail.Timestamps))
	result.GemtextLines = make([]GemMailLine, len(gemmail.GemtextLines))
	result.Tags = make(map[string]Tag, len(gemmail.Tags))

	result.Subject = gemmail.Subject.Clone()
	for i, s := range gemmail.Senders {
		result.Senders[i] = s.Clone()
	}
	for key, val := range gemmail.Receivers {
		result.Receivers[key] = val
	}

	copy(result.Timestamps, gemmail.Timestamps)

	for i, l := range gemmail.GemtextLines {
		switch v := l.(type) {
		case GemMailLineHeading1:
			result.GemtextLines[i] = v.Clone()
		case GemMailLineHeading2:
			result.GemtextLines[i] = v.Clone()
		case GemMailLineHeading3:
			result.GemtextLines[i] = v.Clone()
		case GemMailLineLink:
			result.GemtextLines[i] = v.Clone()
		case GemMailLinePreformattedText:
			result.GemtextLines[i] = v.Clone()
		case GemMailLinePreformattingToggle:
			result.GemtextLines[i] = v.Clone()
		case GemMailLineListItem:
			result.GemtextLines[i] = v.Clone()
		case GemMailLineQuote:
			result.GemtextLines[i] = v.Clone()
		case GemMailLineText:
			result.GemtextLines[i] = v.Clone()
		}
	}

	if cloneTags {
		for tagName, tagData := range gemmail.Tags {
			result.SetTag(tagName, tagData.Value, tagData.Last_modification_timestamp)
		}
	}

	return result
}

func (gemmail *GemMail) TruncatedBodyString(char_limit int) string {
	var builder strings.Builder

	count := 0
outer:
	for _, line := range gemmail.GemtextLines {
		switch t := line.(type) {
		case GemMailLineText:
			for _, r := range string(t) {
				if count >= char_limit {
					break outer
				}
				builder.WriteRune(r)
				count++
			}
			builder.WriteRune(' ')
		case GemMailLineListItem:
			for _, r := range t.String() {
				if count >= char_limit {
					break outer
				}
				builder.WriteRune(r)
				count++
			}
			builder.WriteRune(' ')
		}
	}

	builder.WriteString("...\n")

	return builder.String()
}

func (gemmail *GemMail) SetGemTextBody(gemtext string) {
	tmp := CreateGemMailFromBody(gemtext)
	gemmail.GemtextLines = tmp.GemtextLines
	gemmail.Subject = tmp.Subject
}

// Prepends sender to header information as first sender line of file
func (gemmail *GemMail) PrependSender(address string, blurb string) {
	gemmail.Senders = append([]GemMailLineSender{{address, blurb}}, gemmail.Senders...)
}

// Appends sender to header information, just below the last sender line
func (gemmail *GemMail) AppendSender(address string, blurb string) {
	gemmail.Senders = append(gemmail.Senders, GemMailLineSender{address, blurb})
}
func (gemmail *GemMail) RemoveSender(address string) {
	for index, s := range gemmail.Senders {
		if s.Address == address {
			// Remove it
			gemmail.Senders = remove(gemmail.Senders, index)
			return
		}
	}
}

// Prepends timestamp to header information
func (gemmail *GemMail) PrependTimestamp(timestamp time.Time) {
	gemmail.Timestamps = append([]GemMailLineTimestamp{{timestamp}}, gemmail.Timestamps...)
}

// Appends timestamp to header information
func (gemmail *GemMail) AppendTimestamp(timestamp time.Time) {
	gemmail.Timestamps = append(gemmail.Timestamps, GemMailLineTimestamp{timestamp})
}

func (gemmail *GemMail) AddReceiver(address string) {
	gemmail.Receivers[address] = struct{}{}
}
func (gemmail *GemMail) RemoveReceiver(address string) {
	delete(gemmail.Receivers, address)
}

// For Misfin(B) format
func (gemmail *GemMail) MetadataString_MisfinB() string {
	var builder strings.Builder
	for _, s := range gemmail.Senders {
		builder.WriteString("< ")
		builder.WriteString(s.String())
		builder.WriteByte('\n')
	}

	if len(gemmail.Receivers) > 0 {
		builder.WriteString(":")
		for receiver := range gemmail.Receivers {
			builder.WriteRune(' ')
			builder.WriteString(receiver)
		}
		builder.WriteByte('\n')
	}

	for _, t := range gemmail.Timestamps {
		builder.WriteString("@ ")
		builder.WriteString(t.String())
		builder.WriteByte('\n')
	}

	return builder.String()
}

// For Misfin(C) format
func (gemmail *GemMail) MetadataString() string {
	var builder strings.Builder
	for i, s := range gemmail.Senders {
		builder.WriteString(s.String())
		if i != len(gemmail.Senders)-1 {
			builder.WriteString(",")
		}
	}
	builder.WriteByte('\n')

	if len(gemmail.Receivers) > 0 {
		//builder.WriteString(": ")
		i := 0
		for receiver := range gemmail.Receivers {
			builder.WriteString(receiver)
			if i != len(gemmail.Receivers)-1 {
				builder.WriteString(",")
			}
			i += 1
		}
	}
	builder.WriteByte('\n')

	for i, t := range gemmail.Timestamps {
		builder.WriteString(t.String())
		if i != len(gemmail.Timestamps)-1 {
			builder.WriteString(",")
		}
	}
	builder.WriteByte('\n')

	return builder.String()
}

func (gemmail *GemMail) MessageBodyString() string {
	var builder strings.Builder

	builder.WriteString(strings.TrimRight(gemmail.Subject.String(), "\r\n"))
	builder.WriteString("\n")

	for _, line := range gemmail.GemtextLines {
		str := line.String()
		if strings.HasPrefix(str, "[") {
			str = "\\" + str
		}
		builder.WriteString(str)
		builder.WriteByte('\n')
	}

	return builder.String()
}

// Convert GemMail struct to string
// Always uses Misfin(C) format, where first three lines is always senders, recipients, timestamps. If nothing is located in any one, then they are blank lines
func (gemmail *GemMail) String() string {
	var builder strings.Builder
	for i, s := range gemmail.Senders {
		builder.WriteString(s.String())
		if i != len(gemmail.Senders)-1 {
			builder.WriteString(",")
		}
	}
	builder.WriteByte('\n')

	if len(gemmail.Receivers) > 0 {
		//builder.WriteString(": ")
		i := 0
		for receiver := range gemmail.Receivers {
			builder.WriteString(receiver)
			if i != len(gemmail.Receivers)-1 {
				builder.WriteString(",")
			}
			i += 1
		}
	}
	builder.WriteByte('\n')

	for i, t := range gemmail.Timestamps {
		builder.WriteString(t.String())
		if i != len(gemmail.Timestamps)-1 {
			builder.WriteString(",")
		}
	}
	builder.WriteByte('\n')

	builder.WriteString(strings.TrimRight(gemmail.Subject.String(), "\r\n"))
	builder.WriteByte('\n')

	for _, line := range gemmail.GemtextLines {
		str := line.String()
		if strings.HasPrefix(str, "[") {
			str = "\\" + str
		}
		builder.WriteString(str)
		builder.WriteByte('\n')
	}

	return builder.String()
}

// Convert GemMail struct to string
func (gemmail *GemMail) String_MisfinB() string {
	var builder strings.Builder
	for _, s := range gemmail.Senders {
		builder.WriteString("< ")
		builder.WriteString(s.String())
		builder.WriteByte('\n')
	}

	if len(gemmail.Receivers) > 0 {
		builder.WriteString(":")
		for receiver := range gemmail.Receivers {
			builder.WriteRune(' ')
			builder.WriteString(receiver)
		}
		builder.WriteByte('\n')
	}

	for _, t := range gemmail.Timestamps {
		builder.WriteString("@ ")
		builder.WriteString(t.String())
		builder.WriteByte('\n')
	}

	builder.WriteString(strings.TrimRight(gemmail.Subject.String(), "\r\n"))
	builder.WriteByte('\n')

	for _, line := range gemmail.GemtextLines {
		builder.WriteString(line.String())
		builder.WriteByte('\n')
	}

	return builder.String()
}

// SHA256 hash of message metadata and body in misfin(C) format
func (gemmail *GemMail) SHA256() []byte {
	body := gemmail.String()
	hasher := sha256.New()
	hasher.Write([]byte(body))
	return hasher.Sum(nil)
}

// Returns base64 encoded SHA256 signature signed with the provided private key
func (gemmail *GemMail) GenerateSignature(privateKey crypto.PrivateKey) (string, error) {
	body_hash := gemmail.SHA256()
	var signature []byte
	var sign_err error
	switch v := privateKey.(type) {
	case ecdsa.PrivateKey:
		signature, sign_err = v.Sign(crypto_rand.Reader, body_hash, crypto.SHA256)
	case ed25519.PrivateKey:
		signature, sign_err = v.Sign(crypto_rand.Reader, body_hash, crypto.SHA256)
	case rsa.PrivateKey:
		signature, sign_err = v.Sign(crypto_rand.Reader, body_hash, crypto.SHA256)
	}
	if sign_err != nil {
		return "", sign_err
	}
	return base64.StdEncoding.EncodeToString(signature), nil
}
