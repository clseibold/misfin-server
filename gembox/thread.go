package gembox

import (
	"crypto/sha256"
	"encoding/hex"
	"slices"
	"time"

	"gitlab.com/clseibold/misfin-server/gemmail"
)

func removePtr(slice []*gemmail.GemMail, s int) []*gemmail.GemMail {
	return append(slice[:s], slice[s+1:]...)
}

type Thread struct {
	Subject           string
	Participants      []string
	_participantsHash string
	Mails             []*gemmail.GemMail
}

// TODO: Could be optimized if participants argument was sorted first, and then compared with thread.Participants
func (thread *Thread) HasParticipants(participants []string) bool {
	for _, p := range participants {
		if !slices.Contains(thread.Participants, p) {
			return false
		}
	}

	return true
}

func (thread *Thread) GetParticipantsHash() string {
	if thread._participantsHash != "" {
		return thread._participantsHash
	}

	slices.Sort(thread.Participants)
	h := sha256.New()
	for _, p := range thread.Participants {
		h.Write([]byte(p))
	}
	thread._participantsHash = hex.EncodeToString(h.Sum(nil))
	return thread._participantsHash
}

func (thread *Thread) HasTag(tag string) bool {
	for _, mail := range thread.Mails {
		if mail.HasTag(tag) {
			return true
		}
	}

	return false
}

func (thread *Thread) ContainsSender(address string) bool {
	for _, mail := range thread.Mails {
		if mail.ContainsSender(address) {
			return true
		}
	}

	return false
}

func (thread *Thread) SortMails() {
	slices.SortStableFunc(thread.Mails, SortMailPtrsFunc)
}

// NOTE: Call this when a thread is created
func (thread *Thread) SortParticipants() {
	slices.Sort(thread.Participants)
}

func (thread *Thread) FirstDate() time.Time {
	if len(thread.Mails) == 0 {
		return time.Time{}
	}
	return thread.Mails[0].Timestamps[0].Value
}

func (thread *Thread) LastDate() time.Time {
	if len(thread.Mails) == 0 {
		return time.Time{}
	}
	return thread.Mails[len(thread.Mails)-1].Timestamps[0].Value
}

func (thread *Thread) AddMail(gemmail *gemmail.GemMail) {
	thread.Mails = append(thread.Mails, gemmail)
	thread.SortMails() // TODO: This is pretty slow to do every time a mail is added
}

// Internal - remove via index in Mails slice
func (thread *Thread) removeGemMail(index int) {
	thread.Mails = removePtr(thread.Mails, index)
}

func (thread *Thread) GetGemMailWithID(ID string) (*gemmail.GemMail, int) {
	for i := 0; i < len(thread.Mails); i++ {
		gemmail := thread.Mails[i]
		if gemmail.ID == ID {
			return gemmail, i
		}
	}

	return nil, -1
}

func (thread *Thread) GetGemMailsWithTag(tag string) []*gemmail.GemMail {
	result := make([]*gemmail.GemMail, 0)
	for i := 0; i < len(thread.Mails); i++ {
		gemmail := thread.Mails[i]
		if gemmail.HasTag(tag) {
			result = append(result, gemmail)
		}
	}

	return result
}

func (thread *Thread) RemoveGemMailById(ID string) {
	_, index := thread.GetGemMailWithID(ID)
	if index != -1 {
		thread.removeGemMail(index)
	}
}

func (thread *Thread) Length() int {
	return len(thread.Mails)
}
