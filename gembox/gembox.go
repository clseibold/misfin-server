package gembox

import (
	"bufio"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"slices"
	"strings"
	"time"

	cmap "github.com/orcaman/concurrent-map/v2"
	"gitlab.com/clseibold/misfin-server/gemmail"
)

// This parses a gembox file. See NewGemBoxFormat.md file for format details.

type ThreadKey struct {
	subj             string
	participantsHash string
}

func (threadKey ThreadKey) String() string {
	return fmt.Sprintf("%s|%s", threadKey.subj, threadKey.participantsHash)
}

type GemBox struct {
	Identifier     any // For your use
	MailboxAddress string
	MailboxBlurb   string
	Fingerprint    string
	Description    string
	Mails          []gemmail.GemMail
	CreatedTags    map[string]struct{}
	threads        cmap.ConcurrentMap[ThreadKey, *Thread]
	threadsOrdered []*Thread
}

func NewEmptyGembox(identifier any) *GemBox {
	return &GemBox{Identifier: identifier, Mails: make([]gemmail.GemMail, 0), CreatedTags: make(map[string]struct{}), threads: cmap.NewStringer[ThreadKey, *Thread](), threadsOrdered: make([]*Thread, 0)}
}

func (gembox *GemBox) SortMails() {
	slices.SortStableFunc(gembox.Mails, SortMailsFunc)
}

func SortMailsFunc(a, b gemmail.GemMail) int {
	return a.Timestamps[0].Value.Compare(b.Timestamps[0].Value)
}

func SortMailPtrsFunc(a, b *gemmail.GemMail) int {
	return a.Timestamps[0].Value.Compare(b.Timestamps[0].Value)
}

func (gembox *GemBox) SortThreads() {
	slices.SortStableFunc(gembox.threadsOrdered, SortThreadsFunc)
}

func SortThreadsFunc(a, b *Thread) int {
	return a.LastDate().Compare(b.LastDate())
}

// Gets threads sorted by date of last gemmail in thread. Includes threads of length 1.
func (gembox *GemBox) GetThreads() []*Thread {
	return gembox.threadsOrdered
}

func (gembox *GemBox) GetThreadWithKey(key ThreadKey) (*Thread, bool) {
	return gembox.threads.Get(key)
}

func HashParticipants(participants []string) string {
	slices.Sort(participants)

	h := sha256.New()
	for _, p := range participants {
		if p == "" {
			continue
		}
		h.Write([]byte(p))
	}
	return hex.EncodeToString(h.Sum(nil))
}

// Returns key, thread, and whether it exists
func (gembox *GemBox) GetThreadWithSubjectAndParticipantsHash(subject string, participantsHash string) (*Thread, bool) {
	subject = strings.TrimSpace(strings.TrimPrefix(strings.TrimPrefix(subject, "Re: "), "RE: "))
	//participantsHash := HashParticipants(participants)
	key := ThreadKey{subject, participantsHash}
	return gembox.threads.Get(key)
}

func (gembox *GemBox) GetThreadsWithTag(tag string) []*Thread {
	result := make([]*Thread, 0, len(gembox.threadsOrdered))
	for _, thread := range gembox.threadsOrdered {
		if thread.HasTag(tag) {
			result = append(result, thread)
		}
	}

	return result
}

// Returns key and thread
func (gembox *GemBox) GetOrCreateThread(subject string, participants []string) *Thread {
	participantsHash := HashParticipants(participants)
	subject = strings.TrimSpace(strings.TrimPrefix(strings.TrimPrefix(subject, "Re: "), "RE: "))
	if subject == "" {
		// Add as thread to threadsOrdered, but not in threads map
		thread := &Thread{Subject: subject, Participants: participants}
		gembox.threadsOrdered = append(gembox.threadsOrdered, thread) // Will get sorted when a gemmail is added
		thread.SortParticipants()
		return thread
	} else {
		if thread, ok := gembox.GetThreadWithSubjectAndParticipantsHash(subject, participantsHash); ok {
			return thread
		} else {
			// Create new thread with subject
			thread := &Thread{Subject: subject, Participants: participants}
			key := ThreadKey{subject, thread.GetParticipantsHash()}
			gembox.threads.Set(key, thread)
			gembox.threadsOrdered = append(gembox.threadsOrdered, thread) // Will get sorted when a gemmail is added
			thread.SortParticipants()
			return thread
		}
	}
}

func ParseGemBox(identifier any, gembox string) GemBox {
	var result GemBox
	result.Mails = make([]gemmail.GemMail, 0)
	result.threads = cmap.NewStringer[ThreadKey, *Thread]()
	result.threadsOrdered = make([]*Thread, 0)
	result.Identifier = identifier

	scanner := bufio.NewScanner(strings.NewReader(gembox))

	var inGboxMetadata bool = true
	var inBody bool = false
	var currentGemMail gemmail.GemMail
	var currentGemMailBody strings.Builder
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "[") {
			if !inGboxMetadata && currentGemMail.ID != "" {
				// End of message body, set gemtext body and append message to gembox
				currentGemMail.SetGemTextBody(currentGemMailBody.String())
				result.AppendGemMail(currentGemMail)
			}

			// Start of new message metadata, parse message ID
			msgId := strings.TrimSuffix(strings.TrimPrefix(line, "["), "]")
			currentGemMail = gemmail.GemMail{ID: msgId, Tags: make(map[string]gemmail.Tag), Receivers: make(map[string]struct{})}
			inBody = false
			inGboxMetadata = false
			currentGemMailBody.Reset()
		} else if inBody && strings.HasPrefix(line, "\\[") {
			currentGemMailBody.WriteString("[")
			currentGemMailBody.WriteString(strings.TrimPrefix(line, "\\["))
			currentGemMailBody.WriteString("\n")
		} else if inBody {
			currentGemMailBody.WriteString(line)
			currentGemMailBody.WriteString("\n")
		} else if inGboxMetadata {
			if strings.HasPrefix(line, "Mailbox:") {
				sender := strings.TrimSpace(strings.TrimPrefix(line, "Mailbox:"))
				parts := strings.SplitN(sender, " ", 2)
				if len(parts) == 1 { // TODO: Error out if length is 0
					result.MailboxAddress = parts[0]
				} else if len(parts) == 2 {
					result.MailboxAddress = parts[0]
					result.MailboxBlurb = parts[1]
				}
			} else if strings.HasPrefix(line, "Fingerprint:") {
				result.Fingerprint = strings.TrimSpace(strings.TrimPrefix(line, "Fingerprint:"))
			} else if strings.HasPrefix(line, "Description:") {
				result.Description = strings.TrimSpace(strings.TrimPrefix(line, "Description:"))
			} else if strings.HasPrefix(line, "CreatedTags:") {
				tags := strings.Split(strings.TrimPrefix(line, "CreatedTags:"), ",")
				for _, tag := range tags {
					tag = strings.TrimSpace(tag)
					result.CreatedTags[tag] = struct{}{}
				}
			}
		} else { // In Message Metadata
			if strings.HasPrefix(line, "Senders:") {
				senders := strings.Split(strings.TrimPrefix(line, "Senders:"), ",")
				for _, s := range senders {
					// Skip empty strings
					s := strings.TrimPrefix(s, " ")
					if s == "" {
						continue
					}
					parts := strings.SplitN(s, " ", 2)
					if len(parts) == 1 { // TODO: Error out if length is 0
						currentGemMail.AppendSender(parts[0], "")
					} else {
						currentGemMail.AppendSender(parts[0], parts[1])
					}
				}
			} else if strings.HasPrefix(line, "Recipients:") {
				parts := strings.Split(strings.TrimPrefix(line, "Recipients:"), ",")
				for _, r := range parts {
					r := strings.TrimSpace(r)
					if r == "" {
						continue
					}
					currentGemMail.AddReceiver(r)
				}
			} else if strings.HasPrefix(line, "Timestamps:") {
				timestamps := strings.Split(strings.TrimPrefix(line, "Timestamps:"), ",")
				for _, t := range timestamps {
					// Skip empty strings
					t := strings.TrimSpace(t)
					if t == "" {
						continue
					}
					timestamp, err := time.Parse(time.RFC3339, t)
					if err != nil {
						continue
					}
					currentGemMail.AppendTimestamp(timestamp)
				}
			} else if strings.HasPrefix(line, "#") {
				// Subject line
				inBody = true
				currentGemMailBody.WriteString(line)
				currentGemMailBody.WriteString("\n")
			} else { // Tags
				parts := strings.SplitN(line, ":", 2)
				if len(parts) == 2 {
					tagName := strings.TrimSpace(parts[0])
					value := false
					tagValueParts := strings.SplitN(strings.TrimSpace(parts[1]), " ", 2)
					if len(tagValueParts) == 2 {
						last_modification_timestamp, err := time.Parse(time.RFC3339, strings.TrimSpace(tagValueParts[0]))
						if err != nil {
							continue
						}
						if tagValueParts[1] == "true" || tagValueParts[1] == "True" || tagValueParts[1] == "TRUE" {
							value = true
						}

						currentGemMail.SetTag(tagName, value, last_modification_timestamp)
					}
				}
			}
		}
	}

	// Add the last gemmail
	if currentGemMail.ID != "" {
		currentGemMail.SetGemTextBody(currentGemMailBody.String())
		result.AppendGemMail(currentGemMail)
	}

	// Sort gemmails based on timestamp
	slices.SortStableFunc(result.Mails, SortMailsFunc)

	return result
}

func ParseGemBox_MisfinB_old(identifier any, gembox string) GemBox {
	var result GemBox
	result.Mails = make([]gemmail.GemMail, 0)
	result.threads = cmap.NewStringer[ThreadKey, *Thread]()
	result.threadsOrdered = make([]*Thread, 0)
	result.Identifier = identifier

	scanner := bufio.NewScanner(strings.NewReader(gembox))
	// TODO: scanner.Err()

	var currentGemMail strings.Builder
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "<=====") {
			// Append current GemMail to list
			result.AppendGemMail(gemmail.ParseGemMail_MisfinB(currentGemMail.String()))

			// Start a new GemMail
			currentGemMail.Reset()
		} else {
			currentGemMail.WriteString(line)
			currentGemMail.WriteByte('\n')
		}
	}

	// Append last GemMail, if not empty
	if strings.TrimSpace(currentGemMail.String()) != "" {
		parsedGemMail := gemmail.ParseGemMail_MisfinB(currentGemMail.String())
		result.Mails = append(result.Mails, parsedGemMail)
	}

	// Sort gemmails based on timestamp
	slices.SortStableFunc(result.Mails, SortMailsFunc)

	return result
}

func ParseGemBox_MisfinC_old(identifier any, gembox string) GemBox {
	var result GemBox
	result.Mails = make([]gemmail.GemMail, 0)
	result.threads = cmap.NewStringer[ThreadKey, *Thread]()
	result.threadsOrdered = make([]*Thread, 0)
	result.Identifier = identifier

	scanner := bufio.NewScanner(strings.NewReader(gembox))
	// TODO: scanner.Err()

	var currentGemMail strings.Builder
	for scanner.Scan() {
		line := strings.TrimSpace(scanner.Text())
		if strings.HasPrefix(line, "<=====") {
			// Append current GemMail to list
			result.AppendGemMail(gemmail.ParseGemMail_MisfinC(currentGemMail.String()))

			// Start a new GemMail
			currentGemMail.Reset()
		} else {
			currentGemMail.WriteString(line)
			currentGemMail.WriteByte('\n')
		}
	}

	// Append last GemMail, if not empty
	if strings.TrimSpace(currentGemMail.String()) != "" {
		parsedGemMail := gemmail.ParseGemMail_MisfinC(currentGemMail.String())
		result.Mails = append(result.Mails, parsedGemMail)
	}

	// Sort gemmails based on timestamp
	slices.SortStableFunc(result.Mails, SortMailsFunc)

	return result
}

// Removes element at index from the slice, creating a new resulting slice
func remove(slice []gemmail.GemMail, s int) []gemmail.GemMail {
	return append(slice[:s], slice[s+1:]...)
}

func (gembox *GemBox) GetGemMailAtIndex(index int) *gemmail.GemMail {
	return &gembox.Mails[index]
}

func (gembox *GemBox) GetGemMailWithID(ID string) (*gemmail.GemMail, int) {
	for i := 0; i < len(gembox.Mails); i++ {
		gemmail := &gembox.Mails[i]
		if gemmail.ID == ID {
			return gemmail, i
		}
	}

	return nil, -1
}

func (gembox *GemBox) GetGemMailsWithTag(tag string) []*gemmail.GemMail {
	result := make([]*gemmail.GemMail, 0)
	for i := 0; i < len(gembox.Mails); i++ {
		gemmail := &gembox.Mails[i]
		if gemmail.HasTag(tag) {
			result = append(result, gemmail)
		}
	}

	return result
}

// NOTE: This doesn't lock the mutex, so you must do that manually.
func (gembox *GemBox) AppendGemMail(gemmail gemmail.GemMail) {
	// Panic when GemMail doesn't have an ID
	if gemmail.ID == "" {
		panic("GemMail's ID was not set.")
	}
	gembox.Mails = append(gembox.Mails, gemmail)
	// TODO: Make sure appended GemMail is in correct order based on timestamp to keep the mailbox sorted

	// Add gemmail to thread
	addMailboxHolder := ""
	if _, mailboxHolderInReceivers := gemmail.Receivers[gembox.MailboxAddress]; !gemmail.ContainsSender(gembox.MailboxAddress) && !mailboxHolderInReceivers {
		addMailboxHolder = gembox.MailboxAddress
	}
	thread := gembox.GetOrCreateThread(string(gemmail.Subject), gemmail.GetParticipants(addMailboxHolder))
	thread.AddMail(&gembox.Mails[len(gembox.Mails)-1])
	gembox.SortThreads() // TODO: Doing this with every thread creation and gemmail addition is pretty slow
}

func (gembox *GemBox) removeThread(thread *Thread) {
	for i, t := range gembox.threadsOrdered {
		if t == thread {
			gembox.threadsOrdered = append(gembox.threadsOrdered[:i], gembox.threadsOrdered[i+1:]...)
			break
		}
	}

	gembox.threads.Remove(ThreadKey{thread.Subject, thread.GetParticipantsHash()})
}

// Remove gemmail at specified index from GemBox's mails. NOTE: This doesn't lock the mutex, so you must do that manually.
func (gembox *GemBox) RemoveGemMail(index int) {
	gemmail := &gembox.Mails[index]

	// Remove from thread
	subject := strings.TrimSpace(strings.TrimPrefix(strings.TrimPrefix(string(gemmail.Subject), "Re: "), "RE: "))
	addMailboxHolder := ""
	if _, mailboxHolderInReceivers := gemmail.Receivers[gembox.MailboxAddress]; !gemmail.ContainsSender(gembox.MailboxAddress) && !mailboxHolderInReceivers {
		addMailboxHolder = gembox.MailboxAddress
	}
	if thread, ok := gembox.threads.Get(ThreadKey{subject, gemmail.GetParticipantsHash(addMailboxHolder)}); ok {
		thread.RemoveGemMailById(gemmail.ID)

		// If thread is empty, remove it completely
		if thread.Length() == 0 {
			gembox.removeThread(thread)
		} else {
			gembox.SortThreads() // TODO: Doing this with every thread creation and gemmail addition is pretty slow
		}
	}

	gembox.Mails = remove(gembox.Mails, index)
}

func (gembox *GemBox) RemoveGemMailByID(ID string) {
	_, index := gembox.GetGemMailWithID(ID)
	if index != -1 {
		gembox.RemoveGemMail(index)
	}
}

func (gembox *GemBox) String() string {
	var builder strings.Builder

	// Gembox Metadata
	if gembox.MailboxAddress != "" {
		builder.WriteString("Mailbox: ")
		builder.WriteString(gembox.MailboxAddress)
		if gembox.MailboxBlurb != "" {
			builder.WriteString(" " + gembox.MailboxBlurb)
		}
		builder.WriteString("\n")
	}

	if gembox.Description != "" {
		builder.WriteString("Description: ")
		builder.WriteString(gembox.Description)
		builder.WriteString("\n")
	}

	if gembox.Fingerprint != "" {
		builder.WriteString("Fingerprint: ")
		builder.WriteString(gembox.Fingerprint)
		builder.WriteString("\n")
	}

	if len(gembox.CreatedTags) > 0 {
		builder.WriteString("CreatedTags: ")

		index := 0
		for tag := range gembox.CreatedTags {
			if tag == "" {
				continue
			}
			if index > 0 {
				builder.WriteString(", ")
			}

			builder.WriteString(tag)
			index++
		}

		builder.WriteString("\n")
	}

	// Gembox Messages
	for index := range gembox.Mails {
		mail := &gembox.Mails[index]

		// Message ID
		builder.WriteString("[")
		builder.WriteString(mail.ID)
		builder.WriteString("]\n")

		// Metadata
		if len(mail.Senders) > 0 {
			builder.WriteString("Senders: ")
			for index, sender := range mail.Senders {
				if sender.Address == "" {
					continue
				}
				if index > 0 {
					builder.WriteString(",")
				}
				builder.WriteString(sender.Address)
				if sender.Blurb != "" {
					builder.WriteString(" ")
					builder.WriteString(sender.Blurb)
				}
			}
			builder.WriteString("\n")
		}

		if len(mail.Receivers) > 0 {
			builder.WriteString("Recipients: ")
			index := 0
			for recipient := range mail.Receivers {
				if recipient == "" {
					continue
				}
				if index > 0 {
					builder.WriteString(",")
				}
				builder.WriteString(recipient)
				index++
			}
			builder.WriteString("\n")
		}

		if len(mail.Timestamps) > 0 {
			builder.WriteString("Timestamps: ")
			for index, t := range mail.Timestamps {
				if index > 0 {
					builder.WriteString(",")
				}
				builder.WriteString(t.Value.Format(time.RFC3339))
			}
			builder.WriteString("\n")
		}

		// Tags
		if len(mail.Tags) > 0 {
			for tagName, tag := range mail.Tags {
				if tagName == "" {
					continue
				}

				builder.WriteString(tagName)
				builder.WriteString(": ")
				builder.WriteString(tag.Last_modification_timestamp.Format(time.RFC3339))
				builder.WriteString(" ")
				if tag.Value {
					builder.WriteString("true\n")
				} else {
					builder.WriteString("false\n")
				}
			}
		}

		// Message Subject and Body
		builder.WriteString(mail.MessageBodyString())
	}

	return builder.String()
}

func (gembox *GemBox) String_MisfinC_old() string {
	var builder strings.Builder
	for i := 0; i < len(gembox.Mails); i++ {
		gemmail := &gembox.Mails[i]
		if i != 0 {
			builder.WriteString("<=====\n")
		}
		builder.WriteString(gemmail.String())
	}
	/*for i, gemmail := range gembox.Mails {
		if i != 0 {
			builder.WriteString("<=====\n")
		}
		builder.WriteString(gemmail.String())
	}*/

	return builder.String()
}

func (gembox *GemBox) String_MisfinB_old() string {
	var builder strings.Builder
	for i, gemmail := range gembox.Mails {
		if i != 0 {
			builder.WriteString("<=====\n")
		}
		builder.WriteString(gemmail.String_MisfinB())
	}

	return builder.String()
}
